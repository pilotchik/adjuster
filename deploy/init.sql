DELETE FROM configuration;
DELETE FROM users WHERE login = 'admin';
INSERT INTO users (login,password,email,name,middlename,surname,active,user_type,timestamp,archive,ldap_login) VALUES ('admin','$1$52L7dN3y$HTT/P.4ljyEefOYyxnNUc1','admin@unit.spb','Администратор','Администраторович','Администраторов','1','admin',1,0,'admin');