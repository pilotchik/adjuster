--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.3
-- Started on 2015-12-03 10:47:44 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2321 (class 1262 OID 16390)
-- Dependencies: 2320
-- Name: DATABASE irida_admin; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON DATABASE irida_admin IS '{0,0}';


--
-- TOC entry 2323 (class 2615 OID 2200)
-- Dependencies: 6
-- Name: SCHEMA public; Type: MAC LABEL; Schema: -; Owner: postgres
--

MAC LABEL ON SCHEMA public IS '{0,0}';


--
-- TOC entry 227 (class 3079 OID 11775)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2325 (class 0 OID 0)
-- Dependencies: 227
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2326 (class 3079 OID 11775)
-- Dependencies: 227
-- Name: EXTENSION plpgsql; Type: MAC LABEL; Schema: -; Owner: 
--

MAC LABEL ON EXTENSION plpgsql IS '{0,0}';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 16391)
-- Name: account_fields; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE account_fields (
    id integer NOT NULL,
    name character varying(255),
    description text,
    type character varying(55),
    status integer DEFAULT 1,
    admin_changing integer DEFAULT 1,
    required integer DEFAULT 0,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0,
    info_type character varying(50)
);


ALTER TABLE public.account_fields OWNER TO irida_admin;

--
-- TOC entry 2327 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN account_fields.info_type; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN account_fields.info_type IS 'profile | contacts';


--
-- TOC entry 2328 (class 1259 OID 16391)
-- Dependencies: 170
-- Name: TABLE account_fields; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE account_fields IS '{0,0}';


--
-- TOC entry 171 (class 1259 OID 16402)
-- Name: account_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE account_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_fields_id_seq OWNER TO irida_admin;

--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 171
-- Name: account_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE account_fields_id_seq OWNED BY account_fields.id;


--
-- TOC entry 2330 (class 1259 OID 16402)
-- Dependencies: 171
-- Name: SEQUENCE account_fields_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE account_fields_id_seq IS '{0,0}';


--
-- TOC entry 172 (class 1259 OID 16404)
-- Name: buildings; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE buildings (
    id integer NOT NULL,
    name character varying(255),
    description text,
    image text,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0,
    address text
);


ALTER TABLE public.buildings OWNER TO irida_admin;

--
-- TOC entry 2331 (class 1259 OID 16404)
-- Dependencies: 172
-- Name: TABLE buildings; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE buildings IS '{0,0}';


--
-- TOC entry 173 (class 1259 OID 16412)
-- Name: buildings_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE buildings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.buildings_id_seq OWNER TO irida_admin;

--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 173
-- Name: buildings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE buildings_id_seq OWNED BY buildings.id;


--
-- TOC entry 2333 (class 1259 OID 16412)
-- Dependencies: 173
-- Name: SEQUENCE buildings_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE buildings_id_seq IS '{0,0}';


--
-- TOC entry 174 (class 1259 OID 16414)
-- Name: configuration; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE configuration (
    name character varying(100) NOT NULL,
    value text NOT NULL,
    type character varying(20) DEFAULT 'text'::character varying,
    required integer DEFAULT 1,
    section character varying(50) DEFAULT 'main'::character varying
);


ALTER TABLE public.configuration OWNER TO irida_admin;

--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN configuration.type; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN configuration.type IS 'тип параметра';


--
-- TOC entry 2335 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN configuration.section; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN configuration.section IS 'Раздел настройки';


--
-- TOC entry 2336 (class 1259 OID 16414)
-- Dependencies: 174
-- Name: TABLE configuration; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE configuration IS '{0,0}';


--
-- TOC entry 175 (class 1259 OID 16423)
-- Name: equipments; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE equipments (
    id integer NOT NULL,
    hostname character varying(255),
    description text,
    type character varying(55),
    status integer DEFAULT 1,
    audience_id integer DEFAULT 0,
    arm_top real DEFAULT 0,
    arm_left real DEFAULT 0,
    rotation real DEFAULT 0,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0,
    floor_id integer DEFAULT 0,
    ip inet
);


ALTER TABLE public.equipments OWNER TO irida_admin;

--
-- TOC entry 2337 (class 1259 OID 16423)
-- Dependencies: 175
-- Name: TABLE equipments; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE equipments IS '{0,0}';


--
-- TOC entry 176 (class 1259 OID 16437)
-- Name: equipments_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE equipments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipments_id_seq OWNER TO irida_admin;

--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 176
-- Name: equipments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE equipments_id_seq OWNED BY equipments.id;


--
-- TOC entry 2339 (class 1259 OID 16437)
-- Dependencies: 176
-- Name: SEQUENCE equipments_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE equipments_id_seq IS '{0,0}';


--
-- TOC entry 177 (class 1259 OID 16439)
-- Name: events; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE events (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    repeat_type character varying(100) NOT NULL,
    repeat_day character varying(50),
    room_id integer,
    all_day integer,
    first_time integer,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0,
    day_time character varying(50) DEFAULT '10:00'::character varying
);


ALTER TABLE public.events OWNER TO irida_admin;

--
-- TOC entry 2340 (class 1259 OID 16439)
-- Dependencies: 177
-- Name: TABLE events; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE events IS '{0,0}';


--
-- TOC entry 178 (class 1259 OID 16448)
-- Name: events_exclusions; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE events_exclusions (
    id integer NOT NULL,
    exclusion_time integer,
    event_id integer,
    user_id integer,
    ts integer,
    type character varying(50) DEFAULT 'day'::character varying
);


ALTER TABLE public.events_exclusions OWNER TO irida_admin;

--
-- TOC entry 2341 (class 1259 OID 16448)
-- Dependencies: 178
-- Name: TABLE events_exclusions; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE events_exclusions IS '{0,0}';


--
-- TOC entry 179 (class 1259 OID 16452)
-- Name: events_exclusions_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE events_exclusions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_exclusions_id_seq OWNER TO irida_admin;

--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 179
-- Name: events_exclusions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE events_exclusions_id_seq OWNED BY events_exclusions.id;


--
-- TOC entry 2343 (class 1259 OID 16452)
-- Dependencies: 179
-- Name: SEQUENCE events_exclusions_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE events_exclusions_id_seq IS '{0,0}';


--
-- TOC entry 180 (class 1259 OID 16454)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO irida_admin;

--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 180
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- TOC entry 2345 (class 1259 OID 16454)
-- Dependencies: 180
-- Name: SEQUENCE events_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE events_id_seq IS '{0,0}';


--
-- TOC entry 181 (class 1259 OID 16456)
-- Name: fields_to_users; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE fields_to_users (
    id integer NOT NULL,
    field_id integer DEFAULT 0,
    user_id integer DEFAULT 0,
    value text,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0
);


ALTER TABLE public.fields_to_users OWNER TO irida_admin;

--
-- TOC entry 2346 (class 1259 OID 16456)
-- Dependencies: 181
-- Name: TABLE fields_to_users; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE fields_to_users IS '{0,0}';


--
-- TOC entry 182 (class 1259 OID 16466)
-- Name: fields_to_users_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE fields_to_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fields_to_users_id_seq OWNER TO irida_admin;

--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 182
-- Name: fields_to_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE fields_to_users_id_seq OWNED BY fields_to_users.id;


--
-- TOC entry 2348 (class 1259 OID 16466)
-- Dependencies: 182
-- Name: SEQUENCE fields_to_users_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE fields_to_users_id_seq IS '{0,0}';


--
-- TOC entry 183 (class 1259 OID 16468)
-- Name: flats; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE flats (
    id integer NOT NULL,
    name character varying(255),
    floor_id integer DEFAULT 1,
    status integer DEFAULT 1,
    flat_top integer DEFAULT 0,
    flat_left integer DEFAULT 0,
    description text,
    creator_id integer DEFAULT 1,
    ts integer DEFAULT 0,
    json_data text,
    base64_png text,
    background text,
    background_offset_x integer DEFAULT 0,
    background_offset_y integer DEFAULT 0,
    background_opacity integer DEFAULT 100,
    background_scale integer DEFAULT 100
);


ALTER TABLE public.flats OWNER TO irida_admin;

--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN flats.background; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN flats.background IS 'Фоновое изображение';


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN flats.background_offset_x; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN flats.background_offset_x IS 'Отступ сверху';


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN flats.background_offset_y; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN flats.background_offset_y IS 'Отступ слева';


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN flats.background_opacity; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN flats.background_opacity IS 'Прозрачность';


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN flats.background_scale; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN flats.background_scale IS 'Масштаб';


--
-- TOC entry 2354 (class 1259 OID 16468)
-- Dependencies: 183
-- Name: TABLE flats; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE flats IS '{0,0}';


--
-- TOC entry 184 (class 1259 OID 16480)
-- Name: flats_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE flats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flats_id_seq OWNER TO irida_admin;

--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 184
-- Name: flats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE flats_id_seq OWNED BY flats.id;


--
-- TOC entry 2356 (class 1259 OID 16480)
-- Dependencies: 184
-- Name: SEQUENCE flats_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE flats_id_seq IS '{0,0}';


--
-- TOC entry 185 (class 1259 OID 16482)
-- Name: floors; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE floors (
    id integer NOT NULL,
    name character varying(255),
    numb integer DEFAULT 1,
    building_id integer DEFAULT 1,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0,
    json_data text,
    base64_png text,
    background text,
    background_offset_x integer DEFAULT 0,
    background_offset_y integer DEFAULT 0,
    background_opacity integer DEFAULT 100,
    background_scale integer DEFAULT 100
);


ALTER TABLE public.floors OWNER TO irida_admin;

--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN floors.background; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN floors.background IS 'Фоновое изображение';


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN floors.background_offset_x; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN floors.background_offset_x IS 'Отступ сверху';


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN floors.background_offset_y; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN floors.background_offset_y IS 'Отступ слева';


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN floors.background_opacity; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN floors.background_opacity IS 'Прозрачность';


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN floors.background_scale; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN floors.background_scale IS 'Масштаб';


--
-- TOC entry 2362 (class 1259 OID 16482)
-- Dependencies: 185
-- Name: TABLE floors; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE floors IS '{0,0}';


--
-- TOC entry 186 (class 1259 OID 16492)
-- Name: floors_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE floors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.floors_id_seq OWNER TO irida_admin;

--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 186
-- Name: floors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE floors_id_seq OWNED BY floors.id;


--
-- TOC entry 2364 (class 1259 OID 16492)
-- Dependencies: 186
-- Name: SEQUENCE floors_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE floors_id_seq IS '{0,0}';


--
-- TOC entry 187 (class 1259 OID 16494)
-- Name: index_counter; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE index_counter (
    id integer NOT NULL,
    controller character varying(255),
    user_id integer,
    ts integer DEFAULT 0
);


ALTER TABLE public.index_counter OWNER TO irida_admin;

--
-- TOC entry 2365 (class 1259 OID 16494)
-- Dependencies: 187
-- Name: TABLE index_counter; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE index_counter IS '{0,0}';


--
-- TOC entry 188 (class 1259 OID 16498)
-- Name: index_counter_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE index_counter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.index_counter_id_seq OWNER TO irida_admin;

--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 188
-- Name: index_counter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE index_counter_id_seq OWNED BY index_counter.id;


--
-- TOC entry 2367 (class 1259 OID 16498)
-- Dependencies: 188
-- Name: SEQUENCE index_counter_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE index_counter_id_seq IS '{0,0}';


--
-- TOC entry 189 (class 1259 OID 16500)
-- Name: log; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE log (
    id integer NOT NULL,
    user_id integer,
    ts integer,
    status text,
    controller character varying(250)
);


ALTER TABLE public.log OWNER TO irida_admin;

--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN log.controller; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN log.controller IS 'Название контроллера-инициатора';


--
-- TOC entry 2369 (class 1259 OID 16500)
-- Dependencies: 189
-- Name: TABLE log; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE log IS '{0,0}';


--
-- TOC entry 190 (class 1259 OID 16506)
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_id_seq OWNER TO irida_admin;

--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 190
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE log_id_seq OWNED BY log.id;


--
-- TOC entry 2371 (class 1259 OID 16506)
-- Dependencies: 190
-- Name: SEQUENCE log_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE log_id_seq IS '{0,0}';


--
-- TOC entry 191 (class 1259 OID 16508)
-- Name: notifications; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE notifications (
    id integer NOT NULL,
    recipient_id integer DEFAULT 0,
    author_id integer DEFAULT 0,
    entity_type character varying(50) NOT NULL,
    entity_id integer DEFAULT 0,
    subject text,
    body text,
    status integer DEFAULT 0,
    ts integer DEFAULT 0,
    type_of_notification character varying(50) DEFAULT 'popup'::character varying
);


ALTER TABLE public.notifications OWNER TO irida_admin;

--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN notifications.type_of_notification; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN notifications.type_of_notification IS 'Тип уведомления';


--
-- TOC entry 2373 (class 1259 OID 16508)
-- Dependencies: 191
-- Name: TABLE notifications; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE notifications IS '{0,0}';


--
-- TOC entry 192 (class 1259 OID 16520)
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_id_seq OWNER TO irida_admin;

--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 192
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- TOC entry 2375 (class 1259 OID 16520)
-- Dependencies: 192
-- Name: SEQUENCE notifications_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE notifications_id_seq IS '{0,0}';


--
-- TOC entry 218 (class 1259 OID 17850)
-- Name: polls; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE polls (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    access character varying(50),
    anon_status integer DEFAULT 0,
    active integer DEFAULT 1,
    creator_id integer DEFAULT 0,
    moderator_id integer DEFAULT 0,
    public_status integer DEFAULT 0,
    ts integer DEFAULT 0
);


ALTER TABLE public.polls OWNER TO irida_admin;

--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN polls.access; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN polls.access IS 'Либо публичный доступ (public) либо персональный (persons)';


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN polls.moderator_id; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN polls.moderator_id IS 'Идентификатор модератора (необходим, если создаётся публичный опрос)';


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN polls.public_status; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN polls.public_status IS 'Статус публикации';


--
-- TOC entry 2379 (class 1259 OID 17850)
-- Dependencies: 218
-- Name: TABLE polls; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE polls IS '{0,0}';


--
-- TOC entry 226 (class 1259 OID 17904)
-- Name: polls_answers; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE polls_answers (
    id integer NOT NULL,
    data text,
    poll_quest_id integer,
    poll_id integer,
    result_id integer,
    user_id integer,
    ts integer DEFAULT 0
);


ALTER TABLE public.polls_answers OWNER TO irida_admin;

--
-- TOC entry 2380 (class 1259 OID 17904)
-- Dependencies: 226
-- Name: TABLE polls_answers; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE polls_answers IS '{0,0}';


--
-- TOC entry 225 (class 1259 OID 17902)
-- Name: polls_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE polls_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.polls_answers_id_seq OWNER TO irida_admin;

--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 225
-- Name: polls_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE polls_answers_id_seq OWNED BY polls_answers.id;


--
-- TOC entry 2382 (class 1259 OID 17902)
-- Dependencies: 225
-- Name: SEQUENCE polls_answers_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE polls_answers_id_seq IS '{0,0}';


--
-- TOC entry 217 (class 1259 OID 17848)
-- Name: polls_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE polls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.polls_id_seq OWNER TO irida_admin;

--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 217
-- Name: polls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE polls_id_seq OWNED BY polls.id;


--
-- TOC entry 2384 (class 1259 OID 17848)
-- Dependencies: 217
-- Name: SEQUENCE polls_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE polls_id_seq IS '{0,0}';


--
-- TOC entry 224 (class 1259 OID 17887)
-- Name: polls_quests; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE polls_quests (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    required integer DEFAULT 0,
    active integer DEFAULT 0,
    numb integer DEFAULT 0,
    poll_id integer DEFAULT 0,
    type character varying(50) NOT NULL,
    data text,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0
);


ALTER TABLE public.polls_quests OWNER TO irida_admin;

--
-- TOC entry 2385 (class 1259 OID 17887)
-- Dependencies: 224
-- Name: TABLE polls_quests; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE polls_quests IS '{0,0}';


--
-- TOC entry 223 (class 1259 OID 17885)
-- Name: polls_quests_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE polls_quests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.polls_quests_id_seq OWNER TO irida_admin;

--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 223
-- Name: polls_quests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE polls_quests_id_seq OWNED BY polls_quests.id;


--
-- TOC entry 2387 (class 1259 OID 17885)
-- Dependencies: 223
-- Name: SEQUENCE polls_quests_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE polls_quests_id_seq IS '{0,0}';


--
-- TOC entry 222 (class 1259 OID 17876)
-- Name: polls_results; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE polls_results (
    id integer NOT NULL,
    poll_id integer,
    ts_begin integer DEFAULT 0,
    ts_end integer DEFAULT 0,
    user_id integer,
    anon_status integer DEFAULT 1
);


ALTER TABLE public.polls_results OWNER TO irida_admin;

--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN polls_results.anon_status; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN polls_results.anon_status IS 'Статус анонимности опроса в начале его прохождения';


--
-- TOC entry 2389 (class 1259 OID 17876)
-- Dependencies: 222
-- Name: TABLE polls_results; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE polls_results IS '{0,0}';


--
-- TOC entry 221 (class 1259 OID 17874)
-- Name: polls_results_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE polls_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.polls_results_id_seq OWNER TO irida_admin;

--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 221
-- Name: polls_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE polls_results_id_seq OWNED BY polls_results.id;


--
-- TOC entry 2391 (class 1259 OID 17874)
-- Dependencies: 221
-- Name: SEQUENCE polls_results_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE polls_results_id_seq IS '{0,0}';


--
-- TOC entry 220 (class 1259 OID 17867)
-- Name: polls_users_invites; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE polls_users_invites (
    id integer NOT NULL,
    poll_id integer,
    user_id integer,
    ts integer DEFAULT 0
);


ALTER TABLE public.polls_users_invites OWNER TO irida_admin;

--
-- TOC entry 2392 (class 1259 OID 17867)
-- Dependencies: 220
-- Name: TABLE polls_users_invites; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE polls_users_invites IS '{0,0}';


--
-- TOC entry 219 (class 1259 OID 17865)
-- Name: polls_users_invites_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE polls_users_invites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.polls_users_invites_id_seq OWNER TO irida_admin;

--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 219
-- Name: polls_users_invites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE polls_users_invites_id_seq OWNED BY polls_users_invites.id;


--
-- TOC entry 2394 (class 1259 OID 17865)
-- Dependencies: 219
-- Name: SEQUENCE polls_users_invites_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE polls_users_invites_id_seq IS '{0,0}';


--
-- TOC entry 193 (class 1259 OID 16522)
-- Name: posts; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE posts (
    id integer NOT NULL,
    name character varying(255),
    description text,
    ts integer
);


ALTER TABLE public.posts OWNER TO irida_admin;

--
-- TOC entry 2395 (class 1259 OID 16522)
-- Dependencies: 193
-- Name: TABLE posts; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE posts IS '{0,0}';


--
-- TOC entry 194 (class 1259 OID 16528)
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO irida_admin;

--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 194
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- TOC entry 2397 (class 1259 OID 16528)
-- Dependencies: 194
-- Name: SEQUENCE posts_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE posts_id_seq IS '{0,0}';


--
-- TOC entry 195 (class 1259 OID 16530)
-- Name: posts_to_structure; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE posts_to_structure (
    id integer NOT NULL,
    post_id integer,
    structure_id integer,
    description text,
    ts integer,
    users_count integer DEFAULT 5
);


ALTER TABLE public.posts_to_structure OWNER TO irida_admin;

--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN posts_to_structure.users_count; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN posts_to_structure.users_count IS 'Максимальное количество пользователей на должность';


--
-- TOC entry 2399 (class 1259 OID 16530)
-- Dependencies: 195
-- Name: TABLE posts_to_structure; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE posts_to_structure IS '{0,0}';


--
-- TOC entry 196 (class 1259 OID 16536)
-- Name: posts_to_structure_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE posts_to_structure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_to_structure_id_seq OWNER TO irida_admin;

--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 196
-- Name: posts_to_structure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE posts_to_structure_id_seq OWNED BY posts_to_structure.id;


--
-- TOC entry 2401 (class 1259 OID 16536)
-- Dependencies: 196
-- Name: SEQUENCE posts_to_structure_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE posts_to_structure_id_seq IS '{0,0}';


--
-- TOC entry 197 (class 1259 OID 16538)
-- Name: projects; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE projects (
    id integer NOT NULL,
    name character varying(255),
    description text,
    creator_id integer,
    ts integer DEFAULT 0,
    room_id integer DEFAULT 0
);


ALTER TABLE public.projects OWNER TO irida_admin;

--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN projects.room_id; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN projects.room_id IS 'Идентификатор комнаты, если проект создаётся для комнаты';


--
-- TOC entry 2403 (class 1259 OID 16538)
-- Dependencies: 197
-- Name: TABLE projects; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE projects IS '{0,0}';


--
-- TOC entry 198 (class 1259 OID 16546)
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO irida_admin;

--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 198
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- TOC entry 2405 (class 1259 OID 16546)
-- Dependencies: 198
-- Name: SEQUENCE projects_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE projects_id_seq IS '{0,0}';


--
-- TOC entry 199 (class 1259 OID 16548)
-- Name: restore_hashs; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE restore_hashs (
    id integer NOT NULL,
    type character varying(255),
    input text,
    hash text,
    ts integer DEFAULT 0
);


ALTER TABLE public.restore_hashs OWNER TO irida_admin;

--
-- TOC entry 2406 (class 1259 OID 16548)
-- Dependencies: 199
-- Name: TABLE restore_hashs; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE restore_hashs IS '{0,0}';


--
-- TOC entry 200 (class 1259 OID 16555)
-- Name: restore_hashs_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE restore_hashs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.restore_hashs_id_seq OWNER TO irida_admin;

--
-- TOC entry 2407 (class 0 OID 0)
-- Dependencies: 200
-- Name: restore_hashs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE restore_hashs_id_seq OWNED BY restore_hashs.id;


--
-- TOC entry 2408 (class 1259 OID 16555)
-- Dependencies: 200
-- Name: SEQUENCE restore_hashs_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE restore_hashs_id_seq IS '{0,0}';


--
-- TOC entry 201 (class 1259 OID 16557)
-- Name: rooms; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE rooms (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    cooperation_type character varying(50) NOT NULL,
    connection_notinvited_users integer DEFAULT 0 NOT NULL,
    public_speech_channels_count integer DEFAULT 3 NOT NULL,
    p_user_inviting integer DEFAULT 0 NOT NULL,
    p_room_editing integer DEFAULT 0 NOT NULL,
    c_webcams_translating integer DEFAULT 0 NOT NULL,
    c_interactive_board integer DEFAULT 0 NOT NULL,
    c_messaging integer DEFAULT 0 NOT NULL,
    c_listenerslist_viewing integer DEFAULT 0 NOT NULL,
    type character varying(50) NOT NULL,
    creator_id integer NOT NULL,
    ts integer NOT NULL
);


ALTER TABLE public.rooms OWNER TO irida_admin;

--
-- TOC entry 2409 (class 1259 OID 16557)
-- Dependencies: 201
-- Name: TABLE rooms; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE rooms IS '{0,0}';


--
-- TOC entry 202 (class 1259 OID 16568)
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE rooms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_id_seq OWNER TO irida_admin;

--
-- TOC entry 2410 (class 0 OID 0)
-- Dependencies: 202
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE rooms_id_seq OWNED BY rooms.id;


--
-- TOC entry 2411 (class 1259 OID 16568)
-- Dependencies: 202
-- Name: SEQUENCE rooms_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE rooms_id_seq IS '{0,0}';


--
-- TOC entry 203 (class 1259 OID 16570)
-- Name: rooms_to_projects; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE rooms_to_projects (
    id integer NOT NULL,
    room_id integer,
    project_id integer,
    creator_id integer,
    ts integer DEFAULT 0
);


ALTER TABLE public.rooms_to_projects OWNER TO irida_admin;

--
-- TOC entry 2412 (class 1259 OID 16570)
-- Dependencies: 203
-- Name: TABLE rooms_to_projects; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE rooms_to_projects IS '{0,0}';


--
-- TOC entry 204 (class 1259 OID 16574)
-- Name: rooms_to_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE rooms_to_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_to_projects_id_seq OWNER TO irida_admin;

--
-- TOC entry 2413 (class 0 OID 0)
-- Dependencies: 204
-- Name: rooms_to_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE rooms_to_projects_id_seq OWNED BY rooms_to_projects.id;


--
-- TOC entry 2414 (class 1259 OID 16574)
-- Dependencies: 204
-- Name: SEQUENCE rooms_to_projects_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE rooms_to_projects_id_seq IS '{0,0}';


--
-- TOC entry 205 (class 1259 OID 16576)
-- Name: structure_items; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE structure_items (
    id integer NOT NULL,
    name character varying(255),
    description text,
    parent_id integer DEFAULT 0,
    numb integer DEFAULT 0,
    ts integer
);


ALTER TABLE public.structure_items OWNER TO irida_admin;

--
-- TOC entry 2415 (class 1259 OID 16576)
-- Dependencies: 205
-- Name: TABLE structure_items; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE structure_items IS '{0,0}';


--
-- TOC entry 206 (class 1259 OID 16584)
-- Name: structure_items_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE structure_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.structure_items_id_seq OWNER TO irida_admin;

--
-- TOC entry 2416 (class 0 OID 0)
-- Dependencies: 206
-- Name: structure_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE structure_items_id_seq OWNED BY structure_items.id;


--
-- TOC entry 2417 (class 1259 OID 16584)
-- Dependencies: 206
-- Name: SEQUENCE structure_items_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE structure_items_id_seq IS '{0,0}';


--
-- TOC entry 207 (class 1259 OID 16586)
-- Name: tags; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying(250),
    ts integer DEFAULT 0,
    entity_type character varying(255)
);


ALTER TABLE public.tags OWNER TO irida_admin;

--
-- TOC entry 2418 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN tags.name; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN tags.name IS 'Название тега';


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN tags.ts; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN tags.ts IS 'Время добавления тега';


--
-- TOC entry 2420 (class 1259 OID 16586)
-- Dependencies: 207
-- Name: TABLE tags; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE tags IS '{0,0}';


--
-- TOC entry 208 (class 1259 OID 16593)
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO irida_admin;

--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 208
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- TOC entry 2422 (class 1259 OID 16593)
-- Dependencies: 208
-- Name: SEQUENCE tags_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE tags_id_seq IS '{0,0}';


--
-- TOC entry 209 (class 1259 OID 16595)
-- Name: tags_to_users; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE tags_to_users (
    id integer NOT NULL,
    user_id integer DEFAULT 0,
    tag_id integer DEFAULT 0,
    creator_id integer DEFAULT 0,
    ts integer DEFAULT 0
);


ALTER TABLE public.tags_to_users OWNER TO irida_admin;

--
-- TOC entry 2423 (class 1259 OID 16595)
-- Dependencies: 209
-- Name: TABLE tags_to_users; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE tags_to_users IS '{0,0}';


--
-- TOC entry 210 (class 1259 OID 16602)
-- Name: tags_to_users_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE tags_to_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_to_users_id_seq OWNER TO irida_admin;

--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 210
-- Name: tags_to_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE tags_to_users_id_seq OWNED BY tags_to_users.id;


--
-- TOC entry 2425 (class 1259 OID 16602)
-- Dependencies: 210
-- Name: SEQUENCE tags_to_users_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE tags_to_users_id_seq IS '{0,0}';


--
-- TOC entry 211 (class 1259 OID 16604)
-- Name: users; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    login character varying(100) NOT NULL,
    password character(255) NOT NULL,
    email character varying(150) NOT NULL,
    name character varying(150) NOT NULL,
    middlename character varying(150),
    surname character varying(150),
    active integer DEFAULT 1 NOT NULL,
    comment text,
    user_type character varying(50) DEFAULT 'user'::character varying NOT NULL,
    "timestamp" integer NOT NULL,
    avatar character varying(255) DEFAULT NULL::character varying,
    archive integer DEFAULT 0,
    ldap_login character varying(100),
    last_heartbeat integer DEFAULT 0,
    phone character varying(100),
    settings text,
    remote_host character varying(255),
    remote_ip inet
);


ALTER TABLE public.users OWNER TO irida_admin;

--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN users.phone; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN users.phone IS 'Номер телефона';


--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN users.remote_host; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN users.remote_host IS 'Хост';


--
-- TOC entry 2428 (class 1259 OID 16604)
-- Dependencies: 211
-- Name: TABLE users; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE users IS '{0,0}';


--
-- TOC entry 212 (class 1259 OID 16615)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO irida_admin;

--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 212
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2430 (class 1259 OID 16615)
-- Dependencies: 212
-- Name: SEQUENCE users_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE users_id_seq IS '{0,0}';


--
-- TOC entry 213 (class 1259 OID 16617)
-- Name: users_to_posts; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE users_to_posts (
    id integer NOT NULL,
    post_to_structure_id integer,
    user_id integer,
    description text,
    ts integer
);


ALTER TABLE public.users_to_posts OWNER TO irida_admin;

--
-- TOC entry 2431 (class 1259 OID 16617)
-- Dependencies: 213
-- Name: TABLE users_to_posts; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE users_to_posts IS '{0,0}';


--
-- TOC entry 214 (class 1259 OID 16623)
-- Name: users_to_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE users_to_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_to_posts_id_seq OWNER TO irida_admin;

--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 214
-- Name: users_to_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE users_to_posts_id_seq OWNED BY users_to_posts.id;


--
-- TOC entry 2433 (class 1259 OID 16623)
-- Dependencies: 214
-- Name: SEQUENCE users_to_posts_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE users_to_posts_id_seq IS '{0,0}';


--
-- TOC entry 215 (class 1259 OID 16625)
-- Name: users_to_rooms; Type: TABLE; Schema: public; Owner: irida_admin; Tablespace: 
--

CREATE TABLE users_to_rooms (
    id integer NOT NULL,
    user_id integer DEFAULT 0,
    room_id integer DEFAULT 0,
    role character varying(50) NOT NULL,
    status integer DEFAULT 0,
    ts integer DEFAULT 0,
    inviter_id integer DEFAULT 0
);


ALTER TABLE public.users_to_rooms OWNER TO irida_admin;

--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN users_to_rooms.inviter_id; Type: COMMENT; Schema: public; Owner: irida_admin
--

COMMENT ON COLUMN users_to_rooms.inviter_id IS 'Приглашающий';


--
-- TOC entry 2435 (class 1259 OID 16625)
-- Dependencies: 215
-- Name: TABLE users_to_rooms; Type: MAC LABEL; Schema: -; Owner: irida_admin
--

MAC LABEL ON TABLE users_to_rooms IS '{0,0}';


--
-- TOC entry 216 (class 1259 OID 16633)
-- Name: users_to_rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: irida_admin
--

CREATE SEQUENCE users_to_rooms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_to_rooms_id_seq OWNER TO irida_admin;

--
-- TOC entry 2436 (class 0 OID 0)
-- Dependencies: 216
-- Name: users_to_rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: irida_admin
--

ALTER SEQUENCE users_to_rooms_id_seq OWNED BY users_to_rooms.id;


--
-- TOC entry 2437 (class 1259 OID 16633)
-- Dependencies: 216
-- Name: SEQUENCE users_to_rooms_id_seq; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON SEQUENCE users_to_rooms_id_seq IS '{0,0}';


--
-- TOC entry 2032 (class 2604 OID 16820)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY account_fields ALTER COLUMN id SET DEFAULT nextval('account_fields_id_seq'::regclass);


--
-- TOC entry 2035 (class 2604 OID 16821)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY buildings ALTER COLUMN id SET DEFAULT nextval('buildings_id_seq'::regclass);


--
-- TOC entry 2047 (class 2604 OID 16822)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY equipments ALTER COLUMN id SET DEFAULT nextval('equipments_id_seq'::regclass);


--
-- TOC entry 2051 (class 2604 OID 16823)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 16824)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY events_exclusions ALTER COLUMN id SET DEFAULT nextval('events_exclusions_id_seq'::regclass);


--
-- TOC entry 2058 (class 2604 OID 16825)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY fields_to_users ALTER COLUMN id SET DEFAULT nextval('fields_to_users_id_seq'::regclass);


--
-- TOC entry 2065 (class 2604 OID 16826)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY flats ALTER COLUMN id SET DEFAULT nextval('flats_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 16827)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY floors ALTER COLUMN id SET DEFAULT nextval('floors_id_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 16828)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY index_counter ALTER COLUMN id SET DEFAULT nextval('index_counter_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 16829)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY log ALTER COLUMN id SET DEFAULT nextval('log_id_seq'::regclass);


--
-- TOC entry 2088 (class 2604 OID 16830)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- TOC entry 2131 (class 2604 OID 17853)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY polls ALTER COLUMN id SET DEFAULT nextval('polls_id_seq'::regclass);


--
-- TOC entry 2151 (class 2604 OID 17907)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY polls_answers ALTER COLUMN id SET DEFAULT nextval('polls_answers_id_seq'::regclass);


--
-- TOC entry 2144 (class 2604 OID 17890)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY polls_quests ALTER COLUMN id SET DEFAULT nextval('polls_quests_id_seq'::regclass);


--
-- TOC entry 2140 (class 2604 OID 17879)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY polls_results ALTER COLUMN id SET DEFAULT nextval('polls_results_id_seq'::regclass);


--
-- TOC entry 2138 (class 2604 OID 17870)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY polls_users_invites ALTER COLUMN id SET DEFAULT nextval('polls_users_invites_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 16831)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- TOC entry 2090 (class 2604 OID 16832)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY posts_to_structure ALTER COLUMN id SET DEFAULT nextval('posts_to_structure_id_seq'::regclass);


--
-- TOC entry 2094 (class 2604 OID 16833)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- TOC entry 2096 (class 2604 OID 16834)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY restore_hashs ALTER COLUMN id SET DEFAULT nextval('restore_hashs_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 16835)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY rooms ALTER COLUMN id SET DEFAULT nextval('rooms_id_seq'::regclass);


--
-- TOC entry 2107 (class 2604 OID 16836)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY rooms_to_projects ALTER COLUMN id SET DEFAULT nextval('rooms_to_projects_id_seq'::regclass);


--
-- TOC entry 2110 (class 2604 OID 16837)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY structure_items ALTER COLUMN id SET DEFAULT nextval('structure_items_id_seq'::regclass);


--
-- TOC entry 2112 (class 2604 OID 16838)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- TOC entry 2117 (class 2604 OID 16839)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY tags_to_users ALTER COLUMN id SET DEFAULT nextval('tags_to_users_id_seq'::regclass);


--
-- TOC entry 2123 (class 2604 OID 16840)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2124 (class 2604 OID 16841)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY users_to_posts ALTER COLUMN id SET DEFAULT nextval('users_to_posts_id_seq'::regclass);


--
-- TOC entry 2130 (class 2604 OID 16842)
-- Name: id; Type: DEFAULT; Schema: public; Owner: irida_admin
--

ALTER TABLE ONLY users_to_rooms ALTER COLUMN id SET DEFAULT nextval('users_to_rooms_id_seq'::regclass);


--
-- TOC entry 2154 (class 2606 OID 16659)
-- Name: account_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY account_fields
    ADD CONSTRAINT account_fields_pkey PRIMARY KEY (id);


--
-- TOC entry 2156 (class 2606 OID 16661)
-- Name: buildings_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY buildings
    ADD CONSTRAINT buildings_pkey PRIMARY KEY (id);


--
-- TOC entry 2158 (class 2606 OID 16663)
-- Name: equipments_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY equipments
    ADD CONSTRAINT equipments_pkey PRIMARY KEY (id);


--
-- TOC entry 2162 (class 2606 OID 16665)
-- Name: events_exclusions_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY events_exclusions
    ADD CONSTRAINT events_exclusions_pkey PRIMARY KEY (id);


--
-- TOC entry 2160 (class 2606 OID 16667)
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2164 (class 2606 OID 16669)
-- Name: fields_to_users_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY fields_to_users
    ADD CONSTRAINT fields_to_users_pkey PRIMARY KEY (id);


--
-- TOC entry 2166 (class 2606 OID 16671)
-- Name: flats_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY flats
    ADD CONSTRAINT flats_pkey PRIMARY KEY (id);


--
-- TOC entry 2168 (class 2606 OID 16673)
-- Name: floors_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY floors
    ADD CONSTRAINT floors_pkey PRIMARY KEY (id);


--
-- TOC entry 2170 (class 2606 OID 16675)
-- Name: index_counter_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY index_counter
    ADD CONSTRAINT index_counter_pkey PRIMARY KEY (id);


--
-- TOC entry 2172 (class 2606 OID 16677)
-- Name: log_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- TOC entry 2174 (class 2606 OID 16679)
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- TOC entry 2208 (class 2606 OID 17913)
-- Name: polls_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY polls_answers
    ADD CONSTRAINT polls_answers_pkey PRIMARY KEY (id);


--
-- TOC entry 2200 (class 2606 OID 17864)
-- Name: polls_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY polls
    ADD CONSTRAINT polls_pkey PRIMARY KEY (id);


--
-- TOC entry 2206 (class 2606 OID 17901)
-- Name: polls_quests_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY polls_quests
    ADD CONSTRAINT polls_quests_pkey PRIMARY KEY (id);


--
-- TOC entry 2204 (class 2606 OID 17884)
-- Name: polls_results_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY polls_results
    ADD CONSTRAINT polls_results_pkey PRIMARY KEY (id);


--
-- TOC entry 2202 (class 2606 OID 17873)
-- Name: polls_users_invites_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY polls_users_invites
    ADD CONSTRAINT polls_users_invites_pkey PRIMARY KEY (id);


--
-- TOC entry 2176 (class 2606 OID 16681)
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- TOC entry 2178 (class 2606 OID 16683)
-- Name: posts_to_structure_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY posts_to_structure
    ADD CONSTRAINT posts_to_structure_pkey PRIMARY KEY (id);


--
-- TOC entry 2180 (class 2606 OID 16685)
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- TOC entry 2182 (class 2606 OID 16687)
-- Name: restore_hashs_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY restore_hashs
    ADD CONSTRAINT restore_hashs_pkey PRIMARY KEY (id);


--
-- TOC entry 2184 (class 2606 OID 16689)
-- Name: rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);


--
-- TOC entry 2186 (class 2606 OID 16691)
-- Name: rooms_to_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY rooms_to_projects
    ADD CONSTRAINT rooms_to_projects_pkey PRIMARY KEY (id);


--
-- TOC entry 2188 (class 2606 OID 16693)
-- Name: structure_items_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY structure_items
    ADD CONSTRAINT structure_items_pkey PRIMARY KEY (id);


--
-- TOC entry 2190 (class 2606 OID 16695)
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 2192 (class 2606 OID 16697)
-- Name: tags_to_users_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY tags_to_users
    ADD CONSTRAINT tags_to_users_pkey PRIMARY KEY (id);


--
-- TOC entry 2194 (class 2606 OID 16699)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (login);


--
-- TOC entry 2196 (class 2606 OID 16701)
-- Name: users_to_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY users_to_posts
    ADD CONSTRAINT users_to_posts_pkey PRIMARY KEY (id);


--
-- TOC entry 2198 (class 2606 OID 16703)
-- Name: users_to_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: irida_admin; Tablespace: 
--

ALTER TABLE ONLY users_to_rooms
    ADD CONSTRAINT users_to_rooms_pkey PRIMARY KEY (id);


--
-- TOC entry 2438 (class 1259 OID 16658)
-- Dependencies: 2153
-- Name: INDEX account_fields_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX account_fields_pkey IS '{0,0}';


--
-- TOC entry 2439 (class 1259 OID 16660)
-- Dependencies: 2155
-- Name: INDEX buildings_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX buildings_pkey IS '{0,0}';


--
-- TOC entry 2440 (class 1259 OID 16662)
-- Dependencies: 2157
-- Name: INDEX equipments_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX equipments_pkey IS '{0,0}';


--
-- TOC entry 2441 (class 1259 OID 16664)
-- Dependencies: 2161
-- Name: INDEX events_exclusions_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX events_exclusions_pkey IS '{0,0}';


--
-- TOC entry 2442 (class 1259 OID 16666)
-- Dependencies: 2159
-- Name: INDEX events_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX events_pkey IS '{0,0}';


--
-- TOC entry 2443 (class 1259 OID 16668)
-- Dependencies: 2163
-- Name: INDEX fields_to_users_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX fields_to_users_pkey IS '{0,0}';


--
-- TOC entry 2444 (class 1259 OID 16670)
-- Dependencies: 2165
-- Name: INDEX flats_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX flats_pkey IS '{0,0}';


--
-- TOC entry 2445 (class 1259 OID 16672)
-- Dependencies: 2167
-- Name: INDEX floors_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX floors_pkey IS '{0,0}';


--
-- TOC entry 2446 (class 1259 OID 16674)
-- Dependencies: 2169
-- Name: INDEX index_counter_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX index_counter_pkey IS '{0,0}';


--
-- TOC entry 2447 (class 1259 OID 16676)
-- Dependencies: 2171
-- Name: INDEX log_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX log_pkey IS '{0,0}';


--
-- TOC entry 2448 (class 1259 OID 16678)
-- Dependencies: 2173
-- Name: INDEX notifications_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX notifications_pkey IS '{0,0}';


--
-- TOC entry 2449 (class 1259 OID 17912)
-- Dependencies: 2207
-- Name: INDEX polls_answers_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX polls_answers_pkey IS '{0,0}';


--
-- TOC entry 2450 (class 1259 OID 17863)
-- Dependencies: 2199
-- Name: INDEX polls_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX polls_pkey IS '{0,0}';


--
-- TOC entry 2451 (class 1259 OID 17900)
-- Dependencies: 2205
-- Name: INDEX polls_quests_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX polls_quests_pkey IS '{0,0}';


--
-- TOC entry 2452 (class 1259 OID 17883)
-- Dependencies: 2203
-- Name: INDEX polls_results_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX polls_results_pkey IS '{0,0}';


--
-- TOC entry 2453 (class 1259 OID 17872)
-- Dependencies: 2201
-- Name: INDEX polls_users_invites_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX polls_users_invites_pkey IS '{0,0}';


--
-- TOC entry 2454 (class 1259 OID 16680)
-- Dependencies: 2175
-- Name: INDEX posts_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX posts_pkey IS '{0,0}';


--
-- TOC entry 2455 (class 1259 OID 16682)
-- Dependencies: 2177
-- Name: INDEX posts_to_structure_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX posts_to_structure_pkey IS '{0,0}';


--
-- TOC entry 2456 (class 1259 OID 16684)
-- Dependencies: 2179
-- Name: INDEX projects_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX projects_pkey IS '{0,0}';


--
-- TOC entry 2457 (class 1259 OID 16686)
-- Dependencies: 2181
-- Name: INDEX restore_hashs_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX restore_hashs_pkey IS '{0,0}';


--
-- TOC entry 2458 (class 1259 OID 16688)
-- Dependencies: 2183
-- Name: INDEX rooms_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX rooms_pkey IS '{0,0}';


--
-- TOC entry 2459 (class 1259 OID 16690)
-- Dependencies: 2185
-- Name: INDEX rooms_to_projects_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX rooms_to_projects_pkey IS '{0,0}';


--
-- TOC entry 2460 (class 1259 OID 16692)
-- Dependencies: 2187
-- Name: INDEX structure_items_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX structure_items_pkey IS '{0,0}';


--
-- TOC entry 2461 (class 1259 OID 16694)
-- Dependencies: 2189
-- Name: INDEX tags_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX tags_pkey IS '{0,0}';


--
-- TOC entry 2462 (class 1259 OID 16696)
-- Dependencies: 2191
-- Name: INDEX tags_to_users_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX tags_to_users_pkey IS '{0,0}';


--
-- TOC entry 2463 (class 1259 OID 16698)
-- Dependencies: 2193
-- Name: INDEX users_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX users_pkey IS '{0,0}';


--
-- TOC entry 2464 (class 1259 OID 16700)
-- Dependencies: 2195
-- Name: INDEX users_to_posts_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX users_to_posts_pkey IS '{0,0}';


--
-- TOC entry 2465 (class 1259 OID 16702)
-- Dependencies: 2197
-- Name: INDEX users_to_rooms_pkey; Type: MAC LABEL; Schema: public; Owner: irida_admin
--

MAC LABEL ON INDEX users_to_rooms_pkey IS '{0,0}';


--
-- TOC entry 2324 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-12-03 10:47:44 MSK

--
-- PostgreSQL database dump complete
--

