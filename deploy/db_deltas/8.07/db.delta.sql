CREATE TABLE feedback
(
	id serial NOT NULL,
	problem text,
	status integer DEFAULT 0,
	comment text DEFAULT ''::character varying,
	creator_id integer DEFAULT 0,
	ts integer DEFAULT 0,
	CONSTRAINT feedback_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);
ALTER TABLE feedback OWNER TO irida_admin;


COMMENT ON COLUMN feedback.comment IS 'Комментарий к изменению статуса';


ALTER TABLE rooms ADD COLUMN archive_status integer;
ALTER TABLE rooms ALTER COLUMN archive_status SET DEFAULT 0;
UPDATE rooms SET archive_status = 0;



CREATE TABLE rooms_tokens
(
	id serial NOT NULL,
	room_id integer DEFAULT 0,
	user_id integer DEFAULT 0,
	ts integer DEFAULT 0,
	token text DEFAULT ''::character varying,
	CONSTRAINT rooms_tokens_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);
ALTER TABLE rooms_tokens OWNER TO irida_admin;
