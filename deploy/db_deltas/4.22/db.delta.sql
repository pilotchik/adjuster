CREATE TABLE equipments_to_rooms
(
  id serial NOT NULL,
  room_id integer DEFAULT 0,
  eq_id integer DEFAULT 0,
  CONSTRAINT equipments_to_rooms_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE equipments_to_rooms OWNER TO irida_admin;