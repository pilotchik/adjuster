ALTER TABLE rooms ADD COLUMN c_work_foreign_boards_elements integer;
ALTER TABLE rooms ALTER COLUMN c_work_foreign_boards_elements SET DEFAULT 1;
UPDATE rooms SET c_work_foreign_boards_elements = 1;
COMMENT ON COLUMN rooms.c_work_foreign_boards_elements IS 'Работать с элементами доски других участников';