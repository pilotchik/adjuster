CREATE TABLE files
(
  id serial NOT NULL,
  file_addr text,
  file_key text,
  description text,
  title text,
  ts integer DEFAULT 0,
  CONSTRAINT files_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE files OWNER TO irida_admin;


ALTER TABLE files ADD COLUMN file_secondary_key text;
UPDATE files SET file_secondary_key = '';