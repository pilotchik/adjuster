ALTER TABLE rooms ADD COLUMN pin character varying(255);
COMMENT ON COLUMN rooms.pin IS 'Случайный PIN-код для доступа к комнате любого паблишера';
UPDATE rooms SET pin = 'abcdef';

ALTER TABLE rooms ADD COLUMN listener_pin character varying(255);
COMMENT ON COLUMN rooms.listener_pin IS 'Случайный PIN-код для доступа к комнате любого слушателя';
UPDATE rooms SET listener_pin = 'abc2345def';

ALTER TABLE rooms ADD COLUMN secret character varying(255);
COMMENT ON COLUMN rooms.secret IS 'Случайный код для доступа ведущего к комнате';
UPDATE rooms SET secret = '5642abc2345def';

ALTER TABLE rooms ADD COLUMN secondary_secret character varying(255);
COMMENT ON COLUMN rooms.secondary_secret IS 'Случайный код для доступа запасных ведущих к комнате';
UPDATE rooms SET secondary_secret = '5642abc32562345de234f';


