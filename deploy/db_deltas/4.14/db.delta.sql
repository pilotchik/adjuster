ALTER TABLE rooms ADD COLUMN masterless_mode integer;
ALTER TABLE rooms ALTER COLUMN masterless_mode SET DEFAULT 0;
UPDATE rooms SET masterless_mode = 0;