ALTER TABLE index_counter ADD COLUMN entity_id integer;
ALTER TABLE index_counter ALTER COLUMN entity_id SET DEFAULT 0;
UPDATE index_counter SET entity_id = 0;


CREATE TABLE rooms_favorite_to_users
(
  id serial NOT NULL,
  room_id integer DEFAULT 0,
  user_id integer DEFAULT 0,
  CONSTRAINT rooms_favorite_to_users_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE rooms_favorite_to_users OWNER TO irida_admin;