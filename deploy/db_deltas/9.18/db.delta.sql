ALTER TABLE polls ADD COLUMN room_id integer;
ALTER TABLE polls ALTER COLUMN room_id SET DEFAULT 0;
UPDATE polls SET room_id = 0;
COMMENT ON COLUMN polls.room_id IS 'Идентификатор комнаты';