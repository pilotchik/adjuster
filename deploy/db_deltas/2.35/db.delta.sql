ALTER TABLE floors ADD COLUMN background text;
COMMENT ON COLUMN floors.background IS 'Фоновое изображение';

ALTER TABLE floors ADD COLUMN background_offset_x integer;
ALTER TABLE floors ALTER COLUMN background_offset_x SET DEFAULT 0;
UPDATE floors SET background_offset_x = 0;
COMMENT ON COLUMN floors.background_offset_x IS 'Отступ сверху';

ALTER TABLE floors ADD COLUMN background_offset_y integer;
ALTER TABLE floors ALTER COLUMN background_offset_y SET DEFAULT 0;
UPDATE floors SET background_offset_y = 0;
COMMENT ON COLUMN floors.background_offset_y IS 'Отступ слева';

ALTER TABLE floors ADD COLUMN background_opacity integer;
ALTER TABLE floors ALTER COLUMN background_opacity SET DEFAULT 100;
UPDATE floors SET background_opacity = 100;
COMMENT ON COLUMN floors.background_opacity IS 'Прозрачность';

ALTER TABLE floors ADD COLUMN background_scale integer;
ALTER TABLE floors ALTER COLUMN background_scale SET DEFAULT 100;
UPDATE floors SET background_scale = 100;
COMMENT ON COLUMN floors.background_scale IS 'Масштаб';

ALTER TABLE flats ADD COLUMN background text;
COMMENT ON COLUMN flats.background IS 'Фоновое изображение';

ALTER TABLE flats ADD COLUMN background_offset_x integer;
ALTER TABLE flats ALTER COLUMN background_offset_x SET DEFAULT 0;
UPDATE flats SET background_offset_x = 0;
COMMENT ON COLUMN flats.background_offset_x IS 'Отступ сверху';

ALTER TABLE flats ADD COLUMN background_offset_y integer;
ALTER TABLE flats ALTER COLUMN background_offset_y SET DEFAULT 0;
UPDATE flats SET background_offset_y = 0;
COMMENT ON COLUMN flats.background_offset_y IS 'Отступ слева';

ALTER TABLE flats ADD COLUMN background_opacity integer;
ALTER TABLE flats ALTER COLUMN background_opacity SET DEFAULT 100;
UPDATE flats SET background_opacity = 100;
COMMENT ON COLUMN flats.background_opacity IS 'Прозрачность';

ALTER TABLE flats ADD COLUMN background_scale integer;
ALTER TABLE flats ALTER COLUMN background_scale SET DEFAULT 100;
UPDATE flats SET background_scale = 100;
COMMENT ON COLUMN flats.background_scale IS 'Масштаб';