ALTER TABLE events ADD COLUMN duration integer;
ALTER TABLE events ALTER COLUMN duration SET DEFAULT 45;
UPDATE events SET duration = 60;
COMMENT ON COLUMN events.duration IS 'Продолжительность';