ALTER TABLE rooms ADD COLUMN c_share_self_screen integer;
ALTER TABLE rooms ALTER COLUMN c_share_self_screen SET DEFAULT 0;
UPDATE rooms SET c_share_self_screen = 0;
COMMENT ON COLUMN rooms.c_share_self_screen IS 'Пользователю разрешено делиться своим экраном';


DROP TABLE configuration;

CREATE TABLE configuration
(
	id serial NOT NULL,
	name character varying(100) NOT NULL,
	value text NOT NULL,
	type character varying(20) DEFAULT 'text'::character varying, -- тип параметра
	required integer DEFAULT 1,
	section character varying(50) DEFAULT 'main'::character varying, -- Раздел настройки
	CONSTRAINT configuration_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE configuration OWNER TO irida_admin;
COMMENT ON COLUMN configuration.type IS 'тип параметра';
COMMENT ON COLUMN configuration.section IS 'Раздел настройки';