ALTER TABLE rooms ADD COLUMN с_upload_maxfilesize integer;
ALTER TABLE rooms ALTER COLUMN с_upload_maxfilesize SET DEFAULT 20;
COMMENT ON COLUMN rooms.с_upload_maxfilesize IS 'Максимальный размер загружаемых файлов';

UPDATE rooms SET с_upload_maxfilesize = 20;