CREATE TABLE users_favorite_to_users
(
	id serial NOT NULL,
	user_id integer DEFAULT 0,
	creator_id integer DEFAULT 0,
	CONSTRAINT users_favorite_to_users_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE users_favorite_to_users OWNER TO irida_admin;