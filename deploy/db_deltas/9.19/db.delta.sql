CREATE TABLE threads
(
  id serial NOT NULL,
  "name" character varying(255),
  avatar text,
  clients_s text,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT threads_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);
ALTER TABLE threads OWNER TO irida_admin;


CREATE TABLE thread_clients
(
  id serial NOT NULL,
  user_id integer DEFAULT 0,
  active integer DEFAULT 1,
  thread_id integer DEFAULT 0,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT thread_clients_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);
ALTER TABLE thread_clients OWNER TO irida_admin;