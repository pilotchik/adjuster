CREATE TABLE sked
(
  id serial NOT NULL,
  "name" character varying(150),
  description text,
  image text,
  color character varying(10),
  "type" character varying(50),
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT sked_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sked OWNER TO irida_admin;



ALTER TABLE users ADD COLUMN redirect_url text;