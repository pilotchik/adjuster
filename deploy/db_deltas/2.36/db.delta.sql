ALTER TABLE users ADD COLUMN remote_host character varying(255);
COMMENT ON COLUMN users.remote_host IS 'Хост';


ALTER TABLE posts_to_structure ADD COLUMN users_count integer;
ALTER TABLE posts_to_structure ALTER COLUMN users_count SET DEFAULT 5;
UPDATE posts_to_structure SET users_count = 5;
COMMENT ON COLUMN posts_to_structure.users_count IS 'Максимальное количество пользователей на должность';


CREATE TABLE polls
(
  id serial NOT NULL,
  title character varying(255) NOT NULL,
  description text,
  access character varying(50),
  anon_status integer DEFAULT 0,
  active integer DEFAULT 1,
  creator_id integer DEFAULT 0,
  moderator_id integer DEFAULT 0,
  public_status integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT polls_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN polls.access IS 'Либо публичный доступ (public) либо персональный (persons)';
COMMENT ON COLUMN polls.moderator_id IS 'Идентификатор модератора (необходим, если создаётся публичный опрос)';
COMMENT ON COLUMN polls.public_status IS 'Статус публикации';
ALTER TABLE polls OWNER TO irida_admin;

CREATE TABLE polls_users_invites
(
  id serial NOT NULL,
  poll_id integer,
  user_id integer,
  ts integer DEFAULT 0,
  CONSTRAINT polls_users_invites_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE polls_users_invites OWNER TO irida_admin;


CREATE TABLE polls_results
(
  id serial NOT NULL,
  poll_id integer,
  ts_begin integer DEFAULT 0,
  ts_end integer DEFAULT 0,
  user_id integer,
  anon_status integer DEFAULT 1,
  CONSTRAINT polls_results_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE polls_results OWNER TO irida_admin;

COMMENT ON COLUMN polls_results.anon_status IS 'Статус анонимности опроса в начале его прохождения';


CREATE TABLE polls_quests
(
  id serial NOT NULL,
  title character varying(255) NOT NULL,
  description text,
  required integer DEFAULT 0,
  active integer DEFAULT 0,
  numb integer DEFAULT 0,
  poll_id integer DEFAULT 0,
  type character varying(50) NOT NULL,
  data text,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT polls_quests_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE polls_quests OWNER TO irida_admin;


CREATE TABLE polls_answers
(
  id serial NOT NULL,
  data text,
  poll_quest_id integer,
  poll_id integer,
  result_id integer,
  user_id integer,
  ts integer DEFAULT 0,
  CONSTRAINT polls_answers_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE polls_answers OWNER TO irida_admin;


ALTER TABLE posts ADD COLUMN parent_id integer;
ALTER TABLE posts ALTER COLUMN parent_id SET DEFAULT 0;
UPDATE posts SET parent_id = 0;
COMMENT ON COLUMN posts.parent_id IS 'Идентификатор родительской должности';

ALTER TABLE posts ADD COLUMN level integer;
ALTER TABLE posts ALTER COLUMN level SET DEFAULT 0;
UPDATE posts SET level = 0;
COMMENT ON COLUMN posts.level IS 'Уровень вложенности';