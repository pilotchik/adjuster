ALTER TABLE rooms ADD COLUMN c_board_listeners_drawing integer;
ALTER TABLE rooms ALTER COLUMN c_board_listeners_drawing SET DEFAULT 0;
UPDATE rooms SET c_board_listeners_drawing = 0;

ALTER TABLE rooms ADD COLUMN c_create_listeners_from_listeners integer;
ALTER TABLE rooms ALTER COLUMN c_create_listeners_from_listeners SET DEFAULT 0;
UPDATE rooms SET c_create_listeners_from_listeners = 0;


