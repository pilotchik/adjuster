ALTER TABLE files ADD COLUMN user_id integer;
ALTER TABLE files ALTER COLUMN user_id SET DEFAULT 0;
UPDATE files SET user_id = 0;
COMMENT ON COLUMN files.user_id IS 'Идентификатор создателя файла';

ALTER TABLE sked ADD COLUMN target character varying(255);
ALTER TABLE sked ALTER COLUMN target SET DEFAULT 'popup'::character varying;
UPDATE sked SET target = 'popup';
COMMENT ON COLUMN sked.target IS 'Целевое использование';

ALTER TABLE sked ADD COLUMN type_id integer;
ALTER TABLE sked ALTER COLUMN type_id SET DEFAULT 0;
UPDATE sked SET type_id = 0;
COMMENT ON COLUMN sked.type_id IS 'Идентификатор типа';

ALTER TABLE sked ADD COLUMN status_id integer;
ALTER TABLE sked ALTER COLUMN status_id SET DEFAULT 0;
UPDATE sked SET status_id = 0;
COMMENT ON COLUMN sked.status_id IS 'Идентификатор статуса';
