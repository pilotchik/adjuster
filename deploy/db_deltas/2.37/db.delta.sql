ALTER TABLE rooms ADD COLUMN c_pointer_using integer;
ALTER TABLE rooms ALTER COLUMN c_pointer_using SET DEFAULT 1;
UPDATE rooms SET c_pointer_using = 1;
COMMENT ON COLUMN rooms.c_pointer_using IS 'Использование указки';


ALTER TABLE rooms ADD COLUMN c_random_connection integer;
ALTER TABLE rooms ALTER COLUMN c_random_connection SET DEFAULT 1;
UPDATE rooms SET c_random_connection = 1;
COMMENT ON COLUMN rooms.c_random_connection IS 'Подключаться к произвольным рабочим столам';


CREATE TABLE remote_links
(
  id serial NOT NULL,
  link character varying(255) NOT NULL,
  description text,
  entity_type character varying(50),
  entity_id integer DEFAULT 0,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT remote_links_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

COMMENT ON COLUMN remote_links.entity_type IS 'Тип объекта: комната (room) либо проект (project)';
ALTER TABLE remote_links OWNER TO irida_admin;


ALTER TABLE rooms ADD COLUMN c_self_secvideoch_maximizing integer;
ALTER TABLE rooms ALTER COLUMN c_self_secvideoch_maximizing SET DEFAULT 1;
UPDATE rooms SET c_self_secvideoch_maximizing = 1;
COMMENT ON COLUMN rooms.c_self_secvideoch_maximizing IS 'запретить самостоятельную максимизацию/минимизацию вторичных видеоканалов';

ALTER TABLE rooms ADD COLUMN c_self_betweenfaces_switching integer;
ALTER TABLE rooms ALTER COLUMN c_self_betweenfaces_switching SET DEFAULT 1;
UPDATE rooms SET c_self_betweenfaces_switching = 1;
COMMENT ON COLUMN rooms.c_self_betweenfaces_switching IS 'запретить самостоятельное переключение между гранями';

ALTER TABLE rooms ADD COLUMN c_self_betweendocs_switching integer;
ALTER TABLE rooms ALTER COLUMN c_self_betweendocs_switching SET DEFAULT 1;
UPDATE rooms SET c_self_betweendocs_switching = 1;
COMMENT ON COLUMN rooms.c_self_betweendocs_switching IS 'запретить самостоятельное переключение между документами, запретить открытие документов';

ALTER TABLE rooms ADD COLUMN c_self_betweendocssites_switching integer;
ALTER TABLE rooms ALTER COLUMN c_self_betweendocssites_switching SET DEFAULT 1;
UPDATE rooms SET c_self_betweendocssites_switching = 1;
COMMENT ON COLUMN rooms.c_self_betweendocssites_switching IS 'запретить самостоятельное переключение между страницами документа';

ALTER TABLE rooms ADD COLUMN c_self_scale_changing integer;
ALTER TABLE rooms ALTER COLUMN c_self_scale_changing SET DEFAULT 1;
UPDATE rooms SET c_self_scale_changing = 1;
COMMENT ON COLUMN rooms.c_self_scale_changing IS 'запретить самостоятельное изменение масштаба';

ALTER TABLE rooms ADD COLUMN c_self_board_moving integer;
ALTER TABLE rooms ALTER COLUMN c_self_board_moving SET DEFAULT 1;
UPDATE rooms SET c_self_board_moving = 1;
COMMENT ON COLUMN rooms.c_self_board_moving IS 'запретить самостоятельное перемещение доски';

ALTER TABLE rooms ADD COLUMN c_self_minimap_shifting integer;
ALTER TABLE rooms ALTER COLUMN c_self_minimap_shifting SET DEFAULT 1;
UPDATE rooms SET c_self_minimap_shifting = 1;
COMMENT ON COLUMN rooms.c_self_minimap_shifting IS 'запретить самостоятельное включение/отключение миникарты';

ALTER TABLE rooms ADD COLUMN c_self_desktop_connection integer;
ALTER TABLE rooms ALTER COLUMN c_self_desktop_connection SET DEFAULT 1;
UPDATE rooms SET c_self_desktop_connection = 1;
COMMENT ON COLUMN rooms.c_self_desktop_connection IS 'запретить самостоятельное подключение по VNC, подключение к рабочему столу в автоматическом режиме';

ALTER TABLE rooms ADD COLUMN c_self_docsonboard_publication integer;
ALTER TABLE rooms ALTER COLUMN c_self_docsonboard_publication SET DEFAULT 1;
UPDATE rooms SET c_self_docsonboard_publication = 1;
COMMENT ON COLUMN rooms.c_self_docsonboard_publication IS 'запретить самостоятельное открытие и публикацию документов на доску';

ALTER TABLE rooms ADD COLUMN c_self_video_starting integer;
ALTER TABLE rooms ALTER COLUMN c_self_video_starting SET DEFAULT 1;
UPDATE rooms SET c_self_video_starting = 1;
COMMENT ON COLUMN rooms.c_self_video_starting IS 'запретить самостоятельный запуск видеофайлов';


CREATE TABLE system_info
(
  id serial NOT NULL,
  description text,
  db_version character varying(50),
  CONSTRAINT system_info_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE remote_links OWNER TO irida_admin;

INSERT INTO system_info (db_version) VALUES ('2.37');


ALTER TABLE users ADD COLUMN birthday integer;
ALTER TABLE users ALTER COLUMN birthday SET DEFAULT 0;
UPDATE users SET birthday = 0;
COMMENT ON COLUMN users.birthday IS 'timestamp дня рождения';

ALTER TABLE system_info ADD COLUMN project character varying(255);
COMMENT ON COLUMN system_info.project IS 'Имя проекта';