CREATE TABLE sked_to_projects
(
  id serial NOT NULL,
  project_id integer DEFAULT 0,
  sked_id integer DEFAULT 0,
  CONSTRAINT sked_to_projects_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sked_to_projects OWNER TO irida_admin;