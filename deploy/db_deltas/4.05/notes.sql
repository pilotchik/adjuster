CREATE TABLE notes
(
  id serial NOT NULL,
  subject text,
  body text,
  "size" integer DEFAULT 2,
  "type" character varying(50),
  top_status integer DEFAULT 0,
  structure_item_id integer DEFAULT 0,
  start_time integer DEFAULT 0,
  stop_time integer DEFAULT 0,
  top integer DEFAULT 0,
  "left" integer DEFAULT 0,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT notes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE note_users_status
(
  id serial NOT NULL,
  note_id integer DEFAULT 0,
  status integer DEFAULT 0,
  like_status integer DEFAULT 0,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT note_users_status_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE note_users_positions
(
  id serial NOT NULL,
  note_id integer DEFAULT 0,
  user_id integer DEFAULT 0,
  top integer DEFAULT 0,
  "left" integer DEFAULT 0,
  CONSTRAINT note_users_positions_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE rooms ADD COLUMN record_status integer;
ALTER TABLE rooms ALTER COLUMN record_status SET DEFAULT 0;
COMMENT ON COLUMN rooms.record_status IS 'Статус записи публичных видеопотоков';