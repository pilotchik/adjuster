ALTER TABLE rooms ADD COLUMN c_self_documents_view integer;
ALTER TABLE rooms ALTER COLUMN c_self_documents_view SET DEFAULT 1;
UPDATE rooms SET c_self_documents_view = 1;
COMMENT ON COLUMN rooms.c_self_documents_view IS 'Самостоятельный просмотр документов';