ALTER TABLE users_to_rooms ADD COLUMN tt bigint;
ALTER TABLE users_to_rooms ALTER COLUMN tt SET DEFAULT 0;
UPDATE users_to_rooms SET tt = 0;
COMMENT ON COLUMN users_to_rooms.tt IS 'Время транзакции';

ALTER TABLE users_to_rooms ADD COLUMN operation_type integer;
ALTER TABLE users_to_rooms ALTER COLUMN operation_type SET DEFAULT 0;
UPDATE users_to_rooms SET operation_type = 0;
COMMENT ON COLUMN users_to_rooms.operation_type IS 'Тип операции с записью - 0 - ничего, 1 - добавить, 2 - удалить';

ALTER TABLE events ADD COLUMN last_notify_ts integer;
ALTER TABLE events ALTER COLUMN last_notify_ts SET DEFAULT 0;
UPDATE events SET last_notify_ts = 0;
COMMENT ON COLUMN events.last_notify_ts IS 'Время последнего уведомления о мероприятии';


ALTER TABLE sked ADD COLUMN req_status integer;
ALTER TABLE sked ALTER COLUMN req_status SET DEFAULT 0;
UPDATE sked SET req_status = 0;
COMMENT ON COLUMN sked.req_status IS 'Статус включения по умолчанию для новых проектов';

ALTER TABLE rooms ADD COLUMN connection_files_public_access integer;
ALTER TABLE rooms ALTER COLUMN connection_files_public_access SET DEFAULT 0;
UPDATE rooms SET connection_files_public_access = 0;
COMMENT ON COLUMN rooms.connection_files_public_access IS 'Публичный доступ к файлам';


CREATE TABLE users_to_projects
(
  id serial NOT NULL,
  master_status integer DEFAULT 0,
  user_id integer DEFAULT 0,
  project_id integer DEFAULT 0,
  creator_id integer DEFAULT 0,
  ts integer DEFAULT 0,
  CONSTRAINT users_to_projects_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users_to_projects OWNER TO irida_admin;


ALTER TABLE projects ADD COLUMN public_status integer;
ALTER TABLE projects ALTER COLUMN public_status SET DEFAULT 0;
UPDATE projects SET public_status = 0;
COMMENT ON COLUMN projects.public_status IS 'Статус публичности проекта';


ALTER TABLE users_to_projects ADD COLUMN tt bigint;
ALTER TABLE users_to_projects ALTER COLUMN tt SET DEFAULT 0;
UPDATE users_to_projects SET tt = 0;
COMMENT ON COLUMN users_to_projects.tt IS 'Время транзакции';

ALTER TABLE users_to_projects ADD COLUMN operation_type integer;
ALTER TABLE users_to_projects ALTER COLUMN operation_type SET DEFAULT 0;
UPDATE users_to_projects SET operation_type = 0;
COMMENT ON COLUMN users_to_projects.operation_type IS 'Тип операции с записью - 0 - ничего, 1 - добавить, 2 - удалить';
