<?php

/* 
 * Настройки работы приложения
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$config['config_version'] = '9.19';//Версия
$config['config_view_version_in_title'] = 0;//Отображение версии в названии
$config['config_version_revision'] = '10140';
$config['config_project_code'] = 'viking';

$config['config_yandex_counter'] = 43081564;

$config['config_online_execution_time'] = 60;//Время, прошедщее с последней активности, за которое считается, что пользователь онлайн (секунд)

$config['config_table_rows_on_site'] = 20; //Количество строк по умолчанию отображаемых для таблицы
$config['config_day_time_period'] = 2; //Количество часов в периоде времени для даты

$config['config_use_ldap'] = 0;
$config['config_ldap_dc'] = 'ou = People, dc = unit, dc = spb';
$config['config_ldap_dc_groups'] = 'ou = Group, dc = unit, dc = spb';
$config['config_ldap_server'] = 'bark.unit.spb';

$config['config_ldap_mail'] = 'unit.spb'; //То, что после собачки
$config['config_ldap_secondary_groups'] = 'teachers,students';
$config['config_grant_user_roles'] = 'innouser, training';

//Базовый отступ слева для дерева (в пикселях)
$config['config_base_tree_offset'] = 30;

//Время кэширования статики (в минутах)
$config['config_cache_view_time'] = 60;
$config['config_cache_driver'] = 'file';

//Почта
$config['config_mail_bcc_address'] = 'arhimor@gmail.com'; //скрытая копия

$config['config_notification_types'] = 'notifications_mail,notifications_popup';//Типы уведомлений

// Параметры XMPP
$config['config_xmpp_logtxt'] = false; //ведение лога XMPP
$config['config_xmpp_logfilename'] = 'loggerxmpp.log'; //файл лога
$config['config_xmpp_tlsoff'] = 1; // принудительное отключение шифрования. 1 - выключено, 0 - включено, если сервер поддерживает
