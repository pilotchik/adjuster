<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

        
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct() {
		
            self::$instance =& $this;

            // Assign all the class objects that were instantiated by the
            // bootstrap file (CodeIgniter.php) to local class variables
            // so that CI can run as one big super object.
            foreach (is_loaded() as $var => $class) {
                $this->$var =& load_class($class);
            }

            $this->load =& load_class('Loader', 'core');
            $this->load->initialize();
            log_message('info', 'Controller Class Initialized');
                
            /*
             * Подгрузка конфигурации из базы
             */
            $config_array = $this->config_model->getConfig();
            $this->load->vars($config_array);
            
            /*
             * Обработка системных и пользовательских ошибок
             */
            $get_error = $this->input->get('error');
            $err['system_error'] = isset($get_error) ? str_replace("'", "", $get_error) : '';
            
            $get_error_type = $this->input->get('error_type');
            $err['system_error_type'] = isset($get_error_type) ? $get_error_type : '';
            
            
            $session_errors = $this->session->userdata('error');
            if (!empty($session_errors)) {
                $err['system_error'] = $session_errors['system_error'];
                $err['system_error_type'] = $session_errors['system_error_type'];
            }
            
            $tab['active_tab'] = $this->input->get('tab');
            
            $this->load->vars($err);
            
            $this->load->vars($tab);
            
            date_default_timezone_set('Europe/Moscow');
                
	}

	// --------------------------------------------------------------------

    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
            return self::$instance;
    }


    public function printf_r($var,$n=0) {
        if($n != 0) {
            $var = implode("\n", str_split($var, $n));
        }
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

}
