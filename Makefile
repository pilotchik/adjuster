NAME	= irida.main
DBNAME	= irida_admin
DBUSER  = $(DBNAME)
DBPASS  = $(DBNAME)
PROJECT = $(shell cat .project)
BASE	= /srv/irida/$(NAME)

all:
	@echo ""	
	@echo "####################################################"
	@echo "#                                                  #"
	@echo "#          ###   ####   ###   ###     ##           #"
	@echo "#           #    #  #    #    #  #   #  #          #"
	@echo "#           #    #  #    #    #  #   #  #   ##     #"
	@echo "#           #    ###     #    #  #   ####  #  #    #"
	@echo "#           #    # #     #    #  #   #  #  ####    #"
	@echo "#          ###   #  #   ###   ###    #  #  #  #    #"
	@echo "#                                                  #"
	@echo "####################################################"
	@echo ""
	@echo "make deploy PROJECT={predator|a50|..} BASE=..."
	@echo "make deploy PROJECT=predator BASE=/home/kudralex/projects/irida/"
	@echo ""
	@echo "CREATE ROLE irida_admin LOGIN CREATEUSER;"
	@echo "CREATE DATABASE irida_admin OWNER irida_admin;"
	@echo "\password irida_admin"
	@echo ""
	@echo "openssl dhparam -out dhparam.pem -2 1024 2> dh.log"
	@echo ""

.project:
	@echo -n "PROJECT?> "
	@read project; \
	echo $$project > .project
	@[ -n "`cat .project`" -a -d "deploy/projects/`cat .project`" ] || { echo -e "\tERROR: No such project, variants are:\n`ls deploy/projects`"; rm .project; exit 1; }
	@echo -e "\tYour saved project name in .project is: `cat .project`"

deploy: install
	make -C $(BASE) deploy-inplace

deploy-inplace: .project rights config db apache test

amievil: # yes, i am
	@[ `id -u` -ne 0 ] && { echo -e '\tERROR: You must be root'; exit 1; } || :
	

install: amievil
	#local config changing for dbdump, dbschema, etc
	make config
	#rsync -avP --exclude .git  ./ $(BASE)/
	mkdir -p $(BASE)/upload/previews/ ||:
	apt-get install -y --force-yes libav-tools
	apt-get install -y --force-yes php5
	apt-get install -y --force-yes php5-gd
	apt-get install -y --force-yes php5-pgsql
	apt-get install -y --force-yes php5-curl
	apt-get install -y --force-yes php5-ldap
	apt-get install -y --force-yes php5-fpm
	#apt-get install -y --force-yes php5-sockets
	@apt-get install -y --force-yes libgd2-xpm-dev || echo "libgd2-xpm-dev not found"
	apt-get install -y --force-yes imagemagick
	apt-get install -y --force-yes sendmail-bin
	apt-get install -y --force-yes sendmail
	apt-get install -y --force-yes libreoffice
	make rights


installphp7: amievil
	apt-get install -y --force-yes php7.0-pgsql
	apt-get install -y --force-yes php7.0-mbstring
	apt-get install -y --force-yes php7.0-gd
	apt-get install -y --force-yes php7.0-xml
	apt-get install -y --force-yes libav-tools
	apt-get install -y --force-yes imagemagick
	apt-get install -y --force-yes sendmail
	apt-get install -y --force-yes libreoffice


rights:
	chown apache2 upload -R || chown www-data upload -R
	mkdir -p application/cache/
	chown apache2 application/cache -R || chown www-data application/cache -R
	#chmod a+rwx upload
	#chgrp irida -R upload
	#chmod g+ws -R upload


config: 
	@echo "Making config directory..."
	@mkdir -p /etc/irida/irida.main/config
	@echo "Changing config directory rights..."
	@chown apache2 /etc/irida/irida.main -R || chown www-data /etc/irida/irida.main -R
	@rsync -avP application/default_config/ /etc/irida/irida.main/config/
	@chown apache2 /etc/irida/irida.main/config -R || chown www-data /etc/irida/irida.main/config -R
	@echo "Cpying projects configs..."
	@cp deploy/projects/$(PROJECT)/configs/app_config.php /etc/irida/irida.main/config/app_config.php
	@cp deploy/projects/$(PROJECT)/configs/config.php /etc/irida/irida.main/config/config.php
	@cp deploy/projects/$(PROJECT)/configs/database.php /etc/irida/irida.main/config/database.php
	@cp deploy/projects/$(PROJECT)/configs/filemanager/config.php filemanager/config/config.php
	@cp deploy/projects/$(PROJECT)/configs/.htaccess . || echo ".htaccess not found"
	@cp deploy/projects/$(PROJECT)/configs/index.php .
	@echo "Making log directory..."
	@mkdir -p /var/log/irida/irida.main/
	@echo "Changing log directory rights..."
	@chown apache2 /var/log/irida/irida.main -R || chown www-data /var/log/irida/irida.main -R
	@echo "RELOAD PHP7.0-fpm!!!"
	@service php7.0-fpm restart || echo "PHP7.0-fpm not found"
	@echo "Removing cache"
	@rm -rf application/cache/*


db: amievil deploy/roles.sql
	id $(DBUSER) || useradd -m -r $(DBUSER)

	sed -e 's/@@DBUSER@@/$(DBUSER)/g; s/@@DBPASS@@/$(DBPASS)/g; s/@@DBNAME@@/$(DBNAME)/g' deploy/roles.sql | \
		su - postgres -c psql


dbschema: amievil
	VER=`php -f deploy/getversion.php $(BASE)`; \
	cat deploy/$$VER/$(NAME).schema.pgdump | \
		su - postgres -c 'pg_restore -d $(DBNAME) -Fc -s -c -v'

	su - postgres -c 'LANG= psql -d $(DBNAME) -c "\dt" -P fieldsep=: -P format=unaligned' | grep table | cut -f 2 -d : | \
		while read table; \
		do \
			echo "ALTER TABLE $$table OWNER TO $(DBUSER);"; \
		done | \
	su - postgres -c 'psql -d $(DBNAME)'

dbdata: amievil
	VER=`php -f deploy/getversion.php $(BASE)`; \
	cat deploy/$$VER/$(NAME).data.pgdump | \
		su - postgres -c 'pg_restore -d $(DBNAME) -Fc -a -c -v'

dbconfig: 
	cat deploy/init.sql | \
		su - postgres -c 'psql $(DBNAME)'
	@VER=`php -f deploy/getversion.php $(BASE)`; \
	cat deploy/projects/$(PROJECT)/db/$$VER/$(NAME).configuration.pgdump | \
		su - postgres -c 'pg_restore -d $(DBNAME) -Fc -a -c -v'
	
dbdump: amievil 
	@echo "pg_dump -d irida_admin_clear -h localhost -U irida_admin -Fc -v > deploy/db_originals/irida_empty.pgdump"
	

dbrestore: amievil
	@echo "pg_restore -h localhost -d irida_admin -v -U irida_admin deploy/db_originals/irida_empty.pgdump"


onlyoffice-update:
	cp assets/images/irida.gif /var/www/onlyoffice/documentserver/web-apps/apps/documenteditor/main/resources/img/
	cp deploy/onlyoffice/documenteditor/main/app.js /var/www/onlyoffice/documentserver/web-apps/apps/documenteditor/main/
	cp deploy/onlyoffice/documenteditor/main/index.html /var/www/onlyoffice/documentserver/web-apps/apps/documenteditor/main/
	cp assets/images/irida.gif /var/www/onlyoffice/documentserver/web-apps/apps/presentationeditor/main/resources/img/
	cp deploy/onlyoffice/presentationeditor/main/app.js /var/www/onlyoffice/documentserver/web-apps/apps/presentationeditor/main/
	cp deploy/onlyoffice/presentationeditor/main/index.html /var/www/onlyoffice/documentserver/web-apps/apps/presentationeditor/main/
	cp assets/images/irida.gif /var/www/onlyoffice/documentserver/web-apps/apps/spreadsheeteditor/main/resources/img/
	cp deploy/onlyoffice/spreadsheeteditor/main/app.js /var/www/onlyoffice/documentserver/web-apps/apps/spreadsheeteditor/main/
	cp deploy/onlyoffice/spreadsheeteditor/main/index.html /var/www/onlyoffice/documentserver/web-apps/apps/spreadsheeteditor/main/        
