<?php require_once(APPPATH.'views/required/required_header.php'); ?>

<!-- Добавление тега -->
<div class="modal fade" id="addTagModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/tags/add_tag">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('tags_adding'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="inputAddTag"><?= $this->lang->line('tags_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('tags_name'); ?>" name="tag_name" value="" required="true" id="inputAddTag">
                    </div>

                    <div class="form-group">
                        <label for="input4"><?= $this->lang->line('tag_entity_type'); ?></label>
                        <select class="form-control" name="tag_entity_type">
                            <option value="user" selected><?= $this->lang->line('tag_entity_type_user'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_add'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Изменение тега -->
<div class="modal fade" id="updateTagInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/tags/update" autocomplete="false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('tag_editing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="tag_id" value="" id="inputEditTagID">
                    
                    <div class="form-group required">
                        <label for="inputEditTagName"><?= $this->lang->line('tags_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('tags_name'); ?>" name="tag_name" required="true" id="inputEditTagName">
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение удаления -->
<div class="modal fade" id="removeTagConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/tags/remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('tag_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="tag_id" value="" id="inputRemoveTagHiddenID">
                    <?= $this->lang->line('tag_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="row page-header">
    <div class="col-xs-12 col-sm-8">
        <h2 class=""><?= $this->lang->line('tags_menage'); ?></h2>
    </div>
    <div class="col-xs-12 col-sm-4 headerTools">
        <button type="button" class="btn btn-success btn-sm" id="addTagButtonID" onclick="$('#addTagModal').modal('show');">
            <span class="glyphicon glyphicon-plus"></span> <?= $this->lang->line('btn_add_tag') ?>
        </button>
    </div>
</div>


<table class="table table-striped sortedTable withFilter table-responsive">
    <thead>
        <tr>
            <th><?= $this->lang->line('tags_name'); ?></th>
            <th><?= $this->lang->line('tag_entity_type'); ?></th>
            <th><?= $this->lang->line('tag_use_count'); ?></th>
            <th class="dateLongTD"><?= $this->lang->line('tags_add_date'); ?></th>
            <th class="actionsTD noSort"><?= $this->lang->line('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($tags)) {
            foreach($tags as $tag) {
                ?>
                <tr>
                    <td><?= $tag['name'] ?></td>
                    <td class="centerAlign"><?= $tag['entityTypeTranslate'] ?></td>
                    <td class="centerAlign"><?= $tag['useCount'] ?></td>
                    <td class="centerAlign">
                        <span class='hiddenSpan'><?= $tag['ts'] ?></span>
                        <?= $tag['createDate'] ?>
                    </td>
                    <td class="centerAlign actionsTD">
                        <span class="btn btn-sm btn-primary" onclick="openEditModal('<?= $tag['name'] ?>',<?= $tag['id'] ?>)" title="<?= $this->lang->line('edit') ?>">
                            <span class="glyphicon glyphicon-edit"></span>
                        </span>
                        <span class="btn btn-sm btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" onclick="$('#inputRemoveTagHiddenID').val(<?= $tag['id'] ?>);$('#removeTagConfirmModal').modal('show');">
                            <span class="glyphicon glyphicon-remove"></span>
                        </span>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>
            <?php
        }
        ?>
    </tbody>
</table>


<script>
    
    function openEditModal(tag,tag_id) {
        
        $('#inputEditTagID').val(tag_id);
        $('#inputEditTagName').val(tag);
        $('#updateTagInfoModal').modal('show');
    }
    
</script>


<?php require_once(APPPATH.'views/required/required_footer.php'); ?>