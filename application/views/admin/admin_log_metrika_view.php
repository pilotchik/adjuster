<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeLogConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/log/remove_metrics">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('metrika_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('metrika_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
            window.onload = function () {
                
                var chart2 = new CanvasJS.Chart("chartContainerDates", {

                    title:{
                      text: "<?= $this->lang->line('metrika_days_allocation') ?>"              
                    },
                    data: [//array of dataSeries              
                        { //dataSeries object
                            /*** Change type "column" to "bar", "area", "line" or "pie"***/
                            type: "line",
                            dataPoints: [
                                 <?php foreach($metrik_dates as $metrika) { ?>
                                     { label: "<?= $metrika['date'] ?>",y: <?= $metrika['count'] ?>},
                                 <?php } ?>
                            ]
                        }
                    ]
                });

                chart2.options.title = {fontSize:12};

                chart2.render();



            }


            function renderChart3() {

                setTimeout(function() {
                    var chart3 = new CanvasJS.Chart("chartContainerMonths", {

                        title:{
                          text: "<?= $this->lang->line('metrika_months_allocation') ?>"              
                        },
                        data: [//array of dataSeries              
                            { //dataSeries object
                                /*** Change type "column" to "bar", "area", "line" or "pie"***/
                                type: "line",
                                dataPoints: [
                                     <?php foreach($metrik_months as $metrika) { ?>
                                         { label: "<?= $metrika['date'] ?>",y: <?= $metrika['count'] ?>},
                                     <?php } ?>
                                ]
                            }
                        ]
                    });

                    chart3.options.title = {fontSize:12};

                    chart3.render();
                },500);
            }
</script>



<div class="card users-table-card">
    <div class="header">
        <h4 class="title"><?= $this->lang->line('metrika_dates_allocation') ?></h4>
    </div>
    <div class="content" style="min-height: 470px;">

        <ul class="nav nav-tabs">
            <li <?= !isset($active_tab) ? 'class="active"' : '' ?>><a data-toggle="tab" href="#home"><?= $this->lang->line('metrika_days_allocation') ?></a></li>
            <li <?= isset($active_tab) && $active_tab == 'months' ? 'class="active"' : '' ?>><a data-toggle="tab" href="#months" onclick="renderChart3()"><?= $this->lang->line('metrika_months_allocation'); ?></a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade <?= !isset($active_tab) ? 'in active' : '' ?>">
                <div class="col-xs-12">
                    <div id="chartContainerDates" style="height: 400px; width: 100%;"></div>
                </div>
            </div>


            <div id="months" class="tab-pane fade <?= isset($active_tab) && $active_tab == 'months' ? 'in active' : '' ?>">
                <div class="col-xs-12">
                    <div id="chartContainerMonths" style="height: 400px; width: 100%;"></div>
                </div>
            </div>

        </div>
    
    </div>
</div>
        
<script src="<?= base_url() ?>assets/js/canvasjs.min.js"></script>
        
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>