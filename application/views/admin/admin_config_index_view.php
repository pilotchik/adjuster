<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>



<form class="form-horizontal" role="form" action="<?= base_url() ?>admin/config/update" method="POST">
    
    <ul class="nav nav-tabs">
        <?php foreach ($config as $section => $configs) { ?>
            <li <?= $section == 'main' ? 'class="active"' : '' ?>><a data-toggle="tab" href="#<?= $section ?>"><?= $this->lang->line('config_section_'.$section); ?></a></li>
        <?php } ?> 
    </ul>
    
    <div class="tab-content">
        
        <?php foreach ($config as $section => $configs) { ?>
        
            <div id="<?= $section ?>" class="tab-pane fade <?= $section == 'main' ? 'in active' : '' ?>">
                
                <div class="card users-table-card">
    
                    <div class="content">
                
                        <?php foreach($configs as $key=>$conf) { ?>
                            <div class="form-group <?= $conf['required'] ? 'required' : '' ?>">
                                <label for="input<?= $key ?>" class="col-sm-4 control-label"><?= $this->lang->line($conf['name']); ?></label>
                                <div class="col-sm-8">
                                    <?php if ($conf['type'] == 'text') { ?>
                                        <?php if ($conf['name'] != 'config_datetime_format') { ?>
                                            <input type="text" class="form-control" id="input<?= $key ?>" name="<?= $conf['name'] ?>" placeholder="<?= $this->lang->line($conf['name']); ?>" value="<?php echo htmlspecialchars($conf['value']) ?>" <?= $conf['required'] ? 'required="true"' : '' ?>>
                                        <?php } else { 
                                            if ($conf['name'] == 'config_datetime_format') {
                                                $variants = array(
                                                    'Y.m.d H:i' => 'ГГГГ.ММ.ДД ЧЧ:ММ',
                                                    'H:i d.m.Y' => 'ЧЧ:ММ ДД.ММ.ГГГГ',
                                                    'd.m.Y H:i' => 'ДД.ММ.ГГГГ ЧЧ:ММ'
                                                );    
                                            }
                                            ?>
                                            <select class="form-control initialWidth" name="<?= $conf['name'] ?>" id="input<?= $key ?>" <?= $conf['required'] ? 'required="true"' : '' ?> >
                                                <?php foreach($variants as $varkey => $varvalue) { ?>
                                                    <option value="<?= $varkey ?>" <?= $conf['value'] == $varkey ? 'selected' : '' ?>><?= $varvalue ?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } ?>
                                    <?php } elseif ($conf['type'] == 'boolean') { ?>
                                        <select class="form-control initialWidth" name="<?= $conf['name'] ?>" id="input<?= $key ?>" <?= $conf['required'] ? 'required="true"' : '' ?>>
                                            <option value="0" <?= !$conf['value'] ? 'selected' : '' ?>><?= $this->lang->line('boolean_no') ?></option>
                                            <option value="1" <?= $conf['value'] ? 'selected' : '' ?>><?= $this->lang->line('boolean_yes') ?></option>
                                        </select>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>    
                    
                </div>
            </div>
                    
        <?php } ?>
            
    </div>
    
    
    
    <div class="form-group" style="margin-top: 20px;">
        <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" class="btn btn-success"><?= $this->lang->line('btn_save'); ?></button>
        </div>
    </div>
</form>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>