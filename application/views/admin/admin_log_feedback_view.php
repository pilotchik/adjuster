<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeLogConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/log/remove_feedback">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('log_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('log_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="card users-table-card">
    
    <div class="content">

        <table class="table table-striped sortedTable withFilter table-responsive">
            <thead>
                <tr>
                    <th class="date-long-td">
                        <?= $this->lang->line('event_date'); ?>
                    </th>
                    <th><?= $this->lang->line('event_description'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($feedback_records) && count($feedback_records)) { ?>

                    <?php foreach($feedback_records as $event): ?>
                        <tr>
                            <td class="center-align date-inactive-status"><?= $event['date'] ?></td>
                            <td class="word-wrap-td"><?= htmlspecialchars($event['problem']) ?></td>
                        </tr>
                    <?php endforeach; ?>

                <?php } else { ?>

                    <tr><td colspan="100%" class="not-founded-data-td"><?= $this->lang->line('data_not_found'); ?></td></tr>

                <?php } ?>

            </tbody>
        </table>
        
    </div>
</div>

<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>