<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeLogConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/log/remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('log_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('log_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>






<div class="page-header">
    <div class="headerTools" style="display: inline-flex;text-align: right">
        <form action="<?= base_url() ?>admin/log" method="get" class="form-inline inlineHeaderToolsElement">

            <div class="form-group">
                <label class="sr-only"><?= $this->lang->line('log_filter_start'); ?></label>
                <div id="datetimepicker1">
                    <input type="text" class="form-control" placeholder="<?= $this->lang->line('log_filter_start'); ?>" />
                </div>
                <input type="hidden" name="begin" value="<?= $this->input->get('begin') ? $this->input->get('begin') : time() ?>" id="timestampID">
            </div>

            <div class="form-group">
                <label class="sr-only"><?= $this->lang->line('log_filter_end'); ?></label>
                <div id="datetimepicker2">
                    <input type="text" class="form-control" placeholder="<?= $this->lang->line('log_filter_end'); ?>" />
                </div>
                <input type="hidden" name="end" value="<?= $this->input->get('end') ? $this->input->get('end') : time() ?>" id="timestampID2">
            </div>
            <button type="submit" class="btn btn-primary"><?= $this->lang->line('btn_filter_apply'); ?></button>
        </form>

        <form action="<?= base_url() ?>admin/log" method="get" class="form-inline inlineHeaderToolsElement clear-filter-form">
            <button type="submit" class="btn btn-warning"><?= $this->lang->line('btn_filter_clear'); ?></button>
        </form>
        
    </div>
</div>



<div class="card users-table-card">
    
    <div class="content">

        <form action="<?= base_url() ?>admin/log/remove_selected" method="post" name="remove_selected_form">
            <div class="table-responsive">
                <table class="table table-striped sortedTable withFilter">
                    <thead>
                        <tr>
                            <th class="checkbox-td"></th>
                            <th class="date-long-td"><?= $this->lang->line('event_date'); ?></th>
                            <th class="mini-avatar-td"></th>
                            <th class="user-name-short-td"><?= $this->lang->line('user'); ?></th>
                            <th><?= $this->lang->line('event_title'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($events)) { ?>

                            <?php foreach($events as $event): ?>
                                <tr>
                                    <td class="center-align">
                                        <input type="checkbox" value="<?= $event['id'] ?>" name="events[]" <?= $this->input->get('begin') || $this->input->get('end') ? 'checked' : '' ?>>
                                    </td>
                                    <td class="center-align date-inactive-status"><?= $event['date'] ?></td>
                                    <td class="center-align miniAvatarTD">
                                        <?php if (!empty($event['user']) && isset($event['user']['id'])) { ?>
                                            <a class="photo photo_<?= $event['user']['id'] ?>" href="<?= $event['user']['avatarURL'] ?>" title="<?= $event['user']['fullName'] ?>">
                                                <img class="smallUserAvatarInTables" src="<?= $event['user']['avatarURL_small'] ?>">
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td><span title="<?= htmlspecialchars($event['userFullName']) ?>" class="wordWrapDiv"><?= htmlspecialchars($event['userName']) ?></td>
                                    <td class="word-wrap-td">
                                        <?= htmlspecialchars($event['typeDescription']) ?>
                                        <div class="user-subheader notify-subheader"><?= $event['status'] ?></div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        <?php } else { ?>

                            <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>

                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </form>
        
    </div>
    
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            language: 'ru',
            showClose: true,
            minuteStepping:10,
            defaultDate:'<?= date("d.m.Y H:i",$this->input->get('begin') ? $this->input->get('begin') : time()) ?>'
        });    
    });
    
    $("#datetimepicker1").on("dp.change", function (e) {
        $('#timestampID').val(e.date.format("X"));
        $('#datetimepicker2').data("DateTimePicker").setMinDate(e.date);
    });
    
    
    $(function () {
        $('#datetimepicker2').datetimepicker({
            language: 'ru',
            showClose: true,
            useCurrent: false,
            minuteStepping:10,
            defaultDate:'<?= date("d.m.Y H:i",$this->input->get('end') ? $this->input->get('end') : time()) ?>'
        });    
    });
    
    $("#datetimepicker2").on("dp.change", function (e) {
        $('#timestampID2').val(e.date.format("X"));
        $('#datetimepicker1').data("DateTimePicker").setMaxDate(e.date);
    });
    
</script>
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>