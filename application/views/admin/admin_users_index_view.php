<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение блокировки -->
<div class="modal fade" id="blockUserConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/block">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_blocking'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="" id="inputBlockUserHiddenID">
                    <?= $this->lang->line('user_blocking_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_confirm'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Окно сброса пароля -->
<div class="modal fade" id="updateUserPassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/reset_pass">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_password_reseting'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="" id="updateUserPassUserID">
                    <div class="form-group required">
                        <label for="updateUserPassName"><?= $this->lang->line('user'); ?></label>
                        <p class="form-control-static" id="updateUserPassName" class="wordWrapDiv">-</p>
                    </div>
                    <div class="form-group required">
                        <label for="input2"><?= $this->lang->line('user_new_pass'); ?></label>
                        <input type="password" class="form-control" id="input2" placeholder="<?= $this->lang->line('user_new_pass_input'); ?>" name="user_new_pass" required="true">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение удаления -->
<div class="modal fade" id="removeUserConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="" id="inputRemoveUserHiddenID">
                    <?= $this->lang->line('user_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger" id="btnButtonUserConfirmRemoveID"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="btnButtonUserCancelRemoveID"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Изменение профиля -->
<div class="modal fade" id="updateUserInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/update_user" id="updateUserFormID">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_editing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <ul class="nav nav-tabs">
                        <li <?= !isset($active_tab) ? 'class="active"' : '' ?>><a data-toggle="tab" href="#home"><?= $this->lang->line('user_info'); ?></a></li>
                        <li <?= isset($active_tab) && $active_tab == 'fields' ? 'class="active"' : '' ?>><a data-toggle="tab" href="#fields"><?= $this->lang->line('user_contacts'); ?></a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade <?= !isset($active_tab) ? 'in active' : '' ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="user_id" value="" id="inputEditUserID">

                                    <div class="form-group required">
                                        <label for="inputEditUserSurname"><?= $this->lang->line('user_surname'); ?></label>
                                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_surname'); ?>" name="surname" required="true" id="inputEditUserSurname">
                                    </div>

                                    <div class="form-group required">
                                        <label for="inputEditUserName"><?= $this->lang->line('user_name'); ?></label>
                                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_name'); ?>" name="name" required="true" id="inputEditUserName">
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEditUserMiddlename"><?= $this->lang->line('user_middlename'); ?></label>
                                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_middlename'); ?>" name="middlename" id="inputEditUserMiddlename">
                                    </div>
                                    
                                    <div class="form-group required">
                                        <label for="inputEditUserLogin"><?= $this->lang->line('user_login'); ?></label>
                                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_login'); ?>" name="login" id="inputEditUserLogin" required="true">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="inputEditUserRedirect"><?= $this->lang->line('user_redirect_url'); ?></label>
                                        <!--<input type="text" class="form-control" placeholder="<?= $this->lang->line('user_redirect_url'); ?>" name="redirect_url" id="inputEditUserRedirect">-->
                                        <select class="form-control" id="inputEditUserRedirect" name="redirect_url">
                                            
                                        </select>
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                        <label><?= $this->lang->line('user_birthday'); ?></label>
                                        <div id="datetimepicker1">
                                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_birthday'); ?>" />
                                            <input type="hidden" name="birthday" value="" id="timestampID">
                                        </div>
                                    </div>

                                    <div class="form-group userTypeFormGroup">
                                        <label for="input4"><?= $this->lang->line('user_type'); ?></label>
                                        <select class="form-control" name="type" id="inputEditUserType">
                                            <option value="admin" selected><?= $this->lang->line('user_type_admin'); ?></option>
                                            <option value="user"><?= $this->lang->line('user_type_user'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="inputEditUserComment"><?= $this->lang->line('user_comment'); ?></label>
                                        <textarea class="form-control" rows="3" name="comment" id="inputEditUserComment"></textarea>
                                    </div>
                                    
                                    <?php foreach($additionalFields['profile'] as $field): ?>
                                        <div class="form-group <?= $field['required'] ? 'required' : '' ?>">
                                            <label for="field<?= $field['id'] ?>ID"><?= htmlspecialchars($field['name']) ?></label>
                                            <?php switch ($field['type']) {
                                                case 'text': 
                                                    ?>
                                                    <input type="text" class="form-control" value="" name="field_<?= $field['id'] ?>" id="field<?= $field['id'] ?>ID" placeholder="<?= htmlspecialchars($field['name']) ?>" <?= $field['required'] ? 'required="true"' : '' ?>>
                                                    <?php break;
                                                case 'textarea':
                                                    ?>
                                                    <textarea class="form-control" name="field_<?= $field['id'] ?>" rows="3" id="field<?= $field['id'] ?>ID" <?= $field['required'] ? 'required="true"' : '' ?>></textarea>
                                            <?php break; } ?>

                                            <span class="help-block"><?= htmlspecialchars($field['description']) ?></span>
                                        </div>

                                    <?php endforeach; ?>
                                                                        
                                    <div class="form-group">
                                        <label for="inputEditUserTags"><?= $this->lang->line('user_tags'); ?></label>
                                        <div id="tagContainerID"></div>
                                        <input type="text" class="form-control" name="one_tag" id="inputEditUserTag" oninput="loadTags(this)">
                                        <div id='ajax_load_panel'><ul class='load_panel_list'></ul></div>
                                        <span class="help-block"><?= $this->lang->line('user_tags_input_help'); ?></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div id="fields" class="tab-pane fade <?= isset($active_tab) && $active_tab == 'fields' ? 'in active' : '' ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <label for="inputEditUserMail"><?= $this->lang->line('user_email'); ?></label>
                                        <input type="email" class="form-control" value="" name="email" id="inputEditUserMail" placeholder="<?= $this->lang->line('user_email'); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEditUserPhone"><?= $this->lang->line('user_phone'); ?></label>
                                        <input type="text" class="form-control" value="" name="phone" id="inputEditUserPhone" placeholder="<?= $this->lang->line('user_phone'); ?>">
                                    </div>
                                    
                                    <?php foreach($additionalFields['contacts'] as $field): ?>
                                        <div class="form-group <?= $field['required'] ? 'required' : '' ?>">
                                            <label for="field<?= $field['id'] ?>ID"><?= $field['name'] ?></label>
                                            <?php switch ($field['type']) {
                                                case 'text': 
                                                    ?>
                                                    <input type="text" class="form-control" value="" name="field_<?= $field['id'] ?>" id="field<?= $field['id'] ?>ID" placeholder="<?= $field['name'] ?>" <?= $field['required'] ? 'required="true"' : '' ?>>
                                                    <?php break;
                                                case 'textarea':
                                                    ?>
                                                    <textarea class="form-control" name="field_<?= $field['id'] ?>" rows="3" id="field<?= $field['id'] ?>ID"></textarea>
                                            <?php break; } ?>

                                            <span class="help-block"><?= $field['description'] ?></span>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php if ($this->config->item('config_use_ldap') && isset($ldap_users) && count($ldap_users) && ($this->session->userdata['login'] == 'admin' || $this->session->userdata('role') == 'superuser')) { ?>
    <!-- Добавление пользователей LDAP -->
    <div class="modal fade" id="addLDAPUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" method="POST" action="<?= base_url() ?>admin/users/add_ldap_user">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <h4 class="modal-title"><?= $this->lang->line('user_ldap_adding'); ?></h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="inputAddUserType"><?= $this->lang->line('user_ldap'); ?></label>
                            <select class="form-control" name="ldap_login" id="inputAddUserLogin" onChange="changeLDAPForm()">
                                <?php foreach($ldap_users as $ldap_user) { ?>
                                    <option value="<?= $ldap_user['login'] ?>"><?= $ldap_user['login'] ?>: <?= $ldap_user['fullname'] ?> (<?= $ldap_user['group'] ?>)</option>
                                <?php } ?>
                            </select>
                            
                            <?php foreach($ldap_users as $ldap_user) { ?>
                                <input type="hidden" id="user_<?= $ldap_user['login'] ?>" name="<?= $ldap_user['name'] ?>" surname="<?= $ldap_user['surname'] ?>" middlename="<?= $ldap_user['middlename'] ?>">
                            <?php } ?>
                        </div>
                        
                        <div class="form-group required">
                            <label for="inputAddUserSurname"><?= $this->lang->line('user_surname'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_surname'); ?>" name="surname" value="<?= $default_user['surname'] ?>" required="true" id="inputAddUserSurname">
                        </div>

                        <div class="form-group required">
                            <label for="inputAddUserName"><?= $this->lang->line('user_name'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_name'); ?>" name="name" value="<?= $default_user['name'] ?>" required="true" id="inputAddUserName">
                        </div>

                        <div class="form-group">
                            <label for="inputAddUserMiddlename"><?= $this->lang->line('user_middlename'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_middlename'); ?>" name="middlename" value="<?= $default_user['middlename'] ?>" id="inputAddUserMiddlename">
                        </div>
                        
                        <div class="form-group">
                            <label for="inputAddUserComment"><?= $this->lang->line('user_comment'); ?></label>
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="inputAddUserType"><?= $this->lang->line('user_type'); ?></label>
                            <select class="form-control" name="type" id="inputAddUserType">
                                <option value="admin"><?= $this->lang->line('user_type_admin'); ?></option>
                                <option value="user" selected><?= $this->lang->line('user_type_user'); ?></option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_add'); ?></button>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <script>
    
        function changeLDAPForm() {
            changedLogin = $('#inputAddUserLogin').val()
            
            console.log($('#user_'+changedLogin).attr('surname'));
            $('#inputAddUserSurname').val($('#user_'+changedLogin).attr('surname'));
            $('#inputAddUserName').val($('#user_'+changedLogin).attr('name'));
            $('#inputAddUserMiddlename').val($('#user_'+changedLogin).attr('middlename'));
        }
    
    </script>

<?php } elseif ($this->session->userdata['login'] == 'admin' || $this->session->userdata('role') == 'superuser') { ?>
    
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" method="POST" action="<?= base_url() ?>admin/users/add_simple_user">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <h4 class="modal-title"><?= $this->lang->line('user_adding'); ?></h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group required">
                            <label for="inputAddUserSurname"><?= $this->lang->line('user_surname'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_surname'); ?>" name="surname" required="true" id="inputAddUserSurname">
                        </div>

                        <div class="form-group required">
                            <label for="inputAddUserName"><?= $this->lang->line('user_name'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_name'); ?>" name="name" required="true" id="inputAddUserName">
                        </div>

                        <div class="form-group">
                            <label for="inputAddUserMiddlename"><?= $this->lang->line('user_middlename'); ?></label>
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_middlename'); ?>" name="middlename" id="inputAddUserMiddlename">
                        </div>
                        
                        <div class="form-group">
                            <label for="inputAddUserComment"><?= $this->lang->line('user_comment'); ?></label>
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="inputAddUserType"><?= $this->lang->line('user_type'); ?></label>
                            <select class="form-control" name="type" id="inputAddUserType">
                                <option value="admin"><?= $this->lang->line('user_type_admin'); ?></option>
                                <option value="user" selected><?= $this->lang->line('user_type_user'); ?></option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success" id="btnButtonUserConfirmAddID"><?= $this->lang->line('btn_add'); ?></button>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="btnButtonUserCancelAddID"><?= $this->lang->line('btn_cancel'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
<?php }  ?>

    



<div class="card users-table-card">
    
    <div class="content">

        <table class="table table-striped sortedTable withFilter table-responsive">
            <thead>
                <tr>
                    <th class="mini-avatar-td"></th>
                    <th class="user-name-long-td"><?= $this->lang->line('user'); ?></th>
                    <?php if (($this->session->userdata['login'] == 'admin'  || $this->session->userdata('role') == 'superuser') && !$this->config->item('config_use_ldap')) {?>
                        <th><?= $this->lang->line('user_default_pass'); ?></th>
                    <?php } else { ?>
                        <th><?= $this->lang->line('user_tags'); ?></th>
                    <?php } ?>
                    <?php if ($this->session->userdata['login'] != 'admin' && $this->session->userdata('role') != 'superuser') {?>
                        <th class="date-long-td"><?= $this->lang->line('user_activity_date'); ?></th>
                    <?php } ?>
                    <th style="width:100px;">Группа</th>
                    <th class="wide-actions-td noSort"><?= $this->lang->line('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($users)) { ?>
                    <?php foreach($users as $user) { ?>
                        <tr>
                            <td class="center-align">
                                <a class="photo photo_<?= $user['id'] ?>" href="<?= $user['avatarURL'] ?>" title="<?= $user['fullName'] ?>">
                                    <img class="smallUserAvatarInTables" src="<?= $user['avatarURL_small'] ?>">
                                </a>
                            </td>

                            <td id="username_<?= $user['id'] ?>" class="word-wrap-td">
                                <?= htmlspecialchars($user['fullName']) ?> (<?= $this->config->item('config_use_ldap') ? $user['ldap_login'] : $user['login'] ?>) <?= $user['birthday_status'] ? '&nbsp;<i class="fa fa-glass" title="'.$this->lang->line('user_birthday_today').'"></i>' : '' ?>
                                
                                <?php if ($user['user_type'] == 'admin') { ?>
                                    <i class="fa  fa-user-secret" title="<?= $this->lang->line('user_type_admin') ?>"></i>
                                <?php } ?>
                                
                                <?php if($user['comment'] != '') { ?>
                                    <div class="user-subheader notify-subheader"><?= htmlspecialchars($user['comment']) ?></div>
                                <?php } ?>

                            </td>

                            <?php if (($this->session->userdata['login'] == 'admin' || $this->session->userdata('role') == 'superuser')  && !$this->config->item('config_use_ldap')) {?>
                                <td class="center-align"><?= $user['ldap_login'] ?></td>
                            <?php } else { ?>
                                <td><?= $user['tags'] ?></td>
                            <?php } ?>

                            <?php if ($this->session->userdata['login'] != 'admin' &&  $this->session->userdata('role') != 'superuser') {?>
                                <td class="center-align"><span style="display: none;"><?= $user['last_heartbeat'] ?></span><?= $user['last_date'] ?></td>
                            <?php } ?>

                            <td class="center-align"><?= $user['group'] ?></td>

                            <td class="center-align actions-td">
                                <?php if ($user['user_type'] != 'admin' || $this->session->userdata['userID'] == $user['id'] || $this->session->userdata['login'] == 'admin' || $this->session->userdata('role') == 'superuser') {?>
                                    <span class="btn btn-xs btn-primary" onclick="openEditModal(<?= $user['id'] ?>,'<?= htmlspecialchars($user['name']) ?>','<?= htmlspecialchars($user['surname']) ?>','<?= htmlspecialchars($user['middlename']) ?>','<?= $user['user_type'] ?>','<?= htmlspecialchars($user['comment']) ?>')" title="<?= $this->lang->line('edit') ?>">
                                        <i class="fa fa-edit"></i>
                                    </span>
                                <?php } ?>

                                <?php if ($user['active'] && ($this->session->userdata['userID'] != $user['id'] && $user['user_type'] != 'admin' || $this->session->userdata['login'] == 'admin')) {?>
                                    <span class="btn btn-xs btn-warning" title="<?= $this->lang->line('actions_block'); ?>" onclick="$('#inputBlockUserHiddenID').val(<?= $user['id'] ?>);$('#blockUserConfirmModal').modal('show');">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                <?php } elseif (!$user['active'] && ($this->session->userdata['userID'] != $user['id'] && $user['user_type'] != 'admin' || $this->session->userdata['login'] == 'admin')) { ?>
                                    <a href="<?= base_url() ?>admin/users/unblock/<?= $user['id'] ?>" class="btn btn-xs btn-success" title="<?= $this->lang->line('actions_unblock'); ?>" >
                                        <i class="fa fa-unlock"></i>
                                    </a>
                                <?php } ?>

                                <?php if (!$this->config->item('config_use_ldap') && ($this->session->userdata['login'] == 'admin'  || $this->session->userdata('role') == 'superuser')) {?>
                                    <span class="btn btn-xs btn-warning" onclick="openEditPassModal(<?= $user['id'] ?>)" title="<?= $this->lang->line('user_reset_pass') ?>">
                                        <i class="fa fa-key"></i>
                                    </span>
                                <?php } ?>
                                <?php if ($this->session->userdata['userID'] != $user['id'] && !$this->config->item('config_use_ldap') && ($this->session->userdata['login'] == 'admin'  || $this->session->userdata('role') == 'superuser')) {?>
                                    <span class="btn btn-xs btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" <?= $this->session->userdata['userID'] == $user['id'] ? 'disabled' : '' ?> onclick="$('#inputRemoveUserHiddenID').val(<?= $user['id'] ?>);$('#removeUserConfirmModal').modal('show');" name="<?= htmlspecialchars($user['fullName']) ?>">
                                        <i class="fa fa-remove"></i>
                                    </span>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                        
                <?php } else { ?>

                    <tr><td colspan="100%" class="not-founded-data-td"><?= $this->lang->line('data_not_found'); ?></td></tr>

                <?php } ?>
            </tbody>
        </table>
        
    </div>
    
</div>

<script>
    
    function openEditModal(user_id,name,surname,middlename,type,comment) {
        
        $('#inputEditUserID').val(user_id);
            
        
        $('#inputEditUserSurname').val(surname);
        $('#inputEditUserName').val(name);
        $('#inputEditUserMiddlename').val(middlename);
        $('#inputEditUserComment').html(comment)
        $('#inputEditUserMail').val('');
        $('#inputEditUserPhone').val('');
        $('#inputEditUserRedirect').val('');
        $('#inputEditUserRedirect').html('');
        
        $('#inputEditUserType option[value='+type+']').attr('selected',true);
        
        console.log(user_id);
        if (user_id == <?= $this->session->userdata['userID'] ?>) {
            $('#inputEditUserType').prop('disabled',true);
            $('.userTypeFormGroup').css('display','none');
        } else {
            $('#inputEditUserType').prop('disabled',false);
            $('.userTypeFormGroup').css('display','');
        }
        
        $('#tagContainerID').html('');
        //Получить теги пользователя
        jQuery.ajax({
            type: 'get',
            async: true,
            data: {userID:user_id},
            url: '<?= base_url() ?>ajax/getUsersTags',
            success: function(msg) {
                result = JSON.parse(msg);
                console.log(result);
                result.tags.forEach(function(el){
                    addTag(el.name,'tagContainerID');
                });
                
                result.fields.profile.forEach(function(el){
                    $('#field'+el.id+'ID').val(el.value);
                });
                
                result.fields.contacts.forEach(function(el){
                    $('#field'+el.id+'ID').val(el.value);
                });
                
                $('#inputEditUserMail').val(result.info.email);
                $('#inputEditUserPhone').val(result.info.phone);
                
                $('#inputEditUserLogin').val(result.info.login);
                
                a = new Date(parseInt(result.info.birthday) * 1000);
                $('#datetimepicker1').data("DateTimePicker").setValue(a);
                
                $('#timestampID').val(result.info.birthday);
                
                
                $('#inputEditUserRedirect').append('<option value="" selected><?= $this->lang->line('user_has_no_redirect') ?></option>');
                
                $.each(result.rooms,function(numb,el){
                    $('#inputEditUserRedirect').append('<option value="'+el.id+'">'+el.name+'</option>');
                });
                
                $('#inputEditUserRedirect').val(result.info.redirect_url);
                
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });
        
        $('#updateUserInfoModal').modal('show');
    }
    
    
    function loadTags(elem,tagContainer) {
        
        input_text = $(elem).val()
        re = /\s*,\s*/
        taglist = input_text.split(re);
        tag = taglist[taglist.length - 1];
        tag = tag.trim();
        $('#ajax_load_panel').css({'display':'none'});

        if (tag != "" && tag.length != 0) {
            
            search = getContextSearch('tags','name','entity_type','user',tag,10);

        }
           
    }
    
    
    function openEditPassModal(userID) {
        
        $('#updateUserPassUserID').val(userID);
        $('#updateUserPassName').html($('#username_'+userID).html());
        
        $('#updateUserPassModal').modal('show');
    }
    
    
    function addTag(value,tagContainer) {
        
        if ($.trim(value) != '') {
            code = '<span class="oneTag">'+$.trim(value)+'&nbsp;<i class="fa fa-remove" onClick="removeOneTag(this)"></i><input type="hidden" name="tags[]" value="'+$.trim(value)+'"></span>';
            $('#'+tagContainer).append(code);

            $('#ajax_load_panel').css({'display':'none'});
            $('#inputEditUserTag').val('');
            $('#inputEditUserTag').focus();
        }
    }
    
    
    function removeOneTag(el) {
        $(el).parent().remove();
    }
    
    /**
     * Получение значение из разрешённых таблиц 
     * 
     * @param table - название таблицы
     * @param col - название колонки
     * @param addCol - название дополнительной колонки фильтрации
     * @param addColValue - значение дополнительной колонки фильтрации
     * @param item - часть искомого значения
     * @param limit - ограничение
     * 
     * @returns {result|Array|Object}
     */
    function getContextSearch(table,col,addCol,addColValue,item,limit) {

        result = new Object;

        jQuery.ajax({
            type: 'post',
            async: true,
            data: {tableName:table,colName:col,item:item,addCol:addCol,addColValue:addColValue,limit:limit},
            url: '<?= base_url() ?>ajax/getContextSearch',
            success: function(msg) {
                result = JSON.parse(msg);

                if (result.items.length > 0) {

                    $('#ajax_load_panel').css({'display':'block'})

                    $('.load_panel_list').html('');
                    result.items.forEach(function(el){
                        $('.load_panel_list').append('<li onclick="addTag(\''+el.name+'\',\'tagContainerID\')">'+el.name+'</li>');
                    });
                } else {
                    $('#ajax_load_panel').css({'display':'none'});
                }
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });

        //return result;

    }
    
    
    jQuery(document).ready(function() {
    
        $('.photo').colorbox({rel:'photo'});
                                
    
        $("[data-toggle=popover]").popover();
        
        $('#updateUserFormID').on('keyup keypress',function(e){
            var code = e.keyCode || e.which;
            if(code == 13) {
                e.preventDefault();
                return false;
            }
        });
        
        
        $('#inputEditUserTag').on('keypress',function(e){
            
            active_numb = -1;
            $('.load_panel_list li').each(function(numb,el){
                if ($(el).hasClass('active')) active_numb = numb;
            })
            
            if (e.keyCode == 13 || e.which == 44) {
                
                if (active_numb < 0) {
                    
                    $('body').append('<div class="hiddenSpan" id="hiddenTagCleaner">'+$('#inputEditUserTag').val()+'</div>');
                    
                    addTag($('#hiddenTagCleaner').text(),'tagContainerID');
                    $('#hiddenTagCleaner').remove();
                } else {
                    addTag($('.load_panel_list li:nth-child('+parseInt(active_numb+1)+')').html(),'tagContainerID');
                    $('.load_panel_list').html('');
                }
        
                $('#inputEditUserTag').val('');
            }
            
            
                
            if (e.keyCode == 40) {
                $('.load_panel_list li:nth-child('+parseInt(active_numb+1)+')').removeClass('active');
                $('.load_panel_list li:nth-child('+parseInt(active_numb+2)+')').addClass('active');
            } else if(e.keyCode == 38) {
                $('.load_panel_list li:nth-child('+parseInt(active_numb+1)+')').removeClass('active');
                $('.load_panel_list li:nth-child('+parseInt(active_numb)+')').addClass('active');
            }
            
        });
        
        
    })
    
    
    $(function () {
        $('#datetimepicker1').datetimepicker({
            language: 'ru',
            showClose: true,
            viewMode: 'years',
            pickTime: false,
            disabledHours:true,
            format: 'DD.MM.YYYY',
            defaultDate:'<?= date("d.m.Y H:i",time()) ?>'
        });    
    });
    
    
    $("#datetimepicker1").on("dp.change", function (e) {
        $('#timestampID').val(e.date.format("X"));
    });
    
</script>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>