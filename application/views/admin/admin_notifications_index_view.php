<?php require_once(APPPATH.'views/required/required_header.php'); ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeNotificationsConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/notifications/remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('notification_notifications_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('notification_notifications_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>








<div class="row page-header">
    <div class="col-xs-12 col-sm-8">
        <h2 class=""><?= $this->lang->line('notification_department'); ?></h2>
    </div>
    <div class="col-xs-12 col-sm-4 headerTools">
        <?php if (count($notifications)) { ?>
            <button type="button" class="btn btn-danger" onclick="$('#removeNotificationsConfirmModal').modal('show');">
                <?= $this->lang->line('btn_clear_notifications') ?>
            </button>
        <?php } ?>
    </div>
</div>


<div class="table-responsive">
    <table class="table table-striped sortedTable withFilter">
        <thead>
            <tr>
                <th class="dateLongTD"><?= $this->lang->line('notification_date'); ?></th>
                <th class="dateLongTD"><?= $this->lang->line('notification_type'); ?></th>
                <th><?= $this->lang->line('notification_status'); ?></th>
                <th class="userNameShortTD"><?= $this->lang->line('notification_author'); ?></th>
                <th class="userNameShortTD"><?= $this->lang->line('notification_recipient'); ?></th>
                <th><?= $this->lang->line('notification_entity_type'); ?></th>
                <th><?= $this->lang->line('notification_entity_type_desc'); ?></th>
                <th><?= $this->lang->line('notification_data'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($notifications)) {
                foreach($notifications as $notify): ?>
                    <tr class="<?= $notify['status'] == 1 ? 'success' : '' ?>">
                        <td class="centerAlign">
                            <span class="hiddenSpan"><?= $notify['ts'] ?></span>
                            <?= $notify['date'] ?>
                        </td>
                        <td class="centerAlign"><?= $this->lang->line('user_settings_'.$notify['type_of_notification']) ?></td>
                        <td class="centerAlign"><?= $notify['status'] ? $this->lang->line('notification_status_send') : $this->lang->line('notification_status_not_send') ?></td>
                        <td><span title="<?= $notify['authorUserFullName'] ?>"><?= $notify['authorUserName'] ?></td>
                        <td><span title="<?= $notify['recipientUserFullName'] ?>"><?= $notify['recipientUserName'] ?></td>
                        <td class="centerAlign"><?= $notify['type'] ?></td>
                        <td class="centerAlign"><?= $notify['typeObject'] ?></td>
                        <td>
                            <?= $notify['subject'] ?>
                            <span tabindex="0" clss="" data-toggle="popover" data-trigger="focus" title="<?= $this->lang->line('notification_data_text'); ?>" data-placement="left" data-content="<?= htmlspecialchars($notify['body']) ?>">
                                <span class="glyphicon glyphicon-comment" title="<?= $this->lang->line('notification_data_text'); ?>"></span>
                            </span>
                        </td>
                    </tr>
                <?php endforeach;
            } else { ?>
                <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>
            <?php } ?>
        </tbody>
    </table>
    
</div>


<script>
    
    jQuery(document).ready(function() {
    
        $("[data-toggle=popover]").popover();
    });

</script>


<?php require_once(APPPATH.'views/required/required_footer.php'); ?>