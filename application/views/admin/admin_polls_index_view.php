<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение модерации -->
<div class="modal fade" id="removeLogConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/polls/moderate">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('log_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('log_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class="card users-table-card">
    
    <div class="content">

        <table class="table table-striped sortedTable withFilter table-responsive">
            <thead>
                <tr>
                    <th><?= $this->lang->line('poll_title'); ?></th>
                    <th><?= $this->lang->line('poll_author'); ?></th>
                    <th><?= $this->lang->line('poll_anon_status'); ?></th>
                    <th><?= $this->lang->line('poll_quests_count'); ?></th>
                    <th><?= $this->lang->line('poll_moderator_name'); ?></th>
                    <th class="actions-td noSort"><?= $this->lang->line('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($public_polls)) { ?>
                    <?php foreach($public_polls as $poll) { ?>
                        <tr>
                            <td>
                                <span id="title_<?= $poll['id'] ?>"><?= $poll['title'] ?></span> 
                                <?php if (htmlspecialchars($poll['description']) != '') { ?>
                                    <div class="user-subheader notify-subheader"><?= htmlspecialchars($poll['description']) ?></div>
                                <?php } ?>
                                <span id="description_<?= $poll['id'] ?>" style="display:none;"><?= htmlspecialchars($poll['description']) ?></span>
                            </td>
                            <td>
                                <?= $poll['creatorName'] ?>
                            </td>
                            <td class="center-align">
                                <span id="anon_<?= $poll['id'] ?>" style="display:none;"><?= $poll['anon_status'] ?></span>
                                <?= $poll['anon_status'] ? $this->lang->line('poll_anon_true') : $this->lang->line('poll_anon_false') ?>
                            </td>
                            <td class="center-align" id="questscount_<?= $poll['id'] ?>"><?= $poll['questsCount'] ?></td>
                            <td class="center-align"><?= $poll['moderatorName'] != '' ? $poll['moderatorName'] : '<span class="glyphicon glyphicon-minus"></span>'  ?></td>
                            <td class="center-align actions-td">
                                <button class="btn btn-xs" onclick="$('#questModal_<?= $poll['id'] ?>').modal('show');" title="<?= $this->lang->line('poll_quests'); ?>" style="margin-right:10px;">
                                    <i class="fa fa-list"></i>
                                </button>

                                <div class="modal fade" id="questModal_<?= $poll['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                                                <h4 class="modal-title"><?= $this->lang->line('poll_quests'); ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <?php foreach($poll['quests'] as $quest) { ?>

                                                    <div class="panel panel-success" id="questPanel_<?= $quest['id'] ?>">
                                                        <div class="panel-heading questPanel">
                                                            <?= $quest['title'] ?> <?= $quest['required'] ? '&nbsp;<span class="glyphicon glyphicon-asterisk requiredQuest" title="'.$this->lang->line('poll_quest_required_true').'"></span>' : '' ?>
                                                        </div>

                                                        <div class="panel-body">
                                                            <form class="form-horizontal" role="form">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label"><?= $this->lang->line('poll_quest_type'); ?></label>
                                                                    <div class="col-sm-8">
                                                                        <p class="form-control-static"><?= $this->lang->line('poll_quest_type_'.$quest['type']); ?></p>
                                                                    </div>
                                                                </div>

                                                                <?php if ($quest['description']) { ?>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label"><?= $this->lang->line('poll_quest_description'); ?></label>
                                                                        <div class="col-sm-8">
                                                                            <p class="form-control-static"><?= $quest['description'] ?></p>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>

                                                                <?php if ($quest['type'] == 'selectscale') { ?>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label"><?= $this->lang->line('poll_quest_scale_begin'); ?></label>
                                                                        <div class="col-sm-8">
                                                                            <p class="form-control-static"><?= $quest['vars'][0] ?></p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label"><?= $this->lang->line('poll_quest_scale_end'); ?></label>
                                                                        <div class="col-sm-8">
                                                                            <p class="form-control-static"><?= $quest['vars'][1] ?></p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label"><?= $this->lang->line('poll_quest_scale_count'); ?></label>
                                                                        <div class="col-sm-8">
                                                                            <p class="form-control-static"><?= $quest['vars'][2] ?></p>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </form>
                                                        </div>

                                                        <?php if ($quest['type'] == 'selectone' || $quest['type'] == 'selectmany') { ?>
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th><?= $this->lang->line('poll_quest_vars'); ?></td>
                                                                    </tr>
                                                                </thead>
                                                                <?php foreach($quest['vars'] as $varkey => $var) { ?>
                                                                    <tr>
                                                                        <td><?= $var ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </table>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if (!$poll['moderator_id']) { ?>    
                                    <a class="btn btn-xs btn-success" href="<?= base_url() ?>admin/polls/moderate/<?= $poll['id'] ?>/1" title="<?= $this->lang->line('poll_moderate_ok') ?>">
                                        <i class="fa fa-check"></i>
                                    </a>
                                <?php } else { ?>

                                    <a class="btn btn-xs btn-danger" href="<?= base_url() ?>admin/polls/moderate/<?= $poll['id'] ?>/0" title="<?= $this->lang->line('poll_moderate_block'); ?>">
                                        <i class="fa fa-ban"></i>
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('polls_of_type_not_found'); ?></td></tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
    
</div>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>