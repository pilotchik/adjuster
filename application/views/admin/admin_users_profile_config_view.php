<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Добавление поля -->
<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/profile_add_field">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('profile_field_adding'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="inputAddItemName"><?= $this->lang->line('profile_field_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('profile_field_name'); ?>" name="field_name" value="" required="true" id="inputAddItemName">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddItemDesc"><?= $this->lang->line('profile_field_description'); ?></label>
                        <textarea class="form-control" rows="3" name="field_description" id="inputAddItemDesc"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddItemType"><?= $this->lang->line('profile_field_type'); ?></label>
                        <select class="form-control" name="field_type" id="inputAddItemType">
                            <option value="text" selected><?= $this->lang->line('profile_field_type_text'); ?></option>
                            <option value="textarea"><?= $this->lang->line('profile_field_type_textarea'); ?></option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddItemInfoType"><?= $this->lang->line('profile_field_info_type'); ?></label>
                        <select class="form-control" name="field_info_type" id="inputAddItemInfoType">
                            <option value="profile" selected><?= $this->lang->line('profile_field_info_type_profile'); ?></option>
                            <option value="contacts"><?= $this->lang->line('profile_field_info_type_contacts'); ?></option>
                        </select>
                    </div>
                    
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="field_admin_changing" value="1">  <?= $this->lang->line('profile_field_can_admin_change'); ?>
                        </label>
                    </div>
                    
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="field_required" value="1">  <?= $this->lang->line('profile_field_is_required'); ?>
                        </label>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_add'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editProfileFieldModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/profile_edit_field" autocomplete="false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('profile_field_editing'); ?></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="field_id" value="" class="profileFieldIDs">

                    <div class="form-group required">
                        <label for="inputUpdateProfileFieldNameID"><?= $this->lang->line('profile_field_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('profile_field_name'); ?>" name="field_name" value="" required="true" id="inputUpdateProfileFieldNameID">
                    </div>

                    <div class="form-group">
                        <label for="inputUpdateProfileFieldDescriptionID"><?= $this->lang->line('profile_field_description'); ?></label>
                        <textarea class="form-control" rows="3" name="field_description" id="inputUpdateProfileFieldDescriptionID"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="inputUpdateProfileFieldTypeID"><?= $this->lang->line('profile_field_type'); ?></label>
                        <select class="form-control" name="field_type" id="inputUpdateProfileFieldTypeID">
                            <option value="text"><?= $this->lang->line('profile_field_type_text'); ?></option>
                            <option value="textarea"><?= $this->lang->line('profile_field_type_textarea'); ?></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="inputUpdateProfileFieldInfoTypeID"><?= $this->lang->line('profile_field_info_type'); ?></label>
                        <select class="form-control" name="field_info_type" id="inputUpdateProfileFieldInfoTypeID">
                            <option value="profile"><?= $this->lang->line('profile_field_info_type_profile'); ?></option>
                            <option value="contacts"><?= $this->lang->line('profile_field_info_type_contacts'); ?></option>
                        </select>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="field_admin_changing" id="inputUpdateProfileFieldAdminChangingID" value="1">  <?= $this->lang->line('profile_field_can_admin_change'); ?>
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="field_required" id="inputUpdateProfileFieldRequiredID" value="1">  <?= $this->lang->line('profile_field_is_required'); ?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="inputUpdateProfileFieldInfoStatusID"><?= $this->lang->line('profile_field_status'); ?></label>
                        <select class="form-control" name="field_status" id="inputUpdateProfileFieldInfoStatusID">
                            <option value="0"><?= $this->lang->line('profile_field_is_not_in_use'); ?></option>
                            <option value="1"><?= $this->lang->line('profile_field_is_in_use'); ?></option>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="removeProfileFieldModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>admin/users/profile_remove_field">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('profile_field_removing'); ?></h4>
                </div>
                <div class="modal-body wordWrapDiv">
                    <input type="hidden" name="field_id" value="" class="profileFieldIDs">
                    
                    <?= $this->lang->line('profile_field_removing_confirm'); ?> "<span id="inputRemoveProfileFieldNameID"></span>"?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>






<div class="card users-table-card">
    <div class="content">

        <table class="table table-striped sortedTable withFilter table-responsive">
            <thead>
                <tr>
                    <th><?= $this->lang->line('profile_field_name'); ?></th>
                    <th style="width:150px;"><?= $this->lang->line('profile_field_type'); ?></th>
                    <th style="width:150px;"><?= $this->lang->line('profile_field_info_type'); ?></th>
                    <th style="width:50px;">
                        <span class="glyphicon glyphicon-briefcase" title="<?= $this->lang->line('profile_field_admin_changing_status'); ?>"></span>
                    </th>
                    <th style="width:50px;">
                        <span class="glyphicon glyphicon-certificate" title="<?= $this->lang->line('profile_field_requiring'); ?>"></span>
                    </th>
                    <th style="width:50px;">
                        <span class="glyphicon glyphicon-off" title="<?= $this->lang->line('profile_field_status'); ?>"></span>
                    </th>
                    <th style="width:50px;">
                        <span class="glyphicon glyphicon-user" title="<?= $this->lang->line('profile_using_count'); ?>"></span>
                    </th>
                    <th class="actions-td noSort"><?= $this->lang->line('actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($fields)) { ?>
                    <?php foreach($fields as $field) { ?>
                        <tr>
                            <td class="wordWrapTD">

                                <span id="profile_field_name_<?= $field['id'] ?>"><?= htmlspecialchars($field['name']) ?></span>
                                <span class="hidden-span" id="profile_field_desc_<?= $field['id'] ?>"><?= htmlspecialchars($field['description']) ?></span>
                                <span class="hidden-span" id="profile_field_type_<?= $field['id'] ?>"><?= $field['type'] ?></span>
                                <span class="hidden-span" id="profile_field_info_type_<?= $field['id'] ?>"><?= $field['info_type'] ?></span>
                                <span class="hidden-span" id="profile_field_info_status_<?= $field['id'] ?>"><?= $field['status'] ?></span>
                                <span class="hidden-span" id="profile_field_admin_changing_<?= $field['id'] ?>"><?= $field['admin_changing'] ?></span>
                                <span class="hidden-span" id="profile_field_required_<?= $field['id'] ?>"><?= $field['required'] ?></span>
                                
                                
                                <?php if ($field['description'] != '') { ?>
                                    <div class="user-subheader notify-subheader"><?= htmlspecialchars($field['description']) ?></div>
                                <?php } ?>
                            </td>
                            <td class="center-align">
                                <?= $field['type'] == 'text' ? $this->lang->line('profile_field_type_text') : $this->lang->line('profile_field_type_textarea') ?>
                            </td>
                            <td class="center-align">
                                <?= $field['info_type'] == 'profile' ? $this->lang->line('profile_field_info_type_profile') : $this->lang->line('profile_field_info_type_contacts') ?>
                            </td>
                            <td class="center-align">
                                <?php if ($field['admin_changing']) { ?>
                                    <span class="glyphicon glyphicon-ok" title="<?= $this->lang->line('profile_field_can_admin_change') ?>"></span>
                                <?php } ?>
                            </td>
                            <td class="center-align">
                                <?php if ($field['required']) { ?>
                                    <span class="glyphicon glyphicon-certificate" title="<?= $this->lang->line('profile_field_is_required') ?>"></span>
                                <?php } ?>
                            </td>
                            <td class="center-align">
                                <?php if ($field['status']) { ?>
                                    <span class="glyphicon glyphicon-ok" title="<?= $this->lang->line('profile_field_is_in_use') ?>"></span>
                                <?php } ?>
                            </td>
                            <td class="center-align"><?= $field['count'] ?></td>
                            <td class="center-align actions-td">
                                <span class="btn btn-xs btn-primary" onclick="editProfileField(<?= $field['id'] ?>)" title="<?= $this->lang->line('edit') ?>">
                                    <i class="fa fa-edit"></i>
                                </span>

                                <span class="btn btn-xs btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" onclick="removeProfileField(<?= $field['id'] ?>);">
                                    <i class="fa fa-remove"></i>
                                </span>

                            </td>
                        </tr>

                    <?php } ?>

                <?php } else { ?>

                    <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>

                <?php } ?>
            </tbody>
        </table>
    
    </div>
    
</div>






<script src="<?= base_url() ?>assets/js/admin/irida.admin.js"></script>
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>
