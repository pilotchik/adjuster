<?php require_once(APPPATH.'views/required/required_notauth_header.php'); ?>
    
    <div class="row">
        <div class="col-sm-12">
                
            <form action="<?= base_url() ?>main/restoresend" method="post" autocomplete="off">
                <input class="form-control input-lg" type="email" name="email" value="<?= set_value('email'); ?>" placeholder="<?= $this->lang->line('user_email'); ?>" required="true">
                <div class="formError"><?= form_error('email'); ?></div>
                <br/>
                <div class="pull-right">
                    <button type="submit" class="btn btn-lg btn-success"><?= $this->lang->line('restore_access_send_link'); ?></button>
                </div>
            </form>
            
        </div>
    </div>

    <div class="row mainFormLinksContainer">
        <div class="col-sm-12 text-right">
            <a href="<?= base_url() ?>"><?= $this->lang->line('btn_to_main') ?></a>
        </div>
    </div>


<?php require_once(APPPATH.'views/required/required_notauth_footer.php'); ?>