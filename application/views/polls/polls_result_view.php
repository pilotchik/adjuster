<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<ol class="breadcrumb">
    <li><a href="<?= base_url() ?>polls"><?= $this->lang->line('polls'); ?></a></li>
    <li class="active"><?= $this->lang->line('poll_result'); ?> "<?= $poll['title'] ?>"</li>
</ol>


<div class="card users-table-card poll-results-card">
    
    <div class="content">

        <form class="form-horizontal" role="form">

            <?php if ($poll['description']) { ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?= $this->lang->line('poll_description'); ?></label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?= htmlspecialchars($poll['description']) ?></p>
                    </div>
                </div>
            <?php } ?>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Итоговый балл</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?= htmlspecialchars($all_balls_proz) ?>%</p>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Итоговая оценка</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?= htmlspecialchars($final_mark) ?></p>
				</div>
			</div>

        </form>


        <?php foreach($poll['quests'] as $quest) { ?>

            <div class="panel panel-success" id="questPanel_<?= $quest['id'] ?>">
                <div class="panel-heading questPanel">
                    <?= $quest['title'] ?>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form">
                        <?php if ($quest['description']) { ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= $this->lang->line('poll_quest_description'); ?></label>
                                <div class="col-sm-9">
                                    <p class="form-control-static"><?= $quest['description'] ?></p>
                                </div>
                            </div>
                        <?php } ?>

						<p>Результат: <b><?= $quest['balls'] ?></b></p>
						
						
						
                        
                    </form>

                    
                </div>

                
            </div>



        <?php } ?>


    </div>
    
</div>

<script src="<?= base_url() ?>assets/js/canvasjs.min.js"></script>
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>