<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Добавление опроса -->
<div class="modal fade" id="addPollModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/add_poll">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_adding'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="inputAddPoll"><?= $this->lang->line('poll_title'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('poll_title'); ?>" name="title" value="" required="true" id="inputAddPoll">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddPollDescription"><?= $this->lang->line('poll_description'); ?></label>
                        <textarea class="form-control" rows="3" name="description" id="inputAddPollDescription"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddPollThemeID"><?= $this->lang->line('poll_theme_name'); ?></label>
                        <select class="form-control" name="theme_id" id="inputAddPollThemeID">
							<?php foreach($themes as $theme) { ?>
								<option value="<?= $theme['id'] ?>"><?= $theme['name'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-success"><?= $this->lang->line('btn_add'); ?></button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Изменение параметров опроса -->
<div class="modal fade" id="openEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/update" autocomplete="false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_editing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="id" value="" id="inputEditPollID">
                    
                    <div class="form-group required">
                        <label for="inputEditPollTitle"><?= $this->lang->line('poll_title'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('poll_title'); ?>" name="title" value="" required="true" id="inputEditPollTitle">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEditPollDescription"><?= $this->lang->line('poll_description'); ?></label>
                        <textarea class="form-control" rows="3" name="description" id="inputEditPollDescription"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEditPollThemeID"><?= $this->lang->line('poll_theme_name'); ?></label>
                        <select class="form-control" name="theme_id" id="inputEditPollThemeID">
							<?php foreach($themes as $theme) { ?>
								<option value="<?= $theme['id'] ?>"><?= $theme['name'] ?></option>
							<?php } ?>
                        </select>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение удаления опроса -->
<div class="modal fade" id="removePollConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="inputRemovePollID">
                    <?= $this->lang->line('poll_removing_confirm1'); ?><span id="inputRemovePollTitle" class="confirmTitle"></span><?= $this->lang->line('poll_removing_confirm2'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение архивирования опроса -->
<div class="modal fade" id="archivePollConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/archive">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_archiving'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="inputArchivePollID">
                    <?= $this->lang->line('poll_archiving_confirm1'); ?><span id="inputArchivePollTitle" class="confirmTitle"></span><?= $this->lang->line('poll_archiving_confirm2'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-warning"><?= $this->lang->line('btn_archive'); ?></button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Подтверждение повторного прохождения опроса -->
<div class="modal fade" id="repeatPollConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_repeating'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('poll_repeat_confirm1'); ?><span id="inputRepeatPollTitle" class="confirmTitle"></span><?= $this->lang->line('poll_repeat_confirm2'); ?>
                </div>
                <div class="modal-footer">
                    <a type="submit" href="" id="repeatConfirmLinkID" class="btn btn-xs btn-warning"><?= $this->lang->line('btn_next'); ?></a>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>




<?php if (count($active_polls)) { ?>


    <div class="card users-table-card">
        <div class="header">
            <h4 class="title"><?= $this->lang->line('polls_notcompleted') ?></h4>
        </div>
        <div class="content">

            <div class="row">

                <?php foreach($active_polls as $poll) { ?>

                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 bigPollPanel panelForPoll_<?= $poll['id'] ?>">
                        
                        <div class="card room-card room-panel">
                            
                            <div class="room-image-container">
                                <div class="poll-image"></div>
                                <?php if (!$poll['poll_finish']) { ?>
                                    <a class="room-play-link" href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>"  title="<?= $this->lang->line('poll_play') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                <?php } else { ?>
                                    <a class="room-play-link" target-href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>" onclick="openRepeatModal(<?= $poll['id'] ?>,this,event)" title="<?= $this->lang->line('poll_repeating') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                <?php } ?>
                                    <div class="room-play-button"></div>
                                </a>
                            </div>
                        
                            <div class="content">

                                <div class="room-content-title">
                                    
                                    <?php if (!$poll['poll_finish']) { ?>
                                        <a class="room-play-link" href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>"  title="<?= $this->lang->line('poll_play') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                    <?php } else { ?>
                                        <a class="room-play-link" target-href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>" onclick="openRepeatModal(<?= $poll['id'] ?>,this,event)" title="<?= $this->lang->line('poll_repeating') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                    <?php } ?>
                                        <?= htmlspecialchars($poll['title']) ?>
                                    </a>
                        
                                </div>
                                
                                <div class="room-content-buttons">
                            
                                    <div class="btn-group iridaRoomPanelLeftButtons iridaPollLeftFooter">
										
                                        <?php if ($poll['description']) { ?>
                                            <span class="glyphicon glyphicon glyphicon-info-sign blueInfo" title="<?= htmlspecialchars($poll['description']) ?>"></span>
                                        <?php } ?>
                                        <span class="glyphicon glyphicon-question-sign" title="<?= $this->lang->line('poll_quests_count'); ?>"></span>&nbsp;<?= $poll['questsCount'] ?>

                                    </div>


                                    <div class="btn-group iridaRoomPanelRightButtons" style="margin-left: 10px;">

                                        <div class="btn-group">
                                            <?php if (!$poll['poll_finish']) { ?>
											
                                                <a class="btn btn-xs btn-success" href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>"  title="<?= $this->lang->line('poll_play') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                                    <i class="fa fa-play"></i>
                                                </a>
											
                                            <?php } else { ?>
											
                                                <a class="btn btn-xs btn-success" target-href="<?= base_url() ?>polls/play/<?= $poll['id'] ?>" onclick="openRepeatModal(<?= $poll['id'] ?>,this,event)" title="<?= $this->lang->line('poll_repeating') ?>" <?= $poll['questsCount'] ? '' : 'disabled' ?>>
                                                    <i class="fa fa-repeat"></i>
                                                </a>
											
                                            <?php } ?>

                                            <a class="btn btn-xs " href="<?= base_url() ?>polls/result/<?= $poll['id'] ?>" title="<?= $this->lang->line('poll_results'); ?>" <?= $poll['poll_finish'] ? '' : 'disabled' ?>>
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
											
											<?php if ($this->session->userdata('userType') == 'admin') { ?>
											
												<span class="btn btn-xs btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" onclick="openRemoveModal(<?= $poll['id'] ?>)">
                                                    <i class="fa fa-remove"></i>
                                                </span>
                                            
											<?php } ?>
											
                                        </div>

										
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                    </div>


                <?php } ?>
            </div>
        </div>
    </div>

<?php } ?>
    

<?php if (count($my_polls['active']) || count($my_polls['archive'])) { ?>

    <div class="card users-table-card">
        <div class="header">
            <h4 class="title"><?= $this->lang->line('polls_my') ?></h4>
        </div>
        <div class="content">

			<div id="home" class="tab-pane fade <?= !isset($active_tab) ? 'in active' : '' ?>">

				<div class="table-responsive">
					<table class="table table-striped sortedTable">
						<thead>
							<tr>
								<th><?= $this->lang->line('poll_title'); ?></th>
								<th style="width: 120px;"><?= $this->lang->line('poll_theme_name'); ?></th>
								<th style="width:30px;" title="<?= $this->lang->line('poll_quests_count'); ?>"><i class="fa fa-question-circle"></i></th>
								<th style="width:30px;" title="<?= $this->lang->line('poll_results_count'); ?>"><i class="fa fa-user"></i></th>
								<th style="width:50px;" title="<?= $this->lang->line('poll_add_date'); ?>"><i class="fa fa-calendar"></i></th>
								<th class="wide-actions-td actions-td noSort"><?= $this->lang->line('actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($my_polls['active'])) { ?>
								<?php foreach($my_polls['active'] as $poll) { ?>
									<tr>
										<td>
											<a  href="<?= base_url() ?>polls/one/<?= $poll['id'] ?>" title="<?= $this->lang->line('poll_quests'); ?>" style="margin-right:5px;">
												<span id="title_<?= $poll['id'] ?>"><?= $poll['title'] ?></span> 
											</a>
											<?php if ($poll['description']) { ?>
												<div class="user-subheader notify-subheader"><?= htmlspecialchars($poll['description']) ?></div>
											<?php } ?>
											<span id="description_<?= $poll['id'] ?>" style="display:none;"><?= $poll['description'] ?></span>
										</td>
										<td class="center-align" title="<?= $poll['anon_status'] ? $this->lang->line('poll_anon_true') : $this->lang->line('poll_anon_false') ?>">
											<span id="theme_<?= $poll['id'] ?>" style="display:none;"><?= $poll['theme_id'] ?></span>
											<?= $poll['themeName'] ?>
										</td>
										<td class="center-align" id="questscount_<?= $poll['id'] ?>"><?= $poll['questsCount'] ?></td>
										<td class="center-align"><?= $poll['respsCount'] ?></td>
										<td class="center-align date-inactive-status">
											<span class="hiddenSpan"><?= $poll['ts'] ?></span>
											<?= $poll['createDate'] ?>
										</td>
										<td class="center-align actions-td">

											<div class="btn-group" style="margin-right:10px;">
												<a class="btn btn-xs" href="<?= base_url() ?>polls/all_results/<?= $poll['id'] ?>" title="<?= $this->lang->line('poll_results'); ?>" <?= $poll['respsCount'] ? '' : 'disabled' ?>>
													<i class="fa fa-bar-chart"></i>
												</a>
											</div>

											<div class="btn-group">
												<span class="btn btn-xs btn-primary" onclick="openEditModal(<?= $poll['id'] ?>)" title="<?= $this->lang->line('edit') ?>">
													<span class="glyphicon glyphicon-edit"></span>
												</span>
												<span class="btn btn-xs btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" onclick="openRemoveModal(<?= $poll['id'] ?>)">
													<span class="glyphicon glyphicon-remove"></span>
												</span>
											</div>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('polls_not_found'); ?></td></tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>


        </div>
        
    </div>

<?php } ?>



<script>
    
    
    function showHideUsersTable(el) {
        if ($(el).val() == 'persons') 
            $('.invitesTableClass').css('display','');
        else 
            $('.invitesTableClass').css('display','none');
    }
    
    function selectTablePage(numb) {
        $('.tableRows').css('display','none');
        $('.rowsGroup_'+numb).css('display','');
        $('.pagClass').removeClass('active');
        $('#pag_'+numb).addClass('active');
    }
    
    
    function openRepeatModal(pollID,el,e) {
        e.preventDefault();
        $('#inputRepeatPollTitle').html($('#title_'+pollID).html());
        $('#repeatConfirmLinkID').attr('href',$(el).attr('target-href'));
        $('#repeatPollConfirmModal').modal('show');
    }
    
    
    /**
     * Архивирование опроса
     */
    function openArchiveModal(pollID) {
        $('#inputArchivePollID').val(pollID);
        $('#inputArchivePollTitle').html($('#title_'+pollID).html());
        $('#archivePollConfirmModal').modal('show');
    }
    
    
    /**
     * Удаление опроса
     */
    function openRemoveModal(pollID) {
        $('#inputRemovePollID').val(pollID);
        $('#inputRemovePollTitle').html($('#title_'+pollID).html());
        $('#removePollConfirmModal').modal('show');
    }
    
    
    function openEditModal(pollID) {
        
        $('#inputEditPollID').val(pollID);
        $('#inputEditPollTitle').val($('#title_'+pollID).html());
        $('#inputEditPollDescription').val($('#description_'+pollID).html());
        
		$('#inputEditPollThemeID').val($('#theme_'+pollID).html());
		
        $('#openEditModal').modal('show');
    }
    
    
    function getAJAXPollUsersInfo(pollID) {
        
        users = new Object;
        
        jQuery.ajax({
            type: 'get',
            async: false,
            data: {pollID:pollID},
            url: '<?= base_url() ?>polls/getPollsInvitedUsers',
            success: function(msg) {
                users = JSON.parse(msg).users;
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });
        
        return users;
    }
    
</script>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>