<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>


<ol class="breadcrumb">
    <li><a href="<?= base_url() ?>polls"><?= $this->lang->line('polls'); ?></a></li>
    <li class="active"><?= $this->lang->line('polls_playing'); ?> "<?= $poll['title'] ?>"</li>
</ol>



<div class="card users-table-card poll-results-card">
    
    <div class="content">

		<form action="<?= base_url() ?>polls/finish/<?= $poll['id'] ?>" method="post">


			<div class="questContainer">

				<?php if ($poll['description']) { ?>

					<blockquote>

						<?php if ($poll['description'] != '') { ?>
							<p><?= htmlspecialchars($poll['description']) ?></p>
						<?php } ?>

					</blockquote>

				<?php } ?>


				<?php $i = 1; ?>
				<?php foreach($poll['quests'] as $quest) { ?>
					<div class="panel panel-warning" id="questPanel_<?= $quest['id'] ?>">
						<div class="panel-heading questPanel">
							Вопрос <?= $i ?>. <?= htmlspecialchars($quest['title']) ?> <?= $quest['required'] ? '&nbsp;<span class="glyphicon glyphicon-asterisk requiredQuest" title="'.$this->lang->line('poll_quest_required_true').'"></span>' : '' ?>
						</div>



						<div class="panel-body">
							<?php if ($quest['description']) { ?>

								<p><?= htmlspecialchars($quest['description']) ?></p>

							<?php } ?>

							<?php if ($quest['type'] == 'selectone' || $quest['type'] == 'selectmany') { ?>

								<div class="form-group">
									<label for="inputAddPollQuestAnswer_<?= $quest['id'] ?>"><?= $this->lang->line('poll_quest_answer'); ?></label>
									<div class="form-control answer-span-container" rows="3" name="answer" id="inputUpdatePollQuestAnswer_<?= $quest['id'] ?>" contenteditable="true"></div>
									<div class="test-answers-form" target="inputUpdatePollQuestAnswer_<?= $quest['id'] ?>"></div>
									<input type="hidden" name="answers[]" value="" id="inputUpdatePollQuestAnswer_<?= $quest['id'] ?>Line">
								</div>


							<?php } ?>

						</div>

					</div>

					<?php $i++; ?>

				<?php } ?>


				<div class="center-align poll-submit-btn-container">
					<button type="submit" title="" id="submitPollButtonID" class="btn btn-success">
						<?= $this->lang->line('btn_poll_finish'); ?>
					</button>
				</div>


			</div>
			
		</form>
			
    </div>
</div>


<script>
    
    function questMenager(type,value,questID) {
        
        completChecking = true;
        answer = [];
        
        if (type == 'selectmany') {
            //Проверить, есть ли выделенные чекбоксы
            completChecking = false;
            $('#questPanel_'+questID+' input').each(function(numb,el){
                
                if ($(el).prop('checked')) {
                    answer.push($(el).attr('value'));
                    completChecking = true;
                }
            });
        } else {
            answer.push(value);
        }
        
        if (completChecking) {
            $('#questPanel_'+questID).removeClass('panel-warning').addClass('panel-success');
            $('#questPanel_'+questID+' .questCompletedStatus').fadeIn();
            if (completed.indexOf(questID) < 0) completed.push(questID);
        } else {
            $('#questPanel_'+questID).removeClass('panel-success').addClass('panel-warning');
            $('#questPanel_'+questID+' .questCompletedStatus').css('display','none');
            delete completed[completed.indexOf(questID)];
        }
        
        console.log(type,value,questID,completed,answer);
        
        sendQuestAJAX(completChecking,answer,questID)
        
        if (checkRequired()) 
            $('#submitPollButtonID').attr('disabled',false);
        else
            $('#submitPollButtonID').attr('disabled',true);
        
    }
    
    
    function checkRequired() {
        
        checking = false;
        //Если все элементы requireds имеются в completed, то true
        cnt = 0;
        requireds.forEach(function(el){
            if (completed.indexOf(el) >= 0) cnt++;
        });
        if (cnt == requireds.length) checking = true;
        
        return checking;    
    }
    
    
    function sendQuestAJAX(completeStatus,answer,questID) {
        jQuery.ajax({
            type: 'post',
            async: true,
            data: {pollID:<?= $poll['id'] ?>,questID:questID,completeStatus:completeStatus,answer:answer},
            url: '<?= base_url() ?>polls/saveQuestAnswer',
            success: function(msg) {
                console.log(JSON.parse(msg))
                //answers = JSON.parse(msg).answers;
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });
        
    }
    
    
    
    jQuery(document).ready(function($){
	
		loadAnswersForm();
	
		//if (requireds.length) $('#submitPollButtonID').attr('disabled',true);    
		
    });
    
</script>


<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery-ui.css" />
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-ui.js"></script>
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>