<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Добавление вопроса -->
<div class="modal fade" id="addPollQuestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/add_quest/<?= $poll['id'] ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_quest_adding'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="inputAddPollQuest"><?= $this->lang->line('poll_quest_title'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('poll_quest_title'); ?>" name="title" value="" required="true" id="inputAddPollQuest">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddPollQuestDescription"><?= $this->lang->line('poll_quest_description'); ?></label>
                        <textarea class="form-control" rows="3" name="description" id="inputAddPollQuestDescription"></textarea>
                    </div>
					
					<input type="hidden" name="required" value="1">
                    
					<input type="hidden" name="type" value="selectone">
					
                    <div class="form-group">
                        <label for="inputAddPollQuestAnswer"><?= $this->lang->line('poll_quest_answer'); ?></label>
                        <div class="form-control answer-span-container" rows="3" name="answer" id="inputAddPollQuestAnswer" contenteditable="true"></div>
						<div class="test-answers-form" target="inputAddPollQuestAnswer"></div>
						<input type="hidden" name="answer" value="" id="inputAddPollQuestAnswerLine">
                    </div>
					
					
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_add'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение удаления вопроса -->
<div class="modal fade" id="removePollQuestConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/quest_remove/<?= $poll['id'] ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_quest_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="inputRemovePollQuestID">
                    <?= $this->lang->line('poll_quest_removing_confirm1'); ?><span id="inputRemovePollQuestTitle" class="confirmTitle"></span><?= $this->lang->line('poll_quest_removing_confirm2'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Редактирование параметров вопроса -->
<div class="modal fade" id="editPollQuestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>polls/update_quest/<?= $poll['id'] ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('poll_quest_updating'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="id" value="" id="inputUpdatePollQuestID">
                    
                    <div class="form-group required">
                        <label for="inputEditPollQuest"><?= $this->lang->line('poll_quest_title'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('poll_quest_title'); ?>" name="title" value="" required="true" id="inputEditPollQuest">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEditPollQuestDescription"><?= $this->lang->line('poll_quest_description'); ?></label>
                        <textarea class="form-control" rows="3" name="description" id="inputEditPollQuestDescription"></textarea>
                    </div>
                    
                    <input type="hidden" name="required" value="1">
					
                    <div class="form-group">
                        <label for="inputAddPollQuestAnswer"><?= $this->lang->line('poll_quest_answer'); ?></label>
                        <div class="form-control answer-span-container" rows="3" name="answer" id="inputUpdatePollQuestAnswer" contenteditable="true"></div>
						<div class="test-answers-form" target="inputUpdatePollQuestAnswer"></div>
						<input type="hidden" name="answer" value="" id="inputUpdatePollQuestAnswerLine">
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>






<ol class="breadcrumb">
    <li><a href="<?= base_url() ?>polls"><?= $this->lang->line('polls'); ?></a></li>
    <li class="active"><?= $this->lang->line('poll_quests'); ?> "<?= $poll['title'] ?>"</li>
</ol>


<div class="card users-table-card poll-results-card">
    
    <div class="content">


        <form class="form-horizontal" role="form">

            <?php if ($poll['description'] != '') { ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?= $this->lang->line('poll_description'); ?></label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?= htmlspecialchars($poll['description']) ?></p>
                    </div>
                </div>
            <?php } ?>

            
        </form>


        <h3><?= $this->lang->line('polls_quests'); ?></h3>


        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= $this->lang->line('poll_quest_title'); ?></th>
                        <th class="actions-td"><?= $this->lang->line('actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($poll['quests'])) { ?>
					
                        <?php foreach($poll['quests'] as $quest) { ?>
                            <tr>
								
                                <td>
                                    <span id="title_<?= $quest['id'] ?>"><?= $quest['title'] ?></span>
                                    <?php if (htmlspecialchars($quest['description']) != '') { ?>
                                        <div class="user-subheader notify-subheader"><?= htmlspecialchars($quest['description']) ?></div>
                                    <?php } ?>
                                    <span class="hidden-span" id="description_<?= $quest['id'] ?>"><?= htmlspecialchars($quest['description']) ?></span>
									<span class="hidden-span" id="answer_html_<?= $quest['id'] ?>"><?= $quest['html'] ?></span>
									<span class="hidden-span" id="answer_<?= $quest['id'] ?>"><?= $quest['answer'] ?></span>
                                </td>
                                
                        		<td class="center-align">
									<span class="btn btn-xs btn-primary" onclick="openEditModal(<?= $quest['id'] ?>)" title="<?= $this->lang->line('edit') ?>">
										<i class="fa fa-edit"></i>
									</span>
									<span class="btn btn-xs btn-danger" title="<?= $this->lang->line('actions_remove'); ?>" onclick="openRemoveModal(<?= $quest['id'] ?>)">
										<i class="fa fa-remove"></i>
									</span>
								</td>
								
                            </tr>
                        <?php } ?>
                            
                    <?php } else { ?>
                            
                        <tr><td colspan="100%" class="not-founded-data"><?= $this->lang->line('poll_quests_not_found'); ?></td></tr>
                        
                    <?php } ?>
						
                </tbody>
            </table>

        </div>

    </div>
    
</div>

<script>
    
    var codeWithoutRemove = '<div class="form-group">\n\
        <label>\n\
            <?= $this->lang->line('poll_quest_answer_var'); ?>&nbsp;\n\
        </label>\n\
        <input class="form-control questAnswerInput" name="answer[]" required="true">\n\
    </div>';
                    
    var codeWithRemove = '<div class="form-group">\n\
        <label>\n\
            <?= $this->lang->line('poll_quest_answer_var'); ?>&nbsp;\n\
            <span class="btn btn-xs btn-danger" onclick="$(this).parent().parent().remove()"><span class="glyphicon glyphicon-remove"></span></span>\n\
        </label>\n\
        <input class="form-control questAnswerInput" name="answer[]" required="true">\n\
    </div>';
            
    var codeBeginScale = '<div class="form-group">\n\
        <label>\n\
            <?= $this->lang->line('poll_quest_scale_begin'); ?>&nbsp;\n\
        </label>\n\
        <input class="form-control questAnswerInput" name="answer[]" required="true">\n\
    </div>';
    
    var codeEndScale = '<div class="form-group">\n\
        <label>\n\
            <?= $this->lang->line('poll_quest_scale_end'); ?>&nbsp;\n\
        </label>\n\
        <input class="form-control questAnswerInput" name="answer[]" required="true">\n\
    </div>';
            
    var codeCountScale = '<div class="form-group">\n\
        <label>\n\
            <?= $this->lang->line('poll_quest_scale_count'); ?>&nbsp;\n\
        </label>\n\
        <input class="form-control questAnswerInput" type="number" min="3" value="3" max="10" name="answer[]" required="true">\n\
    </div>';
    
    
    function selectPollType(el) {
        
        type = $(el).val();
        
        console.log(type)
        $('#codeAddVariant').css('display','none');
        $('#questAddFormContainer').html('');

        if (type != '') {
            if (type === 'selectone' || type === 'selectmany') {

                $('#questAddFormContainer').append(codeWithoutRemove);
                $('#questAddFormContainer').append(codeWithoutRemove);

                $('#codeAddVariant').css('display','');

            } else if (type === 'selectscale') {

                $('#questAddFormContainer').append(codeBeginScale);
                $('#questAddFormContainer').append(codeEndScale);
                $('#questAddFormContainer').append(codeCountScale);

            }
        }
        
    }
    
    
    function addPollQuestVar() {
        
        type = $('#inputAddPollQuestType').val();
        $('#questAddFormContainer').append(codeWithRemove);
        
    }
    
    
    function editPollQuestVar() {
        $('#questEditFormContainer').append(codeWithRemove);
    }
    
    
    function openRemoveModal(questID) {
        $('#inputRemovePollQuestID').val(questID);
        $('#inputRemovePollQuestTitle').html($('#title_'+questID).html());
        $('#removePollQuestConfirmModal').modal('show');
    }
    
    
    function openEditModal(questID) {
        $('#inputUpdatePollQuestID').val(questID);
        $('#inputEditPollQuest').val($('#title_'+questID).html());
        $('#inputEditPollQuestDescription').val($('#description_'+questID).html());
        $('#inputEditPollQuestRequired [value="'+$('#required_'+questID).html()+'"]').attr('selected',true);
        
		$('#inputUpdatePollQuestAnswer').html($('#answer_html_'+questID).html());
		$('#inputUpdatePollQuestAnswerLine').val($('#answer_'+questID).html());
		
        $('#codeEditVariant').css('display','none');
        
        $('#questAddFormContainer').html('');
        $('#questEditFormContainer').html('');

        type = $('#type_'+questID).html();
        answers = getAJAXPollQuestAnswersInfo(questID)
        
        if (type === 'selectone' || type === 'selectmany') {
                    
            $('#questEditFormContainer').append(codeWithoutRemove);
            $('#questEditFormContainer').append(codeWithoutRemove);
            
            for (var i = 0; i < answers.length - 2; i++) {
                $('#questEditFormContainer').append(codeWithRemove);
            }
            
            $('#codeEditVariant').css('display','');
            
        } else if (type === 'selectscale') {
            
            $('#questEditFormContainer').append(codeBeginScale);
            $('#questEditFormContainer').append(codeEndScale);
            $('#questEditFormContainer').append(codeCountScale);
            
        }
    
        $('.questAnswerInput').each(function(numb,el){
            $(el).val(answers[numb]);
        })
        
        
        $('#editPollQuestModal').modal('show');
    }
    
    
    function getAJAXPollQuestAnswersInfo(questID) {
        
        answers = new Object;
        
        jQuery.ajax({
            type: 'get',
            async: false,
            data: {questID:questID},
            url: '<?= base_url() ?>polls/getQuestAnswers',
            success: function(msg) {
                answers = JSON.parse(msg).answers;
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });
        
        return answers;
    }
    
	
	
	jQuery(document).ready(function() {
    
		loadAnswersForm();
	
    });
    
    
</script>

<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>