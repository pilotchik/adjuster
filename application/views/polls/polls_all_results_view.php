<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<ol class="breadcrumb">
    <li><a href="<?= base_url() ?>polls"><?= $this->lang->line('polls'); ?></a></li>
    <li class="active"><?= $this->lang->line('poll_result'); ?> "<?= $poll['title'] ?>"</li>
</ol>


<div class="card users-table-card poll-results-card">
    
    <div class="content">

        <form class="form-horizontal" role="form">

            <?php if ($poll['description']) { ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?= $this->lang->line('poll_description'); ?></label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?= htmlspecialchars($poll['description']) ?></p>
                    </div>
                </div>
            <?php } ?>
			
        </form>

		
		<table class="table table-striped sortedTable">
			
			<thead>
				<tr>
					<th>Обучаемый</th>
					<th style="width: 120px;">Группа</th>
					<th style="width: 120px;">Балл</th>
					<th style="width:30px;" title="<?= $this->lang->line('poll_quests_count'); ?>">Оценка</th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($results)) { ?>
					<?php foreach($results as $result) { ?>
						<tr>
							<td><?= $result['info']['user']['fullName'] ?></td>
							<td class="center-align"><?= $result['info']['user']['group'] ?></td>
							<td class="center-align">
								<?= $result['all_balls_proz'] ?>
							</td>
							<td class="center-align">
								<?= $result['final_mark'] ?>
							</td>
						</tr>
						
					<?php } ?>
						
				<?php } else { ?>
						
					<tr><td colspan="100%" class="notFoundedDataTD">Результаты отсутствуют</td></tr>
						
				<?php } ?>
			</tbody>
			
		</table>

    </div>
    
</div>

<script src="<?= base_url() ?>assets/js/canvasjs.min.js"></script>
<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>