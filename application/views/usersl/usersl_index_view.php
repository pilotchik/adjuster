<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>


<div id="usersFavoriteContainer" style="display:none;">

    <div class="card users-table-card">
        <div class="header">
            <h4 class="title"><?= $this->lang->line('landing_favorites_users') ?></h4>
        </div>
        <div class="content">
        
            <table class="table table-striped table-responsive modalTable">
                <thead>
                    <tr>
                        <th class="user-in-cart-status"></th>
                        <th class="mini-avatar-td"></th>
                        <th class="user-name-long-td"><?= $this->lang->line('user'); ?></th>
                        <?php if ($config_components_posts) { ?>
                            <th class="userSeatingTD"><?= $this->lang->line('user_company'); ?></th>
                            <th class="usersPostsTD"><?= $this->lang->line('user_post'); ?></th>
                        <?php } ?>
                        <th><?= $this->lang->line('user_tags'); ?></th>
                        <?php if ($config_components_seating) { ?>
                            <th class="userSeatingTD"><?= $this->lang->line('user_seating'); ?></th>
                        <?php } ?>
                        <th class="state-td" title="<?= $this->lang->line('user_state'); ?>"></th>
                    </tr>
                </thead>
                <tbody id="simpleFavoriteUsers"></tbody>
            </table>
            
        </div>
    </div>
    
</div>


<div class="card users-table-card">
    <div class="header">
        <h4 class="title"><?= $this->lang->line('users_all') ?></h4>
    </div>
    <div class="content">

        <table class="table table-striped table-responsive modalTable sortedTable withFilter">
            <thead>
                <tr>
                    <th class="user-in-cart-status"></th>
                    <th class="mini-avatar-td"></th>
                    <th class="user-name-long-td"><?= $this->lang->line('user'); ?></th>
                    <?php if ($config_components_posts) { ?>
                        <th class="userSeatingTD"><?= $this->lang->line('user_company'); ?></th>
                        <th class="usersPostsTD"><?= $this->lang->line('user_post'); ?></th>
                    <?php } ?>
                    <th><?= $this->lang->line('user_tags'); ?></th>
                    <?php if ($config_components_seating) { ?>
                        <th class="userSeatingTD"><?= $this->lang->line('user_seating'); ?></th>
                    <?php } ?>
                    <th class="state-td"><?= $this->lang->line('user_state'); ?></th>
                </tr>
            </thead>
            <tbody id="simpleCommonUsers">
                <?php if (count($users)) {
                    $number = 0;
                    foreach($users as $user) {
                        if ($user['id'] != $this->session->userdata['userID']) {

                            ?>
                            <tr class="tableRows commonUsersRow user-row"  user-id="<?= $user['id'] ?>" id="usersTR_<?= $user['id'] ?>">
                                <td class="center-align user-in-cart-status">
                                    <span class="btn btn-xs <?= $user['inCart'] ? 'btn-success' : 'btn-default' ?> cart-invite-button cart-invite-button-<?= $user['id'] ?>" onclick="inviteUserInRoom(this)" user-id="<?= $user['id'] ?>" title="<?= $this->lang->line('room_invite_user') ?>">
                                        <i class="fa fa-user-plus"></i>
                                    </span>
                                </td>
                                <td class="center-align miniAvatarTD">
                                    <a class="photo photo_<?= $user['id'] ?>" href="<?= $user['avatarURL'] ?>" title="<?= $user['fullName'] ?>">
                                        <img class="smallUserAvatarInTables" src="<?= $user['avatarURL_small'] ?>">
                                    </a>
                                </td>
                                <td style="position: relative;">
                                    <span class="user-favorite-status" title="<?= !$user['favorite_status'] ? $this->lang->line('user_favorite_add') : $this->lang->line('user_favorite_remove') ?>" onclick="usersFavoriteManager(this)" user-id="<?= $user['id'] ?>" >
                                        <?php if (!$user['favorite_status']) { ?>
                                            <i class="fa fa-star-o"></i>
                                        <?php } else { ?>
                                            <i class="fa fa-star"></i>
                                        <?php } ?>
                                    </span>
                                    
                                    <?php if ($user['favorite_you_status']) { ?>
                                        <span class="user-favorite-you-status" title="<?= $this->lang->line('user_favorite_you_status') ?>">
                                            <i class="fa fa-heart"></i>
                                        </span>
                                    <?php } ?>

                                    <?= htmlspecialchars($user['surname']) ?> <?= htmlspecialchars($user['name']) ?> <?= htmlspecialchars($user['middlename']) ?> <?= $user['birthday_status'] ? '&nbsp;<span class="glyphicon glyphicon-glass birthdayGlyph" title="'.$this->lang->line('user_birthday_today').'"></span>' : '' ?>
                                    
                                    <?php if($user['contacts'] != '') { ?>
                                        <div class="user-subheader"><?= $user['contacts'] ?></div>
                                    <?php } ?>
                                    
                                </td>
                                <?php if ($config_components_posts) { ?>
                                    <td class="center-align userSeatingTD"><?= htmlspecialchars($user['company']) ?></td>
                                    <td class="center-align usersPostsTD"><?= htmlspecialchars($user['post']) ?></td>
                                <?php } ?>
                                <td><?= $user['tags'] ?></td>
                                <?php if ($config_components_seating) { ?>
                                    <td class="center-align">
                                        <?php if (!empty($user['seating'])) { ?>
                                            <a href="<?= base_url() ?>seatingl/view_item?type=<?= $user['seating']['type'] ?>&itemID=<?= $user['seating']['id'] ?>"><?= htmlspecialchars($user['seating']['name']) ?></a>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                                <td class="center-align">
                                    <span class="hiddenSpan"><?= $user['user_state'] ? 1 : 0 ?></span>
                                    <img title="<?= $user['user_state'] ? $this->lang->line('user_online') : $this->lang->line('user_offline') ?>" class="stateIMG" src="<?= base_url() ?>assets/images/icon_admin_<?= $user['user_state'] ? 'green' : 'gray' ?>.png">
                                    <?php if ($user['current_room'] != '') { ?>
                                        <div class="user-current-room" title="<?= $this->lang->line('room_user_current') ?>"><?= $user['current_room'] ?></div>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php  } ?>

                    <?php } ?>

                <?php } else { ?>

                    <tr><td colspan="100%" class="not-founded-data-td"><?= $this->lang->line('data_not_found'); ?></td></tr>

                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
      

<script>

    function selectTablePage(numb) {
        $('.tableRows').css('display','none');
        $('.rowsGroup_'+numb).css('display','');
        $('.pagClass').removeClass('active');
        $('#pag_'+numb).addClass('active');
    }
    
    
    function loadSearchResults(targetTableID) {
        
        var text = $('#searchInputID').val();
        
        $('.paginationContainer').css('display','none');
        
        jQuery.ajax({
            type: 'post',
            async: true,
            data: {text:text},
            url: '<?= base_url() ?>ajax/getUserSearch',
            success: function(msg) {
                result = JSON.parse(msg);
                console.log(result)
                
                $('.'+targetTableID+' tbody').html('');
                if (result.users.length) {
                    result.users.forEach(function(el){
                        
                        el.avatar = '';

                        if (el.avatar_status) {
                            el.avatar = '<a class="photo photo_'+el.login+'" href="<?= base_url() ?>upload/users/'+el.id+'/avatar_'+el.id+'_big.png" title="'+el.surname+' '+el.name+' '+el.middlename+'">\n\
                                <img class="smallUserAvatarInTables" src="<?= base_url() ?>upload/users/'+el.id+'/avatar_'+el.id+'_small.png">\n\
                            </a>';
                        }
                        
                        
                        code = '<tr>\n\
                            <td class="center-align">\n\
                                <span class="btn btn-xs '+(el.inCart ? 'btn-success' : 'btn-default' ) +'" onclick="inviteUserInRoom(this)" user-id="'+el.id +'">\n\
                                    <span class="glyphicon glyphicon-earphone"></span>\n\
                                </span>\n\
                            </td>\n\
                            <td>'+el.avatar+'</td>\n\
                            <td>'+el.surname+' '+el.name+' '+el.middlename+'</td>\n\
                            <td>'+el.tags+'</td>\n\
                            <td class="center-align">\n\
                                <img title="'+ (el.user_state ? '<?= $this->lang->line('user_online') ?>' : '<?= $this->lang->line('user_offline') ?>') + '" class="stateIMG" src="<?= base_url() ?>assets/images/icon_admin_'+ (el.user_state ? 'green' : 'gray' ) +'.png">\n\
                            </td>\n\
                        </tr>';
                        $('.'+targetTableID+' tbody').append(code);
                    });
                    
                    $('.photo').colorbox({rel:'photo'});
                } else {
                    code = '<tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>';
                    $('.'+targetTableID+' tbody').append(code);
                }
                
            },
            error: function(jq) {
                if (jq) {
                    $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                    $('#alertModal').modal('show');
                }
            }
        });
        
    }
    
    
    /*
    function checkAll(el) {
        if ($(el).prop('checked')) {
            $('.inRoomStatus').prop('checked',true);
            $('#inviteButtonID').attr('disabled',false);
        } else {
            $('.inRoomStatus').prop('checked',false);
            $('#inviteButtonID').attr('disabled',true);
        }
    }
    
    $('.inRoomStatus').on('click',function(e,el){
        
        //Пройти по всем и узнать, есть ли среди  них отмеченные
        $('#inviteButtonID').attr('disabled',true);
        
        $('.inRoomStatus').each(function(numb,el){
            if ($(el).prop('checked')) $('#inviteButtonID').attr('disabled',false);
        });    
    });
    */
    
    jQuery(document).ready(function() {
    
        $('.photo').colorbox({rel:'photo'});
        usersFavoriteMoving();
                                
    });
    
    
    function usersFavoriteManager(el) {
        
        userID = $(el).attr('user-id')
        
        if ($(el).find('i').hasClass('fa-star')) {
            
            //Есть в избранном
            fStatus = 0;
            
            //$('.panelForRoom_'+roomID).appendTo('.'+$('.panelForRoom_'+roomID).attr('target-class'));
            
        } else {
            
            //Нет в избранном
            fStatus = 1;
            
            //$('.panelForRoom_'+roomID).appendTo('#simpleFavoriteRoomsID');
            
        }
        
        $(el).find('i').removeClass('fa-star').removeClass('fa-star-o').addClass('fa-circle-o-notch').addClass('fa-spin');
        
        
        
        jQuery.ajax({
            type: 'get',
            async: true,
            data: {userID:userID,fStatus:fStatus},
            url: '<?= base_url() ?>usersl/usersFavoriteManager',
            success: function(msg) {
                
                if (fStatus === 1) {
                    $(el).find('i').removeClass('fa-circle-o-notch').removeClass('fa-spin').addClass('fa-star');
                    $(el).attr('title','<?= $this->lang->line('user_favorite_remove') ?>');
                } else {
                    $(el).find('i').removeClass('fa-circle-o-notch').removeClass('fa-spin').addClass('fa-star-o');
                    $(el).attr('title','<?= $this->lang->line('user_favorite_add') ?>');
                }
                
                usersFavoriteMoving();

                $('.main').animate({scrollTop:0},400);
            },
            error: function(jq) {
                
                if (fStatus === 1) {
                    $(el).find('i').removeClass('fa-circle-o-notch').removeClass('fa-spin').addClass('fa-star-o');
                } else {
                    $(el).find('i').removeClass('fa-circle-o-notch').removeClass('fa-spin').addClass('fa-star');
                }

                if (jq) showAlert(JSON.parse(jq.responseText).error,'danger');
            }
        });
        
        
    }
    
    
    function usersFavoriteMoving() {
    
    
        $.each($('.commonUsersRow'),function(nel,el) {
    
            userID = $(el).attr('user-id')
    
            //console.log($(el).find('.roomPanelFavoriteStatus .fa').hasClass('fa-star'))
    
            if ($(el).find('.user-favorite-status i').hasClass('fa-star')) {

                $(el).appendTo('#simpleFavoriteUsers');

            } else if ($('#usersFavoriteContainer').find('#usersTR_'+userID).length) {
                
                $(el).prependTo('#simpleCommonUsers');
                
            }
            
        });
        
        if ($('#usersFavoriteContainer').find('.commonUsersRow').length)
            $('#usersFavoriteContainer').css('display','');
        else
            $('#usersFavoriteContainer').css('display','none');
    
    }
    

</script>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>