<?php require_once(APPPATH.'views/required/required_notauth_header.php'); ?>
    
    <div class="row">
        <div class="col-sm-12">

            <form action="<?= base_url() ?>main/signup" method="post" autocomplete="off">
                <input class="form-control input-lg" type="text" name="surname" value="<?= set_value('surname'); ?>" placeholder="<?= $this->lang->line('user_surname'); ?>" required="true">
                <div class="formError"><?= form_error('surname'); ?></div>
                <br/>
                <input class="form-control input-lg" type="text" name="name" value="<?= set_value('name'); ?>" placeholder="<?= $this->lang->line('user_name'); ?>" required="true">
                <div class="formError"><?= form_error('name'); ?></div>
                <br/>
                <input class="form-control input-lg" type="text" name="middlename" value="<?= set_value('middlename'); ?>" placeholder="<?= $this->lang->line('user_middlename'); ?>">
                <div class="formError"><?= form_error('middlename'); ?></div>
                <br/>
                <!--<input class="form-control input-lg" type="text" name="username" value="<?= set_value('username'); ?>" placeholder="<?= $this->lang->line('login'); ?>" required="true">
                <div class="formError"><?= form_error('username'); ?></div>
                <br/>-->
                <input class="form-control input-lg" type="email" name="email" value="<?= set_value('email'); ?>" placeholder="<?= $this->lang->line('user_email'); ?>">
                <div class="formError"><?= form_error('email'); ?></div>
                <br/>
                <input class="form-control input-lg" type="password" name="password" value="<?= set_value('password'); ?>" placeholder="<?= $this->lang->line('password'); ?>" required="true">
                <div class="formError"><?= form_error('password'); ?></div>
                <br/>
                <button type="submit" class="btn btn-success btn-lg btn-group-justified"><?= $this->lang->line('sign_up'); ?></button>
            </form>
                
        </div>
    </div>

    
    <div class="row mainFormLinksContainer">
        <div class="col-sm-12 text-right">
            <a href="<?= base_url() ?>"><?= $this->lang->line('btn_to_main') ?></a>
        </div>
    </div>


<?php require_once(APPPATH.'views/required/required_notauth_footer.php'); ?>