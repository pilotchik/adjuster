<?php require_once(APPPATH.'views/required/required_notauth_header.php'); ?>
    
    <div class="row">
        <div class="col-sm-12">
            
            <form action="<?= base_url() ?>main/select_role_fix" method="post" autocomplete="off">
                <input type="hidden" name="login" value="<?= $login ?>">
                <select class="form-control input-lg" name="role" required="true">
                    <option value="default"><?= $this->lang->line('user_default_role'); ?></option>
                    <option value="superuser"><?= $this->lang->line('user_superuser_role'); ?></option>
                </select>
                <br/>
                <p class="pull-right">
                    <button type="submit" class="btn btn-success btn-lg pull-right"><?= $this->lang->line('sign_in'); ?></button>
                </p>
            </form>
                
        </div>
    </div>

<?php require_once(APPPATH.'views/required/required_notauth_footer.php'); ?>