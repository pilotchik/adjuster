<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>



<div class="modal fade" id="changePsswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>profile/change_pass">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_password_changing'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="input1"><?= $this->lang->line('user_current_pass'); ?></label>
                        <input type="password" class="form-control" id="input1" placeholder="<?= $this->lang->line('user_current_pass_input'); ?>" name="current_pass" required="true">
                    </div>
                    <div class="form-group required">
                        <label for="input2"><?= $this->lang->line('user_new_pass'); ?></label>
                        <input type="password" class="form-control" id="input2" placeholder="<?= $this->lang->line('user_new_pass_input'); ?>" name="new_pass" required="true">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success" id="profileBtnChangePasswordSaveID"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" id="profileBtnChangePasswordCancelID" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Изменение аватара -->
<div class="modal fade" id="changeAvatarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>profile/change_avatar">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_avatar_editing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label style="font-weight: initial;"><?= $this->lang->line('user_avatar_webconf'); ?></label>
                            <img class="" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/<?= $user['avatar'] ?>?t=<?= time() ?>" id="cropbox2" >
                            <input type="hidden" name="avatar_conf[]" value="" id="avatar_conf_cropX">
                            <input type="hidden" name="avatar_conf[]" value="" id="avatar_conf_cropY">
                            <input type="hidden" name="avatar_conf[]" value="" id="avatar_conf_cropW">
                            <input type="hidden" name="avatar_conf[]" value="" id="avatar_conf_cropH">
                        </div>
                    
                        <div class="col-sm-6">
                            <label style="font-weight: initial;display: inherit;"><?= $this->lang->line('user_avatar_small'); ?></label>
                            <img class="" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/<?= $user['avatar'] ?>?t=<?= time() ?>" id="cropbox3" >
                            <input type="hidden" name="avatar_small[]" value="" id="avatar_small_cropX">
                            <input type="hidden" name="avatar_small[]" value="" id="avatar_small_cropY">
                            <input type="hidden" name="avatar_small[]" value="" id="avatar_small_cropW">
                            <input type="hidden" name="avatar_small[]" value="" id="avatar_small_cropH">
                        </div>
                    </div>
                    
                    <div class="row" style="display: none;">
                        <div class="col-sm-12">
                            <h4><?= $this->lang->line('user_avatar_big'); ?></h4>
                            <img class="" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/<?= $user['avatar'] ?>?t=<?= time() ?>" id="cropbox1" >
                            <input type="hidden" name="avatar_big[]" value="" id="avatar_big_cropX">
                            <input type="hidden" name="avatar_big[]" value="" id="avatar_big_cropY">
                            <input type="hidden" name="avatar_big[]" value="" id="avatar_big_cropW">
                            <input type="hidden" name="avatar_big[]" value="" id="avatar_big_cropH">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success" id="profileBtnChangeAvatarSaveID"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="profileBtnChangeAvatarCancelID"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Удаление аватара -->
<div class="modal fade" id="removeAvatarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>profile/remove_avatar">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_avatar_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('user_avatar_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger" id="profileBtnRemoveAvatarConfirmID"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="profileBtnRemoveAvatarCancelID"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>








<ul class="nav nav-tabs">
    <li <?= !isset($active_tab) ? 'class="active"' : '' ?>><a data-toggle="tab" href="#home"><?= $this->lang->line('user_info'); ?></a></li>
    <li <?= isset($active_tab) && $active_tab == 'contacts' ? 'class="active"' : '' ?>><a data-toggle="tab" href="#contacts"><?= $this->lang->line('user_contacts'); ?></a></li>
    <li <?= isset($active_tab) && $active_tab == 'settings' ? 'class="active"' : '' ?>><a data-toggle="tab" href="#settings"><?= $this->lang->line('user_settings'); ?></a></li>
</ul>

<div class="tab-content">
    <div id="home" class="tab-pane fade <?= !isset($active_tab) ? 'in active' : '' ?>">
        
        <div class="card users-table-card">
    
            <div class="content">
                
                <form class="form-horizontal" action="<?= base_url() ?>profile/update_main" method="post" enctype="multipart/form-data">
                    <div class="form-group required">
                        <label for="inputsurname1" class="col-sm-3 control-label"><?= $this->lang->line('user_surname'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= htmlspecialchars($user['surname']) ?>" name="surname" id="inputsurname1" placeholder="<?= $this->lang->line('user_surname'); ?>" required="true">
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="formError"><?= form_error('surname'); ?></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="inputname2" class="col-sm-3 control-label"><?= $this->lang->line('user_name'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= htmlspecialchars($user['name']) ?>" name="name" id="inputname2" placeholder="<?= $this->lang->line('user_name'); ?>" required="true">
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="formError"><?= form_error('name'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input3" class="col-sm-3 control-label"><?= $this->lang->line('user_middlename'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= htmlspecialchars($user['middlename']) ?>" name="middlename" id="input3" placeholder="<?= $this->lang->line('user_middlename'); ?>">
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="formError"><?= form_error('middlename'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= $this->lang->line('user_avatar'); ?></label>
                        <?php if ($user['small_avatar_status']) { ?>
                            <div class="col-sm-9">
                                <div class="row" style="padding: 5px 15px;">
                                    <div style="width:260px;float:left">
                                        <label style="font-weight: initial;"><?= $this->lang->line('user_avatar_webconf'); ?></label>
                                        <img class="conf-user-avatar" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/avatar_<?= $user['id'] ?>_conf.png?t=<?= time() ?>" title="<?= $this->lang->line('user_avatar_big'); ?>">
                                    </div>

                                    <div style="width:240px;float:left">
                                        <label style="font-weight: initial;"><?= $this->lang->line('user_avatar_small'); ?></label><br>
                                        <img class="small-user-avatar" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/avatar_<?= $user['id'] ?>_small.png?t=<?= time() ?>" title="<?= $this->lang->line('user_avatar_small'); ?>">
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="form-group">

                            <?php } else {?>
                                <div class="col-sm-9">
                                    <?php if($user['avatar']) { ?>
                                        <img class="bigUserImage" src="<?= base_url() ?>upload/users/<?= $user['id'] ?>/<?= $user['avatar'] ?>?t=<?= time() ?>">
                                    <?php } else { ?>
                                        <p class="form-control-static"><?= $this->lang->line('user_avatar_not_found') ?></p>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <?php if ($user['avatar']) { ?>
                                <div class="col-sm-9 col-sm-offset-3" style="padding: 0px 0px 15px 15px;">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="$('#changeAvatarModal').modal('show');">
                                        <span class="glyphicon glyphicon-edit"></span> <?= $this->lang->line('btn_edit_avatar'); ?>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="$('#removeAvatarModal').modal('show');">
                                        <span class="glyphicon glyphicon-remove"></span> <?= $this->lang->line('btn_remove_avatar'); ?>
                                    </button>
                                </div>
                            <?php } ?>
                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="file" class="form-control" name="userfile" size="1" accept="image/*" id="profileInputAvatarID">
                            </div>
                            <div class="help-block col-sm-9 col-sm-offset-3"><?= $this->lang->line('file_does_not_exceeds_size'); ?> 10 <?= $this->lang->line('mb'); ?></div>
                            <div class="help-block col-sm-9 col-sm-offset-3"><?= $this->lang->line('file_does_have_file_types'); ?> gif, jpeg, png, jpg</div>
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="formError"><?= form_error('userfile'); ?></div>
                            </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= $this->lang->line('user_birthday'); ?></label>
                        <div id="datetimepicker1" class="col-sm-3">
                            <input type="text" class="form-control" placeholder="<?= $this->lang->line('user_birthday'); ?>" />
                        </div>
                        <input type="hidden" name="birthday" value="<?= $user['birthday'] ?>" id="timestampID">
                    </div>




                    <!-- Поля, добавленные в конфигураторе -->

                    <?php foreach($additionalFields['profile'] as $field): ?>

                        <div class="form-group <?= $field['required'] ? 'required' : '' ?>">
                            <label for="field<?= $field['id'] ?>ID" class="col-sm-3 control-label"><?= $field['name'] ?></label>
                            <div class="col-sm-9">
                                <?php switch ($field['type']) {
                                    case 'text': 
                                        ?>
                                        <input type="text" class="form-control" value="<?= htmlspecialchars($field['value']) ?>" name="field_<?= $field['id'] ?>" id="field<?= $field['id'] ?>ID" placeholder="<?= htmlspecialchars($field['name']) ?>" <?= $field['required'] ? 'required="true"' : '' ?>>
                                        <?php break;
                                    case 'textarea':
                                        ?>
                                        <textarea class="form-control" name="field_<?= $field['id'] ?>" rows="3" id="field<?= $field['id'] ?>ID"><?= $field['value'] ?></textarea>
                                        <?php break;
                                } ?>

                            </div>

                            <div class="help-block col-sm-9 col-sm-offset-3"><?= $field['description'] ?></div>
                        </div>


                    <?php endforeach; ?>
                    <!-- -->


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm"><?= $this->lang->line('btn_save'); ?></button>
                            <?= !$this->config->item('config_use_ldap') ? '<button type="button" class="btn btn-primary btn-sm" id="changePasswordButtonID">'.$this->lang->line('btn_change_password').'</button>' : '' ?>
                        </div>
                    </div>

                </form>
    
            </div>
            
        </div>
                
    </div>
     
    
    <div id="contacts" class="tab-pane fade <?= isset($active_tab) && $active_tab == 'contacts' ? 'in active' : '' ?>">
     
        <div class="card users-table-card">
    
            <div class="content">
        
                <form class="form-horizontal" action="<?= base_url() ?>profile/update_contacts" method="post" enctype="multipart/form-data">
                    <div class="form-group required">
                        <label for="input11" class="col-sm-3 control-label"><?= $this->lang->line('user_email'); ?></label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" value="<?= $user['email'] ?>" name="email" id="input11" placeholder="<?= $this->lang->line('user_email'); ?>" required="true">
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="formError"><?= form_error('email'); ?></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input12" class="col-sm-3 control-label"><?= $this->lang->line('user_phone'); ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?= htmlspecialchars($user['phone']) ?>" name="phone" id="input12" placeholder="<?= $this->lang->line('user_phone'); ?>">
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="formError"><?= form_error('phone'); ?></div>
                        </div>
                    </div>

                    <!--
                        <div class="form-group">
                            <label for="input13" class="col-sm-3 control-label"><?= $this->lang->line('user_comment'); ?></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" value="<?= htmlspecialchars($user['comment']) ?>" name="comment" rows="3" id="input13"></textarea>
                            </div>
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="formError"><?= form_error('comment'); ?></div>
                            </div>
                        </div>
                    -->

                    <!-- Поля, добавленные в конфигураторе -->

                    <?php foreach($additionalFields['contacts'] as $field): ?>
                        <div class="form-group <?= $field['required'] ? 'required' : '' ?>">
                            <label for="field<?= $field['id'] ?>ID" class="col-sm-3 control-label"><?= $field['name'] ?></label>
                            <div class="col-sm-9">
                                <?php switch ($field['type']) {
                                    case 'text': 
                                        ?>
                                        <input type="text" class="form-control" value="<?= htmlspecialchars($field['value']) ?>" name="field_<?= $field['id'] ?>" id="field<?= $field['id'] ?>ID" placeholder="<?= htmlspecialchars($field['name']) ?>" <?= $field['required'] ? 'required="true"' : '' ?>>
                                        <?php break;
                                    case 'textarea':
                                        ?>
                                        <textarea class="form-control" name="field_<?= $field['id'] ?>" rows="3" id="field<?= $field['id'] ?>ID"><?= $field['value'] ?></textarea>
                                        <?php break;
                                } ?>

                            </div>

                            <div class="help-block col-sm-9 col-sm-offset-3"><?= $field['description'] ?></div>
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="formError"><?= form_error('userfile'); ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <!-- -->


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm" id="profileBtnSubmitID"><?= $this->lang->line('btn_save'); ?></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        
    </div> 
      
    
    
    <div id="settings" class="tab-pane fade <?= isset($active_tab) && $active_tab == 'settings' ? 'in active' : '' ?>">
        
        <div class="card users-table-card">
    
            <div class="content">
                <p><?= $this->lang->line('user_settings_desc'); ?></p>
                <form class="form-horizontal" action="<?= base_url() ?>profile/update_settings" method="post">

                    <?php foreach($notification_types as $notifyType) : ?>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox" title="<?= $this->lang->line('user_settings_'.$notifyType.'_full'); ?>">
                                    <label>
                                        <input type="checkbox" name="settings[<?= $notifyType ?>]" value="1" <?= isset($settings[$notifyType]) && $settings[$notifyType] ? 'checked' : '' ?>><?= $this->lang->line('user_settings_'.$notifyType); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm" id="profileBtnSubmitSettingsID"><?= $this->lang->line('btn_save'); ?></button>
                        </div>
                    </div>

                </form>
                
            </div>
            
        </div>
        
    </div> 
       
</div>


<script>
    $('#changePasswordButtonID').click(function(){
        $('#changePsswordModal').modal('show');
    })
   
</script>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>


<script src="<?= base_url() ?>assets/js/jquery.cropbox.js"></script>
<script>
    jQuery(document).ready(function() {
        
        
        <?= !$user['small_avatar_status'] && $user['avatar'] ? '$(\'#changeAvatarModal\').modal(\'show\');' : '' ?>
        
        $('#cropbox1').cropbox({
            width: 400,
            height: 400
        }, function() {
            //on load
        }).on('cropbox', function(e, data) {
            console.log(data);
            $('#avatar_big_cropX').val(data.cropX);
            $('#avatar_big_cropY').val(data.cropY);
            $('#avatar_big_cropW').val(data.cropW);
            $('#avatar_big_cropH').val(data.cropH);
        });
        
        $('#cropbox2').cropbox({
            width: 180,
            height: 135,
        }, function() {
            //on load
        }).on('cropbox', function(e, data) {
            console.log(data);
            $('#avatar_conf_cropX').val(data.cropX);
            $('#avatar_conf_cropY').val(data.cropY);
            $('#avatar_conf_cropW').val(data.cropW);
            $('#avatar_conf_cropH').val(data.cropH);
        });
        
        $('#cropbox3').cropbox({
            width: 80,
            height: 80,
            label: ''
            
        }, function() {
            //on load
        }).on('cropbox', function(e, data) {
            console.log(data);
            $('#avatar_small_cropX').val(data.cropX);
            $('#avatar_small_cropY').val(data.cropY);
            $('#avatar_small_cropW').val(data.cropW);
            $('#avatar_small_cropH').val(data.cropH);
        });
        
        
    });
    
    
    $(function () {
        $('#datetimepicker1').datetimepicker({
            language: 'ru',
            showClose: true,
            viewMode: 'years',
            pickTime: false,
            disabledHours:true,
            format: 'DD.MM.YYYY',
            defaultDate:'<?= date("d.m.Y H:i",$user['birthday']) ?>'
        });    
        
    });
    
    $("#datetimepicker1").on("dp.change", function (e) {
        $('#timestampID').val(e.date.format("X"));
    });
</script>