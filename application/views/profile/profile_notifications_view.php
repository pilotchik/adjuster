<?php require_once(APPPATH.'views/required/required_new_header.php'); ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeNotificationsConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>profile/notifications_remove">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('notification_notifications_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <?= $this->lang->line('notification_notifications_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="card users-table-card">
    
    <div class="content">
    
        <table class="table table-striped sortedTable withFilter table-responsive">
            <thead>
                <tr>
                    <th class="date-short-td"><i class="fa  fa-calendar" title="<?= $this->lang->line('notification_date'); ?>"></i></th>
                    <th class="mini-avatar-td"></th>
                    <th class="user-name-short-td"><?= $this->lang->line('notification_author'); ?></th>
                    <th><?= $this->lang->line('notification_data'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($notifications)) {
                    foreach($notifications as $notify): ?>
                        <tr class="<?= $notify['status'] == 1 ? 'success' : '' ?>">
                            <td class="center-align">
                                <span class="hiddenSpan"><?= $notify['ts'] ?></span>
                                <span class="date-inactive-status"><?= $notify['date'] ?></span>
                            </td>
                            <td class="center-align miniAvatarTD">
                                <?php if (!empty($notify['author'])) { ?>
                                    <a class="photo photo_<?= $notify['author_id'] ?>" href="<?= $notify['author']['avatarURL'] ?>" title="<?= $notify['authorUserFullName'] ?>">
                                        <img class="smallUserAvatarInTables" src="<?= $notify['author']['avatarURL_small'] ?>">
                                    </a>
                                <?php } ?>
                            </td>

                            <td><span title="<?= $notify['authorUserFullName'] ?>"><?= $notify['authorUserName'] ?></td>
                            <td>
                                <?= $notify['subject'] ?>
                                <div class="user-subheader notify-subheader"><?= htmlspecialchars($notify['body']) ?></div>
                            </td>
                        </tr>
                    <?php endforeach;
                } else { ?>
                    <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
    

<script>
    
    jQuery(document).ready(function() {
    
        $("[data-toggle=popover]").popover();
    });

</script>


<?php require_once(APPPATH.'views/required/required_new_footer.php'); ?>