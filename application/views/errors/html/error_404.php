<?php
defined('BASEPATH') OR exit('No direct script access allowed');

preg_match('/\/*.php/U', $_SERVER['SCRIPT_NAME'],$hostArr);
$host = mb_substr($_SERVER['SCRIPT_NAME'],0,mb_strpos($_SERVER['SCRIPT_NAME'],'index.php'));

?>

<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="images/favicon.ico">
        <title>404</title>
 
        <!-- Bootstrap core CSS -->
        <link href="<?= $host ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $host ?>assets/css/art.irida.css" rel="stylesheet">
    
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?= $host ?>assets/js/ie-emulation-modes-warning.js"></script>

        <style>
            p {text-align: center !important;}
        </style>
        
    </head>

    <body class="splash">
        
        
        <div class="row">
            <div class="col-md-4 col-md-offset-4  col-sm-4 col-sm-offset-4  text-center">
                
		<h1><?php echo $heading; ?></h1>
                <center>
                    <h4><?php echo $message; ?></h4>
                </center>
                
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <script src="<?= $host ?>js/jquery.min.js"></script>
        <script src="<?= $host ?>js/bootstrap.min.js"></script>
            
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="<?= $host ?>assets/js/vendor/holder.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?= $host ?>assets/js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>