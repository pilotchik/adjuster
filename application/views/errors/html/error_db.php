<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico">
        <title><?php echo $heading; ?></title>
 
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/art.irida.css" rel="stylesheet">
    
        
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?= base_url() ?>assets/js/ie-emulation-modes-warning.js"></script>

    </head>

    <body class="splash">
        
        <div class="row">
            <div class="col-md-12 splashpik"></div>
        </div>
        
        <p>&nbsp;</p>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                
                <div id="errorContainer">
                    <h1><?php echo $heading; ?></h1>
                    <?php echo $message; ?>
                </div>
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
            
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="<?= base_url() ?>assets/js/vendor/holder.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?= base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>
    
    </body>
</html>