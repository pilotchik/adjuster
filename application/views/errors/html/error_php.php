<?php
defined('BASEPATH') OR exit('No direct script access allowed');

preg_match('/\/*.php/U', $_SERVER['SCRIPT_NAME'],$hostArr);
$host = mb_substr($_SERVER['SCRIPT_NAME'],0,mb_strpos($_SERVER['SCRIPT_NAME'],'index.php'));

?>

<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="images/favicon.ico">
        <title>404</title>
 
        <!-- Bootstrap core CSS -->
        <link href="<?= $host ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $host ?>assets/css/art.irida.css" rel="stylesheet">
    
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?= $host ?>assets/js/ie-emulation-modes-warning.js"></script>

    </head>

    <body class="splash">
        
        <div class="row">
            <div class="col-md-4 col-md-offset-4  col-sm-4 col-sm-offset-4  text-center">
                <h3>Ошибка скрипта</h3>

                <p>Тип: <?php echo $severity; ?></p>
                <p>Сообщение:&nbsp;<strong><?php echo $message; ?></strong></p>
                <p>Файл: <?php echo $filepath; ?></p>
                <p>Строка: <?php echo $line; ?></p>

                <?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>
                        <hr>
                        <p>Backtrace:</p>
                        <?php foreach (debug_backtrace() as $error): ?>

                                <?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

                                        <p style="margin-left:10px">
                                        Файл: <?php echo $error['file'] ?><br />
                                        Строка: <?php echo $error['line'] ?><br />
                                        Функция: <?php echo $error['function'] ?>
                                        </p>

                                <?php endif ?>

                        <?php endforeach ?>

                <?php endif ?>

                
            </div>
        </div>
    </body>
</html>
        