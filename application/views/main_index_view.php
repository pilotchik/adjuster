<?php require_once(APPPATH.'views/required/required_notauth_new_header.php'); ?>


    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">

                <!--      Wizard container        -->
                <div class="wizard-container">

                    <div class="card wizard-card" data-color="blue" id="wizardProfile">
                
                        
                            <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->

                            <div class="wizard-header text-center" style="margin-bottom: 20px;">
                                <h3 class="wizard-title">Проверка знаний</h3>
                                <p class="category">по дисциплине «Регулирование движения военных колонн»</p>
                            </div>

                            <div class="wizard-navigation">
                                <div class="progress-with-circle">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 13.5%;"></div>
                                </div>
                            
                                <ul class="nav nav-pills">
                                    <li class="active nav-wizard-pill">
                                        <a href="#about" data-toggle="tab" onclick="$('.progress-bar').css('width','13.5%')">
                                            <div class="icon-circle">
                                                <i class="ti-user"></i>
                                            </div>
                                            <?= $this->lang->line('main_sign_in_title_short') ?>
                                        </a>
                                    </li>
                                    <li class="nav-wizard-pill">
                                        <a href="#account" data-toggle="tab" onclick="$('.progress-bar').css('width','38.5%')">
                                            <div class="icon-circle">
                                                <i class="ti-settings"></i>
                                            </div>
                                            <?= $this->lang->line('registration'); ?>
                                        </a>
                                    </li>
                                    <li class="nav-wizard-pill">
                                        <a href="#address" data-toggle="tab" onclick="$('.progress-bar').css('width','63.5%')">
                                            <div class="icon-circle">
                                                <i class="ti-email"></i>
                                            </div>
                                            <?= $this->lang->line('restore_access_short'); ?>
                                        </a>
                                    </li>
                                    <li class="nav-wizard-pill">
                                        <a href="#feedback" data-toggle="tab" onclick="$('.progress-bar').css('width','86.5%')">
                                            <div class="icon-circle">
                                                <i class="ti-comments-smiley"></i>
                                            </div>
                                            <?= $this->lang->line('feedback_title_short'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="about">
                                        <div class="row">
                                            
                                            <div class="col-sm-12">
                                                <h5 class="info-text"><?= $this->lang->line('main_sign_in_title') ?></h5>
                                            </div>
                                            
                                            <form action="<?= base_url() ?>main/signin" method="post" autocomplete="off">
                                                
                                                <?php if (!$this->config->item('config_use_ldap')) { ?>
                                                
                                                    <div class="col-sm-10 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label><?= $this->lang->line('user_email_or_login'); ?></label>
                                                            <input class="form-control" type="text" value="<?= set_value('username'); ?>" name="username" placeholder="<?= $this->lang->line('user_email_or_login'); ?>" id="signinInputLoginID"> 
                                                            <label class="formError error"><?= form_error('username'); ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-10 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label><?= $this->lang->line('password'); ?></label>
                                                            <input class="form-control" type="password" value="<?= set_value('password'); ?>" name="password" placeholder="<?= $this->lang->line('password'); ?>" id="signinInputPasswordID">
                                                            <label class="formError error"><?= form_error('password'); ?></label>
                                                        </div>
                                                    </div>
                                                
                                                <?php } ?>
                                                
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-success" id="signinButtonConfirmID"><?= $this->lang->line('sign_in'); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="account">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h5 class="info-text"><?= $this->lang->line('main_sign_up_title') ?></h5>
                                            </div>
                                        
                                            <form action="<?= base_url() ?>main/signup" method="post" autocomplete="off">
                                                <div class="col-sm-3 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('user_surname'); ?> <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="text" name="surname" value="<?= set_value('surname'); ?>" placeholder="<?= $this->lang->line('user_surname_example'); ?>" required="true" id="signupInputSurnameID">
                                                        <div class="formError error"><?= form_error('surname'); ?></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('user_name'); ?> <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="text" name="name" value="<?= set_value('name'); ?>" placeholder="<?= $this->lang->line('user_name_example'); ?>" required="true" id="signupInputNameID">
                                                        <div class="formError error"><?= form_error('name'); ?></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 ">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('user_middlename'); ?></label>
                                                        <input class="form-control" type="text" name="middlename" value="<?= set_value('middlename'); ?>" placeholder="<?= $this->lang->line('user_middlename_example'); ?>" id="signupInputMiddlenameID">
                                                        <div class="formError error"><?= form_error('middlename'); ?></div>
                                                    </div>
                                                </div>
                                                
												<div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label>Группа <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="text" name="group" value="<?= set_value('group'); ?>" placeholder="Название группы" required="true" id="signupInputMailID" maxlength="255">
                                                        <div class="formError error"><?= form_error('group'); ?></div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('user_email'); ?> <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="email" name="email" value="<?= set_value('email'); ?>" placeholder="<?= $this->lang->line('user_email'); ?>" required="true" id="signupInputMailID">
                                                        <div class="formError error"><?= form_error('email'); ?></div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('password'); ?> <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="password" name="password" value="<?= set_value('password'); ?>" placeholder="<?= $this->lang->line('password'); ?>" required="true" id="signupInputPasswordID">
                                                        <div class="formError error"><?= form_error('password'); ?></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-success  btn-group-justified" id="signupButtonConfirmID"><?= $this->lang->line('sign_up'); ?></button>
                                                    </div>
                                                </div>
                                                
                                            </form>
                
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="address">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h5 class="info-text"><?= $this->lang->line('restore_access'); ?></h5>
                                            </div>
                                            
                                             <form action="<?= base_url() ?>main/restoresend" method="post" autocomplete="off">
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label><?= $this->lang->line('user_email'); ?> <small>(<?= $this->lang->line('required') ?>)</small></label>
                                                        <input class="form-control" type="email" name="email" value="<?= set_value('email'); ?>" placeholder="<?= $this->lang->line('user_email'); ?>" required="true" id="restoreInputMailID">
                                                        <div class="formError error"><?= form_error('email'); ?></div>
                                                    </div>
                                                </div>
                                                    
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-success" id="restoreButtonConfirmID"><?= $this->lang->line('restore_access_send_link'); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                           
                                        </div>
                                    </div>
                                    
                                    <div class="tab-pane" id="feedback">
                                        <div class="row">
                                            
                                            <div class="col-sm-12">
                                                <h5 class="info-text"><?= $this->lang->line('feedback_title') ?></h5>
                                            </div>
                                            
                                            <form action="<?= base_url() ?>main/feedbackform_send" method="post" autocomplete="off">
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <textarea class="form-control" rows="5" name="problem" value="<?= htmlspecialchars(set_value('problem')); ?>" placeholder="<?= $this->lang->line('feedback_problem'); ?>" required="true" id="feedbackInputProblemID"></textarea>
                                                        <div class="formError error"><?= form_error('problem'); ?></div>
                                                    </div>
                                                </div>
                                                        
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-success btn-group-justified" id="feedbackButtonConfirmID"><?= $this->lang->line('btn_send'); ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="wizard-footer">
                                    <div class="clearfix"></div>
                                </div>
                          
                        </div>
                    </div> <!-- wizard container -->
                </div>
        </div><!-- end row -->
    </div> <!--  big container -->

            
<?php require_once(APPPATH.'views/required/required_notauth_new_footer.php'); ?>
