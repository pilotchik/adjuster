<?php require_once(APPPATH.'views/required/required_notauth_header.php'); ?>
    
    <div class="row">
        <div class="col-sm-12">
            <form action="<?= base_url() ?>main/feedbackform_send" method="post" autocomplete="off">
                <textarea class="form-control input-lg" rows="5" name="problem" value="<?= htmlspecialchars(set_value('problem')); ?>" placeholder="<?= $this->lang->line('feedback_problem'); ?>" required="true"></textarea>
                <div class="formError"><?= form_error('problem'); ?></div>
                <br/>

                <button type="submit" class="btn btn-success btn-lg btn-group-justified"><?= $this->lang->line('btn_send'); ?></button>
            </form>

        </div>
    </div>

    <div class="row mainFormLinksContainer">
        <div class="col-sm-12 text-right">
            <a href="<?= base_url() ?>"><?= $this->lang->line('btn_to_main') ?></a>
        </div>
    </div>

<?php require_once(APPPATH.'views/required/required_notauth_footer.php'); ?>