<?php require_once(APPPATH.'views/required/required_notauth_header.php'); ?>
    
    <div class="row">
        <div class="col-sm-12">
            <form action="<?= base_url() ?>main/restorecomplete" method="post" autocomplete="off">
                <input type="hidden" name="user_id" value="<?= $user['id'] ?>">
                <h3 class="pull-left" style="color:white;"><?= $this->lang->line('restore_access_user_new_pass'); ?></h3>

                <div class="row">
                    <input class="form-control input-lg col-xs-12" type="password" name="new_pass" placeholder="<?= $this->lang->line('user_new_pass'); ?>" required="true">
                    <div class="col-xs-12" style="margin-top:10px;">
                        <p class="pull-right">
                            <button type="submit" class="btn btn-success btn-lg"><?= $this->lang->line('btn_restore_pass'); ?></button>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row mainFormLinksContainer">
        <div class="col-sm-12 text-right">
            <a href="<?= base_url() ?>"><?= $this->lang->line('btn_to_main') ?></a>
        </div>
    </div>


<?php require_once(APPPATH.'views/required/required_notauth_footer.php'); ?>