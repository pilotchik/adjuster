<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico">
        <title><?= $config_title ?></title>
 
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/art.irida.css" rel="stylesheet">
    
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?= base_url() ?>assets/js/ie-emulation-modes-warning.js"></script>

    </head>

    <body class="splash">
  
        <div class="row mainForm">
            <div class="col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-3 col-lg-6 col-lg-offset-3 text-center">
                
                <?php if (isset($message) || isset($system_error) && $system_error != '') { ?>
                    <div class="alert alert-<?= isset($message) ? 'info' : $system_error_type ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <?= isset($message) ? $message : $system_error ?>
                    </div>
                <?php } ?>

                <div class="alert alert-danger alert-dismissible" role="alert" id="cookieAlert" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <?= $this->lang->line('cookie_not_enabled'); ?>
                </div>
