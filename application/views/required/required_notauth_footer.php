            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/js/irida.js"></script>
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
            
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="<?= base_url() ?>assets/js/vendor/holder.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?= base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>
        
        <script>
        
            $(document).ready(function() {
                setCookie('iridaCookieChecking', 1);
                
                if(getCookie('iridaCookieChecking') == null) {
                    $('#cookieAlert').css('display','');
                }
                
            })
            
        </script>
        
        
    </body>
</html>