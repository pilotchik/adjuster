<html>
    <head>
        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico">

        <title><?= $this->config->item('config_view_version_in_title') ? $this->config->item('config_version').' | ': '' ?><?= $config_title ?></title>
 	
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/irida.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/colorbox.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" />
        <link href="<?= base_url() ?>assets/css/art.irida.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/room.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.cropbox.css" />
   
        <script>
            BASE_URL = '<?= base_url() ?>';
            
            var ROOM_GLOBAL_TRANSLATOR = {
                room_users_online_now : '<?= $this->lang->line('room_users_online_now') ?>',
                room_users_online_now_list : '<?= $this->lang->line('room_users_online_now_list') ?>'
            }
            
            var ROOM_REAL_CANDIDATES = {};
            var ROOM_CANDIDATES = {};
            
            var LOCAL_USER_ID = <?= $this->session->userdata['userID'] ?>;
            
        </script>
        
    </head>
    
    <body>

        <div id="iridaRoomBackgroundID">
            <img src="" style="display: none;">
            <img src="" style="display: none;">
        </div>

        
        
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url() ?>">
                    <img src="<?= base_url() ?>assets/images/logo_new.png">
                </a>
            </div>

            <span class="pull-right headart">
                    
                <ul class="nav navbar-nav navbar-right">
                    
                    <li class="iridaAdminHeaderLI">
                        <button type="submit" class="btn btn-info btn-sm viewNotificationBigBTN" title="<?= $this->lang->line('user_notifications') ?>" onclick="$('#viewMyNotificationsModal').modal('show')">
                            <span class="glyphicon glyphicon-bell"></span> <span class="badge">0</span>
                        </button>
                    </li>
                    
                    <li class="iridaAdminHeaderLI">
                        <a type="submit" class="btn btn-warning btn-sm viewNotesBigBTN topstatusNote" style="display:none;" title="<?= $this->lang->line('notes_top_status_notifications') ?>" href="<?= base_url() ?>notes">
                            <?= $this->lang->line('notes_top_status_notifications_title') ?> <span class="badge">0</span>
                        </a>
                    </li>
                    
                    <li class="iridaAdminHeaderLI">
                        <button type="submit" class="btn btn-success btn-sm inviteInFastRoomBigBTN" onclick="$('#addTemporaryRoomModal').modal('show')">
                            <?= $this->lang->line('btn_room_invited_set_title') ?> <span class="badge">0</span>
                        </button>
                    </li>
                    
                    
                    <li class="dropdown <?= $globalActiveLink == 'profile' ? 'active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="<?= $this->session->userdata('shortName') ?>">
                            <?= mb_strlen($this->session->userdata('shortName')) < 80 ? $this->session->userdata('shortName') : mb_substr($this->session->userdata('shortName'),0,75).'...' ?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= base_url() ?>profile">
                                    <?php if (file_exists('./upload/users/'.$this->session->userdata('userID').'/'.'avatar_'.$this->session->userdata('userID').'_small.png')) { ?>
                                        <img class="smallUserAvatarInHeader" src="<?= base_url() ?>upload/users/<?= $this->session->userdata('userID') ?>/avatar_<?= $this->session->userdata('userID') ?>_small.png">
                                    <?php } ?>
                                    <?= $this->lang->line('profile'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#" class="disabled"><?= $this->session->userdata('userTypeName') ?></a></li>
                            <?php if ($this->session->userdata('doubleRole')) { ?>
                                <li><a href="<?= base_url() ?>main/select_role/<?= $this->session->userdata('login') ?>"><?= $this->lang->line('change_role') ?></a></li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= base_url() ?>profile/notifications">
                                    <span class="glyphicon glyphicon-bell"></span>&nbsp;<?= $this->lang->line('user_notifications') ?>
                                </a>
                            </li>
                            <li><a href="<?= base_url() ?>help"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;<?= $this->lang->line('help') ?></a></li>
                            <?php if (!$this->config->item('config_use_ldap')) { ?>
                                <li><a href="<?= base_url() ?>main/deauth"><span class="glyphicon glyphicon-log-out"></span>&nbsp;<?= $this->lang->line('sign_out') ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
                    
                    
            </span>
            
        </div>
    </nav>
        
        
    <!-- Добавление в экспресс-комнату -->
    <div class="modal fade" id="addTemporaryRoomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="<?= base_url() ?>rooms/create_room/temporary" method="POST" class="form-horizontal form-responsive" onsubmit="$('.submitBTNID').css('display','none');$('.spinner-sm').css('display','')">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <h4 class="modal-title"><?= $this->lang->line('room_temporary_adding'); ?></h4>
                    </div>
                    <div class="modal-body">
                        
                        <div class="form-group">
                            <label class="col-sm-12 text-center"><?= $this->lang->line('room_temporary_invited_users'); ?></label>
                        </div>
                        
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table" id="invitedInTemporaryRoomUsers">
                                    <thead>
                                        <tr>
                                            <th><?= $this->lang->line('user'); ?></th>
                                            <th class="activityStatusTD"><?= $this->lang->line('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        
                        <input type="hidden" name="room_cooperation" value="hangout">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success submitBTNID"><?= $this->lang->line('btn_add_temporary_room') ?></button>
                        <img src="<?= base_url() ?>assets/images/loading_spinner.gif" class="spinner-sm" style="display: none;">
                        <button type="button" class="btn btn-sm btn-default submitBTNID" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    
    <!-- Уведомления о приглашении -->
    <div class="modal fade" id="viewMyNotificationsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('user_notifications_view'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 text-center"><?= $this->lang->line('user_notifications_title'); ?></label>

                        <div class="table-responsive col-sm-12">
                            <table class="table" id="myNotificationsTable">
                                <thead>
                                    <tr>
                                        <th class="activityStatusWithoutTextTD" title="<?= $this->lang->line('actions'); ?>"></th>
                                        <th><?= $this->lang->line('user'); ?></th>
                                        <th><?= $this->lang->line('user_notifications_message'); ?></th>
                                        <th style="width:70px;"><i class="fa fa-calendar"></i></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Информация об ошибке -->
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title" id="alertModalTitle"></h4>
                </div>
                <div class="modal-body" id="alertModalBody">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="top-alert">
	<div class="alert-wrapper">
	    <div class="alert">
		<div class="alert-line">
		    <div class="alert-icon">
			<div class="icon"></div>
		    </div>
	    	    <div class="alert-text"></div></div></div></div></div>
        
        

    <div class="container-fluid">
        <div class="row">