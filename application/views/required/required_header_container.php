<div class="col-sm-4 col-md-4 col-lg-3">
    <div class="sidebar">
        <ul class="nav nav-sidebar">
            <?php if ($this->session->userdata('login') == 'admin' || $this->session->userdata('role') == 'superuser') { ?>
                <li class="menuHeader"><?= $this->lang->line('administrating'); ?></li>
                <li class="<?= $globalActiveLink == 'admin/users' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/users" ><?= $this->lang->line('users_contacts'); ?></a></li>
                <li class="<?= $globalActiveLink == 'admin/log' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/log" ><?= $this->lang->line('system_log'); ?></a></li>
                <li class="<?= $globalActiveLink == 'admin/config' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/config" ><?= $this->lang->line('config'); ?></a></li>
                <li class="<?= $globalActiveLink == 'admin/notifications' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/notifications" ><?= $this->lang->line('notification_department'); ?></a></li>
                <li class="<?= $globalActiveLink == 'admin/log/metrika' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/log/metrika" ><?= $this->lang->line('metrika'); ?></a></li>
                <li class="<?= $globalActiveLink == 'admin/log/feedback' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/log/feedback" ><?= $this->lang->line('feedback_title'); ?></a></li>
                <?php if ($config_components_notes) { ?>
                    <li class="<?= $globalActiveLink == 'notes' ? 'active' : ''; ?>"><a href="<?= base_url() ?>notes" ><?= $this->lang->line('notes'); ?>&nbsp;<span id="notesActiveCountID" class="badge" title="<?= $this->lang->line('note_user_has_notviewed_notes'); ?>"></span></a></li>
                <?php } ?>
                <?php if ($config_components_polls) { ?>
                    <li class="<?= $globalActiveLink == 'admin/polls' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/polls" ><?= $this->lang->line('polls_admin_title'); ?></a></li>
                <?php } ?>

            <?php } elseif ($this->session->userdata('userType') == 'admin') { ?>
                <li class="menuHeader"><?= $this->lang->line('administrating'); ?></li>
                <li class="<?= $globalActiveLink == 'admin/users' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/users" ><?= $this->lang->line('users_contacts'); ?></a></li>

                <?php if ($config_components_posts) { ?>
                    <li class="<?= $globalActiveLink == 'admin/structure' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/structure" ><?= $this->lang->line('structure'); ?></a></li>
                    <li class="<?= $globalActiveLink == 'admin/posts' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/posts" ><?= $this->lang->line('posts'); ?></a></li>
                <?php } ?>

                <?php if ($config_components_seating) { ?>
                    <li class="<?= $globalActiveLink == 'admin/seating' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/seating" ><?= $this->lang->line('seating_menage'); ?></a></li>
                <?php } ?>
                
                <?php if ($config_components_polls) { ?>
                    <li class="<?= $globalActiveLink == 'admin/polls' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/polls" ><?= $this->lang->line('polls_admin_title'); ?></a></li>
                <?php } ?>

            <?php } ?>
                    
                    
            <?php if ($this->session->userdata('userType') == 'admin' || $this->session->userdata('login') == 'admin') { ?>
                <!-------------------------------------------------- УПРАВЛЕНИЕ СПРАВОЧНИКАМИ --------------------------------------------------------------->
                <li class="menuHeader"><?= $this->lang->line('administrating_menage'); ?></li>

                <li class="<?= $globalActiveLink == 'admin/users/profile_config' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/users/profile_config" ><?= $this->lang->line('profile_config'); ?></a></li>
                
                <?php if ($config_components_posts) { ?>
                    <li class="<?= $globalActiveLink == 'admin/posts/posts_config' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/posts/posts_config" ><?= $this->lang->line('posts_config'); ?></a></li>
                <?php } ?>

                <!--
                <li class="<?= $globalActiveLink == 'admin/tags' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/tags" ><?= $this->lang->line('tags_menage'); ?></a></li>
                
                -->
                
                <li class="<?= $globalActiveLink == 'admin/sked' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/sked" ><?= $this->lang->line('sked_menage'); ?></a></li>

                <?php if ($config_components_seating) { ?>
                    <li class="<?= $globalActiveLink == 'admin/seating/equipment' ? 'active' : ''; ?>"><a href="<?= base_url() ?>admin/seating/equipment" ><?= $this->lang->line('seating_equipment'); ?></a></li>
                <?php } ?>
                
            <?php }
            if ($this->session->userdata('login') != 'admin' && $this->session->userdata('role') != 'superuser') { ?>
                <li class="menuHeader"><?= $this->lang->line('users'); ?></li>
                <li class="<?= $globalActiveLink == 'usersl' ? 'active' : ''; ?>"><a href="<?= base_url() ?>usersl" ><?= $this->lang->line('users_contacts'); ?></a></li>

                <!--
                <?php if ($config_components_posts) { ?>
                    <li class="<?= $globalActiveLink == 'structurel' ? 'active' : ''; ?>"><a href="<?= base_url() ?>structurel" ><?= $this->lang->line('structure'); ?></a></li>
                <?php } ?>

                <?php if ($config_components_seating) { ?>
                    <li class="<?= $globalActiveLink == 'seatingl' ? 'active' : ''; ?>"><a href="<?= base_url() ?>seatingl" ><?= $this->lang->line('seating_scheme'); ?></a></li>
                <?php } ?>
                -->
                
                <?php if ($config_components_polls) { ?>
                    <li class="<?= $globalActiveLink == 'polls' ? 'active' : ''; ?>"><a href="<?= base_url() ?>polls" ><?= $this->lang->line('polls'); ?>&nbsp;<span id="pollsActiveCountID" class="badge" title="<?= $this->lang->line('poll_user_has_notfinished_polls'); ?>"></span></a></li>
                <?php } ?>
 
                <?php if ($config_components_notes) { ?>
                    <li class="<?= $globalActiveLink == 'notes' ? 'active' : ''; ?>"><a href="<?= base_url() ?>notes" ><?= $this->lang->line('notes'); ?>&nbsp;<span id="notesActiveCountID" class="badge" title="<?= $this->lang->line('note_user_has_notviewed_notes'); ?>"></span></a></li>
                <?php } ?>


                <li class="menuHeader"><?= $this->lang->line('conference'); ?></li>
                <li class="<?= $globalActiveLink == 'rooms' && $this->uri->segment(2) != 'project' ? 'active' : ''; ?>"><a href="<?= base_url() ?>rooms" ><?= $this->lang->line('rooms'); ?></a></li>
                <li class="<?= $globalActiveLink == 'projects' || $this->uri->segment(2) == 'project' ? 'active' : ''; ?>"><a href="<?= base_url() ?>projects"><?= $this->lang->line('projects'); ?></a></li>
                <li class="<?= $globalActiveLink == 'schedule' ? 'active' : ''; ?>"><a href="<?= base_url() ?>schedule" ><?= $this->lang->line('schedule'); ?></a></li>

                <!--<li class="profileLI <?= $globalActiveLink == 'profile' ? 'active' : ''; ?>"><a href="<?= base_url() ?>profile" ><?= $this->lang->line('profile'); ?></a></li>-->
            <?php } else { ?>

                <li class="menuHeader"><?= $this->lang->line('conference'); ?></li>
                <li class="<?= $globalActiveLink == 'rooms' ? 'active' : ''; ?>"><a href="<?= base_url() ?>rooms" ><?= $this->lang->line('rooms'); ?></a></li>
                <li class="<?= $globalActiveLink == 'projects' || $this->uri->segment(2) == 'project' ? 'active' : ''; ?>"><a href="<?= base_url() ?>projects"><?= $this->lang->line('projects'); ?></a></li>

            <?php } ?>
        </ul>
    </div>
</div>
    
<div class="col-sm-8 col-sm-offset-4 col-md-8 col-md-offset-4 col-lg-9 col-lg-offset-3" style="padding: 0px 15px 0px 0px;">
    
    <div class="iridaMainContainer">
    
        <div class="main">

            <?php if (isset($system_error) && isset($system_error_type) && $system_error != '' && $system_error_type != '') { ?>
            
                <script>
                    $(document).ready(function () {
                        showAlert('<?= str_replace(array("\r\n","\r","\n"),"",$system_error) ?>','<?= $system_error_type ?>')
                    });
                </script>
            
                <!--
                <div class="alert alert-<?= $system_error_type ?> alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <strong><?= $system_error_type == 'success' ? $this->lang->line('alert_success') : $this->lang->line('alert_error') ?></strong> <?= $system_error ?>
                </div>
                -->
            <?php } ?>