        <div class="footer">
            <div class="container text-center">
                
            </div>
        </div>
    </div>

</body>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/js/irida.js"></script>
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="<?= base_url() ?>assets/js/paper-dashboard.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/irida.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <!-- <script src="<?= base_url() ?>assets/js/vendor/holder.js"></script>-->
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--<script src="<?= base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>-->
        
        <script src="<?= base_url() ?>assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
        
        <script>
        
            $(document).ready(function() {
                setCookie('iridaCookieChecking', 1);
                
                if(getCookie('iridaCookieChecking') == null) {
                    $('#cookieAlert').css('display','');
                }
                
            })
            
        </script>
        
        <?php if (isset($message) && isset($message_type) && $message != '' && $message_type != '') { ?>
            
            <script>
                $(document).ready(function () {
                    showAlert('<?= str_replace(array("\r\n","\r","\n"),"",$message) ?>','<?= $message_type ?>')
                });
            </script>

        <?php } ?>
        
    </body>
</html>