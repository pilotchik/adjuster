<html>
    <head>
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico">

        <title><?= $config_title ?> <?= isset($site_title) ? ' | '.$site_title : '' ?></title>
 	
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-datetimepicker.min.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/irida.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/paper-dashboard.js?r=<?= $this->config->item('config_version_revision') ?>"></script>
        
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/colorbox.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" />
        <link href="<?= base_url() ?>assets/css/paper-dashboard.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/paper-irida.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/room.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-datetimepicker.min.css?r=<?= $this->config->item('config_version_revision') ?>" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.cropbox.css" />
   
        <script>
            var BASE_URL = '<?= base_url() ?>';
            var LOCAL_USER_ID = <?= $this->session->userdata['userID'] ?>;
            var ROOM_REAL_CANDIDATES = {};
            var ROOM_CANDIDATES = {};
            var CONFIG_VERSION_REVISION = '<?= $this->config->item('config_version_revision') ?>';

        </script>
        
        <?php if (isset($system_error) && isset($system_error_type) && $system_error != '' && $system_error_type != '') { ?>
            
            <script>
                $(document).ready(function () {
                    showAlert('<?= str_replace(array("\r\n","\r","\n"),"",$system_error) ?>','<?= $system_error_type ?>')
                });
            </script>

        <?php } ?>
        
        <?php require_once(APPPATH.'views/required/required_room_translator.php'); ?>
        
    </head>
    
    <body>

        <!-- Добавление в экспресс-комнату -->
        <div class="modal fade" id="addTemporaryRoomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="<?= base_url() ?>rooms/create_room/temporary" method="POST" class="form-horizontal form-responsive" onsubmit="$('.submitBTNID').css('display','none');$('.spinner-sm').css('display','')">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                            <h4 class="modal-title"><?= $this->lang->line('room_temporary_adding'); ?></h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="col-sm-12 text-center"><?= $this->lang->line('room_temporary_invited_users'); ?></label>
                            </div>

                            <div class="form-group">
                                <div class="table-responsive">
                                    <table class="table" id="invitedInTemporaryRoomUsers">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('user'); ?></th>
                                                <th class="activityStatusTD"><?= $this->lang->line('actions'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="room_cooperation" value="hangout">

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-sm btn-success submitBTNID"><?= $this->lang->line('btn_add_temporary_room') ?></button>
                            <img src="<?= base_url() ?>assets/images/loading_spinner.gif" class="spinner-sm" style="display: none;">
                            <button type="button" class="btn btn-sm btn-default submitBTNID" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        
        <!-- Информация об ошибке -->
        <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <h4 class="modal-title" id="alertModalTitle"></h4>
                    </div>
                    <div class="modal-body" id="alertModalBody">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Уведомления о приглашении -->
        <div class="modal fade" id="viewMyNotificationsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                        <h4 class="modal-title"><?= $this->lang->line('user_notifications_view'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-sm-12 text-center"><?= $this->lang->line('user_notifications_title'); ?></label>

                            <div class="table-responsive col-sm-12">
                                <table class="table" id="myNotificationsTable">
                                    <thead>
                                        <tr>
                                            <th class="activityStatusWithoutTextTD" title="<?= $this->lang->line('actions'); ?>"></th>
                                            <th><?= $this->lang->line('user'); ?></th>
                                            <th><?= $this->lang->line('user_notifications_message'); ?></th>
                                            <th style="width:70px;"><i class="fa fa-calendar"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="top-alert">
            <div class="alert-wrapper">
                <div class="alert">
                    <div class="alert-line">
                        <div class="alert-icon">
                            <div class="icon"></div>
                        </div>
                        <div class="alert-text"></div>
                    </div>
                </div>
            </div>                
        </div>
        
        
        <?php require_once(APPPATH.'views/required/required_modals.php'); ?>
        
        
        <div class="wrapper">
            <div class="sidebar" data-background-color="black" data-active-color="danger">
                
                <?php require_once(APPPATH.'views/required/required_new_menu.php'); ?>
                
            </div>
            
            <div class="main-panel">
		<nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar1"></span>
                                <span class="icon-bar bar2"></span>
                                <span class="icon-bar bar3"></span>
                            </button>
                            <span class="navbar-brand" href="#"><?= isset($site_title) ? $site_title : '' ?><?php if (isset($site_subtitle) && $site_subtitle != '' ) { ?><span class="navbar-subheader"><?= $site_subtitle ?></span><?php } ?></span>
                        </div>
                        
                        
                        <?php if (isset($site_add_buttons)) { ?>
                        
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    
                                    <?= $site_add_buttons ?>
                                    
                                    <!--
                                    <li>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="ti-panel"></i>
                                                                            <p>Stats</p>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="ti-bell"></i>
                                                <p class="notification">5</p>
                                                                                    <p>Notifications</p>
                                                                                    <b class="caret"></b>
                                          </a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#">Notification 1</a></li>
                                            <li><a href="#">Notification 2</a></li>
                                            <li><a href="#">Notification 3</a></li>
                                            <li><a href="#">Notification 4</a></li>
                                            <li><a href="#">Another notification</a></li>
                                          </ul>
                                    </li>
                                                            <li>
                                        <a href="#">
                                            <i class="ti-settings"></i>
                                            <p>Settings</p>
                                        </a>
                                    </li>
                                    -->
                                </ul>

                            </div>
                            
                        <?php } ?>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">

                            

        
                    
                    
                    
                    
                    
                    
                        