<!-- Статистика по комнате -->
<div class="modal fade" id="roomStatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                <h4 class="modal-title"><?= $this->lang->line('room_statistic'); ?></h4>
            </div>
            <div class="modal-body">
                
                <div id="chartRoomStatistic" style="height: 400px; width: 100%;"></div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
            </div>
        </div>
    </div>
</div>


<!-- Подключение по ID -->
<div class="modal fade" id="roomConnectOverIDModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                <h4 class="modal-title"><?= $this->lang->line('room_connect_over_id'); ?></h4>
            </div>
            <div class="modal-body">
                
                <form  class="form-horizontal form-responsive" role="form">
                
                    <div class="form-group required">
                        <label class="col-sm-4 control-label"><?= $this->lang->line('room_id'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="" id="connectOverRoomIDValue" placeholder="<?= $this->lang->line('room_id'); ?>" required="true">
                        </div>
                    </div>
                
                </form>
                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-success" onclick="landingConnectOverIDChecking(this)"><?= $this->lang->line('room_btn_connect'); ?></button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
            </div>
        </div>
    </div>
</div>


<?php if ($this->uri->segment(1) == 'landing') { ?>

    <!-- Подключение через пользователей (видеозвонок со стартовой страницы) -->
    <div class="modal fade" id="roomCreatingOverUsersIDModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_connect_over_users'); ?></h4>
                </div>
                <form action="<?= base_url() ?>rooms/create_room/temporary" method="POST" class="form-horizontal form-responsive" onsubmit="$('.submitBTNID').css('display','none');$('.spinner-sm').css('display','')">
                    <div class="modal-body">

                        <p><?= $this->lang->line('room_connect_over_users_description') ?></p>
                        
                        <input type="hidden" name="room_cooperation" value="meeting">
                        
                        <div class="roomAccessLevelsContainer userLevelsInlineCreatorContainer">

                            <div class="roomAccessLevelsUsersContainer roomAccessLevelsCreatorContainer ">
                                <div class="roomAccessLevelsUserContainer row" id="roomAccessLevelsUserContainer_<?= $this->session->userdata('userID') ?>">
                                    <div class="roomAccessLevelsAvatar col-sm-1">
                                        <img src="<?= $this->session->userdata('avatarURL') ?>" class="smallUserAvatarInTables">
                                    </div>
                                    <div class="roomAccessLevelsInfo col-sm-8" title="<?= $this->session->userdata('name') ?>">
                                        <div><?= $this->session->userdata('name') ?> (<?= $this->lang->line('room_users_access_levels_you') ?>)<br><span><?= $this->session->userdata('login') ?></span></div>
                                    </div>
                                    <div class="roomAccessLevelsRole col-sm-2">
                                        <?= $this->lang->line('room_creator_short') ?>
                                    </div>
                                    <div class="roomAccessLevelsDel col-sm-1"></div>
                                </div>
                            </div>
                            <div class="roomAccessLevelsUsersContainer roomAccessLevelsSecondaryMastersContainer"></div>
                            <div class="roomAccessLevelsUsersContainer roomAccessLevelsParticipantsContainer"></div>

                        </div>

                        <div class="roomAccessLevelsSearchContainer usersAccessLevelsSearchInlineContainer">

                            <div class="row">
                                <div class="input-group">
                                    <div id="userContainerID"></div>
                                    <input type="text" class="form-control" id="inputEditUserTag" oninput="loadUsersForCreating(this)" placeholder="<?= $this->lang->line('room_users_search_here') ?>">
                                    <div id="userAjaxLoadPanelID"></div>
                                </div>
                            </div>

                        </div>

                        <div class="inviteContainer" style="display:none"></div>
                        

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('room_btn_create'); ?></button>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>



<!-- Информация о комнате -->
<div class="modal fade" id="roomInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                <h4 class="modal-title"><?= $this->lang->line('room_info'); ?></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-responsive">
                    
                    <ul class="nav nav-tabs roomsTabs">
                        <li class="active"><a data-toggle="tab" href="#imain"><?= $this->lang->line('room_description_title'); ?></a></li>
                        <li><a data-toggle="tab" href="#iconfig"><?= $this->lang->line('room_configuration_title'); ?></a></li>
                        <li id="viewBehaviorTab"><a data-toggle="tab" href="#ibehavior"><?= $this->lang->line('room_behavior_title'); ?></a></li>
                        <!--<li id="viewDesktopsTab"><a data-toggle="tab" href="#idesktops"><?= $this->lang->line('room_desktops_title'); ?></a></li>-->
                        <li><a data-toggle="tab" href="#ilisteners"><?= $this->lang->line('room_listeners_title'); ?></a></li>
                    </ul>

                    <div class="tab-content">

                        <div id="imain" class="tab-pane fade in active">
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_name'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoName"></p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_id'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoID"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_cooperation'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoCooperation"></p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_connection_notinvited_users'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoNotinvited"></p>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_connection_files_public_access'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoFilesPublic"></p>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_creator'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoCreator"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_create_date'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoCreateDate"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_project'); ?></label>
                                <div class="col-sm-8">
                                    <p class="form-control-static" id="roomInfoProject"></p>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div id="iconfig" class="tab-pane fade">
                            
                            <!--
                            <div class="form-group">
                                <label class="col-sm-5 control-label"><?= $this->lang->line('room_public_speech_channels_count'); ?></label>
                                <div class="col-sm-7">
                                    <p class="form-control-static inputEditRoomSpeechChannels"></p>
                                </div>
                            </div>
                            -->

                            <div class="form-group">
                                <label class="col-sm-5 control-label"><?= $this->lang->line('room_users_experience'); ?></label>
                                
                                <div class="col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomNotinvitedConnection statusGlyph"></span> <?= $this->lang->line('room_connection_notinvited_users_allow'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomFilesPublic statusGlyph"></span> <?= $this->lang->line('room_connection_files_public_access'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomUserInviting statusGlyph"></span> <?= $this->lang->line('room_users_experience_inviting'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomEditing statusGlyph"></span> <?= $this->lang->line('room_users_experience_editing'); ?></p>
                                </div>
                            </div>

                            <!--
                            <div class="form-group">
                                <label class="col-sm-5 control-label"><?= $this->lang->line('room_interface'); ?></label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomWebcamTranslating statusGlyph"></span> <?= $this->lang->line('room_config_webcam_translating_and_messaging'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomInteractiveBoard statusGlyph"></span> <?= $this->lang->line('room_config_interactive_board'); ?> </p>
                                </div>
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomMessaging statusGlyph"></span> <?= $this->lang->line('room_config_messaging'); ?> </p>
                                </div>
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomListViewing statusGlyph"></span> <?= $this->lang->line('room_config_listenerslist_viewing'); ?> </p>
                                </div>
                            </div>
                            -->
                            
                            <!--
                                
                            <div class="form-group">
                                <label class="col-sm-5 control-label"><?= $this->lang->line('room_configuration'); ?></label>
                                
                                <div class="col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomPointerUsing statusGlyph"></span> <?= $this->lang->line('room_config_pointer_using'); ?> </p>
                                </div>
                                
                                <div class="col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomRandomConnection statusGlyph"></span> <?= $this->lang->line('room_config_random_connection'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomRecordChannels statusGlyph"></span> <?= $this->lang->line('room_config_record_channels'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-5 col-sm-7">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomMasterlessMode statusGlyph"></span> <?= $this->lang->line('room_config_masterless_mode'); ?> </p>
                                </div>
                                
                            </div>
                            -->
                                
                            
                            <div class="form-group">
                                <label class="col-sm-5 control-label"><?= $this->lang->line('room_config_с_upload_maxfilesize'); ?></label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><span class="inputEditRoomс_upload_maxfilesize"></span></p>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                        <div id="ibehavior" class="tab-pane fade">
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_user_right_configuration'); ?></label>
                                
                                <!--
                                <div class="col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_secvideoch_maximizing statusGlyph"></span> <?= $this->lang->line('room_config_c_self_secvideoch_maximizing'); ?> </p>
                                </div>
                                -->

                                <div class="col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_betweenfaces_switching statusGlyph"></span> <?= $this->lang->line('room_config_c_self_betweenfaces_switching'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider">
                                        <div class="form-divider-title"><?= $this->lang->line('room_board'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_docsonboard_publication statusGlyph"></span> <?= $this->lang->line('room_config_c_self_docsonboard_publication'); ?></p>
                                </div>

                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_betweendocs_switching statusGlyph"></span> <?= $this->lang->line('room_config_c_self_betweendocs_switching'); ?> </p>
                                </div>
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_betweendocssites_switching statusGlyph"></span> <?= $this->lang->line('room_config_c_self_betweendocssites_switching'); ?> </p>
                                </div>
                                -->
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_scale_changing statusGlyph"></span> <?= $this->lang->line('room_config_c_self_scale_changing'); ?></p>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_board_listeners_drawing statusGlyph"></span> <?= $this->lang->line('room_config_c_board_listeners_drawing'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_work_foreign_boards_elements statusGlyph"></span> <?= $this->lang->line('room_config_c_work_foreign_boards_elements'); ?></p>
                                </div>
                                
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider">
                                        <div class="form-divider-title"><?= $this->lang->line('room_desktops'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_share_self_screen statusGlyph"></span> <?= $this->lang->line('room_config_c_share_self_screen'); ?> </p>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_desktop_connection statusGlyph"></span> <?= $this->lang->line('room_config_c_self_desktop_connection'); ?></p>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider">
                                        <div class="form-divider-title"><?= $this->lang->line('room_files_managing'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_documents_view statusGlyph"></span> <?= $this->lang->line('room_config_c_self_documents_view'); ?></p>
                                </div>
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <p class="form-control-static"><span class="glyphicon glyphicon-ok inputEditRoomc_self_video_starting statusGlyph"></span> <?= $this->lang->line('room_config_c_self_video_starting'); ?></p>
                                </div>
                                -->
                                
                            </div>
                            
                        </div>
                        
                        
                        <div id="idesktops" class="tab-pane fade">
                            <div class="table-responsive remoteLinksContainerID">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><?= $this->lang->line('room_remote_link_title'); ?></th>
                                            <th><?= $this->lang->line('room_remote_link_description'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                            
                            
                        <div id="ilisteners" class="tab-pane fade">
                    
                            <div class="table-responsive usersClass">
                                <table class="table" id="roomInfoUsers">
                                    <thead>
                                        <tr>
                                            <th><?= $this->lang->line('room_user'); ?></th>
                                            <th><?= $this->lang->line('room_user_role'); ?></th>
                                            <th><?= $this->lang->line('room_inviter'); ?></th>
                                            <th class="date-long-td"><?= $this->lang->line('room_invite_date'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                    
                        </div>
                        
                        
                    </div>
                        
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
            </div>
        </div>
    </div>
</div>


<!-- Редактирование информации о комнате -->
<div class="modal fade" id="updateRoomInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form  class="form-horizontal form-responsive" role="form" method="POST" action="<?= base_url() ?>rooms/update_room">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_editing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="room_id" value="" id="inputEditRoomID">
                    
                    <input type="hidden" name="project_id" value="0" id="inputEditProjectID">
                    
                    <!--
                    
                    <ul class="nav nav-tabs roomsTabs">
                        <li class="active"><a data-toggle="tab" href="#config"><?= $this->lang->line('room_configuration_title'); ?></a></li>
                        <li id="editBehaviorTab"><a data-toggle="tab" href="#behavior"><?= $this->lang->line('room_behavior_title'); ?></a></li>
                    </ul>

                    <div class="tab-content">

                        <div id="config" class="tab-pane fade in active">
                    
                            -->
                            
                            <div class="form-group required">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_name'); ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="" name="room_name" id="inputEditRoomName" placeholder="<?= $this->lang->line('room_name'); ?>" required="true">
                                </div>
                            </div>
                            
                            <!--
                            <div class="form-group required">
                                <label for="input3" class="col-sm-4 control-label"><?= $this->lang->line('room_public_speech_channels_count'); ?></label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control initialWidth inputTypeNumber inputEditRoomSpeechChannels" step="1" min="1" max="10" onChange="checkNumberType(this,0,10)" value="" name="room_speech_channels" required="true">
                                </div>
                            </div>
                            -->
                            <input type="hidden" name="room_speech_channels" value="3">

                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_cooperation'); ?></label>
                                <div class="col-sm-8">
                                    <!--<p class="form-control-static" id="inputEditRoomCooperation"></p>-->
                                    <select class="form-control initialWidth" name="room_cooperation" id="inputEditRoomCooperation">
                                        <option value="hangout" selected><?= $this->lang->line('room_cooperation_hangout'); ?></option>
                                        <option value="conference"><?= $this->lang->line('room_cooperation_conference'); ?></option>
                                        <option value="lesson"><?= $this->lang->line('room_cooperation_lesson'); ?></option>
                                        <!--<option value="meeting"><?= $this->lang->line('room_cooperation_meeting'); ?></option>-->
                                    </select>
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_users_experience'); ?></label>
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_connection_notinvited_users" class="inputEditRoomNotinvitedConnection"><?= $this->lang->line('room_connection_notinvited_users_allow'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_connection_files_public_access" class="inputEditRoomFilesPublic"><?= $this->lang->line('room_connection_files_public_access'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_p_user_inviting" class="inputEditRoomUserInviting"><?= $this->lang->line('room_users_experience_inviting'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_p_room_editing" class="inputEditRoomEditing"><?= $this->lang->line('room_users_experience_editing'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <!--
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_interface'); ?></label>
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_webcam_translating" class="inputEditRoomWebcamTranslating roomInterface" onclick="checkInterfaceRules()"><?= $this->lang->line('room_config_webcam_translating_and_messaging'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_interactive_board" class="inputEditRoomInteractiveBoard roomInterface" onclick="checkInterfaceRules()"><?= $this->lang->line('room_config_interactive_board'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_messaging" class="inputEditRoomMessaging"><?= $this->lang->line('room_config_messaging'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_listenerslist_viewing" class="inputEditRoomListViewing roomInterface" onclick="checkInterfaceRules()"><?= $this->lang->line('room_config_listenerslist_viewing'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            -->

                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_configuration'); ?></label>
                                
                                <!--
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_pointer_using" class="inputEditRoomPointerUsing"><?= $this->lang->line('room_config_pointer_using'); ?>
                                        </label>
                                    </div>
                                </div>
                                -->
                                
                                <!--
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_random_connection" class="inputEditRoomRandomConnection"><?= $this->lang->line('room_config_random_connection'); ?>
                                        </label>
                                    </div>
                                </div>
                                -->
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_record_status" class="inputEditRoomRecordChannels"><?= $this->lang->line('room_config_record_channels'); ?>
                                        </label>
                                    </div>
                                </div>
                                -->
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="room_c_masterless_mode" class="inputEditRoomMasterlessMode"><?= $this->lang->line('room_config_masterless_mode'); ?>
                                        </label>
                                    </div>
                                    <div class="help-block"><?= $this->lang->line('room_config_masterless_mode_help'); ?></div>
                                </div>
                                -->
                                
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_config_с_upload_maxfilesize'); ?></label>

                                <div class="col-sm-8">
                                    <input type="number" min="1" step="1" max="50" name="с_upload_maxfilesize" class="form-control inputEditRoomUploadMaxfilesize" value="10" required="true">
                                </div>

                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= $this->lang->line('room_user_right_configuration'); ?></label>
                                
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_betweenfaces_switching" class="inputEditRoomc_self_betweenfaces_switching"><?= $this->lang->line('room_config_c_self_betweenfaces_switching'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider form-divider-high">
                                        <div class="form-divider-title"><?= $this->lang->line('room_board'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_docsonboard_publication" class="inputEditRoomc_self_docsonboard_publication"><?= $this->lang->line('room_config_c_self_docsonboard_publication'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_betweendocs_switching" class="inputEditRoomc_self_betweendocs_switching"><?= $this->lang->line('room_config_c_self_betweendocs_switching'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_betweendocssites_switching" class="inputEditRoomc_self_betweendocssites_switching"><?= $this->lang->line('room_config_c_self_betweendocssites_switching'); ?>
                                        </label>
                                    </div>
                                </div>
                                -->
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_scale_changing" class="inputEditRoomc_self_scale_changing"><?= $this->lang->line('room_config_c_self_scale_changing'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_board_listeners_drawing" class="inputEditRoomc_board_listeners_drawing"><?= $this->lang->line('room_config_c_board_listeners_drawing'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_work_foreign_boards_elements" class="inputEditRoomc_work_foreign_boards_elements"><?= $this->lang->line('room_config_c_work_foreign_boards_elements'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider form-divider-high">
                                        <div class="form-divider-title"><?= $this->lang->line('room_desktops'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_share_self_screen" class="inputEditRoomc_share_self_screen"><?= $this->lang->line('room_config_c_share_self_screen'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_desktop_connection" class="inputEditRoomc_self_desktop_connection"><?= $this->lang->line('room_config_c_self_desktop_connection'); ?>
                                        </label>
                                    </div>
                                </div>

                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="form-divider form-divider-high">
                                        <div class="form-divider-title"><?= $this->lang->line('room_files_managing'); ?></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_documents_view" class="inputEditRoomc_self_documents_view"><?= $this->lang->line('room_config_c_self_documents_view'); ?>
                                        </label>
                                    </div>
                                </div>
                                
                                <!--
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="c_self_video_starting" class="inputEditRoomc_self_video_starting"><?= $this->lang->line('room_config_c_self_video_starting'); ?>
                                        </label>
                                    </div>
                                </div>
                                -->
                                
                                
                            </div>
                            
                            <!--
                        </div>
                        
                    </div>
                    -->
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Редактирование информации о ссылках на рабочие столы -->
<div class="modal fade" id="updateRoomRemoteLinksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form  class="form-horizontal form-responsive" role="form" method="POST" action="<?= base_url() ?>rooms/update_remote_links">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_remote_links_and_services_managing'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <!--<h3><span id="inputEditLinksRoomName"></span></h3>-->
                    
                    <input type="hidden" name="room_id" value="" id="inputEditLinksRoomID">

                    <div class="allRemoteLinksContainer">
                        <div class="table-responsive remoteLinksContainerID">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->lang->line('room_remote_link_title'); ?></th>
                                        <th><?= $this->lang->line('room_remote_link_description'); ?></th>
                                        <th class="actionsTD"><?= $this->lang->line('actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <button type="button" class="btn btn-success btn-sm" onclick="addRemoteLink()">
                            <?= $this->lang->line('btn_add_room_remote_link'); ?>
                        </button>
                    </div>    
                    
                    <div class="allMediaServersContainer">
                        <div class="table-responsive remoteMediaServicesContainerID">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->lang->line('equipment_hostname'); ?></th>
                                        <th><?= $this->lang->line('equipment_description'); ?></th>
                                        <th class="actionsTD"><?= $this->lang->line('actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($equipments)) { ?>

                                        <?php foreach($equipments as $eq) { ?>
                                            <tr>
                                                <td><?= $eq['hostname'] ?></td>
                                                <td><?= $eq['description'] ?></td>
                                                <td class="centerAlign">
                                                    <input type="checkbox" name="equipments[<?= $eq['id'] ?>]" value="<?= $eq['id'] ?>" class="equipmentsCheckboxes" id="equipmentCheckbox_<?= $eq['id'] ?>">
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    <?php } else { ?>
                                        <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    
                    <div class="allCamerasContainer">
                        <div class="table-responsive remoteCamerasContainerID">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->lang->line('equipment_hostname'); ?></th>
                                        <th><?= $this->lang->line('equipment_description'); ?></th>
                                        <th class="actionsTD"><?= $this->lang->line('actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($cameras)) { ?>

                                        <?php foreach($cameras as $eq) { ?>
                                            <tr>
                                                <td><?= $eq['hostname'] ?></td>
                                                <td><?= $eq['description'] ?></td>
                                                <td class="centerAlign">
                                                    <input type="checkbox" name="equipments[<?= $eq['id'] ?>]" value="<?= $eq['id'] ?>" class="equipmentsCheckboxes" id="equipmentCheckbox_<?= $eq['id'] ?>">
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    <?php } else { ?>
                                        <tr><td colspan="100%" class="notFoundedDataTD"><?= $this->lang->line('data_not_found'); ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php if ($this->uri->segment(1) == 'rooms' && $this->uri->segment(2) != 'create_standing') { ?>

    <!-- Приглашение пользователей в комнату -->
    <div class="modal fade" id="inviteUsersInRoomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_user_inviting'); ?></h4>
                </div>
                <div class="modal-body">

                    <h3><?= $this->lang->line('room_users_access_levels'); ?></h3>

                    <div class="roomAccessLevelsContainer">

                        <div class="roomAccessLevelsUsersContainer roomAccessLevelsCreatorContainer"></div>
                        <div class="roomAccessLevelsUsersContainer roomAccessLevelsSecondaryMastersContainer"></div>
                        <div class="roomAccessLevelsUsersContainer roomAccessLevelsParticipantsContainer"></div>

                    </div>

                    <h3><?= $this->lang->line('room_users_access_levels_search'); ?></h3>
                    <div class="roomAccessLevelsSearchContainer">

                        <div class="row">
                            <div class="input-group">
                                <div id="userContainerID"></div>
                                <input type="text" class="form-control" id="inputEditUserTag" oninput="loadUsers(this)">
                                <div id="userAjaxLoadPanelID"></div>
                            </div>
                        </div>

                    </div>

                    <div class="inviteContainer" style="display:none"></div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-success submitBTNID" id="fixUsersBTNID" onClick="endInviting()"><?= $this->lang->line('btn_end_inviting'); ?></button>
                    <img src="<?= base_url() ?>assets/images/loading_spinner.gif" class="spinner-sm" style="display: none;">
                    <button type="button" class="btn btn-sm btn-default submitBTNID" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<!-- Подтверждение удаления -->
<div class="modal fade" id="removeRoomConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>rooms/remove_room">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_removing'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="room_id" value="" id="inputRemoveRoomHiddenID">
                    <?= $this->lang->line('room_removing_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение удаления из проекта -->
<div class="modal fade" id="removeRoomFromProjectConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>rooms/remove_room_from_project">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_removing_from_project'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="room_id" value="" id="inputRemoveRoomFromProjectHiddenID">
                    <?= $this->lang->line('room_removing_from_project_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger"><?= $this->lang->line('btn_remove'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Подтверждение архивации -->
<div class="modal fade" id="archiveRoomConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>rooms/archive_room/1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('room_archiving'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="room_id" value="" id="inputArchiveRoomHiddenID">
                    <?= $this->lang->line('room_archiving_confirm'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-warning"><?= $this->lang->line('btn_archive'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>





<!-- Изменение проекта -->
<div class="modal fade" id="openEditAdditionalRoomsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="<?= base_url() ?>projects/add_rooms" autocomplete="false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $this->lang->line('close'); ?></span></button>
                    <h4 class="modal-title"><?= $this->lang->line('project_rooms_users_adding'); ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="project_id" value="" id="inputAddButtonsProjectID">
                    
                    <div class="form-group">
                        <label for="inputEditProjectDescription"><?= $this->lang->line('project_free_rooms'); ?></label>
                        <div id="roomCheckboxesContainer"></div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><?= $this->lang->line('btn_add'); ?></button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('btn_cancel'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



