<div class="sidebar-wrapper">
    <div class="logo">
        <a href="<?= base_url() ?>" class="simple-text">
            <img src="<?= base_url() ?>assets/images/logo_new.png" style="width:50%;">
        </a>
    </div>

    <ul class="nav">

        <div class="logo-avatar">
            <a class="logo-avatar-container" href="<?= base_url() ?>">
                <img src="<?= $this->session->userdata('avatarBigURL'); ?>">
            </a>
            <a href="<?= base_url() ?>profile" class="logo-avatar-settings" id="linkProfileID">
                <img src="<?= base_url() ?>assets/images/paper/dashboard_icon_settings.png">
                <!--<i class="fa fa-cogs fa-lg"></i>-->
            </a>
            <div class="logo-avatar-notifications viewNotificationBigBTN" style="display:none;">
                <button type="submit" class="btn btn-info btn-sm" title="<?= $this->lang->line('user_notifications') ?>" onclick="$('#viewMyNotificationsModal').modal('show')">
                    <i class="fa fa-bell fa-lg"></i>&nbsp;<span class="badge">0</span>
                </button>
            </div>
            <button type="submit" class="btn btn-success btn-sm inviteInFastRoomBigBTN" style="display: none;" onclick="$('#addTemporaryRoomModal').modal('show')" title=" <?= $this->lang->line('btn_room_invited_set_title') ?>">
                <i class="fa fa-user-plus fa-lg"></i>&nbsp;<span class="badge">0</span>
            </button>

            <div class="logo-avatar-name" title="<?= $this->session->userdata('name'); ?>">
                <?= $this->session->userdata('only_name'); ?> 
            </div>

        </div>
        
        <?php if ($this->session->userdata('userType') != 'admin') { ?>
		
			<li class="<?= $globalActiveLink == 'polls' && $this->session->userdata('userType') != 'admin' ? 'active' : ''; ?>" id="liButtonPollsID">
				<a href="<?= base_url() ?>polls">
					<p><i class="fa fa-pie-chart fa-lg"></i>&nbsp;Тестовые наборы</p>
				</a>
			</li>
		
		<?php } ?>
        
        
        <?php if ($this->session->userdata('userType') == 'admin' || $this->session->userdata('login') == 'admin') { ?>
        
            <li class="divider"></li>
            
            <!-------------------------------------------------- УПРАВЛЕНИЕ СПРАВОЧНИКАМИ --------------------------------------------------------------->
            
			<li class="<?= $globalActiveLink == 'polls' ? 'active' : ''; ?>" id="liButtonAdminPollsID">
				<a href="<?= base_url() ?>polls" >
					<p><i class="fa fa-check-circle-o fa-lg"></i>&nbsp;Управление тестами</p>
				</a>
			</li>
            
			<li class="<?= $globalActiveLink == 'admin/sked' ? 'active' : ''; ?>" id="liButtonAdminSkedID">
                <a href="<?= base_url() ?>admin/sked" >
                    <p><i class="fa fa-commenting fa-lg"></i>&nbsp;Справочник тем</p>
                </a>
            </li>

                
        <?php } ?>
        
        
                
                
        <?php if ($this->session->userdata('login') == 'admin' || $this->session->userdata('role') == 'superuser') { ?>
            
            <li class="divider"></li>
            
            <li class="<?= $globalActiveLink == 'admin/users' ? 'active' : ''; ?>" id="liButtonAdminUsersID">
                <a href="<?= base_url() ?>admin/users" >
                    <p><i class="fa fa-users fa-lg"></i>&nbsp;<?= $this->lang->line('users_managing'); ?></p>
                </a>
            </li>
            <li class="<?= $globalActiveLink == 'admin/log' ? 'active' : ''; ?>" id="liButtonLogID">
                <a href="<?= base_url() ?>admin/log">
                    <p><i class="fa fa-history fa-lg"></i>&nbsp;<?= $this->lang->line('system_log'); ?></p>
                </a>
            </li>
            <li class="<?= $globalActiveLink == 'admin/config' ? 'active' : ''; ?>" id="liButtonConfigID">
                <a href="<?= base_url() ?>admin/config" >
                    <p><i class="fa fa-wrench fa-lg"></i>&nbsp;<?= $this->lang->line('config'); ?></p>
                </a>
            </li>
            
			<li class="<?= $globalActiveLink == 'admin/log/metrika' ? 'active' : ''; ?>" id="liButtonMetrikaID">
                <a href="<?= base_url() ?>admin/log/metrika" >
                    <p><i class="fa fa-area-chart fa-lg"></i>&nbsp;<?= $this->lang->line('metrika'); ?></p>
                </a>
            </li>
            
			<li class="<?= $globalActiveLink == 'admin/log/feedback' ? 'active' : ''; ?>" id="liButtonFeedbackID">
                <a href="<?= base_url() ?>admin/log/feedback" >
                    <p><i class="fa fa-reddit-alien fa-lg"></i>&nbsp;<?= $this->lang->line('feedback_title'); ?></p>
                </a>
            </li>
            
        <?php } elseif ($this->session->userdata('userType') == 'admin') { ?>
            
            <li class="divider"></li>
                
            <li class="<?= $globalActiveLink == 'admin/users' ? 'active' : ''; ?>" id="liButtonAdminUsersID">
                <a href="<?= base_url() ?>admin/users">
                    <p><i class="fa fa-users fa-lg"></i>&nbsp;<?= $this->lang->line('users_managing'); ?></p>
                </a>
            </li>
            
			

        <?php } ?>

                
                


        <li class="divider"></li>

            
        <li class="<?= $globalActiveLink == 'profile/notifications' ? 'active' : ''; ?>" id="liButtonNotificationsID">
            <a href="<?= base_url() ?>profile/notifications">
                <p><i class="fa fa-bell fa-lg"></i>&nbsp;<?= $this->lang->line('user_notifications') ?></p>
            </a>
        </li>
        
        <!--
        <li>
            <a href="<?= base_url() ?>help">
                <p><i class="fa fa-question-circle fa-lg"></i>&nbsp;<?= $this->lang->line('help') ?></p>
            </a>
        </li>
        -->
        
        <?php if (!$this->config->item('config_use_ldap')) { ?>
            <li id="liButtonSignOutID">
                <a href="<?= base_url() ?>main/deauth">
                    <p><i class="fa  fa-sign-out fa-lg"></i>&nbsp;<?= $this->lang->line('sign_out') ?></p>
                </a>
            </li>
        <?php } ?>

    </ul>

</div>