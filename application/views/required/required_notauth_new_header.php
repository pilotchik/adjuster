<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <meta name="description" content="">
        <meta name="author" content="">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico">
        <title><?= $config_title ?></title>
 
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        
        <link href="<?= base_url() ?>assets/css/paper-dashboard.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/paper-irida-notauth.css?r=<?= $this->config->item('config_version_revision') ?>" rel="stylesheet">
        
        
        
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/themify-icons.css">
        
        
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?= base_url() ?>assets/js/ie-emulation-modes-warning.js"></script>

        
        
    </head>

    <body>
        
        <div class="top-alert">
            <div class="alert-wrapper">
                <div class="alert">
                    <div class="alert-line">
                        <div class="alert-icon">
                            <div class="icon"></div>
                        </div>
                        <div class="alert-text"></div>
                    </div>
                </div>
            </div>                
        </div>
        
        <div class="image-container set-full-height" style="background-image: url('<?= base_url() ?>assets/images/pik_4black.jpg')">
  
            
            
            