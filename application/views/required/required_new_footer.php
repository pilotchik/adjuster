                        </div>
                
                    </div>
            
                </div>
                
            </div>
            
        </div>
        
        
        
        <div data-active-color="danger" data-background-color="white" class="collapse navbar-collapse off-canvas-sidebar">
            
            <?php require(APPPATH.'views/required/required_new_menu.php'); ?>
        
        </div>
        
        
        
        
        
        
        
        
        
        
    
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <!--<script src="<?= base_url() ?>assets/js/vendor/holder.js"></script>-->
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--<script src="<?= base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>-->
        <script src="<?= base_url() ?>assets/js/jscolor.min.js"></script>
        <script src="<?= base_url() ?>assets/js/canvasjs.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/knockout-3.4.0.js"></script>
        
        <script>
            
            jQuery(document).ready(function() {
                $('.photo').colorbox({rel:'photo'});
            });
            
            <?php if (isset($this->session->userdata['userType'])) { ?>
                
                function heartBeat() {
                    
                    jQuery.ajax({
                        type: 'get',
                        data: {userID:<?= $this->session->userdata['userID'] ?>},
                        url: '<?= base_url() ?>ajax/setHeartBeat',
                        success: function(msg) {
                            //Дописать обработку сессионных пользователей
                            try {
                                //console.log(msg)
                                sanswer = JSON.parse(msg);
                                menageCart(sanswer.cart);
                                menageNotifications(sanswer.notifications);

                                //Количество пользователей по подразделениям
                                menagePosts(sanswer.structure_items);

                                if (sanswer.activePollsCount > 0)
                                    $('#pollsActiveCountID').html(sanswer.activePollsCount);
                                else
                                    $('#pollsActiveCountID').html('');

                                if (sanswer.activeNotesCount > 0)
                                    $('#notesActiveCountID').html(sanswer.activeNotesCount);
                                else
                                    $('#notesActiveCountID').html('');

                                if (sanswer.activeTopNotesCount > 0) {
                                    $('.viewNotesBigBTN').css('display','');
                                    $('.viewNotesBigBTN .badge').html(sanswer.activeTopNotesCount);
                                } else {
                                    $('.viewNotesBigBTN').css('display','none');
                                }

                                setTimeout(heartBeat,10000);
                            } catch(e) {
                                console.error(msg,e)
                            }
                            
                        },
                        error: function(jq) {
                            if (jq && jq.status > 0 && jq.responseText != '' && jq.statusText != '') {
                                $('#alertModalTitle').html('<?= $this->lang->line('error_system') ?>');
                                if (jq.status == 500)
                                    $('#alertModalBody').html(JSON.parse(jq.responseText).error);
                                else if (jq.status == 401)
                                    $('#alertModalBody').html('<?= $this->lang->line('error_system_authorization_required') ?>');
                                else if (jq.status > 0 && jq.responseText != '' && jq.statusText != '') 
                                    $('#alertModalBody').html(jq.status+' '+jq.statusText+' '+jq.responseText);
                                
                                $('#alertModal').modal('show');
                            }
                        }
                    });
                    
                    
                }
                
                
                function inviteUserInRoom(el) {
                
                    $(el).find('.fa').removeClass('fa-user-plus').addClass('fa-circle-o-notch').addClass('fa-spin');
                
                    userID = $(el).attr('user-id');

                    jQuery.ajax({
                        type: 'post',
                        data: {userID:userID},
                        url: '<?= base_url() ?>ajax/menageUserInCart',
                        success: function(msg) {
                            //Дописать обработку сессионных пользователей
                            sanswer = JSON.parse(msg);
                            
                            $(el).find('.fa').addClass('fa-user-plus').removeClass('fa-circle-o-notch').removeClass('fa-spin');
                            
                            $.each($('.cart-invite-button-'+userID),function(nel,btnel){
                            
                                if (sanswer.status == 'adding') {
                                    
                                    if ($(btnel).hasClass('btn')) {
                                        $(btnel).removeClass('btn-default');
                                        $(btnel).addClass('btn-success');
                                        $(btnel).attr('title','<?= $this->lang->line('room_user_invited_in_fastroom') ?>');
                                    }
                                    
                                } else {
                                
                                    if ($(btnel).hasClass('btn')) {
                                        $(btnel).removeClass('btn-success');
                                        $(btnel).addClass('btn-default');
                                        $(btnel).attr('title','<?= $this->lang->line('room_user_notinvited_in_fastroom') ?>');
                                    }

                                    if ($('#userInvite_structure_btn_'+sanswer.userID).hasClass('btn')) {
                                        $('#userInvite_structure_btn_'+sanswer.userID).removeClass('btn-success');
                                        $('#userInvite_structure_btn_'+sanswer.userID).addClass('btn-default');
                                    }
                                }
                            });
                            
                            //menageCart(sanswer.cart,'refreshing');
                            menageCart(sanswer.cart);
                        },
                        error: function(jq) {
                            
                            $(el).find('.fa').addClass('fa-user-plus').removeClass('fa-circle-o-notch').removeClass('fa-spin');
                            console.error(JSON.parse(jq.responseText).error);
                        }
                    });


                }
                
                
                function menageCart(cart,refreshstatus) {
                    
                    var size = 0, key;
                    for(key in cart) {
                        if (cart.hasOwnProperty(key)) size++;
                    }
                    
                    
                    if (size) {
                        $('.inviteInFastRoomBigBTN').fadeIn('fast');
                        $('.inviteInFastRoomBigBTN span').html(size);
                        
                        //invitedInTemporaryRoomUsers
                        $('#invitedInTemporaryRoomUsers tbody').html('');
                        
                        for (key in cart) {
                            $('#invitedInTemporaryRoomUsers tbody').append('<tr><td class="word-wrap-td"><span class="userInfo wordWrapDiv">'+cart[key].name+'</span></td>\n\
                                <td class="center-align ">\n\
                                    <input type="hidden" name="users_participants[]" value="'+cart[key].id+'">\n\
                                    <span class="btn btn-sm btn-danger" onclick="inviteUserInRoom(this)" user-id="'+cart[key].id+'">\n\
                                        <span class="glyphicon glyphicon-remove" title="<?= $this->lang->line('btn_remove') ?>"></span>\n\
                                    </span>\n\
                                </td>\n\
                            </tr>');
                                            
                            
                                            
                        }
                        
                    } else {
                        $('.inviteInFastRoomBigBTN').fadeOut('fast');
                        $('#addTemporaryRoomModal').modal('hide');
                        
                        if (refreshstatus == "refreshing") location.href = location.href;
                    }
                }
                
                
                function menageNotifications(notifications){
                    
                    if (notifications.length) {
                        
                        $('.viewNotificationBigBTN').fadeIn('fast');
                        $('.viewNotificationBigBTN span.badge').html(notifications.length);
                        
                        if (!$('#viewMyNotificationsModal').hasClass('in')) {
                        
                            $('#myNotificationsTable tbody').html('');

                            notifications.forEach(function(el,numb) {

                                code = '<tr>\n\
                                            <td class="center-align">\n\
                                                <span class="glyphicon glyphicon-unchecked" title="<?= $this->lang->line('user_notification_set_checked') ?>" onclick="checkNotification(this)" notify-id="'+el.id+'"></span>\n\
                                                <img src="<?= base_url() ?>assets/images/loading_spinner.gif" style="width:20px;height:20px;display:none" id="notifyLoadingSpinner_'+el.id+'"/>\n\
                                                <span class="glyphicon glyphicon-ok" id="notifyLoadingChecked_'+el.id+'" style="display:none;"></span>\n\
                                            </td>\n\
                                            <td class="word-wrap-td" title="'+el.authorFullName+'">'+el.authorFullName+'</td>\n\
                                            <td class="word-wrap-td">'+el.body+'</td>\n\
                                            <td class="center-align date-inactive-status">'+el.date+'</td>\n\
                                        </tr>';

                                $('#myNotificationsTable tbody').append(code);

                            });
                            
                        }
                        
                    } else {
                        
                        $('.viewNotificationBigBTN').fadeOut('fast');
                        if (!$('#viewMyNotificationsModal').hasClass('in')) $('#viewMyNotificationsModal').modal('hide');
                        
                    }
                    
                }
                
                
                /**
                 * Обновление количества приглашённых пользователей для структурных подразделений (если они есть,конечно)
                 */
                function menagePosts(structure_items) {
                    
                    var size = 0, key;
                    for(key in structure_items) {
                        if (structure_items.hasOwnProperty(key)) size++;
                    }
                    
                    if (size) {
                        for(key in structure_items) {
                            $('.postsCountInv'+key).html(structure_items[key]);
                        }
                    }
                    
                }
                
                jQuery(document).ready(function() {
                    heartBeat();
                });
                
                
                function checkNumberType(el,start,stop) {
        
                    if ($(el).val() == '') 
                        $(el).val(start)
                    else {
                        if ($(el).val() < start) $(el).val(start)
                        if ($(el).val() > stop) $(el).val(stop)
                    }
                }
                
                
                function checkNotification(el) {
                    notifyID = parseInt($(el).attr('notify-id'));
                    
                    $(el).css('display','none');
                    $('#notifyLoadingSpinner_'+notifyID).css('display','');
                    
                    jQuery.ajax({
                        type: 'post',
                        data: {notifyID:notifyID},
                        url: '<?= base_url() ?>ajax/checkNotification',
                        success: function(msg) {
                            //Дописать обработку сессионных пользователей
                            sanswer = JSON.parse(msg);
                            
                            $('#notifyLoadingChecked_'+notifyID).css('display','');
                            $('#notifyLoadingSpinner_'+notifyID).css('display','none');

                            
                            //$(el).parent().parent().remove();
                            
                    
                        },
                        error: function(jq) {
                            $(el).css('display','');
                            $('#notifyLoadingSpinner_'+notifyID).css('display','none');
                        }
                    });
                    
                }
                
                
            <?php } ?>
        
        
            
        </script>
        
        <script>
            var sorted_translations = new Array();
            sorted_translations['loadingdata'] = '<?= $this->lang->line('sorted_table_loading_data') ?>';
            sorted_translations['operationaffectmany'] = '<?= $this->lang->line('sorted_table_operationaffectmany') ?>';
            sorted_translations['filter'] = '<?= $this->lang->line('sorted_table_filter') ?>';
            sorted_translations['rowsperpage'] = '<?= $this->lang->line('sorted_table_rowsperpage') ?>';
            sorted_translations['displayingresults'] = '<?= $this->lang->line('sorted_table_displayingresults') ?>';
            sorted_translations['outof'] = '<?= $this->lang->line('sorted_table_outof') ?>';
            sorted_translations['_SHOWINGONLYACTIVEENTITIES'] = '<?= $this->lang->line('sorted_table_showing_active') ?>';
            sorted_translations['_SHOWINGONLYINACTIVEENTITIES'] = '<?= $this->lang->line('sorted_table_showing_inactive') ?>';
            sorted_translations['_SHOWINGALLENTITIES'] = '<?= $this->lang->line('sorted_table_showing_all') ?>';
            sorted_translations['first_page'] = '<?= $this->lang->line('sorted_table_first_page') ?>';
            sorted_translations['previous_page'] = '<?= $this->lang->line('sorted_table_previous_page') ?>';
            sorted_translations['next_page'] = '<?= $this->lang->line('sorted_table_next_page') ?>';
            sorted_translations['last_page'] = '<?= $this->lang->line('sorted_table_last_page') ?>';
            sorted_translations['data_not_found'] = '<?= $this->lang->line('sorted_table_data_not_found') ?>';
            
            
            var base_url = '<?= base_url() ?>';
            
        </script>
        
        <script type="text/javascript" src="<?= base_url() ?>assets/js/ajax_sorted_table/ajax_sorted_table.js"></script>
        
        <script>
            
            jQuery(document).ready(function() {
                $('td.sortedTableFooter').attr('colspan','100%');
                
                $("[data-toggle=popover]").popover();
                
                iridaAdminInitPanelsSizes();
                
                eF_js_changePageFromCookie();
                
                
            });
            
            $(window).resize(function(){
                iridaAdminInitPanelsSizes()
            });
            
            
            function iridaAdminInitPanelsSizes() {
                
                $('.main').css({
                    width:$('.iridaMainContainer').css('width'),
                    height:parseInt($(window).height()) - (60+30)
                })
                
                $('.sidebar').css('width',parseInt($('.sidebar').parent().css('width')) - 30)
                
                
            }
            
            
            function addUserInContainer(el,containerID,userRowIDPrefix,userID) {
                
                code = '<div class="usersRowsAdded" id="remove_'+userRowIDPrefix+userID+'">\n\
                    <div class="btn btn-xs btn-danger" onclick="removeUserFromContainer(\''+userRowIDPrefix+'\','+userID+')">\n\
                        <span class="glyphicon glyphicon-remove"></span> \n\
                    </div>\n\
                    <span title="'+$(el).next().attr('title')+'">'+$(el).next().html()+'</span>\n\
                    <input type="hidden" name="users[]" value="'+userID+'">\n\
                </div>';
                
                $('#'+containerID).append(code);
                
                $('#'+userRowIDPrefix+userID).fadeOut('fast');
                
            }
            
            function removeUserFromContainer(userRowIDPrefix,userID) {
                $('#remove_'+userRowIDPrefix+userID).remove();
                $('#'+userRowIDPrefix+userID).find('input').prop('checked',false);
                $('#'+userRowIDPrefix+userID).fadeIn('fast');
            }
            
            
            
            
            
        </script>
        
        
        
    </body>
</html>
