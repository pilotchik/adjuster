<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  


require_once APPPATH."/third_party/UPnP-master/src/Core.php";
require_once APPPATH."/third_party/UPnP-master/src/Upnp.php";
require_once APPPATH."/third_party/UPnP-master/src/Mediaserver.php";
require_once APPPATH."/third_party/UPnP-master/src/Mediaserver/Browse.php";


class UPNPAgent {
    
    public function __construct() {
	
    }
    
    static function discover() {
	//Upnp::discover();
        $upnp = new Upnp();
        
        echo 'hello<br>';
        
        //print_r($upnp);
        
        $everything = $upnp->discover();

        if(!count($everything)){
            print_r('no upnp devices found'.PHP_EOL);
        }
        
        //print_r($everything);
        //exit;
        foreach($everything as $device){
            //print_r($device);  //uncomment to see all available array elements for a device.
            $info = $device['description']['device'];
            $summary = $info['friendlyName'].', '.$info['modelName'].', '.$info['UDN'].'<br>';
            print($summary.PHP_EOL);
        }
   
    }
    
    
    static function mediaservers($friendlyName = '') {
        
        $files = array();
        
        $mediaserver = new Mediaserver();

        $servers = $mediaserver->discover();

        if(!count($servers)){
            print_r('no upnp mediaservers found'.PHP_EOL);
        }

        foreach($servers as $server){
            
            
            if (isset($server['description']['device']) && $server['description']['device']['friendlyName'] == $friendlyName) {
                $browse = new Browse($server);
                $directories = $browse->browse();
                //$summary = $info['friendlyName'].', '.$info['modelDescription'].', '.$info['modelName'].', '.$info['UDN'];
                //print($summary.PHP_EOL);
                foreach($directories as $dir) {
                    if ($dir['upnp:class'] == 'object.item.videoItem') $files[] = $dir;
                }
                break;
            }
        }

        return $files;
        
    }

}
