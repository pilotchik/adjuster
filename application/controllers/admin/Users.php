<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin') {
            header('Location:'.base_url ());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            $this->load->vars(array('globalActiveLink' => 'admin/users'));
        }
    }

    
    
    public function index() {
        
        $this->log_model->addIndexCounter('users');
        
        if ($this->config->item('config_use_ldap') && $this->session->userdata('role') == 'superuser') {
            try {
                $data['ldap_users'] = $this->users_model->getNewLDAPUsers();
                foreach($data['ldap_users'] as &$ldap_user) {
                    $name_arr = explode(' ',$ldap_user['fullname']);
                    $ldap_user['surname'] = isset($name_arr[0]) ? $name_arr[0] : '';
                    $ldap_user['name'] = isset($name_arr[1]) ? $name_arr[1] : '';
                    $ldap_user['middlename'] = isset($name_arr[2]) ? $name_arr[2] : '';
                }
                if (count($data['ldap_users'])) {
                    $data['default_user'] = array(
                        'name' => $data['ldap_users'][0]['name'],
                        'surname' => $data['ldap_users'][0]['surname'],
                        'middlename' => $data['ldap_users'][0]['middlename']
                    );
                }
            } catch (Exception $ex) {
                $data['system_error'] = $this->lang->line('error_ldap_use').': '.$ex->getMessage().' ('.$ex->getCode().')';
                $data['system_error_type'] = 'danger';
            }
        }
        
        
        $data['users'] = $this->users_model->getAllUsers('*',true);
        $data['additionalFields'] = $this->users_model->getAllAccountFields(true,true);
        
        $data['site_title'] = $this->lang->line('users_managing');
        
        $data['site_add_buttons'] = '<li>';
        if ($this->config->item('config_use_ldap') && isset($data['ldap_users']) && !empty($data['ldap_users']) && ($this->session->userdata['login'] == 'admin'  || $this->session->userdata('role') == 'superuser')) {
            
            $data['site_add_buttons'] .= '<button type="button" class="btn btn-success" id="addUserButtonID" onclick="$(\'#addLDAPUserModal\').modal(\'show\');">
                <i class="fa fa-plus fa-lg"></i> '.$this->lang->line('btn_add_user').'
            </button>

            <a type="button" class="btn btn-success" href="'.base_url().'admin/users/add_all_ldap_users">
                <i class="fa fa-plus fa-lg"></i> '.$this->lang->line('btn_add_all_user').'
            </a>';

        } elseif (($this->session->userdata['login'] == 'admin'  || $this->session->userdata('role') == 'superuser') && !$this->config->item('config_use_ldap')) {
        
            $data['site_add_buttons'] .= '<button type="button" class="btn btn-success" id="addUserButtonID" onclick="$(\'#addUserModal\').modal(\'show\');">
                <i class="fa fa-plus fa-lg"></i> '.$this->lang->line('btn_add_user').'
            </button>';
        
        }
        $data['site_add_buttons'] .= '</li>';

        
        $this->load->view('admin/admin_users_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    public function block() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->input->post('user_id'));
        
        if (!empty($user)) {
            $update_fields = array('active' => 0);

            $this->users_model->updateUser($user['id'],$update_fields);

            $this->log_model->addMessageLog($this->lang->line('event_users_block').' '.$this->users_model->getFullNameOverID($user['id']),'Users');
            $this->log_model->addErrorInfo($this->users_model->getFullNameOverID($user['id'],true).' '.$this->lang->line('user_profile_blocked_success'),'success');
        }
            
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    public function unblock() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->uri->segment(4));
        
        if (!empty($user)) {
            $update_fields = array('active' => 1);

            $this->users_model->updateUser($user['id'],$update_fields);
            
            $this->log_model->addMessageLog($this->lang->line('event_users_unblock').' '.$this->users_model->getFullNameOverID($user['id']),'Users');
            $this->log_model->addErrorInfo($this->users_model->getFullNameOverID($user['id'],true).' '.$this->lang->line('user_profile_unblocked_success'),'success');
        
        }
            
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    /**
     * Обновление информации о пользователе
     */
    public function update_user() {
        
        $this->form_validation->set_rules('surname', 'lang:user_surname', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('name', 'lang:user_name', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('middlename', 'lang:user_middlename', 'trim|max_length[150]');
        $this->form_validation->set_rules('comment', 'lang:user_comment', 'trim');
        
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('admin_user_profile_change_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $user = $this->users_model->getOneUserInfoOverID($this->input->post('user_id'));
            
            if (!empty($user)) {
                $new_user_type = $this->input->post('type');
                
                if (!$new_user_type) $new_user_type = $user['user_type'];
                
                $birthday = $this->input->post('birthday');
                if (mb_strpos($birthday,'.') !== FALSE) {
                    $birth_array = explode('.', $birthday);
                    
                    $birthday = mktime(0,0,0,$birth_array[0],$birth_array[1],$birth_array[2]);
                }
                
                $emailStatus = false;
                
                $this->load->helper('security');
                
                //Проверить, нет ли этого логина у других пользователей
                $loginStatus = $this->users_model->checkLogin(xss_clean($this->input->post('login')),$user['id']);
                if (xss_clean($this->input->post('email')) != '') $emailStatus = $this->users_model->checkLogin(xss_clean($this->input->post('email')),$user['id']);
                
                if ($loginStatus || $emailStatus)  {
                    $this->log_model->addErrorInfo($this->lang->line('user_error_updating_login_double'),'danger');
                    header('Location:'.base_url ().'admin/users');
                    exit;
                }
                    
                
                $update_fields = array(
                    'name' => xss_clean($this->input->post('name')),
                    'login' => xss_clean($this->input->post('login')),
                    'middlename' => xss_clean($this->input->post('middlename')),
                    'surname' => xss_clean($this->input->post('surname')),
                    'comment' => xss_clean($this->input->post('comment')),
                    'email' => xss_clean($this->input->post('email')),
                    'phone' => xss_clean($this->input->post('phone')),
                    'redirect_url' => $this->input->post('redirect_url'),
                    'user_type' => $new_user_type,
                    'birthday' => $birthday
                );
                
                //Обновить и дополнительные поля
                $additionalFields = $this->users_model->getProfileAdditionalFields($user['id'],true);
                foreach($additionalFields['contacts'] as $addfield) {
                    $this->users_model->updateAdditionalField($addfield['id'],$user['id'],$this->input->post('field_'.$addfield['id']));
                }
                foreach($additionalFields['profile'] as $addfield) {
                    $this->users_model->updateAdditionalField($addfield['id'],$user['id'],$this->input->post('field_'.$addfield['id']));
                }

                $this->users_model->updateUser($user['id'],$update_fields);


                $tags = $this->input->post('tags');

                $this->tags_model->removeAllUsersTags($this->input->post('user_id'));
                
                if (!empty($tags)) {
                    
                    foreach($tags as $inputTag) {
                        
                        //Узнать идентификатор тега
                        $tagName = mb_strtoupper(mb_substr($inputTag,0,1)).mb_substr($inputTag,1);
                        
                        $tagID = $this->tags_model->getTagIDOverName($tagName);
                      
                        //Если такого тега не было и есть разрешение на создание тегов, то создать его
                        if (!$tagID && $this->load->get_var('config_allow_tag_adding')) {
                            $tag_fields = array(
                                'name' => $tagName,
                                'entity_type' => 'user',
                                'ts' => time()
                            );

                            $tagID = $this->tags_model->createTag($tag_fields);

                            $this->log_model->addMessageLog($this->lang->line('event_user_tag_add').' "'.$tagName.'"','Tags');

                            $status = 0;
                        } else {

                            $status = $this->tags_model->checkTagForUserStatus($tagID,$user['id']);
                        }

                        //Прикрепить тег к пользователю, если тег ещё не прикреплён
                        if (!$status) {
                            $userstags_fields = array(
                                'user_id' => $user['id'],
                                'tag_id' => $tagID,
                                'creator_id' => $this->session->userdata['userID'],
                                'ts' => time()
                            );

                            $this->tags_model->addTagsToUser($userstags_fields);
                            
                            $this->log_model->addMessageLog($this->lang->line('event_user_tag_to_user_add').' '.$this->users_model->getFullNameOverID($user['id']).' - "'.$tagName.'"','Tags');
                            
                        }
                    
                    }
                    
                }
                
                //Чистим кэш этого господина
                $this->users_model->clearUsersCache($user['id']);
                    
                $this->log_model->addMessageLog($this->lang->line('event_admin_profile_edit').' '.$this->users_model->getFullNameOverID($user['id']),'Users');

                //Если изменился тип, тоже зафиксировать
                if ($new_user_type != $user['user_type'])
                    $this->log_model->addMessageLog($this->lang->line('event_admin_type_edit').' '.$this->users_model->getFullNameOverID($user['id']).'. '.$this->lang->line('user_type_'.$user['user_type']).'->'.$this->lang->line('user_type_'.$new_user_type),'Users');
            
            }
            
            $this->log_model->addErrorInfo($this->lang->line('admin_user_profile_change_success'),'success');
        
        }
            
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    /**
     * Удаление пользователя
     */
    public function remove() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->input->post('user_id'));
        
        if (!empty($user)) {
            
            $update_fields = array('archive' => 1);
            
            $this->users_model->removeUserDirectory('./upload/users/'.$user['id']);
            
            $this->users_model->updateUser($user['id'],$update_fields);

            $this->users_model->removeUserInfo($this->input->post('user_id'));

            //Удаление комнат пользователя
            $userRoomsIDs = $this->rooms_model->getMyRooms($this->input->post('user_id'),true,true);
            foreach($userRoomsIDs as $roomID) {
                $this->rooms_model->removeRoom($roomID);
            }

            //Удаление проектов пользователя
            $user_projects = $this->projects_model->getUserProjects($this->input->post('user_id'));
            foreach($user_projects as $project) {
                $this->projects_model->removeProject($project['id']);
            }

            $this->log_model->addMessageLog($this->lang->line('event_admin_user_remove').' '.$this->users_model->getFullNameOverID($user['id']),'Users');
            $this->log_model->addErrorInfo($this->lang->line('user_profile_removed_success1').$this->users_model->getFullNameOverID($user['id'],true).$this->lang->line('user_profile_removed_success2'),'success');
        }

        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    
    
    /**
     * Добавление пользователя LDAP
     */
    public function add_ldap_user() {
        
        $this->form_validation->set_rules('ldap_login', 'lang:user_ldap', 'trim|required');
        $this->form_validation->set_rules('surname', 'lang:user_surname', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('name', 'lang:user_name', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('middlename', 'lang:user_middlename', 'trim|max_length[150]');
        $this->form_validation->set_rules('comment', 'lang:user_comment', 'trim');
            
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('error_admin_user_ldap_add_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            if ($this->users_model->checkLogin('ldap_'.$this->input->post('ldap_login'))) {
                
                //Восстанавливаем
                $user = $this->users_model->getOneUserInfo('ldap_'.$this->input->post('ldap_login'));
                
                $update_fields = array(
                    'archive' => 0,
                    'timestamp' => time()
                );
                
                $this->users_model->updateUser($user['id'],$update_fields);
                $new_user_id = $user['id'];
                
            } else {
            
            
                $reg_fields = array(
                    'login' => 'ldap_'.$this->input->post('ldap_login'),
                    'password' => 'ldap_'.$this->input->post('ldap_login'),
                    'email' => $this->input->post('ldap_login').'@'.$this->config->item('config_ldap_mail'),
                    'name' => $this->input->post('name'),
                    'middlename' => $this->input->post('middlename'),
                    'surname' => $this->input->post('surname'),
                    'comment' => pg_escape_string($this->input->post('comment')),
                    'user_type' => $this->input->post('type'),
                    'ldap_login' => $this->input->post('ldap_login'),
                );

                $new_user_id = $this->users_model->createUser($reg_fields);

                //Грант роли доступа
                $grants = array();
                if ($this->config->item('config_grant_user_roles')) {
                    $gr_arr = explode(',', $this->config->item('config_grant_user_roles'));
                    foreach($gr_arr as $gr) {
                        $gr = trim($gr);
                        if ($gr != '') $grants[] = $gr;
                    }

                    $this->users_model->createRole($this->input->post('ldap_login'));

                    foreach($grants as $gr) {
                        $this->users_model->grantRole($this->input->post('ldap_login'),$gr);
                    }

                }

                $message = '<h1>'.$this->lang->line('user_ldap_registration_mail_message1').$reg_fields['name'].' '.$reg_fields['middlename'].'</h1><br>'.$this->lang->line('user_ldap_registration_mail_message2').'<br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message3').'<br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message4').'<br><br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message5').'<br>';

                /*
                 * Отправка письма об успешной регистрации
                 */
                $mail_fields = array(
                    'to' => $reg_fields['email'],
                    'subject' => $this->lang->line('user_ldap_registration_mail_subject').' "'.$this->load->get_var('config_title_short').'"',
                    'message' => $message
                );
                $this->mail_model->sendMail($mail_fields);


                
                /*
                // Отправка уведомлений по XMPP
                $xmpp_params = array(
                    'user' => $this->load->get_var('config_xmpp_user'),
                    'pass' => $this->load->get_var('config_xmpp_pass'),
                    'host' => $this->load->get_var('config_xmpp_host'),
                    'port' => $this->load->get_var('config_xmpp_port'),
                    'domain' =>  $this->load->get_var('config_xmpp_domain'),
                    'resource' => '',
                    'status' => $this->load->get_var('config_xmpp_status'),
                    'logtxt' => $this->config->item('config_xmpp_logtxt'),
                    'log_file_name' => $this->config->item('config_xmpp_logfilename'),
                    'tls_off' => $this->config->item('config_xmpp_tlsoff')
                );

                $this->load->library('xmpp', $xmpp_params);

                $this->xmpp->connect();

                @$this->xmpp->sendMessage($reg_fields['email'],$this->lang->line('user_ldap_registration_mail_message2').' <a href="'.base_url().'">'.$this->load->get_var('config_title_short').'</a>');
                 * 
                 */
            }
            
            $this->log_model->addMessageLog($this->lang->line('event_admin_ldap_user_add').' '.$this->input->post('ldap_login'),'Users');
            $this->log_model->addErrorInfo($this->lang->line('user').' '.$this->users_model->getFullNameOverID($new_user_id,true).' '.$this->lang->line('user_ldap_added_success'),'success');
            
        }
            
        header('Location:'.base_url ().'admin/users');
    }
    
    
    /**
     * Добавление обычных пользователей
     */
    public function add_simple_user() {
        
        $this->form_validation->set_rules('surname', 'lang:user_surname', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('name', 'lang:user_name', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('middlename', 'lang:user_middlename', 'trim|max_length[150]');
        $this->form_validation->set_rules('comment', 'lang:user_comment', 'trim');
            
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('error_admin_user_simple_add_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $newLogin = $this->users_model->getLastID() + 131450;
            $newPass = chr(rand(65,90)).chr(rand(65,90)).chr(rand(97,110)).chr(rand(97,115)).rand(100,999);
                    
            
            $reg_fields = array(
                'login' => $newLogin,
                'password' => $newPass,
                'email' => '',
                'name' => $this->input->post('name'),
                'middlename' => $this->input->post('middlename'),
                'surname' => $this->input->post('surname'),
                'comment' => pg_escape_string($this->input->post('comment')),
                'user_type' => $this->input->post('type'),
                'ldap_login' => $newPass,
            );
                
            $new_user_id = $this->users_model->createUser($reg_fields);
            
            $this->log_model->addMessageLog($this->lang->line('event_admin_simple_user_add').' '.$this->input->post('name').' '.$this->input->post('surname').' ('.$newLogin.')','Users');
            $this->log_model->addErrorInfo($this->lang->line('user').' '.$this->input->post('name').' '.$this->input->post('surname').' ('.$newLogin.') '.$this->lang->line('user_ldap_added_success'),'success');
            
        }
        
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    /**
     * Обнуление пароля
     */
    public function reset_pass() {
        
        $this->form_validation->set_rules('user_id', 'lang:user_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('user_new_pass', 'lang:user_new_pass', 'trim|required');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('error_admin_userpass_reseting_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $user = $this->users_model->getOneUserInfoOverID($this->input->post('user_id'));
            
            $fields = array(
                'password' => @crypt($this->input->post('user_new_pass')),
                'ldap_login' => $this->input->post('user_new_pass')
            );
            
            $this->users_model->updateUser($user['id'],$fields);

            $this->log_model->addMessageLog($this->lang->line('event_admin_userpass_reseting').' '.$user['shortName'],'Users');
            $this->log_model->addErrorInfo($this->lang->line('user_pass_reseting_success'),'success');
            
        }
            
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
    /**
     * Получение конфигурации профиля
     */
    public function profile_config() {
        
        $data['globalActiveLink'] = 'admin/users/profile_config';
        
        $data['fields'] = $this->users_model->getAllAccountFields();
        
        $data['site_title'] = $this->lang->line('profile_config');
        
        $data['site_add_buttons'] = '<li>';
            $data['site_add_buttons'] .= '<button type="button" class="btn btn-success" onclick="$(\'#addItemModal\').modal(\'show\');">
                <i class="fa fa-plus fa-lg"></i> '.$this->lang->line('btn_add_field').'
            </button>';
            $data['site_add_buttons'] .= '</li>';
        
        $this->load->view('admin/admin_users_profile_config_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    /**
     * Создание поля учётной записи
     */
    public function profile_add_field() {
        
        $this->form_validation->set_rules('field_name', 'lang:profile_field_name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('field_description', 'lang:profile_field_description', 'trim');
        $this->form_validation->set_rules('field_type', 'lang:profile_field_type', 'trim|required');
        $this->form_validation->set_rules('field_info_type', 'lang:profile_field_info_type', 'trim|required');
        
        if (!$this->form_validation->run()) {

            $this->log_model->addErrorInfo($this->lang->line('error_admin_profile_field_adding_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $fields = array(
                'name' => $this->input->post('field_name'),
                'description' => $this->input->post('field_description'),
                'type' => $this->input->post('field_type'),
                'info_type' => $this->input->post('field_info_type'),
                'status' => 1,
                'admin_changing' => $this->input->post('field_admin_changing') == 1 ? 1 : 0,
                'required' => $this->input->post('field_required') == 1 ? 1 : 0,
                'creator_id' => $this->session->userdata['userID'],
                'ts' => time()
            );
            
            $this->users_model->createField($fields);
            
            $this->log_model->addMessageLog($this->lang->line('event_admin_profile_field_add').' "'.$fields['name'].'"','Users');
            $this->log_model->addErrorInfo($this->lang->line('profile_field_adding_success'),'success');
            
        }
        
        header('Location:'.base_url ().'admin/users/profile_config');
        
    }
    
    
    /**
     * Создание поля учётной записи
     */
    public function profile_edit_field() {
        
        $this->form_validation->set_rules('field_id', 'lang:profile_field_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('field_name', 'lang:profile_field_name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('field_description', 'lang:profile_field_description', 'trim');
        $this->form_validation->set_rules('field_type', 'lang:profile_field_type', 'trim|required');
        $this->form_validation->set_rules('field_info_type', 'lang:profile_field_info_type', 'trim|required');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('error_admin_profile_field_updating_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $field = $this->users_model->getOneField($this->input->post('field_id'));
            
            if (!empty($field)) {
            
                $fields = array(
                    'name' => $this->input->post('field_name'),
                    'description' => $this->input->post('field_description'),
                    'type' => $this->input->post('field_type'),
                    'info_type' => $this->input->post('field_info_type'),
                    'status' => $this->input->post('field_status'),
                    'admin_changing' => $this->input->post('field_admin_changing') == 1 ? 1 : 0,
                    'required' => $this->input->post('field_required') == 1 ? 1 : 0,
                );

                $this->users_model->updateField($field['id'],$fields);

                $this->log_model->addMessageLog($this->lang->line('event_admin_profile_field_update').' "'.$fields['name'].'"','Users');
                $this->log_model->addErrorInfo($this->lang->line('profile_field_updating_success'),'success');
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('profile_field_error_not_found'),'danger');
                
            }
            
        }
        
        header('Location:'.base_url ().'admin/users/profile_config');
        
    }
    
    
    /**
     * Удаление поля учётной записи
     */
    public function profile_remove_field() {
        
        $this->form_validation->set_rules('field_id', 'lang:profile_field_id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {

            $this->log_model->addErrorInfo($this->lang->line('error_admin_profile_field_removing_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $field = $this->users_model->getOneField($this->input->post('field_id'));
            
            if (!empty($field)) {
            
                $this->users_model->removeField($field['id']);

                $this->log_model->addMessageLog($this->lang->line('event_admin_profile_field_remove').' "'.$field['name'].'"','Users');
                $this->log_model->addErrorInfo($this->lang->line('profile_field_removing_success'),'success');
            
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('profile_field_error_not_found'),'danger');
                
            }
            
        }
        
        header('Location:'.base_url ().'admin/users/profile_config');
        
    }
    
    
    /**
     * Добавление пользователя LDAP
     */
    public function add_all_ldap_users() {
        
        $data['ldap_users'] = $this->users_model->getNewLDAPUsers();
        foreach($data['ldap_users'] as $ldap_user) {
            $name_arr = explode(' ',$ldap_user['fullname']);
            $ldap_user['surname'] = isset($name_arr[0]) ? $name_arr[0] : '';
            $ldap_user['name'] = isset($name_arr[1]) ? $name_arr[1] : '';
            $ldap_user['middlename'] = isset($name_arr[2]) ? $name_arr[2] : '';
        
            if ($this->users_model->checkLogin('ldap_'.$ldap_user['login'])) {
                
                //Восстанавливаем
                $user = $this->users_model->getOneUserInfo('ldap_'.$ldap_user['login']);
                
                $update_fields = array(
                    'archive' => 0,
                    'timestamp' => time()
                );
                
                $this->users_model->updateUser($user['id'],$update_fields);
                
            } else {
            
                $reg_fields = array(
                    'login' => 'ldap_'.$ldap_user['login'],
                    'password' => 'ldap_'.$ldap_user['login'],
                    'email' => $ldap_user['login'].'@'.$this->config->item('config_ldap_mail'),
                    'name' => $ldap_user['name'],
                    'middlename' => $ldap_user['middlename'],
                    'surname' => $ldap_user['surname'],
                    'comment' => '',
                    'user_type' => 'user',
                    'ldap_login' => $ldap_user['login'],
                );

                $new_user_id = $this->users_model->createUser($reg_fields);

                //Грант роли доступа
                $grants = array();
                if ($this->config->item('config_grant_user_roles')) {
                    $gr_arr = explode(',', $this->config->item('config_grant_user_roles'));
                    foreach($gr_arr as $gr) {
                        $gr = trim($gr);
                        if ($gr != '') $grants[] = $gr;
                    }

                    $this->users_model->createRole($ldap_user['login']);

                    foreach($grants as $gr) {
                        $this->users_model->grantRole($ldap_user['login'],$gr);
                    }

                }

                $message = '<h1>'.$this->lang->line('user_ldap_registration_mail_message1').$reg_fields['name'].' '.$reg_fields['middlename'].'</h1><br>'.$this->lang->line('user_ldap_registration_mail_message2').'<br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message3').'<br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message4').'<br><br>';
                $message .= $this->lang->line('user_ldap_registration_mail_message5').'<br>';

                /*
                 * Отправка письма об успешной регистрации
                 */
                $mail_fields = array(
                    'to' => $reg_fields['email'],
                    'subject' => $this->lang->line('user_ldap_registration_mail_subject').' "'.$this->load->get_var('config_title_short').'"',
                    'message' => $message
                );
                $this->mail_model->sendMail($mail_fields);
                
            }
            
            $this->log_model->addMessageLog($this->lang->line('event_admin_ldap_user_add').' '.$ldap_user['login'],'Users');

        }
        
        $this->log_model->addErrorInfo($this->lang->line('user_ldap_all_added_success'),'success');
        header('Location:'.base_url ().'admin/users');
        
    }
    
    
}
