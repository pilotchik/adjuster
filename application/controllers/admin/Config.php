<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin')
            header('Location:'.base_url ());
        else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            $this->load->vars(array('globalActiveLink' => 'admin/config'));
        }
    }

    
    
    public function index() {
        
        $this->log_model->addIndexCounter('config');
        
        $data['config'] = $this->config_model->getConfigForAdmin();
        
        $data['site_title'] = $this->lang->line('config');
        
        $this->load->view('admin/admin_config_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    
    public function update() {
        $config = $this->config_model->getConfig();
        
        foreach ($config as $key=>$value) {
            $this->config_model->updateConfig($key, pg_escape_string($this->input->post($key)));
        }
        
        $this->log_model->addMessageLog($this->lang->line('event_admin_configuration_changed'),'Config');
        $this->log_model->addErrorInfo($this->lang->line('admin_config_changed'),'success');
        header('Location:'.base_url ().'admin/config');
        
    }
    
    
}
