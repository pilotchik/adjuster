<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Polls extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin') {
            header('Location:'.base_url ());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            
            $this->load->model('polls_model');
            $this->load->vars(array('globalActiveLink' => 'admin/polls'));
        }
    }

    
    public function index() {
        
        $data['public_polls'] = $this->polls_model->getPublicPolls();
        
        $data['site_title'] = $this->lang->line('polls_public_moderating');
        
        $this->load->view('admin/admin_polls_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    /**
     * Модерация
     */
    public function moderate() {
        
        $pollID = $this->uri->segment('4');
        $status = $this->uri->segment('5');
        
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            $fields = array(
                'moderator_id' => $status ? $this->session->userdata['userID'] : 0
            );
            $this->polls_model->updatePoll($pollID,$fields);
            
            //Если статус изменился на true, то оповестить об этом всех пользователей
            if ($status) {
                    
                $users = $this->users_model->getAllUsers('id');
                
                foreach($users as $user) {
                    
                    $notify_array = array(
                        'recipient_id' => $user['id'],
                        'entity_type' => 'poll',
                        'entity_id' => $pollID,
                        'subject' => $this->lang->line('notification_poll_invite_subject').' "'.$poll['title'].'"',
                        'body' => $this->lang->line('notification_poll_invite_body').' <a href="'.base_url().'polls/play/'.$pollID.'" target="blank">"'.$poll['title'].'"</a>',
                    );

                    $this->notification_model->makeNotification($notify_array,false);

                }
                
                $this->notification_model->sendAllNotifications();
                
            }
            
            $this->log_model->addMessageLog(($status ? $this->lang->line('event_poll_moderate') : $this->lang->line('event_poll_unmoderate')).' "'.htmlspecialchars($poll['title']).'"','Polls');
            $this->log_model->addErrorInfo($this->lang->line('poll_moderate_success'),'success');
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            
        }
        
        header('Location:'.base_url ().'admin/polls');

    }
    
}
