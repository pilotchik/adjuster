<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {
    
    function __construct() {
        
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin') {
            header('Location:'.base_url ());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            
            $this->load->vars(array('globalActiveLink' => 'admin/notifications'));
            
        }
    }

    
    public function index() {
        
        $data['notifications'] = $this->notification_model->getNotifications(250,0);
        
        $this->load->view('admin/admin_notifications_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    /**
     * Очистка журнала
     */
    public function remove() {
        $this->notification_model->removeAllNotifications();
        
        $this->log_model->addErrorInfo($this->lang->line('notification_removing_success'),'success');
        header('Location:'.base_url ().'admin/notifications');

    }

        
}
