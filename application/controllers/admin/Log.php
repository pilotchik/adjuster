<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin') {
            header('Location:'.base_url ());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            $this->load->vars(array('globalActiveLink' => 'admin/log'));
        }
    }

    public function index() {
        
        $startTS = $this->input->get('begin');
        $startTS = isset($startTS) ? $this->input->get('begin') : 0;
        $endTS = $this->input->get('end');
        $endTS = isset($endTS) ? $this->input->get('end') : 0;
        
        $data['events'] = $this->log_model->getEvents($startTS,$endTS);
        
        
        foreach($data['events'] as $ekey => $event) {
            $data['events'][$ekey]['user'] = $this->users_model->getOneUserInfoOverID($event['user_id'],true);
            $data['events'][$ekey]['userName'] = $this->users_model->getFullNameOverID($event['user_id'],true);
            $data['events'][$ekey]['userFullName'] = $this->users_model->getFullNameOverID($event['user_id']);
            $data['events'][$ekey]['date'] = date($this->load->get_var('config_datetime_format'),$event['ts']);
            $data['events'][$ekey]['status'] = htmlspecialchars($event['status']);
            switch($event['controller']) {
                case 'Users': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_user'); break;
                case 'Profile': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_profile'); break;
                case 'Main': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_main'); break;
                case 'Tags': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_tags'); break;
                case 'Posts': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_posts'); break;
                case 'Rooms': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_rooms'); break;
                case 'Structure': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_structure'); break;
                case 'Projects': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_projects'); break;
                case 'Schedule': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_schedule'); break;
                case 'Equipments': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_equipment'); break;
                case 'Notes': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_notes'); break;
                case 'Polls': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_polls'); break;
                case 'Files': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_files'); break;
                case 'Sked': $data['events'][$ekey]['typeDescription'] = $this->lang->line('event_sked'); break;
                case 'Im': $data['events'][$ekey]['typeDescription'] = $this->lang->line('im_event_description'); break;
                default: $data['events'][$ekey]['typeDescription'] = ''; break;
            }
            
        }
        
        
        $data['site_title'] = $this->lang->line('system_log');
        $data['site_subtitle'] = $this->input->get('begin') ? ' ('.date($this->load->get_var('config_datetime_format'),($this->input->get('begin') ? $this->input->get('begin') : time())).' - '.date($this->load->get_var('config_datetime_format'),($this->input->get('end') ? $this->input->get('end') : time())).')' : '';
        
        $data['site_add_buttons'] = '<li>';
        
        if (count($data['events'])) {
            $data['site_add_buttons'] .= '<button type="submit" class="btn btn-sm btn-danger inlineHeaderToolsElement" onclick="document.remove_selected_form.submit();">'.$this->lang->line('btn_filter_clear_selected').'</button>';
            $data['site_add_buttons'] .= '</li><li>';
            $data['site_add_buttons'] .= '<button type="button" class="btn btn-danger btn-sm" onclick="$(\'#removeLogConfirmModal\').modal(\'show\');">'.$this->lang->line('btn_clear_log').'</button>';
        }
        
        $data['site_add_buttons'] .= '</li>';
        
        $this->load->view('admin/admin_log_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    /**
     * Очистка журнала
     */
    public function remove() {
        
        $this->log_model->removeAllLogs();
        
        $this->log_model->addErrorInfo($this->lang->line('log_removing_success'),'success');
        header('Location:'.base_url ().'admin/log');
    
    }
    
    
    /**
     * Удаление выбранных событий
     */
    public function remove_selected() {
        
        $events = $this->input->post('events');
        
        if (count($events)) {
            
            foreach($events as $event) {
                $this->log_model->removeOneLogEvent($event);
            }
            $this->log_model->addErrorInfo($this->lang->line('log_selected_removing_success'),'success');
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('log_selected_removing_fail'),'danger');
            
        }
        
        header('Location:'.base_url ().'admin/log');
    
    }

    
    public function metrika() {
        
        $data['globalActiveLink'] = 'admin/log/metrika';
        
        
        /*
         * Собрать статистику по дням (пока)
         */
        $data['metrik_dates'] = array();
        $startDayFromBD = $this->log_model->getStartMetriks();
        
        $previousDate = $startDay = mktime(0,0,0, date("m",$startDayFromBD), date("d",$startDayFromBD), date("Y",$startDayFromBD));
        
        $allMetricsRecords = $this->log_model->getMetriksRecordsInRange($startDay,time());
        
        for ($i = $startDay +  60 * 60 * 24; $i < time() + 60 * 60 * 24; $i = $i + 60 * 60 * 24) {
            
            $recordsCount = 0;
            
            foreach($allMetricsRecords as $metricRecord) {
                
                if ($metricRecord['ts'] >= $previousDate && $metricRecord['ts'] < $i) $recordsCount++;
                
            }
            
            $data['metrik_dates'][] = array(
                'date' => date('d.m.Y',$previousDate),
                //'count' => $this->log_model->getMetriksDateRangeCount($previousDate,$i)
                'count' => $recordsCount
            );
            $previousDate = $i;
        }
        
        if (date("Y",$startDay) == date("Y")) {
            $monthCount = date("m") - date("m",$startDay);
        } else {
            $monthCount = (date("Y") - date("Y",$startDay) - 1) * 12 +  (12 - date("m",$startDay) + date("m"));
        }
        
        
        $previousDate = mktime(0,0,0, date("m",$startDay), 1, date("Y",$startDay));
        for ($i = 0; $i <= $monthCount; $i++) {
            $nextDate = mktime(0,0,0, date("m",$startDay) + $i + 1, 1, date("Y",$startDay));
            
            $recordsCount = 0;
            
            foreach($allMetricsRecords as $metricRecord) {
                
                if ($metricRecord['ts'] >= $previousDate && $metricRecord['ts'] < $nextDate) $recordsCount++;
                
            }
            
            $data['metrik_months'][] = array(
                'date' => date('m.Y',$previousDate),
                //'count' => $this->log_model->getMetriksDateRangeCount($previousDate,$nextDate)
                'count' => $recordsCount
            );
            
            $previousDate = $nextDate;
        }
        
        $data['site_title'] = $this->lang->line('metrika');
        
        $data['site_add_buttons'] = '<li>';
        $data['site_add_buttons'] .= '<button type="button" class="btn btn-danger" onclick="$(\'#removeLogConfirmModal\').modal(\'show\');">'.$this->lang->line('btn_clear_metrika').'</button>';
        $data['site_add_buttons'] .= '</li>';
        
        $this->load->view('admin/admin_log_metrika_view',$data);
        $this->session->unset_userdata('error');
    }
    
    
    public function remove_metrics() {
        $this->log_model->removeAllMetrics();
        
        $this->log_model->addErrorInfo($this->lang->line('metrika_removing_success'),'success');
        header('Location:'.base_url ().'admin/log/metrika');

    }
    
    
    public function feedback() {
        
        $data['globalActiveLink'] = 'admin/log/feedback';
        
        $data['feedback_records'] = $this->log_model->getLastFeedbackMessages();
        
        $data['site_title'] = $this->lang->line('feedback_title');
        
        if (!empty($data['feedback_records'])) {
            $data['site_add_buttons'] = '<li>';
            $data['site_add_buttons'] .= '<button type="button" class="btn btn-danger" onclick="$(\'#removeLogConfirmModal\').modal(\'show\');">'.$this->lang->line('btn_clear_log').'</button>';
            $data['site_add_buttons'] .= '</li>';
        }
        
        $this->load->view('admin/admin_log_feedback_view',$data);
        $this->session->unset_userdata('error');
    }
    
    
    public function remove_feedback() {
        
        $this->log_model->removeAllFeedback();
        
        $this->log_model->addErrorInfo($this->lang->line('feedback_success_removing'),'success');
        header('Location:'.base_url ().'admin/log/feedback');
        
    }
    
    
}
