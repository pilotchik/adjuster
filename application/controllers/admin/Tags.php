<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType != 'admin') {
            header('Location:'.base_url());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            $this->load->vars(array('globalActiveLink' => 'admin/tags'));
        }
    }

    public function index() {
        
        $data['tags'] = $this->tags_model->getAllTags();
        $this->load->view('admin/admin_tags_index_view',$data);
    }
    
    
    /**
     * Ручное создание тега
     */
    public function add_tag() {
        $this->form_validation->set_rules('tag_name', 'lang:tags_name', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('error_admin_tag_create_fail').'&error_type=danger');
        } else {
            
            $tag_fields = array(
                'name' => mb_strtoupper(mb_substr($this->input->post('tag_name'),0,1)).mb_substr($this->input->post('tag_name'),1),
                'entity_type' => $this->input->post('tag_entity_type'),
                'ts' => time()
            );
                
            $this->tags_model->createTag($tag_fields);
        
            $this->log_model->addMessageLog($this->lang->line('event_admin_tag_add').' "'.$this->input->post('tag_name').'"','Tags');
                
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('tag_added_success').'&error_type=success');
        }
    }
    
    
    public function update() {
        
        $this->form_validation->set_rules('tag_name', 'lang:tags_name', 'trim|required');
        $this->form_validation->set_rules('tag_id', 'lang:tags_name', 'required');
        
        if (!$this->form_validation->run()) {
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('error_admin_tag_update_fail').'&error_type=danger');
        } else {
            
            $tag_fields = array(
                'name'  => $this->input->post('tag_name')
            );
                
            $this->tags_model->updateTag($this->input->post('tag_id'),$tag_fields);
        
            $this->log_model->addMessageLog($this->lang->line('event_admin_tag_update').' "'.$this->input->post('tag_name').'"','Tags');
                
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('tag_updated_success').'&error_type=success');
        }
        
        
    }
    
    
    public function remove() {
        $this->form_validation->set_rules('tag_id', 'lang:tags_name', 'required');
        if (!$this->form_validation->run()) {
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('error_admin_tag_removing_fail').'&error_type=danger');
        } else {
            
            $tag = $this->tags_model->getOneTag($this->input->post('tag_id'));
            
            if(!empty($tag)) {
                
                $this->tags_model->removeTag($this->input->post('tag_id'));

                $this->log_model->addMessageLog($this->lang->line('event_admin_tag_remove').' "'.$tag['name'].'"','Tags');
                
            }
            
            header('Location:'.base_url ().'admin/tags?error='.$this->lang->line('tag_removed_success').'&error_type=success');
            
        }
    }
    
}
