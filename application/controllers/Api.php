<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Обработка AJAX-запросов
 */

class Api extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
    }
    
    
    /**
     * Получение полной информации о комнате
     */
    public function getRoomInfo() {
        
        $roomID = $this->input->get('roomID');
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        if ($roomID && ctype_digit($roomID)) {
            
            $room = $this->rooms_model->getOneRoom($roomID);
            $roomUsers = $this->rooms_model->getRoomUsers($roomID,true);
            
            $resultArray = array('info'=> $room,'users' => $roomUsers);
            if ($format == 'json')
                echo json_encode($resultArray);
            else
                $this->printf_r($resultArray);
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_roomid')));
        }
        
    }
    
    
    
    /**
     * Получить информацию о проекте
     */
    public function getProjectInfo() {
        
        $projectID = $this->input->get('projectID');
        
        if ($projectID) {
            
            $project = $this->projects_model->getOneProject($projectID);
            $projectRooms = $project['rooms'];
            
            $userID = $this->session->userdata['userID'];
            if ($userID) $freeRooms = $this->rooms_model->getFreeRooms($userID);
            
            
            echo json_encode(array('info'=> $project,'rooms' => $projectRooms,'freeRooms' => $freeRooms));
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_projectid')));
        }
    }

    
    public function getUserInfo() {
        
        $userLogin = $this->input->get('login');
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        
        if ($userLogin) {
            
            $user = $this->users_model->getOneUserInfo($userLogin);
            if (!empty($user)) {
                $tags = $this->users_model->getUserTags($user['id']);

                if ($format == 'json')
                    echo json_encode(array('user'=> $user,'tags' => $tags));
                else
                    $this->printf_r (array('user'=> $user,'tags' => $tags));
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_user_not_found')));
            }
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_login')));
        }
        
    }
    
    
    public function getFiles() {
        $projectID = $this->input->get('project');
        $roomID = $this->input->get('room');
        $type = $this->input->get('type');
        
        $tag = $this->input->get('tag');
        $tag = urldecode($tag);
        if ($tag != '') $tag = mb_strtoupper(mb_substr($tag,0,1)).mb_substr($tag,1);
        
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        $forServer = $this->input->get('for_server') ? true : false;
            
        if ($projectID) {
            //video|text
            $projectFiles = $this->projects_model->getProjectFiles($projectID,$type,$tag,$forServer);
            
            if ($format != 'json') {
                $this->printf_r($projectFiles);
            } else {
                echo json_encode($projectFiles);
            }
            
        } elseif ($roomID) {
            
            if (is_numeric($roomID)) {
            
                $projectID = $this->rooms_model->getRoomsProjectID($roomID);

                if ($projectID) {
                    $projectFiles = $this->projects_model->getProjectFiles($projectID,$type,$tag,$forServer);
                    
                    if ($format != 'json') {
                        $this->printf_r($projectFiles);
                    } else {
                        echo json_encode($projectFiles);
                    }
                } else {
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('error_ajax_no_roomid')));
                }
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_no_roomid')));
            }
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_projectid')));
        }
    }


    
    public function getskedtypes() {
        
        $this->load->model('sked_model');
        
        $projectID = $this->input->get('project');
        $projectID = ctype_digit($projectID) && $projectID != '' ? $projectID : 0;
        
        $types = $this->sked_model->getAllItems('types',$projectID);

        $format = $this->input->get('format');

        if ($format != 'text') {
            echo json_encode(array('items' => $types));
        } else {
            $this->printf_r($types);
        }

    }
    
    
    public function getskedstatuses() {
        
        $this->load->model('sked_model');
        
        $projectID = $this->input->get('project');
        $projectID = ctype_digit($projectID) && $projectID != '' ? $projectID : 0;
        
        $statuses = $this->sked_model->getAllItems('statuses',$projectID);
        
        $format = $this->input->get('format');

        if ($format != 'text') {
            echo json_encode(array('items' => $statuses));
        } else {
            $this->printf_r($statuses);
        }

    }
    
}
