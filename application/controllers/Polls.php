<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Polls extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType == 'guest') {
            header('Location:'.base_url ());
            exit;
        } else {
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
            
            $this->load->vars(array('globalActiveLink' => 'polls'));
            $this->load->model('polls_model');
        }
    }
    
    
    /**
     * Главная страница
     */
    public function index() {
        
        $this->log_model->addIndexCounter('polls');
        
        //Получение активных опросов
        $data['active_polls'] = $this->polls_model->getPollsForMe($this->session->userdata['userID']);
        
		//Получение пройденных опросов
        $data['my_polls'] = array(
            'active' => $this->polls_model->getMyPolls($this->session->userdata['userID']),
            'archive' => $this->polls_model->getMyPolls($this->session->userdata['userID'],false)
        );
        
		$this->load->model('sked_model');
		$data['themes'] = $this->sked_model->getAllItems();
        
        $data['site_title'] = $this->lang->line('polls');
        
		
		if ($this->session->userdata('userType') == 'admin') {
			
			$data['site_add_buttons'] = '<li>';
			$data['site_add_buttons'] .= '<button type="button" class="btn btn-success" onclick="$(\'#addPollModal\').modal(\'show\');">
				<i class="fa fa-plus fa-lg"></i> '.$this->lang->line('btn_add_poll').'
			</button>';
			$data['site_add_buttons'] .= '</li>';
			
		}
        
        $this->load->view('polls/polls_index_view',$data);
        $this->session->unset_userdata('error');
         
    }
    
    
    /**
     * Добавление пользователей
     */
    public function add_poll() {
        
        $this->form_validation->set_rules('title', 'lang:poll_title', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('description', 'lang:poll_description', 'trim');
        $this->form_validation->set_rules('theme_id', 'lang:poll_theme_id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_creating_fail').'<br>'.  validation_errors(),'danger');
            
        } else {
            
            $this->load->helper('security');
            
            $fields = array(
                'title' => xss_clean($this->input->post('title',true)),
                'description' => $this->input->post('description',true),
                'access' => 'public',
                'active' => 1,
                'anon_status' => 0,
                'moderator_id' => $this->session->userdata['userID'],
                'creator_id' => $this->session->userdata['userID'],
                'room_id' => 0,
				'theme_id' => $this->input->post('theme_id'),
                'ts' => time()
            );
            
            $poll_id = $this->polls_model->createPoll($fields);
            
            $this->log_model->addMessageLog($this->lang->line('event_poll_create').' "'.$fields['title'].'"','Polls');
            $this->log_model->addErrorInfo($this->lang->line('poll_creating_success'),'success');
            
        }
        
        header('Location:'.base_url ().'polls');
        
    }
    
    
    /**
     * Получение отмеченных для опроса пользователей
     */
    public function getPollsInvitedUsers() {
        
        $pollID = $this->input->get('pollID');
        echo json_encode(array('users' => $this->polls_model->getInvitedUsersInPoll($pollID,true))); 
        
        
    }
    
    
    /**
     * Обновить информацию
     */
    public function update() {
        
        $this->form_validation->set_rules('title', 'lang:poll_title', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('description', 'lang:poll_description', 'trim');
        $this->form_validation->set_rules('theme_id', 'lang:poll_theme_id', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('id', 'lang:id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_updating_fail').'<br>'.  validation_errors(),'danger');
            
        } else {
            
            $pollID = $this->input->post('id');
            $oldPoll = $this->polls_model->getOnePollOverID($pollID);
            
            if (!empty($oldPoll)) {
            
                
                $fields = array(
                    'title' => $this->input->post('title',true),
                    'description' => $this->input->post('description',true),
                    'access' => 'public',
                    'anon_status' => 0,
                    'public_status' => '1',
					'theme_id' => $this->input->post('theme_id'),
                );

                $this->polls_model->updatePoll($pollID,$fields);


                $this->log_model->addMessageLog($this->lang->line('event_poll_update').' "'.htmlspecialchars($fields['title']).'"','Polls');
                $this->log_model->addErrorInfo($this->lang->line('poll_updating_success'),'success');
                
            }
            
        }
        
        header('Location:'.base_url ().'polls');
        
    }
    
    
    /**
     * Архивация
     */
    public function archive() {
        $this->form_validation->set_rules('id', 'lang:id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_archiving_fail').'<br>'.  validation_errors(),'danger');
            
        } else {
            
            $poll = $this->polls_model->getOnePollOverID($this->input->post('id'));
            
            if (!empty($poll)) {
                $fields = array(
                    'active' => 0,
                    'public_status' => 0
                );

                $this->polls_model->updatePoll($poll['id'],$fields);

                $this->log_model->addMessageLog($this->lang->line('event_poll_archive').' "'.htmlspecialchars($poll['title']).'"','Polls');
                $this->log_model->addErrorInfo($this->lang->line('poll_archiving_success'),'success');
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_archiving_fail'),'danger');
                
            }
            
        }
        
        header('Location:'.base_url ().'polls');
        
    }
    
    
    
    /**
     * Удаление
     */
    public function remove(){
        
        $this->form_validation->set_rules('id', 'lang:id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_removing_fail').'<br>'.  validation_errors(),'danger');
            
        } else {
            
            $poll = $this->polls_model->getOnePollOverID($this->input->post('id'));
            
            if (!empty($poll)) {
                
                $this->polls_model->removePoll($poll['id']);

                $this->log_model->addMessageLog($this->lang->line('event_poll_remove').' "'.htmlspecialchars($poll['title']).'"','Polls');

                $this->log_model->addErrorInfo($this->lang->line('poll_removing_success'),'success');
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_error_not_found'),'danger');
                
            }
            
        }
        
        header('Location:'.base_url ().'polls');
        
    }
    
    
    /**
     * Просмотр и редактирование состава вопросов
     */
    public function one() {
        
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            /*
             * Проверить доступ пользователя к списку вопросов опроса:
             * Допускать только администратора или создателя опроса
             */
            if ($this->session->userdata('userType') == 'admin' || $poll['creator_id'] == $this->session->userdata['userID']) {
                
                $this->log_model->addIndexCounter('polls');
        
                $data['poll'] = $poll;
                
                $data['site_title'] = $this->lang->line('polls_one_title').'&ndash; '.htmlspecialchars($poll['title']).'';
                
				$data['site_add_buttons'] = '<li>';
				$data['site_add_buttons'] .= '<button type="button" class="btn btn-success" onclick="$(\'#addPollQuestModal\').modal(\'show\');"><i class="fa fa-plus"></i>&nbsp;'.$this->lang->line('btn_add_poll_quest').'</button>';
				$data['site_add_buttons'] .= '</li>';

                $this->load->view('polls/polls_one_view',$data);
                $this->session->unset_userdata('error');
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
                header('Location:'.base_url ().'polls');
                exit;
                
            }
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');

        }
    }
    
    
    /**
     * Добавление вопроса
     */
    public function add_quest() {
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            if ($this->session->userdata('userType') == 'admin' || $poll['creator_id'] == $this->session->userdata['userID']) {
            
                $this->form_validation->set_rules('title', 'lang:poll_quest_title', 'trim|required');
                $this->form_validation->set_rules('description', 'lang:poll_quest_description', 'trim');
                $this->form_validation->set_rules('required', 'lang:poll_quest_required_status', 'trim|required|is_natural');
                $this->form_validation->set_rules('type', 'lang:poll_quest_type', 'trim|required');

                if (!$this->form_validation->run()) {
                    
                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_adding_fail').'<br>'.  validation_errors(),'danger');
                    header('Location:'.base_url ().'polls');
                    
                } else {

                    $fields = array(
                        'title' => $this->input->post('title',true),
                        'description' => $this->input->post('description',true),
                        'active' => 1,
                        'required' => $this->input->post('required'),
                        'type' => $this->input->post('type'),
                        'poll_id' => $pollID,
                        'numb' => 1,
                        'data' => serialize($this->input->post('answer')),
                        'creator_id' => $this->session->userdata['userID'],
						'answer' => $this->input->post('answer'),
                        'ts' => time()
                    );

                    $this->polls_model->createPollQuest($fields);

                    $this->log_model->addMessageLog($this->lang->line('event_poll_quest_added').' "'.htmlspecialchars($fields['title']).'" ('.$poll['title'].')','Polls');

                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_adding_success'),'success');
                    header('Location:'.base_url ().'polls/one/'.$pollID);
                    
                }
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
                header('Location:'.base_url ().'polls');
                
            }
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_error_not_found'),'danger');
            header('Location:'.base_url ().'polls');
            
        }
    }
    
    
    /**
     * Удаление вопроса
     */
    public function quest_remove() {
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        
        
        if (!empty($poll)) {
            
            if ($this->session->userdata('userType') == 'admin' || $poll['creator_id'] == $this->session->userdata['userID']) {
            
                $this->form_validation->set_rules('id', 'lang:id', 'trim|required|is_natural_no_zero');
                
                if (!$this->form_validation->run()) {
                    
                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_removing_fail').'<br>'.  validation_errors(),'danger');
                    header('Location:'.base_url ().'polls/one/'.$pollID);
                    
                } else {

                    $this->polls_model->removePollQuest($this->input->post('id'));

                    $this->log_model->addMessageLog($this->lang->line('event_poll_quest_removed').' '.$poll['title'],'Polls');

                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_removing_success'),'success');
                    header('Location:'.base_url ().'polls/one/'.$pollID);
                    
                }
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
                header('Location:'.base_url ().'polls');
                
            }
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_error_not_found'),'danger');
            header('Location:'.base_url ().'polls');
            
        }
    }
    
    
    /**
     * Получение ответов на вопрос
     */
    public function getQuestAnswers() {
        
        $questID = $this->input->get('questID');
        
        $quest = $this->polls_model->getQuest($questID);
        
        $answers = array();
        
        if ($quest['data'] != '') $answers = unserialize($quest['data']);
        
        echo json_encode(array('answers' => $answers));
        
    }
    
    
    /**
     * Обновление параметров вопроса
     */
    public function update_quest() {
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            if ($this->session->userdata('userType') == 'admin' || $poll['creator_id'] == $this->session->userdata['userID']) {
            
                $this->form_validation->set_rules('id', 'lang:poll_quest_required_status', 'trim|required|is_natural_no_zero');
                $this->form_validation->set_rules('title', 'lang:poll_quest_title', 'trim|required');
                $this->form_validation->set_rules('description', 'lang:poll_quest_description', 'trim');
                $this->form_validation->set_rules('required', 'lang:poll_quest_required_status', 'trim|required|is_natural');
                
                if (!$this->form_validation->run()) {
                    
                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_updating_fail').'<br>'.  validation_errors(),'danger');
                    header('Location:'.base_url ().'polls/one/'.$pollID);
                    
                } else {

                    $fields = array(
                        'title' => $this->input->post('title',true),
                        'description' => $this->input->post('description',true),
                        'active' => 1,
                        'required' => $this->input->post('required'),
                        'answer' => $this->input->post('answer')
                    );

                    $this->polls_model->updatePollQuest($this->input->post('id'),$fields);

                    $this->log_model->addMessageLog($this->lang->line('event_poll_quest_updated').' "'.htmlspecialchars($fields['title']).'" ('.$poll['title'].')','Polls');
                    
                    $this->log_model->addErrorInfo($this->lang->line('poll_quest_updating_success'),'success');
                    header('Location:'.base_url ().'polls/one/'.$pollID);
                    
                }
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
                header('Location:'.base_url ().'polls');
                
            }
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');
                
        }
    }
    
    
    /**
     * Проведение опроса
     */
    public function play() {
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            //Сформировать массив идентификаторов опросов, к которым у пользователя есть доступ
            $pollsIDs = array();
            
            $active_polls = $this->polls_model->getPollsForMe($this->session->userdata['userID']);
            foreach($active_polls as $apoll) {
                $pollsIDs[] = $apoll['id'];
            }
            
            
            if (in_array($poll['id'],$pollsIDs) || $poll['creator_id'] == $this->session->userdata['userID']) {
            
                /*
                 * Проверять, были ли уже результаты о выполнении опроса
                 * 
                 * Если уже был начат опрос, но не закончен, то скрыть вопросы, которые уже были пройдены
                 * Если был закончен, то удалить всю информацию о результатах и зарегистрироваться в опросе снова
                 * 
                 */
                $result = $this->polls_model->getMyPollResult($this->session->userdata['userID'],$poll['id']);

                if (empty($result)) {
                    
                    $this->log_model->addMessageLog($this->lang->line('event_poll_started').' "'.$poll['title'].'"','Polls');
                    
                    //Создать запись о начале прохождения опроса
                    $this->polls_model->startPollPlay($this->session->userdata['userID'],$poll['id'],$poll['anon_status']);

                } else {
                    
                    if ($result['info']['ts_end']) {
                        //Опрос был завершён и пользователь хочет ещё раз - удалить все его результаты по этому опросу
                        $this->polls_model->removeMyPollResult($this->session->userdata['userID'],$poll['id']);
                        
                        //Создать запись о начале прохождения опроса
                        $this->polls_model->startPollPlay($this->session->userdata['userID'],$poll['id'],$poll['anon_status']);
                        
                    } else {
                        //Удалить из массива вопросов все вопросы, которые пользователь уже прошёл
                        $completedQuestsIDs = array();
                        foreach($result['answers'] as $answer) {
                            $completedQuestsIDs[] = $answer['poll_quest_id'];
                        }
                        
                        foreach($poll['quests'] as $key=>$quest) {
                            if (in_array($quest['id'],$completedQuestsIDs)) unset($poll['quests'][$key]);
                        }
                    }
                }
                
                //Отметить уведомление об опросе, если оно было, как прочитанное
                $notifys = $this->notification_model->getUserEntityNotifications('poll',$pollID,$this->session->userdata['userID']);
                foreach($notifys as $notify) {
                    $this->notification_model->updateNotification($notify['id'],array('status' => 1),$this->session->userdata['userID']);
                }
                
                $this->log_model->addIndexCounter('polls');
        
                $data['poll'] = $poll;
                
                $data['site_title'] = $this->lang->line('polls_playing').' &ndash; '.htmlspecialchars($poll['title']).'';
                
                $this->load->view('polls/polls_play_view',$data);
                $this->session->unset_userdata('error');
                 
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
                header('Location:'.base_url ().'polls');
                
            }
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');
                
        }
        
    }
    
    
    /**
     * Асинхронное сохранение ответов
     */
    public function saveQuestAnswer() {
        
        $this->form_validation->set_rules('pollID', 'lang:pollID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('questID', 'lang:questID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('completeStatus', 'lang:completeStatus', 'trim|required');
        
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_save_poll_answer').validation_errors()));
        } else {
            $pollID = $this->input->post('pollID');
            $questID = $this->input->post('questID');
            $answer = $this->input->post('answer');
            $completeStatus = $this->input->post('completeStatus') == 'true' ? true : false;
            
            $poll = $this->polls_model->getOnePollOverID($pollID);
        
            if (!empty($poll)) {
            
                if (!$completeStatus) {
                    //Если $completeStatus == false, значит, вопрос не выполнен - удалить статус выполнения из базы
                    $this->polls_model->removeUserAnswer($this->session->userdata['userID'],$pollID,$questID);
                    $status = 'removing';
                } else {

                    //Получить идентификатор результата
                    $result = $this->polls_model->getMyPollResult($this->session->userdata['userID'],$pollID);

                    if (empty($result)) {

                        //Так случилось, что записи о сдаче опроса нет
                        $this->polls_model->startPollPlay($this->session->userdata['userID'],$pollID,$poll['anon_status']);
                        $result = $this->polls_model->getMyPollResult($this->session->userdata['userID'],$pollID);
                        
                    }

                    $resultID = $result['info']['id'];
                    //Собрать идентификаторы сданных 
                    $completedQuestsIDs = array();
                    foreach($result['answers'] as $ranswer) {
                        $completedQuestsIDs[] = $ranswer['poll_quest_id'];
                    }

                    //Два случая - обновление ответа, либо регистрация ответа
                    if (in_array($questID, $completedQuestsIDs)) {
                        //На вопрос уже был дан ответ - обновляем его
                        $fields = array(
                            'data' => serialize($answer),
                            'ts' => time()
                        );

                        $this->polls_model->updateUserAnswer($this->session->userdata['userID'],$pollID,$questID,$fields);
                        $status = 'updating';
                    } else {
                        //Ответа на вопрос ещё не было - создаём запись в БД
                        $fields = array(
                            'data' => serialize($answer),
                            'poll_quest_id' => $questID,
                            'poll_id' => $pollID,
                            'result_id' => $resultID,
                            'user_id' => $this->session->userdata['userID'],
                            'ts' => time()
                        );

                        $this->polls_model->createUserAnswer($fields);

                        $status = 'inserting';
                    }
                }

                echo json_encode(array('pollID' => $pollID,'questID' => $questID,'answer' => $answer,'completeStatus' => $completeStatus,'status' => $status));
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('poll_not_found')));
                
            }
                
        }
    }

    
    /**
     * Завершение опроса
     */
    public function finish() {
        $pollID = $this->uri->segment('3');
        $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
			
			/*
			 * Проверять, были ли уже результаты о выполнении опроса
			 * Если да, то завершить опрос, если нет - редирект на главную с ошибкой
			 */
			$result = $this->polls_model->getMyPollResult($this->session->userdata['userID'],$poll['id']);

			if (empty($result)) {

				$this->log_model->addErrorInfo($this->lang->line('poll_access_check_fail'),'danger');
				header('Location:'.base_url ().'polls');
				exit;

			} else {

				$aSumm = 0;
				$aBalls = 0;
				
				$answers = $this->input->post('answers');

				foreach($poll['quests'] as $qkey => $quest) {
					
					$qBalls = 0;
					
					if (isset($answers[$qkey])) {
						
						if ($quest['answer'] == $answers[$qkey]) {
							
							$qBalls = $quest['active'];
							
						} else {
		
							$tmp = 0;
							$var1 = similar_text($quest['answer'], $answers[$qkey], $tmp);
							$qBalls = $tmp/100 * $quest['active'];
							
						}
						
					}
					
					$aBalls += $quest['active']; 
					$aSumm += $qBalls;
					
					$fields = array(
						'data' => $qBalls,
						'poll_quest_id' => $quest['id'],
						'poll_id' => $poll['id'],
						'result_id' => $result['info']['id'],
						'user_id' => $this->session->userdata['userID'],
						'ts' => time()
					);

					$this->polls_model->createUserAnswer($fields);

					
				}
				
				$this->polls_model->updateMyPollResult($result['info']['id'],array('ts_end' => time()));

				$this->log_model->addMessageLog($this->lang->line('event_poll_finished').' "'.$poll['title'].'"','Polls');

				$this->log_model->addErrorInfo($this->lang->line('poll_finished_success'),'success');
				header('Location:'.base_url ().'polls/result/'.$pollID);

			}
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');

        }
    }
    
    
    /**
     * Просмотр результатов
     */
    public function result() {
        
        $pollID = $this->uri->segment('3');
        $data['poll'] = $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            //Сформировать массив идентификаторов опросов, к которым у пользователя есть доступ
                
			$this->log_model->addIndexCounter('polls');

			$data['result'] = $this->polls_model->getMyPollResult($this->session->userdata['userID'],$poll['id']);
			
			//printf_r($data['result']);
			$sAll = 0;
			$sTrue = 0;
			
			foreach($data['poll']['quests'] as $qkey => $quest) {
				
				$data['poll']['quests'][$qkey]['balls'] = 0;
				
				foreach($data['result']['answers'] as $answer) {
					
					if ($answer['poll_quest_id'] == $quest['id']) {
						$data['poll']['quests'][$qkey]['balls'] = $answer['data'];
						$sTrue += $answer['data'];
						break;
					}
					
				}
				
				$sAll += $quest['active'];
				
			}
			
			$data['all_balls_proz'] = $sAll ? round($sTrue/$sAll*100,2) : 0;
			
			$data['final_mark'] = 2;
			
			if ($data['all_balls_proz'] >= 80) {
				
				$data['final_mark'] = 5;
				
			} elseif($data['all_balls_proz'] >= 60) {
				
				$data['final_mark'] = 4;
				
			} elseif($data['all_balls_proz'] >= 40) {
				
				$data['final_mark'] = 3;
				
			}
			
			
			
			$data['site_title'] = $this->lang->line('polls_results').' &ndash; '.htmlspecialchars($poll['title']).'';

			$this->load->view('polls/polls_result_view',$data);
			$this->session->unset_userdata('error');
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');
            
        }
		
    }
	
	
	
	public function all_results() {
		
		$pollID = $this->uri->segment('3');
        $data['poll'] = $poll = $this->polls_model->getOnePollOverID($pollID);
        
        if (!empty($poll)) {
            
            //Сформировать массив идентификаторов опросов, к которым у пользователя есть доступ
                
			$this->log_model->addIndexCounter('polls');

			$data['results'] = $this->polls_model->getAllPollResults($poll['id']);
			
			foreach($data['results'] as $dkey => $result) {
			
				//printf_r($data['result']);
				$sAll = 0;
				$sTrue = 0;

				foreach($data['poll']['quests'] as $qkey => $quest) {

					$data['poll']['quests'][$qkey]['balls'] = 0;

					foreach($result['answers'] as $answer) {

						if ($answer['poll_quest_id'] == $quest['id']) {
							$data['poll']['quests'][$qkey]['balls'] = $answer['data'];
							$sTrue += $answer['data'];
							break;
						}

					}

					$sAll += $quest['active'];

				}

				$data['results'][$dkey]['all_balls_proz'] = $sAll ? round($sTrue/$sAll*100,2) : 0;

				$data['results'][$dkey]['final_mark'] = 2;

				if ($data['results'][$dkey]['all_balls_proz'] >= 80) {

					$data['results'][$dkey]['final_mark'] = 5;

				} elseif($data['results'][$dkey]['all_balls_proz'] >= 60) {

					$data['results'][$dkey]['final_mark'] = 4;

				} elseif($data['results'][$dkey]['all_balls_proz'] >= 40) {

					$data['results'][$dkey]['final_mark'] = 3;

				}

				
			}
			
			
			$data['site_title'] = $this->lang->line('polls_results').' &ndash; '.htmlspecialchars($poll['title']).'';

			$this->load->view('polls/polls_all_results_view',$data);
			$this->session->unset_userdata('error');
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('poll_not_found'),'danger');
            header('Location:'.base_url ().'polls');
            
        }
		
		
		
	}
    
    
    
    
}
