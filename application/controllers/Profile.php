<?php

/**
 * Профиль пользователя
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Редирект на главную, если guest
        if ($userType == 'guest') {
            header('Location:'.base_url());
            exit;
        } else {
            $this->load->vars(array('globalActiveLink' => 'profile'));
            if (!$this->users_model->checkActivity($this->session->userdata['userID'])) header('Location:'.base_url().'main/index');
        }
    }
    
    
    /**
     * Главная страница
     */
    public function index() {
        
        $this->log_model->addIndexCounter('profile');
        
        $data['site_title'] = $this->lang->line('user_profile');
        
        $data['user'] = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
        
        if (!is_dir('./upload/users/'.$this->session->userdata['userID'].'/')) mkdir('./upload/users/'.$this->session->userdata['userID'].'/',0755,TRUE);
        
        //Проверить, имеются ли фотографии уменьшенного размера
        $data['user']['small_avatar_status'] = file_exists('./upload/users/'.$this->session->userdata['userID'].'/'.'avatar_'.$this->session->userdata['userID'].'_small.png');
        
        $data['settings'] = $this->session->userdata('settings');
        
        $data['notification_types'] = explode(',',$this->config->item('config_notification_types'));
        
        $data['additionalFields'] = $this->users_model->getProfileAdditionalFields($this->session->userdata['userID']);
        
        $this->load->view('profile/profile_index_view',$data);
        $this->session->unset_userdata('error');
        
    }
    
    
    /**
     * Обновление главной информации профиля
     */
    public function update_main() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
        
        if (!empty($user)) {
        
            $config['upload_path'] = './upload/users/'.$user['id'].'/';
            $config['file_name'] = 'avatar_'.$user['id'];
            $config['allowed_types'] = 'gif|jpeg|png|jpg';
            $config['max_size'] = 10240;
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;

            if (!is_dir($config['upload_path'])) mkdir($config['upload_path'],0755,TRUE);

            $this->load->library('upload', $config);

            $file_name = $user['avatar'];

            if ($_FILES['userfile']['name'] != '' && !$this->upload->do_upload()) {

                $this->log_model->addErrorInfo($this->lang->line('profile_error_updating_upload').'<br>'.$this->upload->display_errors(),'danger');
                header('Location:'.base_url ().'profile');
                exit;

            } elseif ($_FILES['userfile']['name'] != '') {
                
                $file_data = $this->upload->data();
                $file_name = $file_data['file_name'];

                @unlink('./upload/users/'.$user['id'].'/'.'avatar_'.$user['id'].'_small.png');
                @unlink('./upload/users/'.$user['id'].'/'.'avatar_'.$user['id'].'_big.png');
                @unlink('./upload/users/'.$user['id'].'/'.'avatar_'.$user['id'].'_conf.png');

            }


            // Валидация полей 
            $this->form_validation->set_rules('surname', 'lang:user_surname', 'trim|required|max_length[150]');
            $this->form_validation->set_rules('name', 'lang:user_name', 'trim|required|max_length[150]');
            $this->form_validation->set_rules('middlename', 'lang:user_middlename', 'trim|max_length[150]');

            // Включить проверку дополнительных полей
            $additionalFields = $this->users_model->getProfileAdditionalFields($this->session->userdata['userID']);
            foreach($additionalFields['profile'] as $addfield) {
                $this->form_validation->set_rules('field_'.$addfield['id'], $addfield['name'], 'trim'.($addfield['required'] ? '|required' : ''));
            }


            if (!$this->form_validation->run()) {

                //удалить загруженный файл, если он был, а аватара до этого не было
                !$user['avatar'] && $file_name ? @unlink($config['upload_path'].$file_name) : '';

                $this->log_model->addErrorInfo($this->lang->line('profile_error_updating').'<br>'.validation_errors(),'danger');

                //exit;
            } else {

                //Всё ок

                $update_fields = array(
                    'name' => $this->input->post('name',true),
                    'middlename' => $this->input->post('middlename',true),
                    'surname' => $this->input->post('surname',true),
                    'birthday' => $this->input->post('birthday'),
                    'avatar' => $file_name
                );
                
                //Обновить и дополнительные поля
                foreach($additionalFields['profile'] as $addfield) {
                    $this->users_model->updateAdditionalField($addfield['id'],$this->session->userdata['userID'],$this->input->post('field_'.$addfield['id'],true));
                }

                $user = $this->users_model->updateUser($user['id'],$update_fields);
                
                $this->session->set_userdata('name', $user['surname'].' '.$user['name'].' '.$user['middlename']);
                $this->session->set_userdata('shortName', $user['shortName']);
                $this->session->set_userdata('only_name', $user['name']);
                
                $this->log_model->addMessageLog($this->lang->line('event_profile_edit'),'Profile');
                $this->log_model->addErrorInfo($this->lang->line('user_profile_update_success'),'success');

            }
        }
        
        header('Location:'.base_url ().'profile');
        
    }
    
    
    /**
     * Обновление информации о контактах пользователя
     */
    public function update_contacts() {
        
        $this->form_validation->set_rules('email', 'lang:user_email', 'trim|valid_email|max_length[150]');
        $this->form_validation->set_rules('phone', 'lang:user_phone', 'trim|max_length[100]');
        $this->form_validation->set_rules('comment', 'lang:user_comment', 'trim');
            
        
        // Включить проверку дополнительных полей
        $additionalFields = $this->users_model->getProfileAdditionalFields($this->session->userdata['userID']);
        foreach($additionalFields['contacts'] as $addfield) {
            $this->form_validation->set_rules('field_'.$addfield['id'], $addfield['name'], 'trim'.($addfield['required'] ? '|required' : ''));
        }
        
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('profile_error_updating').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $update_fields = array(
                'email' => $this->input->post('email',true),
                'comment' => $this->input->post('comment',true),
                'phone' => $this->input->post('phone',true)
            );
                
            //Обновить и дополнительные поля
            foreach($additionalFields['contacts'] as $addfield) {
                $this->users_model->updateAdditionalField($addfield['id'],$this->session->userdata['userID'],$this->input->post('field_'.$addfield['id'],true));
            }
            
            $this->users_model->updateUser($this->session->userdata['userID'],$update_fields);

            $this->log_model->addMessageLog($this->lang->line('event_profile_contacts_edit'),'Profile');
            $this->log_model->addErrorInfo($this->lang->line('user_profile_update_success'),'success');
            
        }
        
        header('Location:'.base_url ().'profile');
    }
    
    
    /**
     * Обновление пароля
     */
    public function change_pass() {
        $this->form_validation->set_rules('current_pass', 'lang:user_current_pass', 'trim|required');
        $this->form_validation->set_rules('new_pass', 'lang:user_new_pass', 'trim|required');
        if (!$this->form_validation->run()) {
            
            $this->log_model->addErrorInfo($this->lang->line('user_profile_password_update_fail').'<br>'.validation_errors(),'danger');
            
        } else {
            
            $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
            
            $current_pass = $this->input->post('current_pass');
            $new_pass = $this->input->post('new_pass');
            
            $pass = crypt($current_pass,$user['password']);
            
            if ($pass == trim($user['password'])) {

                $update_fields = array('password' => @crypt($new_pass));
                
                $this->users_model->updateUser($user['id'],$update_fields);

                
                $this->log_model->addMessageLog($this->lang->line('event_profile_password_update'),'Profile');
                $this->log_model->addErrorInfo($this->lang->line('user_profile_update_success'),'success');
                
            } else {
                
                $this->log_model->addErrorInfo($this->lang->line('user_profile_password_update_fail'),'danger');
                
            }
            
        }
        
        header('Location:'.base_url ().'profile');
    }
    
    
    /**
     * Обновление настроек
     */
    public function update_settings() {
        
        $settings = $this->input->post('settings');
        
        $notificationTypes = explode(',',$this->config->item('config_notification_types'));
        
        $newSettings = array();
        foreach($notificationTypes as $type) {
            $newSettings[$type] = isset($settings[$type]) ? 1 : 0;
        }
        
        $update_fields = array('settings' => serialize($newSettings));
        
        $this->users_model->updateUser($this->session->userdata['userID'],$update_fields);
        
        $this->session->set_userdata('settings', $settings);
        
        $this->log_model->addErrorInfo($this->lang->line('user_profile_update_success'),'success');
        header('Location:'.base_url ().'profile');
        
    }
    
    
    /**
     * Изменение настроек аватара
     */
    public function change_avatar() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
        
        $ava_big = $this->input->post('avatar_big');
        $ava_small = $this->input->post('avatar_small');
        $ava_conf = $this->input->post('avatar_conf');
        
        $config['image_library'] = 'gd2';
        $config['source_image'] = './upload/users/'.$user['id'].'/'.$user['avatar'];
        $config['new_image'] = './upload/users/'.$user['id'].'/avatar_'.$user['id'].'_big.png';
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $ava_big[2];
        $config['height'] = $ava_big[3];
        $config['x_axis'] = $ava_big[0];
        $config['y_axis'] = $ava_big[1];

        $this->load->library('image_lib', $config);
        if (!$this->image_lib->crop()) {
            
            $this->log_model->addErrorInfo($this->lang->line('profile_error_avatar_update').'<br>'.$this->image_lib->display_errors(),'danger');
            header('Location:'.base_url ().'profile');
            exit;
            
        } 
        
        $config['source_image'] = './upload/users/'.$user['id'].'/avatar_'.$user['id'].'_big.png';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 400;
        $config['height'] = 400;
        $this->image_lib->initialize($config);
        
        if (!$this->image_lib->resize()) {
            $this->log_model->addErrorInfo($this->lang->line('profile_error_avatar_update').'<br>'.$this->image_lib->display_errors(),'danger');
            header('Location:'.base_url ().'profile');
            exit;
        }
        
        $config['source_image'] = './upload/users/'.$user['id'].'/'.$user['avatar'];
        $config['new_image'] = './upload/users/'.$user['id'].'/avatar_'.$user['id'].'_small.png';
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $ava_small[2];
        $config['height'] = $ava_small[3];
        $config['x_axis'] = $ava_small[0];
        $config['y_axis'] = $ava_small[1];
        $this->image_lib->initialize($config);
        if (!$this->image_lib->crop()) {
            $this->log_model->addErrorInfo($this->lang->line('profile_error_avatar_update').'<br>'.$this->image_lib->display_errors(),'danger');
            header('Location:'.base_url ().'profile');
            exit;
        }
        
        $config['source_image'] = './upload/users/'.$user['id'].'/'.$user['avatar'];
        $config['new_image'] = './upload/users/'.$user['id'].'/avatar_'.$user['id'].'_conf.png';
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $ava_conf[2];
        $config['height'] = $ava_conf[3];
        $config['x_axis'] = $ava_conf[0];
        $config['y_axis'] = $ava_conf[1];
        $this->image_lib->initialize($config);
        if (!$this->image_lib->crop()) {
            $this->log_model->addErrorInfo($this->lang->line('profile_error_avatar_update').'<br>'.$this->image_lib->display_errors(),'danger');
            header('Location:'.base_url ().'profile');
            exit;
        }
        
        $this->users_model->clearUsersCache($this->session->userdata['userID']);
        $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
        $this->session->set_userdata('avatarURL',$user['avatarURL_conf'] != '' ? $user['avatarURL_conf'] : $user['avatarURL_small']);
        $this->session->set_userdata('avatarBigURL',$user['avatarURL']);
        
        $this->log_model->addErrorInfo($this->lang->line('user_profile_avatar_update_success'),'success');
        header('Location:'.base_url ().'profile');
        
    }
    
    
    
    public function remove_avatar() {
        
        $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
        
        if (!empty($user)) {
            
            if (file_exists('./upload/users/'.$user['id'].'/'.$user['avatar'])) {
            
                if (!unlink('./upload/users/'.$user['id'].'/'.$user['avatar'])) {
                    $this->log_model->addErrorInfo($this->lang->line('user_profile_avatar_removed_fail'),'danger');
                    header('Location:'.base_url ().'profile');
                    exit;
                } else {
                    unlink('./upload/users/'.$user['id'].'/avatar_'.$user['id'].'_small.png');
                    unlink('./upload/users/'.$user['id'].'/avatar_'.$user['id'].'_big.png');
                    unlink('./upload/users/'.$user['id'].'/avatar_'.$user['id'].'_conf.png');
                }
                
            }
                    
            $update_fields = array('avatar' => '');

            $user = $this->users_model->updateUser($user['id'],$update_fields);

            $this->session->set_userdata('avatarURL',$user['avatarURL_conf'] != '' ? $user['avatarURL_conf'] : $user['avatarURL_small']);
            $this->session->set_userdata('avatarBigURL',$user['avatarURL']);

            $this->log_model->addErrorInfo($this->lang->line('user_profile_avatar_removed_success'),'success');
            
        } else {
            
            $this->log_model->addErrorInfo($this->lang->line('user_profile_avatar_removed_fail'),'danger');
            
        }
        
        header('Location:'.base_url ().'profile');
        
    }
    
    
    public function notifications() {
        
        $this->log_model->addIndexCounter('profile');
        $this->load->vars(array('globalActiveLink' => 'profile/notifications'));
        
        $data['notifications'] = $this->notification_model->getUserNotifications($this->session->userdata['userID']);
        
        $data['site_title'] = $this->lang->line('notification_department');
        
        if (count($data['notifications'])) {
            $data['site_add_buttons'] = '<li>'
                    . '<button type="button" class="btn btn-danger btn-sm" onclick="$(\'#removeNotificationsConfirmModal\').modal(\'show\');">'.$this->lang->line('btn_clear_notifications').'</button>'
                    . '</li>';
        }
        
        
        $this->load->view('profile/profile_notifications_view',$data);
        $this->session->unset_userdata('error');
    }
    
    
    public function notifications_remove() {
        
        $this->notification_model->removeAllNotifications($this->session->userdata['userID']);
        
        $this->log_model->addErrorInfo($this->lang->line('notification_removing_success'),'success');
        
        header('Location:'.base_url ().'profile/notifications');
    }
    
    
}
