<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    /**
     * Главная страница
     */
    public function index() {
        
        $this->log_model->addIndexCounter('main');
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        if ($this->config->item('config_use_ldap') && $userType == 'guest') {
            
            $ldap_login = $this->db->username;
            //Проверить, имеется ли такой пользователь в БД
            $user_info = $this->users_model->getOneUserInfoOverLDAPLogin($ldap_login);
            
            if (!empty($user_info)) {
                
                if ($this->users_model->checkActivity($user_info['id'])) {
                    //Всё хорошо
                    
                    if ($this->checkDoubleRole($ldap_login))
                        $this->select_role($user_info['login']);
                    else
                        $this->auth($user_info['login']);
                    
                } else {
                    $data['message'] = $this->lang->line('error_login_ldap_user_blocked');
                    $data['message_type'] = 'danger';
                    $this->load->view('main_index_ldap_error',$data);
                }
                
            } else {
                $data['message'] = $this->lang->line('error_login_ldap_user_not_registred');
                $data['message_type'] = 'danger';
                $this->load->view('main_index_ldap_error',$data);
            }
            
        } else {
        
            if (isset($this->session->userdata['login']) && !$this->users_model->checkActivity($this->session->userdata['userID'])) {
                
                $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);
                
                if (!$user['archive']) {
                    $this->userBlocked();
                } else {
                    
                    $this->load->view('main_index_view');
                    
                }
                
            } else {
             
                $this->redirect();

                //Если сессии нет, то отобразить форму ввода логина и пароля
                $this->load->view('main_index_view');
            }
        }
    }
    
    
    public function signin() {
        
        $this->form_validation->set_rules('username', 'lang:login', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
            
        if (!$this->form_validation->run()) {
            
            $data['message'] = $this->lang->line('signin_error').'<br>'.  validation_errors();
            $data['message_type'] = 'danger';
            
            $this->load->view('main_index_view',$data);
            
        } else {
            
            $select_role = false;
            /*
             * Проверка данных
             */
            $login = $this->input->post('username');
            $pass = $this->input->post('password');
            $data = array();
            
            if ($this->users_model->checkLogin($login)) {
                
                $user = $this->users_model->getOneUserInfo($login);
                    
                /*
                 * Проверка активности
                 */
                if ($this->users_model->checkActivity($user['id'])) {
                    
                    /*
                     * Проверка пароля
                     */
                    
                    $pass = crypt($pass,trim($user['password']));
                    if ($this->users_model->checkPass($login,$pass)) {
                        //Всё хорошо, авторизуем
                        
                        //Если двойная роль, то показать страницу выбора роли
                        if ($this->checkDoubleRole($login)) {
                            $this->select_role($login);
                            $select_role = true;
                        } else {
                            $this->auth($login);
                            exit;
                        }
                        
                    } else {
                        
                        $data['message'] = $this->lang->line('login_and_pass_incorrect');
                        $data['message_type'] = 'danger';
                        
                    }
                    
                } else {
                    
                    header('Location:'.base_url().'main/userBlocked');
                    
                }
                
            } else {
                $data['message'] = $this->lang->line('login_incorrect');
                $data['message_type'] = 'danger';
            }
            
            !$select_role ? $this->load->view('main_index_view',$data) : '';
        }
    }
    
    
    public function userBlocked() {
        $data['message'] = $this->lang->line('login_user_blocked');
        $data['message_type'] = 'danger';
        $this->load->view('main_index_view',$data);
    }
    
    
    /**
     * Аутентификация по логину
     * 
     * @param type $login
     */
    private function auth($login,$role = 'default') {
        
        $user = $this->users_model->getOneUserInfo($login);
        
        $session_data = array(
            'name' => $user['surname'].' '.$user['name'].' '.$user['middlename'],
            'only_name' => $user['name'],
            'avatarURL' => $user['avatarURL_conf'] != '' ? $user['avatarURL_conf'] : $user['avatarURL_small'],
            'avatarBigURL' => $user['avatarURL'],
            'shortName' => $user['shortName'],
            'login' => $user['login'],
            'redirectURL' => $user['redirect_url'],
            'role' => $role,
            'doubleRole' => $this->checkDoubleRole($user['login'],$user['ldap_login']),
            'userType' => $user['user_type'],
            'userTypeName' => $user['user_type'] == 'admin' ? $this->lang->line('user_type_admin') : $this->lang->line('user_type_user'),
            'userID' => $user['id'],
            'settings' => unserialize($user['settings']),
            'logged_in' => TRUE
        );
        
        $this->session->set_userdata($session_data);
        $this->log_model->addMessageLog($this->lang->line('event_signin'),'Main');
        
        $this->redirect();   
    }
    
    
    /**
     * Редирект на необходимую, в зависимости от прав, страницу
     */
    private function redirect() {
        
        //Редирект на главную, если guest
        if ($this->session->userdata('userType') == 'admin' && $this->session->userdata('login') != 'admin' && $this->session->userdata('role') != 'superuser') {
            header('Location:'.base_url().'polls');
        } elseif ($this->session->userdata('userType') == 'admin' && $this->session->userdata('login') != 'admin' && $this->session->userdata('role') == 'superuser') {
            header('Location:'.base_url().'admin/log');
        } elseif ($this->session->userdata('userType') == 'admin' && $this->session->userdata('login') == 'admin' && $this->session->userdata('role') == 'default') {
            header('Location:'.base_url().'admin/log');
        } elseif ($this->session->userdata('userType') == 'user' && $this->session->userdata('redirectURL')) {
            
            if (mb_strpos($this->session->userdata('redirectURL'),'http') !== FALSE || $this->load->get_var('config_janus_videoroom_server') == '') {

                header('Location:'.base_url().'polls');
                
            } elseif ($this->load->get_var('config_janus_videoroom_server') != '') {
                
                $redirectURL = base_url().'rooms/one_room/'.$this->session->userdata('redirectURL');
                header('Location:'.$redirectURL);
            }
            
        } elseif ($this->session->userdata('userType') == 'user') {header('Location:'.base_url().'polls');}
    }
    
    
    /**
     * Функция определения дополнительных привелегий
     */
    private function checkDoubleRole($login,$ldap_login = '') {
        $double_role = false;
                    
        //Проверить на двойную роль
        if ($login != 'admin') {
            $superusers_logins = $this->load->get_var('config_superusers_logins');
            $superusers_logins_arr = explode(',',$superusers_logins);

            foreach($superusers_logins_arr as $su_login) {
                if ($login == trim($su_login) || $ldap_login == trim($su_login)) {
                    $double_role = true;
                    break;
                }
            }
        }
        
        return $double_role;
    }
    
    
    /**
     * Регистрация
     */
    public function registr() {
        
        $this->load->view('main_index_view');
        
    }
    
    
    /**
     * Обратная связь
     */
    public function feedbackform() {
        $data['message'] = $this->lang->line('feedback_form_info');
        $data['message_type'] = 'success';
        $this->load->view('main_index_view',$data);
    }
    
    
    /**
     * Отправка сообщения обратной связью
     */
    public function feedbackform_send() {
        
        $this->form_validation->set_rules('problem', 'lang:feedback_problem', 'trim|required|max_length[1500]');
            
        if (!$this->form_validation->run()) {
            
            $this->feedbackform();
            
        } else {
            
            $fields = array(
                'problem' => $this->input->post('problem',true),
                'status' => 0,
                'comment' => '',
                'creator_id' => 0,
                'ts' => time()
            );
            
            $feedbackID = $this->log_model->addFeedbackMessage($fields);
            
            $adminUserRecord = $this->users_model->getOneUserInfo('admin');
            
            $notify_array = array(
                'recipient_id' => $adminUserRecord['id'],
                'entity_type' => 'feedback',
                'entity_id' => $feedbackID,
                'subject' => $this->lang->line('feedback_notification_new_subject'),
                'body' => $this->lang->line('feedback_notification_new_body'),
            );

            $this->notification_model->makeNotification($notify_array);
            $this->log_model->addMessageLog($this->lang->line('feedback_event_notification_new'),'Main');
            
            $data['message'] = $this->lang->line('feedback_success_sending');
            $data['message_type'] = 'success';
            $this->load->view('main_index_view',$data);
        }
        
        
    }
    
    
    
    /**
     * Интерфейс выбора роли
     */
    public function select_role($login) {
        
        $data['login'] = $login;
        $data['message'] = $this->lang->line('user_select_role');
        
        $this->load->view('main_select_role_view',$data);
    }
    
    
    public function select_role_fix() {
        $this->form_validation->set_rules('login', 'lang:login', 'trim|required');
        $this->form_validation->set_rules('role', 'role', 'trim');
        
        if (!$this->form_validation->run()) {
            header('Location:'.base_url());
        } else {
            $this->auth($this->input->post('login'), $this->input->post('role'));
        }
        
    }
    
    
    
    /**
     * Обработчик регистрационной формы
     */
    public function signup() {
        $this->form_validation->set_rules('surname', 'lang:user_surname', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('name', 'lang:user_name', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('middlename', 'lang:user_middlename', 'trim|max_length[150]');
		$this->form_validation->set_rules('group', 'lang:user_group', 'trim|max_length[255]');
        //$this->form_validation->set_rules('username', 'lang:login', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:user_email', 'trim|valid_email|max_length[150]');
            
        if (!$this->form_validation->run()) {
            
            $data['message'] = $this->lang->line('registr_error');
            $data['message_type'] = 'danger';
            $this->load->view('main_index_view',$data);
            
            
        } else {
            //Проверить логин
            $login = $this->input->post('email');
            if ($this->users_model->checkLogin($login)) {
                
                //Такой логин уже есть
                $data['message'] = $this->lang->line('registr_error_doublelogin');
                $data['message_type'] = 'danger';
                $this->load->view('main_index_view',$data);
                
            } else {
                
                $this->load->helper('security');
                
                $reg_fields = array(
                    'login' => xss_clean($this->input->post('email')),
                    'password' => $this->input->post('password'),
                    'email' => xss_clean($this->input->post('email')),
                    'name' => xss_clean($this->input->post('name')),
					'group' => xss_clean($this->input->post('group')),
                    'middlename' => xss_clean($this->input->post('middlename')),
                    'surname' => xss_clean($this->input->post('surname'))
                );
                
                $new_user_id = $this->users_model->createUser($reg_fields);
        
                $this->log_model->addMessageLog($this->lang->line('event_signup'),'Main',$new_user_id);
                
                $data['message'] = $this->lang->line('registr_self_ok');
                $data['message_type'] = 'success';
                
                $this->load->view('main_index_view',$data);

            }
        }
    }
    
    
    /**
     * Выход
     */
    public function deauth() {
        
        $log_fields = array(
            'user_id' => $this->session->userdata['userID'] ? $this->session->userdata['userID'] : 0,
            'status' => $this->lang->line('event_deauth'),
            'controller' => 'Main'
        );
        $this->log_model->addLog($log_fields);
        
        $this->session->sess_destroy();
        header('Location:'.base_url());
    }
    
    
    /**
     * Восстановление пароля (морда)
     */
    public function restore() {
        $this->load->view('main_index_view');
    }
    
    
    /**
     * Восстановление пароля (обработка)
     */
    public function restoresend() {
        
        $this->form_validation->set_rules('email', 'lang:user_email', 'trim|valid_email|max_length[150]');
            
        if (!$this->form_validation->run()) {
            
            $data['message'] = $this->lang->line('restore_error_not_valid_mail');
            $data['message_type'] = 'danger';
            $this->load->view('main_index_view',$data);
            
        } else {
            
            //Проверка существования учётной записи с таким электронным адресом
            if ($this->users_model->checkMail($this->input->post('email',true))) {
                
                //Отправка ссылки. Регистрация отправленного хэша

                $message = $this->lang->line('restore_access_mail_description').'<br>';
                $hash = md5(time());
                $message .= '<a href="'.base_url().'main/restorecheck/'.$hash.'">'.$this->lang->line('restore_access_mail_subject').'</a>';

                $fields = array(
                    'input' => $this->input->post('email'),
                    'type' => 'mail',
                    'hash' => $hash,
                    'ts' => time()
                );
                
                $this->users_model->addRestoreHash($fields);
                
                $mail_fields = array(
                    'to' => $this->input->post('email'),
                    'subject' => $this->lang->line('restore_access_mail_subject').' "'.$this->load->get_var('config_title_short').'"',
                    'message' => $message
                );
                
                $this->mail_model->sendMail($mail_fields);
                
                $error = $this->lang->line('error_restore_pass_mail_send');
                $error_type = 'success';
                
            } else {
                $error = $this->lang->line('error_restore_pass_mail_not_found');
                $error_type = 'danger';
            }
            
            header('Location:'.base_url ().'?error='.$error.'&error_type='.$error_type);
        }
    }
    
    
    /**
     * Восстановление пароля, обработка хэша
     */
    public function restorecheck() {
        
        $hash = $this->uri->segment('3') ? $this->uri->segment('3') : 0;
        
        if ($hash && $this->users_model->checkHash($hash)) {
            
            $data['user'] = $this->users_model->getUserOverHash($hash,'mail');
            
            $this->load->view('main_restorecheck_view',$data);
            
        } else {
            
            $data['message'] = $this->lang->line('error_restore_hash_incorrect');
            $data['message_type'] = 'danger';
            $this->load->view('main_index_view',$data);
            
        }
    }
    
    
    /**
     * Обновление пароля
     */
    public function restorecomplete() {
        
        $this->form_validation->set_rules('user_id', 'lang:user_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('new_pass', 'lang:user_pass', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            $data['message'] = $this->lang->line('error_restore_pass_failed').'<br>'.validation_errors();
            $data['message_type'] = 'danger';
            $this->load->view('main_index_view',$data);
            
        } else {
            
            $user = $this->users_model->getOneUserInfoOverID($this->input->post('user_id'));
            
            $new_pass = $this->input->post('new_pass');
            
            $update_fields = array('password' => @crypt($new_pass));
                
            $this->users_model->updateUser($user['id'],$update_fields);

            $this->log_model->addMessageLog($this->lang->line('event_profile_password_restore'),'Profile');
            
            //Удалить хэш
            $this->users_model->removeHash($user['email'],'mail');

            header('Location:'.base_url ().'?error='.$this->lang->line('restore_access_password_reset_success').'&error_type=success');
        }
    }
    
    
}
