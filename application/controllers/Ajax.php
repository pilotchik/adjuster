<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Обработка AJAX-запросов
 */

class Ajax extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        
        $userType = isset($this->session->userdata['userType']) ? $this->session->userdata['userType'] : 'guest';
        
        //Остановка, если guest
        //if ($userType == 'guest') exit;
    }
    
    
    /**
     * Биение сердца
     */
    public function setHeartBeat() {
        
        if (isset($this->session->userdata['userID'])) {
        
            $user = $this->users_model->getOneUserInfoOverID($this->session->userdata['userID']);

            $update_fields = array(
                'last_heartbeat' => time(),
                'remote_ip' => $_SERVER['REMOTE_ADDR'],
                'remote_host' => gethostbyaddr($_SERVER['REMOTE_ADDR'])
            );
            
            $minCacheTime = $this->config->item('config_cache_view_time') * 60 < 180 ? $this->config->item('config_cache_view_time') * 60 : 180;

            if ($user['last_heartbeat'] + $minCacheTime < time()) $this->users_model->updateUser($this->session->userdata['userID'],$update_fields);

            if ($this->input->get('roomID')) $this->log_model->addIndexCounter('rooms',$this->input->get('roomID'));
             
            
            $this->load->model('polls_model');

            echo json_encode(array('userID' => $this->input->get('userID'),
                'time' => time(),
                'remoteIP' => $update_fields['remote_ip'],
                'cart' => $this->cart->contents(),
                'structure_items' => $this->load->get_var('config_components_posts') && !$this->input->get('roomID') ? $this->structure_model->getItemsUsersCountInCart() : array(),
                'activePollsCount' => $this->load->get_var('config_components_polls') && !$this->input->get('roomID') ? $this->polls_model->getActivePollsCount($this->session->userdata['userID']) : array(),
                'activeNotesCount' => $this->load->get_var('config_components_notes') && !$this->input->get('roomID') ? $this->notes_model->getActiveNotesCount($this->session->userdata['userID']) : array(),
                'activeTopNotesCount' => $this->load->get_var('config_components_notes') && !$this->input->get('roomID') ? $this->notes_model->getActiveNotesCount($this->session->userdata['userID'],true) : array(),
                'notifications' => !$this->input->get('roomID') ? $this->notification_model->getUserNewPopupNotifications($this->session->userdata['userID']) : array(),
                'roomUsers' => $this->input->get('roomID') ? $this->rooms_model->getRoomUsers($this->input->get('roomID'),true,true) : array(),
                'roomInfo' => $this->input->get('roomID') ? $this->rooms_model->getOneRoom($this->input->get('roomID')) : array(),
                'roomPolls' => $this->input->get('roomID') ? $this->polls_model->getOneRoomActivePolls($this->input->get('roomID'),$this->session->userdata['userID']) : array(),
            ));
            
        } else {
            
            echo json_encode(array('status' => 403, 'error' => $this->lang->line('error_ajax_user_not_authorized')));
            
        }
        
    }
    
    
    
    /**
     * Операции с пользователем
     */
    public function menageUserInCart() {
        
        $this->form_validation->set_rules('userID', 'lang:user_id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_user_invite_queue_failed').validation_errors()));
        } else {
            
            $user = $this->users_model->getOneUserInfoOverID($this->input->post('userID'));
            
            $inCart = false;
            //Проверить, имеется ли пользователь в корзине
            foreach($this->cart->contents() as $item) {
                if ($item['id'] == $user['id']) {
                    $inCart = $item['rowid'];
                    break;
                }
            }
            
            if (!$inCart) {
                //проверить, был ли пользователь уже в корзине
                $status = 'adding';

                $data = array(
                    'id'      => $user['id'],
                    'qty'     => 1,
                    'price'   => 1,
                    'name'    => $user['fullName']
                );

                $this->cart->insert($data);
            } else {
                $status = 'removing';
                $this->cart->remove($inCart);
            }
            
            echo json_encode(array('userID' => $this->input->post('userID'),'cart' => $this->cart->contents(),'status' => $status));
        
        }
        
    }
    
    
    /**
     * Получение полной информации о комнате
     */
    public function getRoomInfo() {
        
        $roomID = $this->input->get('roomID');
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        $tt = $this->input->get('tt');
        $tt = isset($tt) ? $tt : 0;
        
        if ($roomID && ctype_digit($roomID)) {
            
            if ($tt) {
                
                $this->rooms_model->cleanInvites($roomID,$tt);
                
            }
            
            $room = $this->rooms_model->getOneRoom($roomID);
            $roomUsers = $this->rooms_model->getRoomUsers($roomID,true);
            
            $room['name'] = str_replace('"', '\\"', $room['name']);
            $room['name'] = str_replace("'", '\\"', $room['name']);
            
            $resultArray = array('info'=> $room,'users' => $roomUsers);
            if ($format == 'json')
                echo json_encode($resultArray);
            else
                $this->printf_r($resultArray);
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_roomid')));
        }
        
    }
    
    
    /**
     * Удаление пользователя
     */
    public function removeRoomUser() {
        
        $this->form_validation->set_rules('roomID', 'lang:room_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('room_error_remove_user').validation_errors()));
        } else {
            
            $roomID = $this->input->post('roomID');
            $userID = $this->input->post('userID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $room = $this->rooms_model->getOneRoom($roomID);
        
            if (!empty($room)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->rooms_model->getInvitedUsersIDs($roomID);

                if (in_array($userID, $oldUsersIDs)) {

                    //Если есть инвайт с этим же tt и operationType, то удалить
                    if (!$this->rooms_model->getInviteOverTT($roomID,$userID,1)) {
                        $this->rooms_model->updateInviteAny($roomID,$userID,array('tt' => $tt,'operation_type' => 2));
                    } else {
                        $this->rooms_model->removeInvite($roomID,$userID);
                    }
                    
                    echo json_encode(array('room' => $roomID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('room_error_user_not_found_in_room')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                
            }
            
        }
        
    }
    
    
    public function changeRoomUserRole() {
        
        $this->form_validation->set_rules('roomID', 'lang:room_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('role', 'lang:room_user_role', 'trim|required|in_list[secondary_master,default]');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('room_error_change_users_role').validation_errors()));
        } else {
            
            $roomID = $this->input->post('roomID');
            $userID = $this->input->post('userID');
            $role = $this->input->post('role');
            
            //Информация о комнате
            $room = $this->rooms_model->getOneRoom($roomID);
        
            if (!empty($room)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->rooms_model->getInvitedUsersIDs($roomID);

                if (in_array($userID, $oldUsersIDs)) {

                    $this->rooms_model->updateInvite($roomID,$userID,$role);
                    
                    echo json_encode(array('room' => $roomID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('room_error_user_not_found_in_room')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                
            }
            
        }
        
    }
    
    
    /**
     * Добавление пользователя в комнату
     */
    public function addRoomUser() {
        
        $this->form_validation->set_rules('roomID', 'lang:room_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('room_error_add_user_in_room').validation_errors()));
        } else {
            
            $roomID = $this->input->post('roomID');
            $userID = $this->input->post('userID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $room = $this->rooms_model->getOneRoom($roomID);
        
            if (!empty($room)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->rooms_model->getInvitedUsersIDs($roomID);

                if (!in_array($userID, $oldUsersIDs)) {

                    //$this->rooms_model->updateInviteAny($roomID,$userID,array('tt' => $tt,'operation_type' => 1));
                    if ($this->rooms_model->getInviteOverTT($roomID,$userID,2)) {
                        $this->rooms_model->removeInvite($roomID,$userID);
                    }
                    
                    
                    $invite_array = array(
                        'user_id' => $userID,
                        'room_id' => $roomID,
                        'role' => 'default',
                        'inviter_id' => $this->session->userdata['userID'],
                        'ts' => time(),
                        'tt' => $tt,
                        'operation_type' => 1
                    );

                    $this->rooms_model->inviteUser($invite_array);
                    
                    echo json_encode(array('room' => $roomID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('room_error_user_was_found_in_room')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                
            }
            
        }
        
    }
    
    
    public function addProjectUser() {
        
        $this->form_validation->set_rules('projectID', 'lang:projectID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('project_error_add_user_in_project').validation_errors()));
        } else {
            
            $projectID = $this->input->post('projectID');
            $userID = $this->input->post('userID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $project = $this->projects_model->getOneProject($projectID,false);
        
            if (!empty($project)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->projects_model->getProjectUsers($project['id'],$project['creator_id'],true);

                if (!in_array($userID, $oldUsersIDs)) {

                    //$this->rooms_model->updateInviteAny($roomID,$userID,array('tt' => $tt,'operation_type' => 1));
                    if ($this->projects_model->getProjectInviteOverTT($projectID,$userID,2)) $this->projects_model->removeProjectInvite($roomID,$userID);
                    
                    
                    $invite_array = array(
                        'master_status' => 0,
                        'user_id' => $userID,
                        'project_id' => $projectID,
                        'creator_id' => $this->session->userdata['userID'],
                        'ts' => time(),
                        'tt' => $tt,
                        'operation_type' => 1
                    );

                    $this->projects_model->inviteProjectUser($invite_array);

                    echo json_encode(array('project' => $projectID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('project_error_user_was_found_in_project')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('projects_error_not_found')));
                
            }
            
        }
        
    }
    
    
    
    public function changeProjectUserRole() {
        
        $this->form_validation->set_rules('projectID', 'lang:projectID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('masterStatus', 'lang:masterStatus', 'trim|required|is_natural');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('project_error_change_users_role').validation_errors()));
        } else {
            
            $projectID = $this->input->post('projectID');
            $userID = $this->input->post('userID');
            $masterStatus = $this->input->post('masterStatus');
            
            //Информация о комнате
            $project = $this->projects_model->getOneProject($projectID,false);
        
            if (!empty($project)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->projects_model->getProjectUsers($projectID,$project['creator_id'],true);

                if (in_array($userID, $oldUsersIDs)) {

                    $this->projects_model->updateProjectInvite($projectID,$userID,$masterStatus);
                    echo json_encode(array('project' => $projectID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('project_error_user_was_not_found_in_project')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('projects_error_not_found')));
                
            }
            
        }
        
    }
    
    
    public function removeProjectUser() {
        
        $this->form_validation->set_rules('projectID', 'lang:projectID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('userID', 'lang:userID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('project_error_remove_participant').validation_errors()));
        } else {
            
            $projectID = $this->input->post('projectID');
            $userID = $this->input->post('userID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $project = $this->projects_model->getOneProject($projectID,false);
        
            if (!empty($project)) {
                //Пользователи, которые уже были приглашены
                $oldUsersIDs = $this->projects_model->getProjectUsers($projectID,$project['creator_id'],true);

                if (in_array($userID, $oldUsersIDs)) {

                    //Если есть инвайт с этим же tt и operationType, то удалить
                    if (!$this->projects_model->getProjectInviteOverTT($projectID,$userID,1)) {
                        $this->projects_model->updateProjectInviteAny($projectID,$userID,array('tt' => $tt,'operation_type' => 2));
                    } else {
                        $this->projects_model->removeProjectInvite($projectID,$userID);
                    }
                    
                    echo json_encode(array('project' => $projectID));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('project_error_user_was_not_found_in_project')));
                    
                }
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('projects_error_not_found')));
                
            }
            
        }
        
    }
    
    
    
    public function fixRoomUsers() {
        $this->form_validation->set_rules('roomID', 'lang:room_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('room_error_add_user_in_room').validation_errors()));
        } else {
            
            $roomID = $this->input->post('roomID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $room = $this->rooms_model->getOneRoom($roomID);
        
            if (!empty($room)) {
                
                $invitesCandidates = $this->rooms_model->getInvitesCandidates($roomID);
                $notificationTypes = explode(',',$this->config->item('config_notification_types'));

                foreach($invitesCandidates as $candidate) {

                    if ($candidate['operation_type'] == 1) {
                        
                        $sendNotification = true;
                        
                        //Проверить, был ли человек в проекте, и то, если проект приватный. 
                        //Если не было, а человек приглашающий владелец проекта, то добавить пользователя ещё и в проект до кучи
                        if ($room['projectID'] != $room['default_project_id']) {
                            
                            $project = $this->projects_model->getOneProject($room['projectID'],false);
                            
                            if (!$project['public_status'] && !$project['room_id']) {
                                
                                if ($project['my_role'] || !$project['my_role'] && in_array($candidate['user_id'],$project['usersIDs'])) {
                                    
                                    if (!in_array($candidate['user_id'],$project['usersIDs'])) {
                                    
                                        $invite_array = array(
                                            'master_status' => 0,
                                            'user_id' => $candidate['user_id'],
                                            'project_id' => $project['id'],
                                            'creator_id' => $this->session->userdata['userID'],
                                            'ts' => time(),
                                            'tt' => 0,
                                            'operation_type' => 0
                                        );

                                        $this->projects_model->inviteProjectUser($invite_array);
                                    }

                                    $notify_array = array(
                                        'recipient_id' => $candidate['user_id'],
                                        'entity_type' => 'project',
                                        'entity_id' => $project['id'],
                                        'subject' => $this->lang->line('project_notification_room_invite_subject1').' "'.$room['name'].'" '.$this->lang->line('project_notification_room_invite_subject2').' "'.$project['name'].'"',
                                        'body' => $this->lang->line('project_notification_room_invite_body1').' "'.$room['name'].'" '.$this->lang->line('project_notification_room_invite_body2').' "'.$project['name'].'", '.$this->lang->line('project_notification_room_invite_body3'),
                                    );
                                    
                                    $this->notification_model->makeNotification($notify_array);
                                    
                                    $sendNotification = false;
                                    
                                } else {
                                    
                                    //Как попал пользователь в комнату, если комната в приватном проекте, 
                                    $this->rooms_model->removeInvite($roomID,$candidate['user_id']);
                                    $sendNotification = false;
                                    
                                }
                                
                            }
                            
                        }
                        
                        if ($sendNotification) {
                        
                            $notify_array = array(
                                'recipient_id' => $candidate['user_id'],
                                'entity_type' => 'room',
                                'entity_id' => $roomID,
                                'subject' => $this->lang->line('notification_room_invite_subject').' "'.$room['name'].'"',
                                'body' => $this->lang->line('notification_room_invite_body').' <a href="'.base_url().'rooms/one_room/'.$roomID.'" target="blank">"'.$room['name'].'"</a>',
                            );

                            $this->notification_model->makeNotification($notify_array);
                            
                        }
                        
                        $this->rooms_model->updateInviteAny($roomID,$candidate['user_id'],array('tt' => 0,'operation_type' => 0));
                        
                    } else {

                        $this->rooms_model->removeInvite($roomID,$candidate['user_id']);

                        $notify_array = array(
                            'recipient_id' => $candidate['user_id'],
                            'entity_type' => 'room',
                            'entity_id' => $roomID,
                            'subject' => $this->lang->line('room_notification_uninvite_subject').' "'.$room['name'].'"',
                            'body' => $this->lang->line('room_notification_uninvite_body').' "'.$room['name'].'"',
                        );

                        $this->notification_model->makeNotification($notify_array);
                        
                    }

                }

                echo json_encode(array('room' => $roomID));
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                
            }
        }
    }
    
    
    
    public function fixProjectUsers() {
        
        $this->form_validation->set_rules('projectID', 'lang:projectID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('tt', 'lang:tt', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('project_error_add_user_in_project').validation_errors()));
        } else {
            
            $projectID = $this->input->post('projectID');
            $tt = $this->input->post('tt');
            
            //Информация о комнате
            $project = $this->projects_model->getOneProject($projectID,false);
        
            if (!empty($project)) {
                
                $invitesCandidates = $this->projects_model->getProjectInvitesCandidates($projectID);
                
                foreach($invitesCandidates as $candidate) {

                    if ($candidate['operation_type'] == 1) {
                        
                        $this->projects_model->updateProjectInviteAny($projectID,$candidate['user_id'],array('tt' => 0,'operation_type' => 0));
                                                
                        $notify_array = array(
                            'recipient_id' => $candidate['user_id'],
                            'entity_type' => 'project',
                            'entity_id' => $projectID,
                            'subject' => $this->lang->line('project_notification_invite_subject').' "'.$project['name'].'"',
                            'body' => $this->session->userdata['name'].' '.$this->lang->line('project_notification_invite_body').' "'.$project['name'].'"',
                        );
                        
                    } else {

                        $this->projects_model->removeProjectInvite($projectID,$candidate['user_id']);
                        
                        $notify_array = array(
                            'recipient_id' => $candidate['user_id'],
                            'entity_type' => 'project',
                            'entity_id' => $projectID,
                            'subject' => $this->lang->line('project_notification_uninvite_subject').' "'.$project['name'].'"',
                            'body' => $this->lang->line('project_notification_uninvite_body').' "'.$project['name'].'"',
                        );

                    }
                    
                    $this->notification_model->makeNotification($notify_array,false);

                }

                $this->notification_model->sendAllNotifications();
                
                echo json_encode(array('room' => $projectID));
                
            } else {
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                
            }
        }
    }
    
    
    
    /**
     * Получить информацию о проекте
     */
    public function getProjectInfo() {
        
        $projectID = $this->input->get('projectID');
        
        $tt = $this->input->get('tt');
        $tt = isset($tt) ? $tt : 0;
        
        if ($projectID) {
            
            if ($tt) $this->projects_model->cleanProjectInvites($projectID,$tt);
                
            
            $project = $this->projects_model->getOneProject($projectID);
            $projectRooms = $project['rooms'];
            
            $freeRooms = array();
            $userID = $this->session->userdata('userID');
            if ($userID) $freeRooms = $this->rooms_model->getFreeRooms($userID);
            
            echo json_encode(array('info'=> $project,'rooms' => $projectRooms,'freeRooms' => $freeRooms));
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_projectid')));
        }
    }
    
    
    /**
     * Контекстный поиск по таблицам
     */
    public function getContextSearch() {
        $this->form_validation->set_rules('tableName', 'lang:project_name', 'trim|required');
        $this->form_validation->set_rules('colName', 'lang:project_name', 'trim|required');
        $this->form_validation->set_rules('item', 'lang:project_description', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_content_search')));
        } else {
            
            $this->load->helper('string');
            
            if ($this->input->post('tableName') != 'tags') {
            
                $request_fields = array(
                    'table' => $this->input->post('tableName',true),
                    'column' => $this->input->post('colName',true),
                    'item'  => htmlspecialchars(strip_quotes(mb_strtolower(mb_substr($this->input->post('item',true), 0, 1)).mb_substr($this->input->post('item',true),1))),
                    'addColumn' => $this->input->post('addCol',true),
                    'addColumnValue' => $this->input->post('addColValue',true),
                    'limit' => $this->input->post('limit') ? $this->input->post('limit') : 5
                );
            }
            
            
            $request_fields2 = array(
                'table' => $this->input->post('tableName',true),
                'column' => $this->input->post('colName',true),
                'item'  => htmlspecialchars(strip_quotes(mb_strtoupper(mb_substr($this->input->post('item',true), 0, 1)).mb_substr($this->input->post('item',true),1))),
                'addColumn' => $this->input->post('addCol',true),
                'addColumnValue' => $this->input->post('addColValue',true),
                'limit' => $this->input->post('limit') ? $this->input->post('limit') : 5
            );
            
            
            $items1 = $this->input->post('tableName',true) != 'tags' ? $this->tags_model->getContext($request_fields) : array();
            $items2 = $this->tags_model->getContext($request_fields2);
            
            $items = array_merge($items1,$items2);
            
            echo json_encode(array('items'=> $items));
        }
    }
    
    
    /**
     * Получить теги пользователя
     */
    public function getUsersTags() {
        
        $userID = $this->input->get('userID');
        
        if (!$userID || !ctype_digit($userID)) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_userid_not_exist')));
        } else {
            
            $tags = $this->users_model->getUserTags($userID);
            
            $user = $this->users_model->getOneUserInfoOverID($userID);
            
            $fields = $this->users_model->getProfileAdditionalFields($userID,true);
            
            
            $rooms = $this->rooms_model->getRoomsForMe($userID);
            
            echo json_encode(array('tags'=> $tags,'fields' => $fields,'info' => $user,'rooms' => $rooms));
        }
        
    }
    
    
    /**
     * Получить информацию о мероприятии
     */
    public function getEventInfo() {
        
        $eventID = $this->input->get('eventID');
        
        if ($eventID) {
            
            $roomUsers = array();
            
            $event = $this->schedule_model->getOneEvent($eventID);
            if (!empty($event)) $roomUsers = $this->rooms_model->getRoomUsers($event['room_id'],true);
            
            echo json_encode(array('info'=> $event,'users' => $roomUsers));
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_eventid')));
        }
        
    }
    
    
    public function sendEventNotification() {
        
        $eventID = $this->input->get('eventID');
        
        if ($eventID) {
            
            $event = $this->schedule_model->getOneEvent($eventID);
            if (!empty($event)) {
                //$roomUsers = $this->rooms_model->getRoomUsers($event['room_id'],true);
                $this->schedule_model->sendOneEventNotification($event);
            }
            
            echo json_encode(array('info'=> $event));
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_eventid')));
        }
        
    }
    
    
    public function getUserInfo() {
        
        $userLogin = $this->input->get('login');
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        
        if ($userLogin) {
            
            $user = $this->users_model->getOneUserInfo($userLogin);
            if (!empty($user)) {
                $tags = $this->users_model->getUserTags($user['id']);

                if ($format == 'json')
                    echo json_encode(array('user'=> $user,'tags' => $tags));
                else
                    $this->printf_r (array('user'=> $user,'tags' => $tags));
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_user_not_found')));
            }
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_login')));
        }
        
    }
    
    
    public function getFiles() {
        $projectID = $this->input->get('project');
        $roomID = $this->input->get('room');
        $type = $this->input->get('type');
        
        $tag = $this->input->get('tag');
        $tag = urldecode($tag);
        if ($tag != '') $tag = mb_strtoupper(mb_substr($tag,0,1)).mb_substr($tag,1);
        
        $format = $this->input->get('format');
        $format = isset($format) ? $format : 'json';
        
        $forServer = $this->input->get('for_server') ? true : false;
        
        if ($projectID) {
            //video|text
            $projectFiles = $this->projects_model->getProjectFiles($projectID,$type,$tag,$forServer);
            
            if ($format != 'json') {
                $this->printf_r($projectFiles);
            } else {
                echo json_encode($projectFiles);
            }
            
        } elseif ($roomID) {
            
            if (is_numeric($roomID)) {
            
                $room = $this->rooms_model->getOneRoom($roomID);
                
                $projectFiles = $this->projects_model->getProjectFiles($room['default_project_id'],$type,$tag,$forServer);
                
                if ($room['project_id'] != $room['default_project_id']) {
                    
                    $trueProjectFiles = $this->projects_model->getProjectFiles($room['project_id'],$type,$tag,$forServer);
                    
                    $projectFiles = array_merge($projectFiles,$trueProjectFiles);
                    
                }
                

                if ($format != 'json') {
                    $this->printf_r($projectFiles);
                } else {
                    echo json_encode($projectFiles);
                }
                
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_no_roomid')));
            }
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_projectid')));
        }
    }
    
    
    /**
     * Фоновое сохранение объектов и изображения подложки
     */
    public function saveCanvas() {
        
        $type = $this->input->post('type');
        $itemID = $this->input->post('itemID');
        $jsonData = $this->input->post('jsonData');
        $base64_png = $this->input->post('png');
        
        $fields = array(
            'json_data' => $jsonData,
            'base64_png' => $base64_png
        );
        
        if ($type == 'floor' && $itemID)
            $this->seating_model->updateFloor($itemID,$fields);
        elseif($type == 'flat' && $itemID)
            $this->seating_model->updateFlat($itemID,$fields);
        
        echo json_encode(array('type' => $type));
        
    }
    
    
    /**
     * Скачать подложку
     */
    public function downloadCanvas() {
        
        $type = $this->input->get('type');
        $itemID = $this->input->get('itemID');
        
        
        if ($type == 'floor' && $itemID)
            $item = $this->seating_model->getOneFloor($itemID);
        elseif($type == 'flat' && $itemID)
            $item = $this->seating_model->getOneFlat($itemID);
        
        if (!empty($item) && $item['json_data'] != '') {
            
            $this->load->helper('download');
            
            if ($type == 'floor')
                $name = $type.' - '.$item['building']['name'].' - '.($item['name'] != '' ? $item['name'] : $item['numb']).".json";
            elseif($type == 'flat' && $itemID)
                $name = $type.' - '.$item['floor']['building']['name'].' - '.$item['floor']['name'].' - '.$item['name'].".json";
            
            force_download($name, $item['json_data']);
            
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_download_canvas')));
        }
        
    }
    
    
    /**
     * Фоновое сохранение координат объекта
     */
    public function saveCoords() {
        $this->form_validation->set_rules('type', 'lang:equipment_object_coord_type', 'trim|required|alpha');
        $this->form_validation->set_rules('elID', 'lang:equipment_object_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('top', 'lang:equipment_object_top', 'trim|required|integer');
        $this->form_validation->set_rules('left', 'lang:equipment_object_left', 'trim|required|integer');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_save_coords').validation_errors()));
        } else {
            
            $type = $this->input->post('type');
            $elID = $this->input->post('elID');
            $top = $this->input->post('top');
            $left = $this->input->post('left');
            
            if ($type == 'flat') {
                $fields = array(
                    'flat_left' => $left,
                    'flat_top'  => $top
                );
                
                $this->seating_model->updateFlat($elID,$fields);
            } else {
                $fields = array(
                    'arm_top' => $top,
                    'arm_left' => $left
                );
                
                $this->seating_model->updateEquipment($elID,$fields);
            }
            
            
            echo json_encode(array('type' => $type,'elID' => $elID,'left'=>$left,'top'=>$top));
        }
    }
    
    
    /**
     * Получить свободное оборудование и отметить то, что уже используется
     */
    public function getEquipments() {
        
        $this->form_validation->set_rules('type', 'lang:equipment_object_coord_type', 'trim|required|alpha');
        $this->form_validation->set_rules('itemID', 'lang:equipment_object_id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_get_equipments').validation_errors()));
        } else {
            
            $type = $this->input->post('type');
            $itemID = $this->input->post('itemID');
            
            $freeequipments = array(
                'cams' => $this->seating_model->getAllFreeEquipments('cam'),
                'arms' => $this->seating_model->getAllFreeEquipments('arm'),
            );
            
            $itemequipments = array(
                'cams' => $this->seating_model->getAllItemEquipments($type,$itemID,'cam'),
                'arms' => $this->seating_model->getAllItemEquipments($type,$itemID,'arm'),
            );
            
            echo json_encode(array('type' => $type,'itemID' => $itemID,'freeequipments' => $freeequipments,'itemequipments' => $itemequipments));
            
        }
        
    }
    
    
    /**
     * добавить или удалить оборудование из этажа/помещения
     */
    public function updateEquipmentToItem() {
        
        $this->form_validation->set_rules('type', 'lang:equipment_object_coord_type', 'trim|required|alpha');
        $this->form_validation->set_rules('itemID', 'lang:equipment_object_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('eqID', 'lang:equipment_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('operation', 'lang:building_equipment_operation', 'trim|required|alpha');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_update_building_equipments').validation_errors()));
        } else {
            
            /*
             * operations = 'add' || 'remove'
             * либо добавляем оборудование, либо удаляем
             */
            
            $operation = $this->input->post('operation');
            $type = $this->input->post('type');
            $itemID = $this->input->post('itemID');
            $eqID = $this->input->post('eqID');
            
            if ($operation == 'remove') {
                $fields = array(
                    'audience_id' => 0,
                    'floor_id' => 0
                );
                
            } else {
                $itemField = $type == 'floor' ? 'floor_id' : 'audience_id';
                $fields = array(
                    $itemField => $itemID,
                    'arm_top' => 0,
                    'arm_left' => 0
                );
                
            }
            $this->seating_model->updateEquipment($eqID,$fields);
            
            $eq = $this->seating_model->getOneEquipment($eqID);
            
            echo json_encode($eq);
        }
        
    }
    
    
    /**
     * Обновить угол поворота оборудования (пока только камер)
     */
    public function saveRotation() {
        $this->form_validation->set_rules('eqID', 'lang:equipment_object_id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('angle', 'lang:equipment_object_rotation_angle', 'trim|required|integer');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_update_equipments_rotation').validation_errors()));
        } else {
            
            $eqID = $this->input->post('eqID');
            $angle = $this->input->post('angle');
            
            $fields = array(
                'rotation' => $angle
            );

            $this->seating_model->updateEquipment($eqID,$fields);

            echo json_encode(array('eqID' => $eqID,'angle' => $angle));
            
        }
        
    }
    
    
    
    /**
     * Получить пользователей на АРМах
     */
    public function getOnlineEquipmentsUsers() {
        
        $arms = $this->input->post('arms');
        if (count($arms) > 0) {
            $arms_info = array();
            foreach ($arms as $arm) {
                $arms_info[$arm] = $this->seating_model->getUserOnARM($arm);
            }
            echo json_encode($arms_info);
        }
    }
   
    
    
    /**
     * Асинхронный поиск пользователя
     */
    public function getUserSearch() {
        
        $this->form_validation->set_rules('text', 'lang:text', 'trim');
        $this->form_validation->set_rules('limit', 'lang:limit', 'trim|is_natural');
        $this->form_validation->set_rules('roomID', 'lang:roomID', 'trim|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_search_users').validation_errors()));
        } else {
            $text = $this->input->post('text',true);
            $limit = $this->input->post('limit');
            
            $roomID = $this->input->post('roomID') ? $this->input->post('roomID') : 0;
            
            $filteredUsersIDs = array();
            
            if ($roomID && ctype_digit($roomID)) {
                $oldUsersIDs = $this->rooms_model->getInvitedUsersIDs($roomID);
                
                $room = $this->rooms_model->getOneRoom($roomID);
                
                if ($room['projectID'] != $room['default_project_id']) {
                            
                    $project = $this->projects_model->getOneProject($room['projectID'],false);

                    if (!$project['public_status']) {

                        if (!$project['my_role']) {

                            foreach($project['users'] as $userRecord) {
                            
                                $filteredUsersIDs[] = $userRecord['user_id'];
                                
                            }

                        }

                    }

                }
                
            }
            
            $this->load->helper('string');
            $text = strip_quotes($text);
            
            $users = $this->users_model->getUsersOverSearch($text,false,$limit,$oldUsersIDs,$filteredUsersIDs);
            echo json_encode(array('users' => $users,'text' => $text));
        }
        
    }
    
    
    /**
     * Асинхронный поиск пользователя
     */
    public function getUserSearchForProject() {
        
        $this->form_validation->set_rules('text', 'lang:text', 'trim');
        $this->form_validation->set_rules('limit', 'lang:limit', 'trim|is_natural');
        $this->form_validation->set_rules('projectID', 'lang:projectID', 'trim|required|is_natural');
        $this->form_validation->set_rules('projectCreatorID', 'lang:projectCreatorID', 'trim|required|is_natural');
        
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_search_users').validation_errors()));
        } else {
            $text = $this->input->post('text',true);
            $limit = $this->input->post('limit');
            
            $projectID = $this->input->post('projectID') ? $this->input->post('projectID') : 0;
            $projectCreatorID = $this->input->post('projectCreatorID') ? $this->input->post('projectCreatorID') : 0;
            
            $oldUsersIDs = array();
            
            $filteredUsersIDs = array();
            
            if ($projectID && ctype_digit($projectID)) {
                //$oldUsersIDs = $this->projects_model->getProjectUsers($projectID,$projectCreatorID,true);
                
                $project = $this->projects_model->getOneProject($projectID,false);

                if (!$project['public_status']) {

                    if (!$project['my_role']) {

                        foreach($project['users'] as $userRecord) {

                            $filteredUsersIDs[] = $userRecord['user_id'];

                        }

                    }

                }
                
            } else {
                
                $oldUsersIDs[] = $projectCreatorID;
                
            }
            
            $this->load->helper('string');
            $text = strip_quotes($text);
            
            $users = $this->users_model->getUsersOverSearch($text,false,$limit,$oldUsersIDs,$filteredUsersIDs);
            echo json_encode(array('users' => $users,'text' => $text));
        }
        
    }
    
    
    /**
     * Отметить как прочитанное уведомление
     */
    public function checkNotification() {
        $this->form_validation->set_rules('notifyID', 'notifyID', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_update_notifications').validation_errors()));
        } else {
            
            $notifyID = $this->input->post('notifyID');
            $this->notification_model->updateNotification($notifyID,array('status' => 1),$this->session->userdata('userID'));

            echo json_encode(array('notifyID' => $notifyID));   
        }
    }
    

    /**
     * Обновить фоновое изображение
     */
    public function saveBackground() {
        $this->form_validation->set_rules('type', 'lang:text', 'trim|required');
        $this->form_validation->set_rules('param', 'lang:text', 'trim|required');
        $this->form_validation->set_rules('itemID', 'lang:text', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('value', 'lang:text', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_update_background').validation_errors()));
        } else {
            
            $itemID = $this->input->post('itemID');
            $value = $this->input->post('value',true);
            $param = $this->input->post('param');
            $type = $this->input->post('type');
            
            if ($type == 'floor') {
                $fields = array(
                    $param => $value
                );
                $this->seating_model->updateFloor($itemID,$fields);
            } else {
                $fields = array(
                    $param => $value
                );
                $this->seating_model->updateFlat($itemID,$fields);
            }
            
            echo json_encode(array('item' => $itemID,'status'=>'ok'));
            
        }
        
        
    }
    
    
    
    /**
     * TEST
     */
    public function getAllDirObjects() {
        
        $projectFiles = array();
        $this->load->model('tools_model');
        $projectFiles = $this->tools_model->getDirectoryMap('./upload/projects/35','');
        $this->printf_r($projectFiles);
        exit;
    }
    
    
    
    public function saveDoc() {
        
        $projectID = $this->input->post('projectID');
        $fileName = $this->input->post('fileName');
        $fileBlob = $this->input->post('fileBlob');
        
        //Логин сохранившего
        $actor = $this->input->post('actor');
        //users - список логинов участников {"user1":0,"user2":0,"user3":1} (0 - читатель, 1 - писатель)
        $users = $this->input->post('users');
        
        if (ctype_digit($projectID)) {
    
            $this->load->model('tools_model');
            
            $projectFiles = $this->tools_model->getDirectoryMap('./upload/projects/'.$projectID,'');
            
            $suffix = '';
            
            foreach($projectFiles as $fileInPath) {
                if (mb_strpos($fileInPath,$fileName) !== FALSE && mb_strpos($fileInPath,'_old_documents_versions_') === FALSE) {
                    $suffix = $fileInPath;
                    break;
                }
            }
            
            $directoryWithFile = mb_strrpos($suffix,'/') === FALSE ? './upload/projects/'.$projectID : './upload/projects/'.$projectID.'/'.mb_substr($suffix,0,mb_strrpos($suffix,'/'));
            
            $oldFilePath = $directoryWithFile.'/'.$fileName;

            $t = date('d.m.Y H:i');
            $destPathDir = $directoryWithFile.'/_old_documents_versions_/'.$fileName.'/'.$t.'/';

            if (!is_dir($destPathDir)) mkdir($destPathDir,0777,TRUE);

            if (!rename($oldFilePath, $destPathDir.$fileName)) {
                echo json_encode(array('status' => 'moving failed','destPathDir' => $destPathDir,'directoryWithFile' => $directoryWithFile,'suffix' => $suffix));
            } else {

                if (!move_uploaded_file($_FILES['fileBlob']['tmp_name'], $oldFilePath)) {
                    echo json_encode(array('status' => 'error'));
                    rename($destPathDir.$fileName,$oldFilePath);
                    rmdir($destPathDir);
                } else {
                    echo json_encode(array('status' => 'ok'));
                }

                /*
                $fp = fopen($oldFilePath,"w");
                if ($fp) {

                    stream_set_write_buffer($fp, 0);
                    fwrite($fp,$fileBlob);
                    fclose($fp);

                    echo json_encode(array('status' => 'ok'));
                } else {
                    echo json_encode(array('status' => 'opening failed'));
                }
                 * 
                 */
            }
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('status' => 'error','msg' => 'projectID is undefined'));
        }
    }
    
    
    /**
     * Получить список тегов проекта
     */
    public function getProjectTags() {
        $this->form_validation->set_rules('path', 'required_path', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_get_project_tags').validation_errors()));
        } else {
            
            $path = $this->input->post('path');
            
            preg_match("/\/projects\/(.*)\//U", $path, $data);
            if (empty($data)) {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_get_project_tags').validation_errors()));
            } else {
                $projectID = $data[1];

                $project = $this->projects_model->getOneProject($projectID);
                
                $tags = $this->projects_model->getFileTags($path);
                
                echo json_encode(array('path' => $path,'project' => $project,'tags' => $tags));
            }
            
        }
    }
    
    
    public function addProjectTags() {
        $this->form_validation->set_rules('path', 'required_path', 'trim|required');
        $this->form_validation->set_rules('tags', 'required_tags', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_set_project_tags').validation_errors()));
        } else {
            
            $path = $this->input->post('path');
            
            $tags = $this->input->post('tags');
            
            $tags_array = explode(',',$tags);
            foreach($tags_array as $tag) {
                
                $ttag = trim($tag);
                $fields = array(
                    'name' => mb_strtoupper(mb_substr($ttag,0,1)).mb_substr($ttag,1),
                    'path' => $path
                );
                
                $this->projects_model->addTagToFile($fields);
                
            }
            
            echo json_encode(array('path' => $path,'tags_string' => $tags,'tags_array' => $tags_array));
            
        }
    }
    
    
    public function removeProjectTag() {
        $this->form_validation->set_rules('path', 'required_path', 'trim|required');
        $this->form_validation->set_rules('tag', 'required_tag', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_remove_project_tag').validation_errors()));
        } else {
            
            $path = $this->input->post('path');
            $tag = $this->input->post('tag');
            
            $this->projects_model->removeTagFromFile($path,$tag);
            
            echo json_encode(array('path' => $path,'tag' => $tag));
            
            
        }
    }
    
    
    public function getfilestags() {
        
        $roomID = $this->input->get('roomId');
        
        
        $projectID = $this->input->get('project');
        
        if ($roomID != '') $projectID = $this->rooms_model->getRoomsProjectID($roomID);
            
        if ($projectID != '') {
            
            $project = $this->projects_model->getOneProject($projectID);
            if (!empty($project)) {
                $tags = $project['projectTags'];

                $format = $this->input->get('format');

                if ($format != 'text') {
                    echo json_encode(array('info'=> $project,'tags' => $tags));
                } else {
                    $this->printf_r($tags);
                }
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('error_ajax_project_not_found')));
            }
            
        
        } else {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('error_ajax_no_projectid')));
        }
        
    }
    
    
    
    public function getskedtypes() {
        
        $this->load->model('sked_model');
        
        $projectID = $this->input->get('project');
        $projectID = ctype_digit($projectID) && $projectID != '' ? $projectID : 0;
        
        $types = $this->sked_model->getAllItems('types',$projectID,true);

        $format = $this->input->get('format');

        if ($format != 'text') {
            echo json_encode(array('items' => $types));
        } else {
            $this->printf_r($types);
        }

    }
    
    
    public function getskedstatuses() {
        
        $this->load->model('sked_model');
        
        $projectID = $this->input->get('project');
        $projectID = ctype_digit($projectID) && $projectID != '' ? $projectID : 0;
        
        $statuses = $this->sked_model->getAllItems('statuses',$projectID,true);
        
        $format = $this->input->get('format');

        if ($format != 'text') {
            echo json_encode(array('items' => $statuses));
        } else {
            $this->printf_r($statuses);
        }

    }
    
    
    
    public function updateRoomBackground() {
        
        $this->form_validation->set_rules('roomID', 'roomID', 'trim|required');
        $this->form_validation->set_rules('src', 'src', 'trim|required');
        
        if (!$this->form_validation->run()) {
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('room_error_background_updating').validation_errors()));
        } else {
            
            $roomID = $this->input->post('roomID');
            $src = $this->input->post('src');
            
            if ($roomID && ctype_digit($roomID)) {
                
                $room = $this->rooms_model->getOneRoom($roomID);
                
                if (!empty($room)) {
                    
                    if (!is_dir('./upload/rooms/')) mkdir('./upload/rooms/',0755,TRUE);
                    if (!is_dir('./upload/rooms/'.$room['id'].'/')) mkdir('./upload/rooms/'.$room['id'].'/',0755,TRUE);
                    
                    $img = mb_substr($src,mb_strlen('data:image/png;base64,'));
                    $img = str_replace(' ','+', $img);
                    file_put_contents('./upload/rooms/'.$room['id'].'/'.time().'.png', base64_decode($img));
                    
                    echo json_encode(array('room' => $room['id']));
                    
                } else {
                    
                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                    exit;
                    
                }
                
            } else {
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_access_id_not_valid')));    
            }
            
        }
        
    }
    
    
    
    public function getRoomsGIFs() {
        
        $roomsArray = $this->input->post('roomsArray');
        $answerArray = array();
            
        if (isset($roomsArray) && !empty($roomsArray)) {
            
            foreach($roomsArray as $room) {

                $file = '';
                $times = '';

                try {
                    
                    //Проверить png да и удалить лишние
                    if (file_exists('./upload/rooms/'.$room.'/animated.gif')) {
                        //throw new Exception('convert command not found');
                        unlink('./upload/rooms/'.$room.'/animated.gif');
                    }
                    
                    $times = mb_substr(time(),0,mb_strlen(time()) - 5);
                    
                    exec('convert -delay 250 -loop 0 ./upload/rooms/'.$room.'/'.$times.'*.png ./upload/rooms/'.$room.'/animated.gif');
					
                    if (!file_exists('./upload/rooms/'.$room.'/animated.gif')) throw new Exception('convert command not found');
                    
                    $file = base_url().'upload/rooms/'.$room.'/animated.gif';
                    
                } catch (Exception $ex) {
                    
                    
                    
                }

                $answerArray[] = array(
                    'room_id' => $room,
                    'file' => $file,
                    'times' => $times
                );
                
            }
            
        }
        
        
        echo json_encode(array('array' => $roomsArray,'answer' => $answerArray));
        
        
    }
    
    
    
    public function docs_callbacks() {
        
        if (($body_stream = file_get_contents("php://input"))===FALSE){
            echo "Bad Request";
        }
        
        $data = json_decode($body_stream, TRUE);
        
        //$dataString = '';
        $this->load->model('tools_model');
        $dataString = $this->tools_model->getArrayAsString($data);
        
        
        log_message('debug', '');
        log_message('debug', 'POST FROM ONLYOFFICE: '.$data["status"].' '.(isset($data["url"]) ? $data['url'] : '').' '.$data['key']);
        log_message('debug', $dataString);
        log_message('debug', '');
        

        if ($data["status"] == 2){
            
            $downloadUri = $data["url"];
            
            log_message('debug', $data["key"]);
        
            $arrContextOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false
                )
            );
            
            if (($new_data = file_get_contents($downloadUri,false,  stream_context_create($arrContextOptions)))===FALSE){
                echo "Bad Response";
                
            } else {
                
                $fileRecord = $this->projects_model->getFileOverSecondaryKey($data["key"]);
                
                if (!empty($fileRecord)) {
                    
                    preg_match('/projects\/(.*)\//U', $fileRecord['file_addr'],$matches);

                    if (isset($matches[1])) {

                        $directoryWithFile = './upload/projects/'.$matches[1];

                        $t = date('d.m.Y');
                        $destPathDir = $directoryWithFile.'/_old_documents_versions_/'.$fileRecord['title'].'/'.$t.'/';

                        if (!is_dir($destPathDir)) mkdir($destPathDir,0777,TRUE);

                        $fileName = $fileRecord['title'].'_'.date('d.m.Y H:i').'.'.mb_substr(strrchr($fileRecord['file_addr'],'.'),1);
                        
                        if (!rename($fileRecord['file_addr'], $destPathDir.$fileName)) {
                            
                            //echo json_encode(array('status' => 'moving failed','destPathDir' => $destPathDir,'directoryWithFile' => $directoryWithFile,'suffix' => $suffix));
                            
                            log_message('debug', 'moving failed :(');
                            
                        } else {
                        
                            log_message('debug', (empty($fileRecord) ? 'FILE RECORD NOT FOUND :(' : $fileRecord['file_addr']));
                            file_put_contents($fileRecord['file_addr'], $new_data, LOCK_EX);
                            
                        }
                    
                    } else {
                    
                        log_message('debug', (empty($fileRecord) ? 'PROJECT NOT FOUND :(' : $fileRecord['file_addr']));

                    }
                        
                } else {

                    log_message('debug', (empty($fileRecord) ? 'FILE RECORD NOT FOUND :(' : $fileRecord['file_addr']));
                    
                } 
            }
            
        }
        
        echo "{\"error\":0}";
        
    }
    
    
    /**
     * Получение количества пользователей в комнатах
     * 
     * @throws Exception
     */
    public function getRoomsOnlines() {
        
        $roomsArray = $this->input->post('roomsArray');
        $answerArray = array();
            
        if (isset($roomsArray) && !empty($roomsArray)) {
            
            $metriksStat = $this->log_model->getMetriksUsersDateRangeCount(time() - $this->config->item('config_online_execution_time'),time(),'rooms',0,false);
            
            foreach($roomsArray as $room) {
                
                $usersInRoom = array();
                
                foreach($metriksStat as $stat) {
                    
                    if ($stat['entity_id'] == $room) $usersInRoom[] = $stat;
                    
                }
                
                $usersStr = '';
                foreach($usersInRoom as $userRecord) {
                    $usersStr .= ($usersStr == '' ? ' ' : ' | ').htmlspecialchars($this->users_model->getFullNameOverID($userRecord['user_id'],true));
                }

                $answerArray[] = array(
                    'room_id' => $room,
                    'count' => count($usersInRoom),
                    'users' => $usersStr
                );
                
            }
            
        }
        
        echo json_encode(array('array' => $roomsArray,'answer' => $answerArray));
        
        
    }
    
    
    
    public function fileCallbackUpload() {
        
        $this->form_validation->set_rules('userID', 'user_id', 'trim|required|is_natural');
        $this->form_validation->set_rules('addr', 'addr', 'trim|required');
        
        if (!$this->form_validation->run()) {
            
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('files_error_upload_info').validation_errors()));
            
        } else {
            
            $fileAdr = $this->input->post('addr');
            
            $fileAdr = '.'.mb_substr($fileAdr,mb_strpos($fileAdr,'/upload/'));
        
            if (file_exists($fileAdr)) {


                $fileExt = mb_substr(strrchr($fileAdr,'.'),1);
                //echo mb_strrchr($fileAdr, '/');
                $title = mb_substr($fileAdr,  mb_strrpos($fileAdr, '/') + 1,mb_strrpos($fileAdr, '.') - mb_strrpos($fileAdr, '/') - 1);

                //Проверить, есть ли запись о файле в базе, если нет - то добавить
                $fileRecord = $this->projects_model->getFileOverKey(md5($fileAdr));

                $newKey = md5($fileAdr.filectime($fileAdr).'_');

                if (empty($fileRecord)) {

                    $fields = array(
                        'file_addr' => $fileAdr,
                        'file_key' => md5($fileAdr),
                        'file_secondary_key' => $newKey,
                        'description' => '',
                        'title' => $title,
                        'user_id' => $this->session->userdata('userID'),
                        'ts' => time()
                    );

                    $this->projects_model->addFileOverKey($fields);
                    $this->log_model->addMessageLog($this->lang->line('files_event_upload_info').' "'.htmlspecialchars($fields['title'].'.'.$fileExt).'"','Files');

                } 
                
                echo json_encode(array('file'=> $fields));
                
            } else {

                log_message('debug', 'DOCS FILE NOT FOUND: '.$fileAdr);

                header("HTTP/1.0 500");
                echo json_encode(array('error'=> 'DOCS FILE NOT FOUND: '.$fileAdr));

            }
            
            
        }
        
    }
    
    
    
    function checkRoomID() {
        
        $this->form_validation->set_rules('roomID', 'lang:room_id', 'trim|required|is_natural_no_zero');
        
        if (!$this->form_validation->run()) {
            
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('files_error_upload_info').validation_errors()));
            
        } else {
            
            $roomID = $this->input->post('roomID');
            
            if (ctype_digit($roomID) && $roomID > 0) {
            
                $room = $this->rooms_model->getOneRoom($roomID);

                if (!empty($room)) {

                    $allowedRooms = $this->rooms_model->getRoomsForMe($this->session->userdata['userID'],true);
                    
                    if (in_array($roomID, $allowedRooms) || $room['connection_notinvited_users'] || $room['c_create_listeners_from_listeners']) {

                        //Всё ок
                        echo json_encode(array('status'=> 'ok'));

                    } else {

                        header("HTTP/1.0 500");
                        echo json_encode(array('error'=> $this->lang->line('room_error_access_denied')));
                        exit;

                    } 
                    
                } else {

                    header("HTTP/1.0 500");
                    echo json_encode(array('error'=> $this->lang->line('room_error_room_not_found')));
                    exit;

                }

            } else { 
                
                header("HTTP/1.0 500");
                echo json_encode(array('error'=> $this->lang->line('room_error_access_id_not_valid')));
                exit;

            }
            
        }
        
        
    }
    
    
    
    public function getPopulatedRooms() {
        
        $usersLimit = $this->input->post('ulimit');
        
        if ($usersLimit == '') $usersLimit = 5;
        
        
        /* Рассчитать наиболее популярных пользователей, для этого:
         * 1. Выбрать все комнаты, в которых пользователь участвовал за последний месяц
         * 2. Сформировать массив пользователей-участников этих комнат
         * 3. Вычислить рейтинг пользователей и вывести первые N пользователей
         */
        
        //$start_ts = mktime(0,0,0,date("n"),date("j"),date("Y"));
        
        $roomFavoriteRooms = $roomFavoriteRooms = array();
        
        $lastMonthRoomsFullStat = $this->log_model->getMetriksEntityIDInDateRange(time() - 60 * 60 * 24 * 30, time(),'rooms',$this->session->userdata('userID'),true);
        $roomFavoriteRoomsArray = $roomFavoriteFavoriteRoomsArray = array();
        
        foreach($lastMonthRoomsFullStat as $lmrRecord) {
            
            $roomInfo = $this->rooms_model->getOneRoom($lmrRecord['entity_id']);
            
            if (!empty($roomInfo) && !$roomInfo['archive_status'] && ($roomInfo['userAccess'] || $roomInfo['connection_notinvited_users'])) {
            
                $roomFavoriteRooms[$lmrRecord['entity_id']] = $this->log_model->getMetriksUsersDateRangeCount(time() - 60 * 60 * 24 * 30, time(),'rooms',$lmrRecord['entity_id']);
                $roomFavoriteRoomsArray[] = $lmrRecord['entity_id'];
                
            }
            
        }
        
        arsort($roomFavoriteRooms);
        
        $roomFavoriteRooms = array_slice($roomFavoriteRooms, 0, 3, true);
        
        foreach($roomFavoriteRooms as $rkey => $room) {
            
            $roomFavoriteRooms[$rkey] = $this->rooms_model->getOneRoom($rkey);
            
            $roomFavoriteFavoriteRoomsArray[] = $rkey;
           
        }
        
        
        $roomFavoriteUsers = array();
        
        foreach($roomFavoriteRoomsArray as $lmrRecord) {
            
            $roomUsers = $this->rooms_model->getRoomUsers($lmrRecord,true,true,true);
            
            foreach($roomUsers as $userRecord) {
                
                if ($userRecord != $this->session->userdata('userID')) {
                
                    if (!isset($roomFavoriteUsers[$userRecord])) {
                        $roomFavoriteUsers[$userRecord] = 1;
                    } else {
                        $roomFavoriteUsers[$userRecord]++;
                    }
                    
                    //Если комната в тройке самых популярных, то добавить этому пользователю ещё один балл
                    if (in_array($lmrRecord, $roomFavoriteFavoriteRoomsArray)) $roomFavoriteUsers[$userRecord]++;
                    
                }
                
            }
            
        }
        
        
        arsort($roomFavoriteUsers);
        
        $roomFavoriteUsers = array_slice($roomFavoriteUsers, 0, $usersLimit, true);
        foreach($roomFavoriteUsers as $rfukey => $rfRecord) {
            
            $roomFavoriteUsers[$rfukey] = $this->users_model->getOneUserInfoOverID($rfukey);
           
        }
        
        echo json_encode(array('rooms'=> $roomFavoriteRooms,'users' => $roomFavoriteUsers));
        
        
    }
    
    
    /**
     * Получить список комнат с трансляциями
     */
    public function getTranslationsRooms() {
        
        //$project = $this->input->post('project');
        
        $roomsTranslations = $this->rooms_model->getRoomsTranslations();
        
        echo json_encode(array('rooms'=> $roomsTranslations));
        
    }
    
    
    
    /**
     * Получить хронометраж активности
     */
    public function getChronometrie() {
        
        $ranges = array();
                
        $max = 0;
        $max_hour = 0;
        
        $lastMonthRoomsFullStat = $this->log_model->getMetriksEntityIDInDateRange(time() - 60 * 60 * 24 * 30, time(),'rooms',$this->session->userdata('userID'),false);
        
        foreach($lastMonthRoomsFullStat as $statRecord) {
            
            if (isset($ranges[date('H',$statRecord['ts'])])) 
                $ranges[date('H',$statRecord['ts'])]++;
            else 
                $ranges[date('H',$statRecord['ts'])] = 1;
            
        }
        
        foreach($ranges as $rkey => $range) {
            
            if ($range > $max) {
                $max = $range;
                $max_hour = $rkey;

            }

        }
        
        echo json_encode(array('chronometrie'=> $ranges,'max' => $max,'max_hour' => $max_hour));
        
    }
    
    
    /**
     * Получить информацию о созданных комнатах и перцентилях
     */
    public function getRoomsPerzentil() {
        
        if (isset($this->session->userdata['userID'])) {
        
            $rooms = $this->rooms_model->getRoomsUserStatistic();

            $myCount = 0;

            foreach($rooms as $stat) {
                if ($stat['creator_id'] == $this->session->userdata('userID')) {
                    $myCount = $stat['count'];
                    break;
                }
            }

            $pzt = 0;

            if ($myCount) {

                //Посчитать, сколько людей создали меньше комнат
                foreach($rooms as $stat) {
                    if ($stat['count'] < $myCount) $pzt++;
                }

                $pzt = count($rooms) ? round($pzt/count($rooms)*100,0) : 0;

            }

            echo json_encode(array('stat'=> $myCount,'perzentil' => $pzt));
            
        } else {
            
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('authentication required')));
            
        }
        
    }
    
    
    
    public function getUsersPortion() {
        
        $exUsers = $this->input->post('exusers');
        $limit = $this->input->post('portion');
        
        if (isset($this->session->userdata['userID'])) {
        
            $exUsers[] = $this->session->userdata('userID');
            
            echo json_encode(array('status'=> 'ok','users' => $this->users_model->getAllUsersForDialogs($exUsers,$limit)));
            
        } else {
          
            header("HTTP/1.0 500");
            echo json_encode(array('error'=> $this->lang->line('authentication required')));
            
        }     
        
        
    }
    
    
    
}



