<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['upload_userfile_not_set'] = 'Не удалось найти переменную с именем "userfile"';
$lang['upload_file_exceeds_limit'] = 'Загружаемый файл превышает максимально допустимый размер файла, установленный настройками сервера';
$lang['upload_file_exceeds_form_limit'] = 'Загружаемый файл превышает максимально допустимый размер файла, установленный настройками формы';
$lang['upload_file_partial'] = 'Файл был загружен частично';
$lang['upload_no_temp_directory'] = 'Файл загружен не был - не удалось найти место временного хранилища';
$lang['upload_unable_to_write_file'] = 'Файл не может быть записан на диск';
$lang['upload_stopped_by_extension'] = 'Загрузка была остановлена из-за недопустимого расширение';
$lang['upload_no_file_selected'] = 'Вы не выбрали файл для загрузки';
$lang['upload_invalid_filetype'] = 'Тип файла, который Вы пытаетесь загрузить, недопустим ';
$lang['upload_invalid_filesize'] = 'Файл, который Вы пытаетесь загрузить больше разрешённого объёма';
$lang['upload_invalid_dimensions'] = 'Загружаемое изображение не подходит к установленным настройками ограничениями ширины и высоты';
$lang['upload_destination_error'] = 'Файл не был загружен, так как его не удалось сохранить в каталог пользователя. Проверьте её существование';
$lang['upload_no_filepath'] = 'Путь загрузки некорректен';
$lang['upload_no_file_types'] = 'У файла отстутствует тип. Загрузить его невозможно';
$lang['upload_bad_filename'] = 'Файл, который Вы пытаетесь загрузить, уже имеется на сервере';
$lang['upload_not_writable'] = 'Файл не был загружен, так как его не удалось сохранить в каталог пользователя. Проверьте права';
