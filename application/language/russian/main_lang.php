<?php
/**
 * Переводы
 *
 * @package	CodeIgniter
 * @author	Kudralex
 * @since	Version 1.0.0 (19.08.2015)
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['irida']                                                      = 'Ирида';
$lang['edit']                                                       = 'Редактировать';
$lang['users']                                                      = 'Организация';
$lang['user']                                                       = 'Пользователь';
$lang['boolean_no']                                                 = 'Нет';
$lang['boolean_yes']                                                = 'Да';
$lang['user_type']                                                  = 'Тип';
$lang['user_tags']                                                  = 'Теги';
$lang['user_type_user']                                             = 'Пользователь';
$lang['user_type_admin']                                            = 'Администратор';
$lang['rooms']                                                      = 'Комнаты';
$lang['projects']                                                   = 'Проекты';
$lang['schedule']                                                   = 'Расписание';
$lang['config']                                                     = 'Конфигурирование';
$lang['key']                                                        = 'Параметр';
$lang['value']                                                      = 'Значение';
$lang['alert']                                                      = 'Внимание!';
$lang['seating_scheme']                                             = 'Схема размещения';
$lang['structure']                                                  = 'Структура организации';
$lang['system_log']                                                 = 'Журнал системы';
$lang['posts']                                                      = 'Штатный состав';
$lang['posts_config']                                               = 'Должности';
$lang['profile']                                                    = 'Профиль';
$lang['description_not_found']                                      = 'Описание отсутствует';
$lang['search']                                                     = 'Фильтр...';
$lang['edit_image_old']                                             = 'Оставить прежним';
$lang['edit_image_remove']                                          = 'Удалить';
$lang['edit_image_new']                                             = 'Обновить';
$lang['registr_back_pretitle']                                      = 'Я вспомнил, что у меня уже есть учётная запись!';
$lang['you']                                                        = 'Вы';
$lang['cookie_not_enabled']                                         = 'Внимание! У Вас отключено сохрание куки. Некоторые функции системы могут работать некорректно.';
$lang['welcome']                                                    = 'Добро пожаловать в систему конференцсвязи';
$lang['wait_please_scanning']                                       = 'Подождите, идёт сканирование...';
$lang['weniger_spater']                                             = 'Временно недоступно';
$lang['required']                                                   = 'обязательно';

$lang['date_slash_time']                                            = 'Время/дата';
$lang['today']                                                      = 'Сегодня';
$lang['current_week']                                               = 'Текущая неделя';
$lang['day_mon_long']                                               = 'Понедельник';
$lang['day_tue_long']                                               = 'Вторник';
$lang['day_wed_long']                                               = 'Среда';
$lang['day_thu_long']                                               = 'Четверг';
$lang['day_fri_long']                                               = 'Пятница';
$lang['day_sat_long']                                               = 'Суббота';
$lang['day_sun_long']                                               = 'Воскресенье';
$lang['day_mon_short']                                              = 'Пн';
$lang['day_tue_short']                                              = 'Вт';
$lang['day_wed_short']                                              = 'Ср';
$lang['day_thu_short']                                              = 'Чт';
$lang['day_fri_short']                                              = 'Пт';
$lang['day_sat_short']                                              = 'Сб';
$lang['day_sun_short']                                              = 'Вс';

$lang['month_jan']                                                  = 'Январь';
$lang['month_feb']                                                  = 'Февраль';
$lang['month_mar']                                                  = 'Март';
$lang['month_apr']                                                  = 'Апрель';
$lang['month_may']                                                  = 'Май';
$lang['month_jun']                                                  = 'Июнь';
$lang['month_jul']                                                  = 'Июль';
$lang['month_aug']                                                  = 'Август';
$lang['month_sep']                                                  = 'Сентябрь';
$lang['month_oct']                                                  = 'Октябрь';
$lang['month_nov']                                                  = 'Ноябрь';
$lang['month_dec']                                                  = 'Декабрь';


//Восстановление доступа
$lang['restore_access']                                             = 'Восстановление доступа';
$lang['restore_access_send_link']                                   = 'Отправить ссылку для восстановления доступа';
$lang['restore_access_short']                                       = 'Сбросить пароль';
$lang['restore_access_mail_subject']                                = 'Восстановление доступа к системе';
$lang['restore_access_mail_description']                            = 'Кто-то запросил сброс пароля в системе. Если это были не Вы, то просто проигнорируйте это сообщение. Если это были Вы, перейдите по ссылке ниже';
$lang['restore_access_user_new_pass']                               = 'Введите новый пароль';
$lang['restore_access_password_reset_success']                      = 'Пароль успешно сброшен! Попробуйте войти, указав новые параметры учётной записи';
$lang['restore_error_not_valid_mail']                               = 'Восстановить доступ не удалось, так как был указан некорректный адрес электронной почты';


//Пользователь
$lang['user_profile']                                               = 'Профиль пользователя';
$lang['user_id']                                                    = 'Идентификатор пользователя';
$lang['user_name']                                                  = 'Имя';
$lang['user_name_example']                                          = 'Иван';
$lang['user_reg_date']                                              = 'Дата регистрации';
$lang['user_activity_date']                                         = 'Последняя активность';
$lang['user_activity_dates']                                        = 'Aктивность';
$lang['user_surname']                                               = 'Фамилия';
$lang['user_surname_example']                                       = 'Иванов';
$lang['user_middlename']                                            = 'Отчество';
$lang['user_info']                                                  = 'Информация';
$lang['user_middlename_example']                                    = 'Петрович';
$lang['user_online']                                                = 'В сети';
$lang['user_offline']                                               = 'Не в сети';
$lang['user_ldap']                                                  = 'Учетная запись домена';
$lang['user_contacts']                                              = 'Контакты';
$lang['user_avatar']                                                = 'Изображение пользователя';
$lang['user_avatar_not_found']                                      = 'Изображение не загружено';
$lang['user_state']                                                 = 'Состояние';
$lang['user_comment']                                               = 'Комментарий';
$lang['user_email']                                                 = 'E-mail';
$lang['user_phone']                                                 = 'Телефон';
$lang['user_profile_update_success']                                = 'Информация профиля успешно изменена!';
$lang['user_pass']                                                  = 'Пароль пользователя';
$lang['user_password_changing']                                     = 'Изменение пароля';
$lang['user_current_pass']                                          = 'Текущий пароль';
$lang['user_new_pass']                                              = 'Новый пароль';
$lang['user_current_pass_input']                                    = 'Введите текущий пароль';
$lang['user_new_pass_input']                                        = 'Введите новый пароль';
$lang['user_blocking']                                              = 'Блокировка пользователя';
$lang['user_removing']                                              = 'Удаление пользователя';
$lang['user_editing']                                               = 'Редактирование информации о пользователе';
$lang['user_ldap_adding']                                           = 'Добавление доменного пользователя';
$lang['user_blocking_confirm']                                      = 'Вы уверены, что хотите заблокировать пользователя?';
$lang['user_removing_confirm']                                      = 'Вы уверены, что хотите удалить пользователя?';
$lang['user_profile_blocked_success']                               = 'успешно заблокирован';
$lang['user_profile_unblocked_success']                             = 'успешно разблокирован';
$lang['user_profile_removed_success1']                              = 'Информация о пользователе "';
$lang['user_profile_removed_success2']                              = '" успешно удалёна';
$lang['user_tags_input_help']                                       = 'Введите тег и нажмите "Ввод"';
$lang['user_adding']                                                = 'Добавление пользователя';
$lang['user_default_pass']                                          = 'Сгенерированный пароль';
$lang['user_reset_pass']                                            = 'Сбросить пароль';
$lang['user_password_reseting']                                     = 'Сброс пароля';
$lang['user_pass_reseting_success']                                 = 'Старый пароль успешно сброшен и пользователю назначен новый, который он может сменить самостоятельно';
$lang['user_settings']                                              = 'Настройки уведомлений';
$lang['user_settings_desc']                                         = 'Настройте способы получения уведомлений о событиях в системе, связанных с Вами:';
$lang['user_settings_notifications_mail_full']                      = 'Уведомлять о событиях письмом на электронную почту';
$lang['user_settings_notifications_mail']                           = 'Электронная почта';
$lang['user_settings_notifications_popup_full']                     = 'Уведомлять о событиях с помощью всплывающих окон';
$lang['user_settings_notifications_popup']                          = 'Всплывающие окна';
$lang['user_settings_notifications_xmpp_full']                      = 'Уведомлять о событиях с помощью корпоративного чата';
$lang['user_settings_notifications_xmpp']                           = 'Корпоративный чат';
$lang['user_notifications']                                         = 'Уведомления';
$lang['user_notifications_view']                                    = 'Просмотр уведомлений';
$lang['user_notifications_title']                                   = 'Вам были отправлены следующие уведомления';
$lang['user_notifications_message']                                 = 'Сообщение';
$lang['user_notification_set_checked']                              = 'Отметить как прочитанное';
$lang['user_select_role']                                           = 'У Вас имеются дополнительные привелегии. Укажите роль, с которой Вы будете авторизованы';
$lang['user_default_role']                                          = 'По умолчанию';
$lang['user_superuser_role']                                        = 'Суперадминистратор';
$lang['user_ldap_registration_mail_subject']                        = 'Регистрация в системе ';
$lang['user_ldap_registration_mail_message1']                       = 'Уважаемый(ая) ';
$lang['user_ldap_registration_mail_message2']                       = 'Поздравляем! Вы успешно зарегистрированы в системе организации конференцсвязи!';
$lang['user_ldap_registration_mail_message3']                       = 'Возможно, Вам понадобится настроить браузер. Для этого в адресной строке браузера Firefox введите команду "<b>about:config</b>", и установите новое значение "<b>irida,.unit.spb</b>" для полей "<b>network.negotiate-auth.delegation-uris</b>" и "<b>network.negotiate-auth.trusted-uris</b>"';
$lang['user_ldap_registration_mail_message4']                       = 'Если Вы используете браузер Chromium, то Вам необходимо запустить его из консоли со следующими параметрами: <b>chromium --auth-negotiate-delegate-whitelist                ="irida,*.unit.spb" --auth-server-whitelist                ="irida,*.unit.spb"</b>';
$lang['user_ldap_registration_mail_message5']                       = 'Если при попытке посещения ресурса возникнет ошибка "Internal Server Error", то, скорее всего, Вам необходимо заново получить Kerberos-ключи командой "<b>kinit -f</b>" в консоли.';
$lang['user_avatar_editing']                                        = 'Редактирование изображения пользователя';
$lang['user_avatar_big']                                            = 'Крупное изображение';
$lang['user_avatar_small']                                          = 'Миниатюра';
$lang['user_profile_avatar_update_success']                         = 'Информация об изображении пользователя успешно обновлена';
$lang['user_avatar_webconf']                                        = 'Изображение для вебконференции';
$lang['user_avatar_removing']                                       = 'Удаление изображения пользователя';
$lang['user_avatar_removing_confirm']                               = 'Вы действительно хотите удалить изображение из своего профиля?';
$lang['user_profile_avatar_removed_success']                        = 'Изображение профиля успешно удалено';
$lang['user_profile_avatar_removed_fail']                           = 'Недостаточно прав для удаления изображения пользователя. Обратитесь к администратору';
$lang['user_ldap_all_added_success']                                = 'Все свободные пользователи домена были успешно добавлены';
$lang['user_seating']                                               = 'Размещение';
$lang['user_birthday']                                              = 'Дата рождения';
$lang['user_birthday_today']                                        = 'Сегодня отмечает свой день рождения';
$lang['user_post']                                                  = 'Должность';
$lang['user_company']                                               = 'Организация';
$lang['user_redirect_url']                                          = 'Редирект на комнату при входе';
$lang['user_login']                                                 = 'Логин пользователя';
$lang['user_has_no_redirect']                                       = 'Без редиректа';
$lang['users_system']                                               = 'Пользователи системы';
$lang['user_email_or_login']                                        = 'Адрес электронной почты или логин';
$lang['user_favorite_add']                                          = 'Добавить в список избранных пользователей';
$lang['user_favorite_remove']                                       = 'Удалить из списка избранных пользователей';
$lang['users_all']                                                  = 'Все контакты';
$lang['users_managing']                                             = 'Учётные записи';
$lang['user_favorite_you_status']                                   = 'Для этого пользователя Вы являетесь избранным контактом';

$lang['user_error_updating_login_double']                           = 'Изменить логин или почту пользователя не удалось, так как они повторяли бы логин или почту другого пользователя';
$lang['user_error_not_found']                                       = 'Пользователь не найден';




//Профиль пользователя
$lang['profile_config']                                             = 'Поля учётной записи';
$lang['profile_field_id']                                           = 'Идентификатор поля учётной записи';
$lang['profile_field_adding']                                       = 'Добавление поля учётной записи';
$lang['profile_field_name']                                         = 'Название поля';
$lang['profile_field_status']                                       = 'Статус активности';
$lang['profile_field_description']                                  = 'Комментарий к заполнению поля';
$lang['profile_field_type']                                         = 'Тип данных поля';
$lang['profile_field_type_text']                                    = 'Простой текст';
$lang['profile_field_type_textarea']                                = 'Текстовое поле';
$lang['profile_field_info_type']                                    = 'Тип информации';
$lang['profile_field_info_type_profile']                            = 'Профиль';
$lang['profile_field_info_type_contacts']                           = 'Контакты';
$lang['profile_field_can_admin_change']                             = 'Поле может быть изменено администратором';
$lang['profile_field_is_required']                                  = 'Поле является обязательным';
$lang['profile_field_adding_success']                               = 'Поле учётной записи успешно добавлено в справочник';
$lang['profile_field_admin_changing_status']                        = 'Возможность изменения поля администратором';
$lang['profile_field_requiring']                                    = 'Обязательность';
$lang['profile_field_is_in_use']                                    = 'Поле активно';
$lang['profile_field_is_not_in_use']                                = 'Поле не предлагается для заполнения';
$lang['profile_using_count']                                        = 'Количество пользователей, заполнивших поле';
$lang['profile_field_editing']                                      = 'Изменение параметров поля';
$lang['profile_field_updating_success']                             = 'Параметры поля успешно изменены';
$lang['profile_field_removing']                                     = 'Удаление поля учётной записи';
$lang['profile_field_removing_confirm']                             = 'Вы действительно хотите удалить поле';
$lang['profile_field_removing_success']                             = 'Информация о поле успешно удалена';
$lang['profile_field_mains']                                        = 'Основная информация';
$lang['profile_field_additionals']                                  = 'Дополнительная информация';

$lang['profile_error_updating_upload']                              = 'Обновить информацию профиля не удалось из-за ошибки при загрузке аватара';
$lang['profile_error_updating']                                     = 'Обновить информацию профиля не удалось';
$lang['profile_error_avatar_update']                                = 'Изменить параметры изображения не удалось';

$lang['profile_field_error_not_found']                              = 'Информация о поле учётной записи не найдена';



/*
 * Разное
 */
$lang['dateformat_description']                                     = 'Описание ключей представления даты и времени';
$lang['actions']                                                    = 'Действия';
$lang['organizational_structure']                                   = 'Организационная структура';
$lang['data_not_found']                                             = 'Данные не найдены';
$lang['actions_block']                                              = 'Заблокировать';
$lang['actions_unblock']                                            = 'Разблокировать';
$lang['actions_remove']                                             = 'Удалить';
$lang['actions_archive']                                            = 'Архивировать';
$lang['actions_edit']                                               = 'Редактировать';
$lang['login']                                                      = 'Логин';
$lang['password']                                                   = 'Пароль';
$lang['registration']                                               = 'Регистрация';
$lang['administrating']                                             = 'Администрирование';
$lang['administrating_menage']                                      = 'Управление справочниками';
$lang['sign_in']                                                    = 'Войти';
$lang['main_sign_in_title']                                         = 'Вход в систему';
$lang['main_sign_in_title_short']                                   = 'Вход';
$lang['sign_up']                                                    = 'Зарегистрироваться';
$lang['sign_out']                                                   = 'Выйти';
$lang['change_role']                                                = 'Сменить роль';
$lang['login_incorrect']                                            = 'В системе не найдены пользователи с таким логином';
$lang['login_and_pass_incorrect']                                   = 'Вы указали неверный пароль. Попробуйте повторить попытку';
$lang['login_user_blocked']                                         = 'Пользователь с указанным Вами логином заблокирован. Обратитесь к администратору лично или';
$lang['close']                                                      = 'Закрыть';
$lang['registr_error_doublelogin']                                  = 'Указанный Вами логин уже используется';
$lang['registr_error']                                              = 'Ошибка заполнения формы регистрации';
$lang['registr_self_ok']                                            = 'Регистрация успешно пройдена. Теперь Вы можете войти в систему';
$lang['alert_error']                                                = 'Ошибка!';
$lang['alert_success']                                              = 'Отлично!';
$lang['file_does_not_exceeds_size']                                 = 'Объём файла не должен превышать';
$lang['file_does_have_file_types']                                  = 'Файл должен иметь одно из разрешённых расширений:';
$lang['file_set_empty_if_remove']                                   = 'Оставьте поле пустым, если хотите удалить изображение';
$lang['file_set_empty_if_no_changes']                               = 'Оставьте поле пустым, если не хотите менять изображение';
$lang['mb']                                                         = 'МБ';
$lang['main_sign_up_title']                                         = 'Регистрация новой учётной записи';
$lang['main_subheader']                                             = 'Для бизнеса, маркетинга и обучения.';
$lang['main_header']                                                = 'Средство совместной работы и коллективных коммуникаций';
$lang['user_profile_password_update_fail']                          = 'Не удалось обновить пароль, проверьте корректность введённых данных';
$lang['admin_user_profile_change_fail']                             = 'Обновить информацию о пользователе не удалось, проверьте корректность введённых данных';
$lang['admin_user_profile_change_success']                          = 'Профиль пользователя успешно обновлён';
$lang['user_ldap_added_success']                                    = 'успешно зарегистрирован в системе';
$lang['admin_config_changed']                                       = 'Настройки системы успешно изменены';
$lang['users_contacts']                                             = 'Пользователи';
$lang['conference']                                                 = 'Конференцсвязь';
$lang['seating_menage']                                             = 'Схема размещения';
$lang['signin_error']                                               = 'Ошибка входа';


//Пользователи
$lang['users_btn_view_others']                                      = 'Показать остальных';

$lang['users_not_found']                                            = 'Пользователи не найдены';


/*
 * Теги
 */
$lang['tags_menage']                                                = 'Теги';
$lang['tags_adding']                                                = 'Добавить тег';
$lang['tags_name']                                                  = 'Тег';
$lang['tag_added_success']                                          = 'Тег успешно добавлен';
$lang['tags_add_date']                                              = 'Дата и время добавления';
$lang['tag_editing']                                                = 'Изменение тега';
$lang['tag_updated_success']                                        = 'Информация о теге успешно изменена';
$lang['tag_entity_type_user']                                       = 'Пользователи';
$lang['tag_entity_type']                                            = 'Категория';
$lang['tag_removing']                                               = 'Удаление тега';
$lang['tag_removing_confirm']                                       = 'Вы действительно хотите удалить тег?';
$lang['tag_removed_success']                                        = 'Тег успешно удалён';
$lang['tag_use_count']                                              = 'Активность';

//Комнаты
$lang['room']                                                       = 'Комната';
$lang['room_title']                                                 = 'Комната';
$lang['room_configuration_title']                                   = 'Конфигурация';
$lang['room_desktops_title']                                        = 'Экраны пользователей';
$lang['room_description_title']                                     = 'Описание';
$lang['room_listeners_title']                                       = 'Участники';
$lang['room_behavior_title']                                        = 'Настройки поведения';
$lang['room_virtuals_title']                                        = 'Виртуальные машины';
$lang['room_remote_links_editing']                                  = 'Управление ссылками на экраны пользователей комнаты';
$lang['rooms_list']                                                 = 'Список комнат';
$lang['room_user_right_configuration']                              = 'Участнику разрешено';
$lang['rooms_standing_create']                                      = 'Создание комнаты';
$lang['room_name']                                                  = 'Название комнаты';
$lang['room_cooperation']                                           = 'Взаимодействие';
$lang['room_cooperation_report']                                    = 'Доклад';
$lang['room_remote_links']                                          = 'Экраны пользователей';
$lang['room_goto_room']                                             = 'Перейти к конференции';
$lang['room_cooperation_meeting']                                   = 'Совместное обсуждение';
$lang['room_cooperation_meeting_help']                              = 'Все слышат друг друга. Поведение для Участников: свободное, может настраиваться. Количество Участников до 20 человек.';
$lang['room_cooperation_lesson']                                    = 'Урок';
$lang['room_cooperation_lesson_help']                               = 'Все видят и слышат Ведущего. Для вывода в паблик Участнику необходимо попросить слово. Поведение для Участников: следуют за Ведущим. Количество Cлушателей неограничено.';
$lang['room_cooperation_hangout']                                   = 'Видеосовещание';
$lang['room_cooperation_hangout_help']                              = 'Все участники видят и слышат друг друга. Поведение для Участников: свободное, может настраиваться. Количество участников до 20ти человек.';
$lang['room_cooperation_conference']                                = 'Селекторное совещание';
$lang['room_cooperation_conference_help']                           = 'Одновременно Выступающих до 9ти. Выступающие могут менятся по ходу мероприятия. Поведение для Участников: свободное, может настраиваться. Количество cлушателей неограничено.';
$lang['room_updating_remote_links_fail']                            = 'Не удалось обновить ссылки на экраны пользователей';
$lang['room_updating_remote_links_success']                         = 'Информация о подключениях успешно сохранена';
$lang['room_connection_notinvited_users']                           = 'Приватность';
$lang['room_connection_notinvited_users_allow']                     = 'Разрешить подключение неприглашенных пользователей в режиме трансляции';
$lang['room_connection_files_public_access']                        = 'Разрешить публичный доступ к файлам';
$lang['room_connection_files_public_access_yes']                    = 'Публичный доступ к файлам разрешён';
$lang['room_connection_files_public_access_no']                     = 'Публичный доступ к файлам запрещён';
$lang['room_connection_notinvited_users_yes']                       = 'Подключение неприглашённых пользователей разрешено';
$lang['room_connection_notinvited_users_no']                        = 'Подключение неприглашённых пользователей запрещено';
$lang['room_public_speech_channels_count']                          = 'Допустимое количество каналов для публичных выступлений';
$lang['room_users_experience']                                      = 'Возможности участников';
$lang['room_users_experience_inviting']                             = 'Приглашать других гостей';
$lang['room_users_experience_editing']                              = 'Редактировать параметры комнаты';
$lang['room_configuration']                                         = 'Конфигурация';
$lang['room_config_webcam_translating']                             = 'Трансляция видеопотока с веб-камер участников';
$lang['room_config_interactive_board']                              = 'Отображается панель с блоком "Доска"';
$lang['room_config_messaging']                                      = 'Обмен текстовыми сообщениями';
$lang['room_config_listenerslist_viewing']                          = 'Отображается панель с блоком "Участники"';
$lang['room_config_pointer_using']                                  = 'Использовать указку';
$lang['room_config_random_connection']                              = 'Подключаться к произвольным экранам пользователей';
$lang['room_config_record_channels']                                = 'Запись публичных выступлений участников';
$lang['room_config_masterless_mode']                                = 'Отсутствие главного ведущего, только приватные видеоканалы';
$lang['room_users']                                                 = 'Участники';
$lang['room_user']                                                  = 'Участник';
$lang['room_inviter']                                               = 'Пригласивший';
$lang['room_invite_date']                                           = 'Дата приглашения';
$lang['room_invite_user']                                           = 'Пригласить пользователя в создаваемую комнату';
$lang['room_user_inviting']                                         = 'Приглашение участников в комнату';
$lang['room_user_in_room']                                          = 'Участвует в конференции';
$lang['select_all_users']                                           = 'Выбрать всех пользователей';
$lang['room_user_role']                                             = 'Роль';
$lang['room_user_role_admin']                                       = 'Администратор';
$lang['room_user_role_master']                                      = 'Ведущий';
$lang['room_user_role_secondary_master']                            = 'Ассистент';
$lang['room_user_role_default']                                     = 'Участник';
$lang['room_user_role_onrequest']                                   = 'Вход по запросу';
$lang['room_user_hand_up']                                          = 'Попросить слова';
$lang['room_creating']                                              = 'Параметры создаваемой комнаты';
$lang['room_creating_for_project']                                  = 'Параметры создаваемой комнаты для проекта';
$lang['room_created_success1']                                      = 'Комната';
$lang['room_created_success2']                                      = 'была успешно добавлена в список Ваших комнат';
$lang['room_send_notification_for_all']                             = 'Оповестить всех о приглашении в комнату';
$lang['room_send_notification_for_all_new']                         = 'Оповестить всех вновь добавленных о приглашении в комнату';
$lang['room_standings']                                             = 'Постоянные';
$lang['room_temporarys']                                            = 'Временные';
$lang['room_statistic']                                             = 'Статистика общения в комнате (количество уникальных посетителей в день)';
$lang['room_statistic_balls']                                       = 'Использование комнаты в условных баллах';
$lang['room_info']                                                  = 'Информация о комнате';
$lang['room_create_date']                                           = 'Дата создания';
$lang['room_creator']                                               = 'Владелец комнаты';
$lang['room_creator_short']                                         = 'Владелец';
$lang['room_editing']                                               = 'Редактирование параметров комнаты';
$lang['room_updating_fail']                                         = 'Изменить информацию о комнате не удалось. Проверьте корректность введённых данных';
$lang['room_updating_success']                                      = 'Информация о комнате успешно обновлена';
$lang['room_removing']                                              = 'Удаление комнаты';
$lang['room_removing_confirm']                                      = 'Вы действительно хотите удалить комнату?';
$lang['room_removing_fail']                                         = 'Удалить комнату не удалось. Обратитесь к администратору';
$lang['room_removing_success']                                      = 'Комната успешно удалена';
$lang['room_removing_from_project']                                 = 'Удаление комнаты из проекта';
$lang['room_removing_from_project_confirm']                         = 'Вы действительно хотите удалить комнату из проекта?';
$lang['rooms_not_found']                                            = 'Данные о комнатах не найдены';
$lang['rooms_not_found_temporary']                                  = 'Временные комнаты не найдены';
$lang['rooms_free_not_found']                                       = 'Комнаты вне проектов не найдены';
$lang['room_users_adding']                                          = 'Приглашение участников в комнату';
$lang['room_users_access_levels']                                   = 'Состав участников';
$lang['room_users_access_levels_you']                               = 'Вы';
$lang['room_users_access_levels_remove']                            = 'Удалить пользователя из состава участников комнаты';
$lang['room_users_access_levels_search']                            = 'Поиск новых участников';
$lang['room_project']                                               = 'Проект';
$lang['room_temporary_name']                                        = 'Комната';
$lang['room_files']                                                 = 'Файловое хранилище комнаты';
$lang['room_files_title']                                           = 'Файловое хранилище комнаты';
$lang['room_files_view_title']                                      = 'Просмотр файлового хранилища комнаты';
$lang['room_files_type']                                            = 'Тип файлового хранилища';
$lang['room_files_type_project']                                    = 'Файловое хранилище проекта';
$lang['room_files_type_rooms']                                      = 'Файловое хранилище комнаты';
$lang['room_user_invited_in_fastroom']                              = 'Пользователь добавлен в список участников комнаты';
$lang['room_user_notinvited_in_fastroom']                           = 'Пользователь не добавлен в список участников комнаты';
$lang['room_temporary_adding']                                      = 'Создание комнаты';
$lang['room_temporary_invited_users']                               = 'Участники комнаты';
$lang['room_videos']                                                = 'Записи видеоконференции';
$lang['room_videos_view_title']                                     = 'Записи конференции';
$lang['room_remote_link_title']                                     = 'Ссылка';
$lang['room_remote_link_description']                               = 'Описание';
$lang['room_remote_link_adding']                                    = 'Добавление ссылки на экран пользователя';
$lang['room_config_c_self_secvideoch_maximizing']                   = 'Максимизировать/минимизировать вторичные видеоканалы';
$lang['room_config_c_self_betweenfaces_switching']                  = 'Переключаться между блоками';
$lang['room_config_c_self_docsonboard_publication']                 = 'Открывать документы';
$lang['room_config_c_self_betweendocssites_switching']              = 'Переключаться между страницами документа';
$lang['room_config_c_self_scale_changing']                          = 'Изменять масштаб и перемещать доску';
$lang['room_config_c_self_board_moving']                            = 'Перемещать доску';
$lang['room_config_c_self_minimap_shifting']                        = 'Включать/отключать миникарту';
$lang['room_config_c_self_desktop_connection']                      = 'Автоматическое подключение к демонстрации экрана ведущего';
$lang['room_config_c_self_betweendocs_switching']                   = 'Переключаться между открытыми документами';
$lang['room_config_c_board_listeners_drawing']                      = 'Рисовать на доске';
$lang['room_config_c_share_self_screen']                            = 'Делиться своим экраном';
$lang['room_config_c_self_video_starting']                          = 'Запускать воспроизведение файлов';
$lang['room_config_c_self_documents_view']                          = 'Самостоятельный просмотр документов';
$lang['room_config_c_work_foreign_boards_elements']                 = 'Работать с элементами доски других участников';
$lang['room_config_с_upload_maxfilesize']                           = 'Максимальный объём загружаемого на доску файла (в Мб)';
$lang['room_desk']                                                  = 'Доска комнаты';
$lang['room_remote_desktops']                                       = 'Подключить экраны пользователей';
$lang['room_remote_mediaservers']                                   = 'Подключить хранилище медиа-данных';
$lang['room_remote_cameras']                                        = 'Подключить камеры';
$lang['room_remote_desktops_menage']                                = 'Управление подключениями экранов пользователей';
$lang['room_remote_mediaservers_menage']                            = 'Управление подключениями хранилищ медиа-данных';
$lang['room_remote_cameras_menage']                                 = 'Управление подключениями камер';
$lang['room_chat']                                                  = 'Чат';
$lang['room_chat_send_message']                                     = 'Отправить';
$lang['room_config_changing']                                       = 'Для комнаты была изменена конфигурация';
$lang['room_config_true']                                           = 'Да';
$lang['room_config_false']                                          = 'Нет';
$lang['room_config_cooperation_type_was_changed']                   = 'Для комнаты был изменён режим взаимодействия. В ближайшее время страница будет обновлена';

$lang['room_interface']                                             = 'Интерфейс комнаты по умолчанию';
$lang['room_config_webcam_translating_and_messaging']               = 'Отображается панель с блоками "Видео" и "Чат"';
$lang['room_audio_pubic_channel']                                   = 'Аудио публичного канала';
$lang['room_config_masterless_mode_help']                           = 'При включении параметра блок "Видео" будет отключён';
$lang['room_remote_links_and_services']                             = 'Добавить подключение к оборудованию';
$lang['room_remote_links_and_services_managing']                    = 'Управление подключениями';
$lang['room_mediaservices_editing']                                 = 'Дополнительные сервисы';
$lang['room_users_you_are_master']                                  = 'Вы ведущий, чего ж ещё желать';
$lang['room_user_switched_role']                                    = 'Роль пользователя успешно изменена';
$lang['room_user_notswitched_role']                                 = 'Изменить роль пользователя не удалось';
$lang['room_user_marks_enabled']                                    = 'Теперь Вам видны правки пользователя на доске';
$lang['room_user_marks_disabled']                                   = 'Правки пользователя на доске теперь не видны';
$lang['room_user_boards_marks_disable']                             = 'Скрыть правки на доске';
$lang['room_user_boards_marks_enable']                              = 'Отображать правки на доске';
$lang['room_user_send_private_message']                             = 'Приватное сообщение';
$lang['room_user_change_in_listener']                               = 'Перевести в участники';
$lang['room_user_change_in_publisher']                              = 'Вывести в публичный канал';
$lang['room_user_change_in_master']                                 = 'Сделать ведущим';
$lang['room_user_change_from_master']                               = 'Отобрать права ведущего';
$lang['room_user_publish_request_pending_title']                    = 'Запрос на вывод в публичный канал';
$lang['room_user_you_was_unpublished']                              = 'Вы были отключены от публичного канала';
$lang['room_user_you_have_rejected_from_publish']                   = 'Вам было отказано в получении доступа к публичному каналу';
$lang['room_user_publish_request_pending']                          = 'попросил вывести его в публичный канал. Согласиться?';
$lang['room_user_sending_offer_success']                            = 'Пользователю отправлено приглашение на участие в публичном канале';
$lang['room_user_sending_reject_success']                           = 'Пользователю отправлен отказ на заявку участия в публичном канале';
$lang['room_user_sending_reject_refused']                           = 'Пользователь отказался принимать заявку на участие в публичном канале';
$lang['room_user_sending_request_refused']                          = 'Пользователь отменил подачу заявки на участие в публичном канале';
$lang['room_room_info_not_defined']                                 = 'Параметры комнаты не определены';
$lang['room_user_publish_offer']                                    = 'Приглашение к участию в публичном канале';
$lang['room_user_publish_offer_from_master']                        = 'Ведущий пригласил Вас поучаствовать в публичном канале. Вы подтверждаете своё участие?';
$lang['room_user_cancel']                                           = 'Отмена';
$lang['room_user_confirm']                                          = 'Согласиться';
$lang['room_user_current']                                          = 'Текущая комната пользователя';
$lang['room_camera_not_defined']                                    = 'Веб-камера отсутствует';
$lang['room_local_camera_publication']                              = 'Публикация...';
$lang['room_video_not_accessible']                                  = 'Видео недоступно';
$lang['room_user_you_have_message']                                 = 'Новое сообщение...';
$lang['rooms_with_files_public_access']                             = 'Комнаты с публичным доступом к файлам';
$lang['rooms_with_archived_status']                                 = 'Архив комнат';
$lang['room_docs_colloborationg']                                   = 'Совместное редактирование документа';
$lang['room_user_return_master']                                    = 'Вернуть права ведущего владельцу комнаты';
$lang['room_users_search']                                          = 'Поиск пользователей среди участников';
$lang['room_users_search_here']                                     = 'Начните ввод имени пользователя, его логин или теги...';
$lang['room_users_online_count']                                    = 'Количество пользователей онлайн';
$lang['room_users_online_count_with_guests']                        = 'Количество пользователей онлайн + количество гостей';
$lang['room_users_online_now']                                      = 'Количество пользователей онлайн на данный момент';
$lang['room_users_online_now_list']                                 = 'На данный момент в комнате находятся:';
$lang['room_key_help']                                              = 'Инструкция по установке сертификата безопасности';
$lang['room_key_download_certificate']                              = 'Скачать сертификат';
$lang['room_key_download_chrome_screensharing_extension']           = 'Скачать расширение для Chrome';
$lang['room_download_media_title']                                  = 'Выгрузка медиа-файла';
$lang['room_download_media_cancel']                                 = 'Отмена';
$lang['room_download_media_waiting']                                = 'Подождите, идёт подготовка медиа-данных к выгрузке';
$lang['room_favorite_add']                                          = 'Добавить в закладки';
$lang['room_favorite_remove']                                       = 'Убрать из закладок';
$lang['rooms_favorites']                                            = 'Избранные комнаты';
$lang['rooms_for_me']                                               = 'Комнаты с моим участием';
$lang['room_archiving']                                             = 'Архивация комнаты';
$lang['room_archiving_confirm']                                     = 'Вы действительно хотите перенести комнату в архив? Подключение к комнате будет недоступным, но Вы сможете просмотреть файловое хранилище и историю записей.';
$lang['room_chat_input_here']                                       = 'Начните вводить сообщение...';
$lang['room_sharing_help']                                          = 'Инструкция по настройке браузера для демонстрации экрана';
$lang['room_message_game_selected_authors']                         = 'Игра с автором выделенных сообщений';
$lang['room_message_delete_selected']                               = 'Удалить выбранные сообщения';
$lang['room_message_delete_all_in_thread']                          = 'Удалить текущий чат';
$lang['room_message_ban_selected_authors']                          = 'Заблокировать авторов выделенных сообщений';
$lang['room_message_ban_user']                                      = 'Заблокировать возможность отправки сообщений пользователем';
$lang['room_message_unban_user']                                    = 'Разблокировать возможность отправки сообщений пользователем';
$lang['room_message_user_was_forbidded']                            = 'Возможность отправки сообщений была заблокирована для пользователя';
$lang['room_message_you_are_forbidded']                             = 'Возможность отправки Вами сообщений заблокирована';
$lang['room_chat_input_blocked']                                    = 'Отправка заблокирована';
$lang['room_message_you_are_allowed']                               = 'Возможность отправки Вами сообщений разблокирована';
$lang['room_message_user_was_allowed']                              = 'Возможность отправки сообщений была разблокирована для пользователя';
$lang['room_message_answer_to_selected']                            = 'Приватный диалог с автором выделенных сообщений';
$lang['room_game_you_win']                                          = 'Вы выиграли';
$lang['room_game_you_loose']                                        = 'Вы проиграли';
$lang['room_game_your_step']                                        = 'Ваш ход';
$lang['room_game_waiting_for_step']                                 = 'Ожидание хода';
$lang['room_game_standoff']                                         = 'Ничья';
$lang['room_translation_title']                                     = 'Трансляция видеоконференции';
$lang['room_connect_over_id']                                       = 'Подключение к комнате по её идентификатору';
$lang['room_id']                                                    = 'Идентификатор комнаты';
$lang['rooms_view_all']                                             = 'Посмотреть все комнаты';
$lang['room_connect_over_users']                                    = 'Видеозвонок';
$lang['room_connect_over_users_description']                        = 'Выберите пользователей, которых Вы хотите пригласить к видеосовещанию';
$lang['rooms_populated_not_found']                                  = 'Собрать статистику по популярным комнатам не удалось... Начните пользоваться системой с организации видеозвонка';
$lang['room_streaming_view_title']                                  = 'Прямой эфир';
$lang['room_streaming_wating']                                      = 'На данный момент трансляция комнаты не ведётся... Ожидайте';
$lang['room_streaming']                                             = 'Прямой эфир';
$lang['rooms_for_translations']                                     = 'Комнаты с публичными трансляциями';
$lang['room_board']                                                 = 'Доска';
$lang['room_desktops']                                              = 'Рабочие столы';
$lang['room_files_managing']                                        = 'Файлы';
$lang['room_message_quote']                                         = 'Цитировать';
$lang['room_polls_manager']                                         = 'Центр управления тестами комнаты';
$lang['room_polls_not_found']                                       = 'Информация об тестах в текущей комнате не найдена';
$lang['room_polls_adding_title']                                    = 'Создание теста';
$lang['room_polls_title']                                           = 'Текст вопроса';
$lang['room_polls_description']                                     = 'Описание вопроса';
$lang['room_polls_respondents_count']                               = 'Количество результатов';
$lang['room_polls_playing']                                         = 'Опрос для участников комнаты';
$lang['room_polls_for_listeners_description']                       = 'Ведущий запустил тест для участников комнаты. Пожалуйста, ответьте на представленные ниже вопросы';
$lang['room_polls_view_results']                                    = 'Показать результаты респондентов';


$lang['room_btn_video']                                             = 'Видео';
$lang['room_btn_desk']                                              = 'Доска';
$lang['room_btn_desktops']                                          = 'Удалённые рабочие столы';
$lang['room_btn_files']                                             = 'Файлы';
$lang['room_btn_unpublish']                                         = 'Прекратить вещание';
$lang['room_btn_publish']                                           = 'Возобновить вещание';
$lang['room_btn_publish_waiting_cancel']                            = 'Отменить запрос слова';
$lang['room_btn_publish_waiting_cancel_short']                      = 'Отменить запрос';
$lang['room_btn_mute_toggle']                                       = 'Выключить/включить микрофон';
$lang['room_btn_add_room_in_project']                               = 'Добавить существующую комнату';
$lang['room_btn_record_all']                                        = 'Записать эфир';
$lang['room_btn_record_stop']                                       = 'Остановить запись эфира';
$lang['room_btn_archived_view']                                     = 'Показать архив';
$lang['room_btn_remove']                                            = 'Удалить комнату';
$lang['room_btn_archive']                                           = 'Перенести комнату в архив';
$lang['room_btn_dearchive']                                         = 'Перенести комнату из архива в список активных комнат';
$lang['room_btn_archived_unview']                                   = 'Скрыть архив';
$lang['room_btn_connect']                                           = 'Подключиться';
$lang['room_btn_listeners_manager']                                 = 'Управление участниками комнаты';
$lang['room_btn_create']                                            = 'Создать';
$lang['room_btn_streaming']                                         = 'Прямой эфир';
$lang['room_btn_mute_toggle_mute']                                  = 'Микрофон включен. Нажмите, чтобы отключить';
$lang['room_btn_mute_toggle_unmute']                                = 'Микрофон выключен. Нажмите, чтобы включить';
$lang['room_btn_polls_managing']                                    = 'Управление тестами в комнате';
$lang['room_btn_poll_refresh']                                      = 'Обновить';
$lang['room_btn_poll_add']                                          = 'Создать тест';
$lang['room_btn_poll_add_confirm']                                  = 'Создать';
$lang['room_btn_poll_remove']                                       = 'Удалить тест';
$lang['room_btn_poll_start']                                        = 'Запустить тест';
$lang['room_btn_poll_stop']                                         = 'Остановить тест';
$lang['room_btn_reload_page_now']                                   = 'Повторить сейчас';

//Уведомления
$lang['room_notification_uninvite_subject']                         = 'Удаление из списка участников комнаты';
$lang['room_notification_uninvite_body']                            = 'Вы были удалены из списка участников комнаты ';

//Ошибки
$lang['room_error_cant_connect_to_svg_server']                      = 'Не удалось подключиться к серверу хранения изображения с доски. Обратитесь к администратору системы';
$lang['room_error_cant_find_room_info']                             = 'Не удалось найти информацию о комнате. Проверьте корректность запроса';
$lang['room_error_cant_connect_to_media_server']                    = 'Не удалось подключиться к медиа-серверу. Обратитесь к администратору системы';
$lang['room_error_cant_get_svg_images']                             = 'Не удалось получить данные от сервера хранения изображений с доски. Попробуйте повторить попытку позже';
$lang['room_error_interface']                                       = 'Ошибка в параметрах комнаты';
$lang['room_error_interface_desc']                                  = 'Обратите внимание, хотя бы один из блоков интерфейса должен быть по умолчанию отображён';
$lang['room_error_access_denied']                                   = 'Доступ к информации об этой комнате Вам запрещён. Обратитесь к организатору комнаты для получения прав';
$lang['room_error_access_id_not_valid']                             = 'Идентификатор комнаты некорректен';
$lang['room_error_remove_user']                                     = 'Удалить пользователя из списка участников комнаты не удалось';
$lang['room_error_room_not_found']                                  = 'Комната с указанным идентификатором не найдена';
$lang['room_error_user_not_found_in_room']                          = 'Пользователя с указанным идентификатором не существует в комнате';
$lang['room_error_change_users_role']                               = 'Изменить роль пользователя в комнате не удалось';
$lang['room_error_add_user_in_room']                                = 'Добавить пользователя в комнату не удалось';
$lang['room_error_user_was_found_in_room']                          = 'Пользователь с указанным идентификатором уже имеется в списке участников';
$lang['room_error_background_updating']                             = 'Обновить изображение комнаты не удалось';
$lang['room_error_you_have_not_rights_for_creating']                = 'Комната ещё не проинициализирована её владельцем, а у Вас не хватает для этого прав';
$lang['room_error_docs_file_not_found']                             = 'Запрашиваемый файл не найден, проверьте корректность ссылки';
$lang['room_error_max_publishers_limit_reached']                    = 'Достигнут предел количества медиа-потоков пользователей, находящихся в публичном канале';
$lang['room_error_you_are_listener']                                = 'Вы не авторизованы! Некоторые функции комнаты Вам недоступны';
$lang['room_error_download_file']                                   = 'Скачать файл не удалось, повторите попытку позже';
$lang['room_error_has_been_destroyed']                              = 'Комната была удалена!';
$lang['room_error_token_not_valid']                                 = 'Билет недействителен. Обратитесь к администратору';
$lang['room_error_get_statistic']                                   = 'Получить статистику по посещению комнаты не удалось';
$lang['room_error_janus_host_unreachable']                          = 'Не удалось подключиться к сервису организации конференцсвязи. Пожалуйста, повторите попытку позже.';
$lang['room_error_creating']                                        = 'Создать комнату не удалось! Убедитесь в корректности введённых данных';
$lang['room_error_get_polls']                                       = 'Не удалось получить информацию об тестах для текущей комнаты';
$lang['room_error_create_polls_required_fields']                    = 'Опрос не может быть создан, так как некоторые обязательные поля не заполнены';

//Предупреждения
$lang['room_warning_teacher_is_not_in_room']                        = 'Ведущий отсутствует или не определён.<br>Ожидайте...';
$lang['room_warning_teacher_paused_in_room']                        = 'Ведущий приостановил вещание.<br>Ожидайте...';
$lang['room_warning_site_reload']                                   = 'Страница автоматически будет обновлена через';
$lang['room_warning_site_reload_sec']                               = 'секунд';

//События
$lang['room_event_remove_from_project']                             = 'Пользователь удалил из проекта комнату';
$lang['room_event_user_login']                                      = 'Пользователь вошёл в комнату';
$lang['room_event_archiving']                                       = 'Пользователь архивировал комнату';
$lang['room_event_dearchiving']                                     = 'Пользователь деархивировал комнату';
$lang['room_event_token_created']                                   = 'Пользователь создал токен для публичного доступа к комнате';
$lang['room_event_token_removed']                                   = 'Пользователь удалил токен для публичного доступа к комнате';
$lang['room_event_user_docs_editing']                               = 'Пользователь перешёл к совместному редактированию документа';
$lang['room_event_poll_added']                                      = 'Пользователь добавил в комнату тест';

//Успехи
$lang['room_success_removing_from_project']                         = 'Комната успешно удалена из проекта';
$lang['room_success_archiving']                                     = 'Комната успешно архивирована!';
$lang['room_success_dearchiving']                                   = 'Комната успешно перемещена из архива в список активных комнат!';



        
/*
 * Проекты
 */
$lang['project']                                                    = 'Проект';
$lang['project_adding']                                             = 'Добавление проекта';
$lang['project_name']                                               = 'Название проекта';
$lang['project_description']                                        = 'Описание';
$lang['project_rooms']                                              = 'Комнаты';
$lang['project_creating']                                           = 'Создание проекта';
$lang['project_creating_fail']                                      = 'Не удалось создать проект. Проверьте корректность введённых данных';
$lang['project_creating_success']                                   = 'Информация о проекте успешно добавлена';
$lang['project_creating_properties']                                = 'Параматры создаваемого проекта';
$lang['projects_not_found']                                         = 'Проекты не найдены';
$lang['project_rooms_count']                                        = 'Количество комнат';
$lang['project_add_date']                                           = 'Дата создания';
$lang['project_editing']                                            = 'Изменение данных о проекте';
$lang['project_updating_fail']                                      = 'Изменить информацию о проекте не удалось';
$lang['project_updating_success']                                   = 'Информация о проекте успешно изменена';
$lang['project_removing']                                           = 'Удаление проекта';
$lang['project_removing_confirm']                                   = 'Вы действительно хотите удалить проект?';
$lang['project_removing_fail']                                      = 'Удалить проект не удалось';
$lang['project_removing_success']                                   = 'Проект успешно удалён';
$lang['project_view_one']                                           = 'Просмотр файлового хранилища проекта';
$lang['project_access_check_fail']                                  = 'Вам запрещён доступ к запрашиваемому проекту. Обратитесь к администратору';
$lang['project_exist_check_fail']                                   = 'Файловое хранилище не найдено. Обратитесь к администратору';
$lang['project_access_check_mkdir_fail']                            = 'Невозможно создать каталог проекта. Обратитесь к администратору';
$lang['project_author']                                             = 'Автор проекта';
$lang['project_rooms']                                              = 'Комнаты проекта';
$lang['project_rooms_long']                                         = 'Информация о комнатах проекта';
$lang['project_users']                                              = 'Участники';
$lang['project_files']                                              = 'Файловое хранилище проекта';
$lang['project_rooms_users_adding']                                 = 'Добавление комнат в проект';
$lang['project_free_rooms']                                         = 'Доступные для добавления комнаты';
$lang['projects_remove_room_from_project']                          = 'Удалить комнату из проекта';
$lang['project_users_count']                                        = 'Количество участников проекта';
$lang['project_public_status']                                      = 'Доступ к проекту';
$lang['project_public_status_true']                                 = 'Для всех пользователей';
$lang['project_public_status_true_help']                            = 'Доступ к проекту неограничен и он появляется у всех пользователей в списке проектов';
$lang['project_public_status_false']                                = 'Для выбранных пользователей';
$lang['project_public_status_false_help']                           = 'Проект является приватным, а доступ ограничен заданным количеством участников';
$lang['projects_private']                                           = 'Приватные проекты';
$lang['projects_public']                                            = 'Публичные проекты';
$lang['project_my_role']                                            = 'Моя роль';
$lang['project_role_master_short']                                  = 'Владелец';
$lang['project_role_creator_short']                                 = 'Владелец';
$lang['project_role_participant_short']                             = 'Участник';
$lang['project_users_access_levels_you']                            = 'Вы';
$lang['project_users_access_levels_remove']                         = 'Удалить пользователя из состава участников проекта';
$lang['project_participants_inviting']                              = 'Участники проекта';

$lang['project_notification_invite_subject']                        = 'Приглашение к участию в работе над проектом';
$lang['project_notification_invite_body']                           = 'пригласил Вас к совместной работе над проектом';
$lang['project_notification_uninvite_subject']                      = 'Удаление из списка участников проекта';
$lang['project_notification_uninvite_body']                         = 'Вы были отстранены от участия в работе над проектом';
$lang['project_notification_published_subject']                     = 'Изменение статуса проекта';
$lang['project_notification_published_true_body1']                  = 'У проекта';
$lang['project_notification_published_true_body2']                  = 'изменился статус. Теперь он является публичным и доступен всем пользователям системы. Обратите внимание, что данные, хранящиеся в проекты, стали публичными.';
$lang['project_notification_published_false_body1']                 = 'У проекта';
$lang['project_notification_published_false_body2']                 = 'изменился статус. Теперь он является приватным и доступен участникам комнат, которые входили в состав проекта.';
$lang['project_notification_room_in_project_subject1']              = 'Комната';
$lang['project_notification_room_in_project_subject2']              = 'теперь часть проекта';
$lang['project_notification_room_in_project_body1']                 = 'Комната';
$lang['project_notification_room_in_project_body2']                 = 'теперь является частью проекта';
$lang['project_notification_room_in_project_body3']                 = 'Вы, как участник этой комнаты, приглашены к совместной работе над проектом.';
$lang['project_notification_room_invite_subject1']                  = 'Приглашение в комнату';
$lang['project_notification_room_invite_subject2']                  = 'и проект';
$lang['project_notification_room_invite_body1']                     = 'Вы были приглашены в комнату';
$lang['project_notification_room_invite_body2']                     = 'Так как комната является частью проекта';
$lang['project_notification_room_invite_body3']                     = 'Вы автоматически стали участником совместной работы над ним';

$lang['project_btn_invite_user']                                    = 'Управление участниками проекта';

$lang['projects_error_id_not_valid']                                = 'Указан неверный идентификатор проекта';
$lang['projects_error_not_found']                                   = 'Проект с указанным идентификатором не найден';
$lang['project_error_add_user_in_project']                          = 'Добавить участника в проект не удалось';
$lang['project_error_user_was_found_in_project']                    = 'Выбранный пользователь уже имеется в списке участников';
$lang['project_error_user_was_not_found_in_project']                = 'Выбранного пользователя нет в списке участников проекта';
$lang['project_error_change_users_role']                            = 'Изменить роль участника проекта не удалось';
$lang['project_error_remove_participant']                           = 'Удалить участника из проекта не удалось';

$lang['projects_success_rooms_added']                               = 'Комнаты успешно добавлены в проект';



/*
 * Журнал
 */
$lang['log_removing']                                               = 'Очистка журнала';
$lang['log_removing_confirm']                                       = 'Вы действительно хотите очистить журнал?';
$lang['log_removing_success']                                       = 'Все записи журнала успешно удалены';
$lang['log_selected_removing_success']                              = 'Выбранные записи журнала успешно удалены';
$lang['log_selected_removing_fail']                                 = 'Записи, указанные для удаления, не найдены';
$lang['log_filter_start']                                           = 'Начало диапазона';
$lang['log_filter_end']                                             = 'Окончание диапазона';

        
/*
 * Расписание
 */
$lang['schedule_event_adding']                                      = 'Добавление мероприятия';
$lang['schedule_event_editing']                                     = 'Изменение параметров мероприятия';
$lang['schedule_event_name']                                        = 'Название мероприятия';
$lang['schedule_event_date']                                        = 'Дата и время первого мероприятия';
$lang['schedule_event_all_day']                                     = 'Весь день';
$lang['schedule_event_repeat_type']                                 = 'Повторять';
$lang['schedule_event_repeat_type_mode']                            = 'Режим повторения';
$lang['schedule_event_repeat_type_title']                           = 'Тип повтора';
$lang['schedule_event_repeat_type_no']                              = 'Не повторять';
$lang['schedule_event_repeat_type_daily']                           = 'Ежедневно';
$lang['schedule_event_repeat_type_weekly']                          = 'Еженедельно';
$lang['schedule_event_repeat_type_monthly']                         = 'Ежемесячно';
$lang['schedule_event_repeat_type_annually']                        = 'Ежегодно';
$lang['schedule_event_repeat_days']                                 = 'Дни повтора';
$lang['schedule_event_room']                                        = 'Комната мероприятия';
$lang['schedule_event_comment']                                     = 'Описание мероприятия';
$lang['schedule_event_time']                                        = 'Время мероприятия';
$lang['schedule_event_creator']                                     = 'Автор';
$lang['schedule_event_creating_fail']                               = 'Создать мероприятие не удалось. Проверьте корректность введённых данных';
$lang['schedule_event_creating_success']                            = 'Мероприятие успешно добавлено';
$lang['schedule_event_info']                                        = 'Информация о мероприятии';
$lang['schedule_event_first_date']                                  = 'Дата и время первого мероприятия';
$lang['schedule_event_updating_fail']                               = 'Изменить информацию о мероприятии не удалось. Проверьте корректность введённых данных';
$lang['schedule_event_updating_success']                            = 'Информация о мероприятии успешно изменена';
$lang['schedule_event_removing_one']                                = 'Отмена мероприятия';
$lang['schedule_event_removing_one_confirm']                        = 'Вы действительно хотите отменить мероприятие';
$lang['schedule_event_planned_on']                                  = 'запланированное на';
$lang['schedule_event_remove_one_fail']                             = 'Отменить мероприятие на указанный день не удалось. Проверьте корректность введённых данных';
$lang['schedule_event_removing_one_success']                        = 'Информация об отмене мероприятия успешно сохранена';
$lang['schedule_event_removing_all']                                = 'Удаление мероприятия';
$lang['schedule_event_removing_all_confirm']                        = 'Вы действительно хотите удалить весь цикл мероприятий';
$lang['schedule_view_week']                                         = 'Посмотреть расписание на неделю';
$lang['schedule_event_canceled']                                    = 'Мероприятие отменено';
$lang['schedule_event_duration']                                    = 'Продолжительность ограничена:';
$lang['schedule_event_duration_minutes']                            = 'минут';

//Уведомления
$lang['schedule_notification_event_planned_subject']                = 'Напоминание об участии в мероприятии';
$lang['schedule_notification_event_planned_body']                   = 'Напомнимаем Вам о том, что вы принимаете участие в мероприятии!';

$lang['schedule_btn_event_remove_all']                              = 'Удалить';
$lang['schedule_btn_event_cancel']                                  = 'Отменить конкретное мероприятие';
$lang['schedule_btn_notification_send']                             = 'Отправить участникам уведомление о мероприятии';

$lang['schedule_log_removing']                                      = 'Пользователь удалил весь цикл мероприятий';

$lang['schedule_success_removing_all']                              = 'Мероприятие успешно удалено';
$lang['schedule_success_notifications_was_send']                    = 'Уведомления успешно отправлены';

$lang['schedule_error_remove_all']                                  = 'Удалить мероприятие не удалось';
$lang['schedule_error_event_not_found']                             = 'Мероприятие с указанным идентификатором не найдено';




/*
 * Должности
 */
$lang['post_adding']                                                = 'Добавление должности';
$lang['post_name']                                                  = 'Название должности';
$lang['post_description']                                           = 'Описание должности';
$lang['post_added_success']                                         = 'Должность успешно добавлена';
$lang['post_use_count']                                             = 'Количество использований';
$lang['post_editing']                                               = 'Изменение должности';
$lang['post_updated_success']                                       = 'Информация о должности успешно обновлена';
$lang['post_removing']                                              = 'Удаление должности';
$lang['post_removing_confirm']                                      = 'Вы действительно хотите удалить должность?';
$lang['post_removed_success']                                       = 'Должность успешно удалена';
$lang['post_company_text']                                          = 'Текстовое представление';
$lang['post_company_chart']                                         = 'Графическое представление';
$lang['post_structure_name']                                        = 'Название подразделения';
$lang['post_structure_post']                                        = 'Должность подразделения';
$lang['post_user']                                                  = 'Пользователь';
$lang['post_description']                                           = 'Описание выполняемых функций';
$lang['post_posts_for_item_not_found']                              = 'Должности подразделения не найдены';
$lang['post_structure_adding']                                      = 'Назначение пользователя на должность';
$lang['post_structure_editing']                                     = 'Изменение пользователя для должности';
$lang['post_structure_added_success']                               = 'Пользователь успешно назначен на должность';
$lang['post_structure_removed_success']                             = 'Пользователь успешно освобождён от занимаемой должности';
$lang['post_structure_updated_success']                             = 'Информация о выполняемых функциях пользователя на должности успешно обновлена';
$lang['post_structure_user_removing']                               = 'Освобождение от занимаемой должности';
$lang['post_structure_user_removing_confirm']                       = 'Вы действительно хотите освободить пользователя от занимаемой должности?';
$lang['post_structure_post_free_vacancy']                           = 'Вакантных мест на должность';
$lang['post_users_room_inviting']                                   = 'Приглашение пользователей в комнату';
$lang['post_users_count']                                           = 'Максимальное количество пользователей';
$lang['post_parent_name']                                           = 'Название родительской должности';


//Структура
$lang['structure_text']                                             = 'Текстовое представление';
$lang['structure_chart']                                            = 'Графическое представление';
$lang['structure_select_item_for_editing']                          = 'Выберите подразделение для редактирования';
$lang['structure_select_item_for_viewing_users']                    = 'Выберите подразделение для просмотра списка сотрудников';
$lang['structure_create_root_item']                                 = 'Создайте корневое подразделение';
$lang['structure_items_not_found']                                  = 'Информация о подразделениях не найдена';
$lang['structure_adding']                                           = 'Добавление подразделения';
$lang['structure_name']                                             = 'Название подразделения';
$lang['structure_description']                                      = 'Описание подразделения';
$lang['structure_parent_name']                                      = 'Подразделение';
$lang['structure_item_added_success']                               = 'Подразделение успешно добавлено';
$lang['structure_subitems_count']                                   = 'Количество подчинённых подразделений';
$lang['structure_posts_count']                                      = 'Количество должностей подразделения';
$lang['structure_posts']                                            = 'Должности';
$lang['structure_post_adding']                                      = 'Добавление должности в подразделение';
$lang['structure_added_posts']                                      = 'Добавленные в подразделение должности';
$lang['structure_post_description']                                 = 'Описание должности в рамках подразделения';
$lang['structure_post_removing']                                    = 'Удаление должности из подразделения';
$lang['structure_post_removing_confirm']                            = 'Вы действительно хотите исключить должность из подразделения?';
$lang['structure_post_editing']                                     = 'Изменение параметров должности подразделения';
$lang['structure_editing']                                          = 'Изменение данных о подразделении';
$lang['structure_removing']                                         = 'Удаление подразделения';
$lang['structure_removing_confirm']                                 = 'Вы действительно хотитет удалить подразделение?';
$lang['structure_removed_success']                                  = 'Подразделение успешно удалено';
$lang['structure_users_invite_count']                               = 'Количество приглашённых сотрудников подразделения';
$lang['structure_tree_expand']                                      = 'Развернуть';
$lang['structure_tree_collapse']                                    = 'Свернуть';
$lang['structure_post_users_count']                                 = 'Максимальное количество пользователей, занимающих должность';
$lang['structure_items']                                            = 'Подразделения';
$lang['structure_items_list_sorting']                               = 'Для упорядочивания структурных подразделений перетаскивайте их, устанавливая необходимый порядок';
$lang['structure_items_list_sorting_success']                       = 'Изменения сортировки в дереве подразделений будут доступны после обновления страницы';
$lang['structure_parent_item']                                      = 'Название родительского подразделения';


//Схема размещения
$lang['seating_equipment']                                          = 'Оборудование';


//Оборудование
$lang['equipment_adding']                                           = 'Добавление оборудования';
$lang['equipment_id']                                               = 'Идентификатор оборудования';
$lang['equipment_ip']                                               = 'IP-адрес';
$lang['equipment_type']                                             = 'Тип оборудования';
$lang['equipment_type_arm']                                         = 'Рабочее место';
$lang['equipment_type_arms']                                        = 'Рабочие места';
$lang['equipment_type_media']                                       = 'Медиасервер';
$lang['equipment_type_cam']                                         = 'Камера';
$lang['equipment_type_cams']                                        = 'Камеры';
$lang['equipment_description']                                      = 'Описание';
$lang['equipment_added_success']                                    = 'Оборудование успешно добавлено';
$lang['equipment_audience']                                         = 'Аудитория';
$lang['equipment_place']                                            = 'Использование';
$lang['equipment_activity']                                         = 'Активность';
$lang['equipment_in_use']                                           = 'Оборудование используется';
$lang['equipment_not_in_use']                                       = 'Оборудование не используется';
$lang['equipment_editing']                                          = 'Изменение параметров оборудования';
$lang['equipment_updated_success']                                  = 'Информация об оборудовании успешно обновлена';
$lang['equipment_hostname']                                         = 'Хост или URL';
$lang['equipment_hostname_desc']                                    = 'Хост или URL для доступа к оборудованию';
$lang['equipment_removing']                                         = 'Удаление оборудования';
$lang['equipment_removing_confirm']                                 = 'Вы действительно хотите удалить оборудование?';
$lang['equipment_removed_success']                                  = 'Устройство успешно удалено';
$lang['equipment_object_coord_type']                                = 'Тип объекта';
$lang['equipment_object_id']                                        = 'Идентификатор объекта';
$lang['equipment_object_top']                                       = 'Отступ сверху';
$lang['equipment_object_left']                                      = 'Отступ слева';
$lang['equipment_cam_rotate_left']                                  = 'Повернуть против часовой стрелки';
$lang['equipment_cam_rotate_right']                                 = 'Повернуть по часовой стрелке';
$lang['equipment_object_rotation_angle']                            = 'Угол поворота устройства';


//Метрика
$lang['metrika']                                                    = 'Метрика';
$lang['metrika_name']                                               = 'Название операции';
$lang['metrika_removing']                                           = 'Удаление метрики';
$lang['metrika_removing_confirm']                                   = 'Вы действительно хотите удалить все метрики?';
$lang['metrika_removing_success']                                   = 'Метрики успешно удалены';
$lang['metrika_proc']                                               = 'Количество переходов, %';
$lang['metrika_main']                                               = 'Главная страница входа';
$lang['metrika_rooms']                                              = 'Доступ к комнатам';
$lang['metrika_profile']                                            = 'Просмотр профиля';
$lang['metrika_projects']                                           = 'Доступ к проектам';
$lang['metrika_schedule']                                           = 'Доступ к мероприятиям';
$lang['metrika_seatingl']                                           = 'Просмотр схемы размещения';
$lang['metrika_structurel']                                         = 'Просмотр организационной структуры';
$lang['metrika_usersl']                                             = 'Просмотр списка пользователей';
$lang['metrika_controllers_allocation']                             = 'Распределение по контроллерам';
$lang['metrika_days_allocation']                                    = 'Распределение по дням';
$lang['metrika_months_allocation']                                  = 'Распределение по месяцам';
$lang['metrika_dates_allocation']                                   = 'Распределение по датам';
$lang['metrika_polls']                                              = 'Просмотр тестов';
$lang['metrika_notes']                                              = 'Просмотр объявлений';
$lang['metrika_im']                                                 = 'Сообщения';


//Здание
$lang['building_buildings_list']                                    = 'Список зданий';
$lang['building_adding']                                            = 'Добавление здания';
$lang['building_id']                                                = 'Идентификатор здания';
$lang['building_name']                                              = 'Название здания';
$lang['building_address']                                           = 'Адрес';
$lang['building_description']                                       = 'Описание';
$lang['building_info']                                              = 'Информация о здании';
$lang['building_buildings_not_found']                               = 'Информация о зданиях не найдена';
$lang['building_address_not_found']                                 = 'Адрес не указан';
$lang['building_updating']                                          = 'Изменение информации о здании';
$lang['building_edit_image']                                        = 'Действия с изображением здания';
$lang['building_added_success']                                     = 'Информация о здании успешно добавлена';
$lang['building_updated_success']                                   = 'Информация о здании успешно обновлена';
$lang['building_removing']                                          = 'Удаление здания';
$lang['building_removing_confirm']                                  = 'Вы действительно хотите удалить здание?';
$lang['building_removed_success']                                   = 'Информация о здании успешно удалена';
$lang['building_not_found']                                         = 'Здание с указанным идентификатором не найдено';
$lang['building_one_building_view_title']                           = 'План здания';
$lang['building_floor_low']                                         = 'Этаж №';
$lang['building_map']                                               = 'Карта здания';
$lang['building_floor_updating']                                    = 'Изменение параметров этажа';
$lang['building_floor_name']                                        = 'Название этажа';
$lang['building_floor_name_set_empty_desc']                         = 'Оставьте поле пустым, если планируется использовать нумерацию';
$lang['building_floor_update_success']                              = 'Информация об этаже успешно обновлена';
$lang['building_item_update_fileupload_fail']                       = 'Не удалось загрузить фоновое изображение, проверьте параметры файла';
$lang['building_floor_adding']                                      = 'Добавление этажа';
$lang['building_floor_adding_type']                                 = 'Расположение добавляемого этажа';
$lang['building_floor_adding_type_after']                           = 'Выше текущего';
$lang['building_floor_adding_type_before']                          = 'Ниже текущего';
$lang['building_floor_adding_success']                              = 'Информация об этаже успешно добавлена';
$lang['building_floor_removing']                                    = 'Удаление информации об этаже';
$lang['building_floor_removing_confirm']                            = 'Вы уверены, что хотите удалить информацию об этаже?';
$lang['building_floor_removing_success']                            = 'Информация об этаже успешно удалена';
$lang['building_flat_adding']                                       = 'Добавление помещения';
$lang['building_floor_background_removing']                         = 'Удаление фонового изображения';
$lang['building_floor_background_removing_confirm']                 = 'Вы действительно хотите удалить фоновое изображение?';
$lang['building_flat_name']                                         = 'Название помещения';
$lang['building_flat_description']                                  = 'Описание помещения';
$lang['building_flat_adding_success']                               = 'Информация о помещении успешно добавлена';
$lang['building_background']                                        = 'Подложка';
$lang['building_background_current_use']                            = 'Использовать подложку текущего расположения';
$lang['building_equipment_adding']                                  = 'Добавление оборудования';
$lang['building_equipment_operation']                               = 'Операция по удалению/добавлению оборудования';
$lang['building_flat_removing']                                     = 'Удаление помещения';
$lang['building_flat_removing_confirm']                             = 'Вы действительно хотите удалить помещение';
$lang['building_flat']                                              = 'Помещение';
$lang['building_flat_updating_success']                             = 'Информация о помещении успешно обновлена';
$lang['building_background_updating']                               = 'Изменение подложки';
$lang['building_background_file']                                   = 'Файл с параметрами подложки';
$lang['building_flat_removing_success']                             = 'Информация о помещении успешно удалена';
$lang['building_background_updating_success']                       = 'Параметры подложки успешно обновлены';
$lang['building_background_image_not_found']                        = 'Изображение этажа не найдено';
$lang['building_user_on_arm_info_not_found']                        = 'Информация о пользователе на рабочем месте не найдена';
$lang['building_image']                                             = 'Изображение здания';
$lang['building_seating_image']                                     = 'Фоновое изображение';
$lang['building_background_image_settings']                         = 'Параметры фонового изображения';
$lang['building_background_image_scale']                            = 'Масштаб фонового изображения';
$lang['building_background_image_scale_sh']                         = 'Масштаб, %';
$lang['building_background_image_opacity']                          = 'Прозрачность фонового изображения';
$lang['building_background_image_opacity_sh']                       = 'Прозрачность, %';
$lang['building_background_image_offset_y']                         = 'Отступ фонового изображения сверху';
$lang['building_background_image_offset_y_sh']                      = 'Отступ сверху, пикс.';
$lang['building_background_image_offset_x']                         = 'Отступ фонового изображения слева';
$lang['building_background_image_offset_x_sh']                      = 'Отступ слева, пикс.';


//Опросы
$lang['polls']                                                      = 'Тестовые наборы';
$lang['polls_notcompleted']                                         = 'Доступные тесты';
$lang['polls_admin_title']                                          = 'Модерация тестов';
$lang['polls_completed']                                            = 'Пройденные тесты';
$lang['polls_my']                                                   = 'Мои тесты';
$lang['polls_of_type_not_found']                                    = 'Опросы данного типа не найдены';
$lang['polls_quests']                                               = 'Вопросы теста';
$lang['polls_one_title']                                            = 'Опрос';
$lang['polls_results']                                              = 'Результаты теста';
$lang['poll_adding']                                                = 'Добавление теста';
$lang['poll_title']                                                 = 'Название теста';
$lang['poll_add_date']                                              = 'Дата создания';
$lang['poll_quests_count']                                          = 'Количество вопросов';
$lang['poll_results_count']                                         = 'Количество результатов';
$lang['poll_public_status']                                         = 'Статус публикации';
$lang['poll_public_status_ok']                                      = 'Опрос опубликован';
$lang['poll_public_status_false']                                   = 'Опрос не опубликован';
$lang['poll_public_status_description']                             = '(для публикации теста необходимо, чтобы он содержал хотя бы один вопрос)';
$lang['poll_not_moderated']                                         = 'Опрос после публикации должен пройти процедуру модерации администратором';
$lang['poll_description']                                           = 'Описание теста';
$lang['poll_access']                                                = 'Доступ к тесту';
$lang['poll_access_public']                                         = 'Публичный';
$lang['poll_access_public_description']                             = '(потребуется модерация теста администратором перед публикацией)';
$lang['poll_access_persons']                                        = 'Персональный';
$lang['poll_users']                                                 = 'Участники теста';
$lang['poll_anon_status']                                           = 'Тип теста по анонимности';
$lang['poll_anon_status_description']                               = 'Тип теста по отображению результатов: открытый (пользователи будут видеть имена респондентов) или анонимный';
$lang['poll_anon_false']                                            = 'Ответы персонифицированы';
$lang['poll_anon_true']                                             = 'Анонимный';
$lang['poll_creating_fail']                                         = 'Создать тест не удалось. Проверьте корректность введённых данных';
$lang['poll_creating_success']                                      = 'Опрос успешно создан! Добавьте в него вопросы и опубликуйте';
$lang['poll_play']                                                  = 'Приступить к тесту';
$lang['poll_results']                                               = 'Результаты теста';
$lang['poll_invited_users']                                         = 'Целевые респонденты теста';
$lang['poll_editing']                                               = 'Изменение параметров теста';
$lang['poll_updating_fail']                                         = 'Изменить параметры теста не удалось. Проверьте корректность введённых данных';
$lang['poll_updating_success']                                      = 'Параметры теста успешно изменены!';
$lang['poll_quests']                                                = 'Вопросы теста';
$lang['poll_archiving']                                             = 'Архивирование теста';
$lang['poll_archiving_confirm1']                                    = 'Вы уверены, что хотите архивировать тест "';
$lang['poll_archiving_confirm2']                                    = '"? Результаты теста будут доступны, но прохождение теста будет невозможно.';
$lang['poll_archiving_fail']                                        = 'Архивировать тест не удалось';
$lang['poll_archiving_success']                                     = 'Опрос успешно архивирован';
$lang['poll_removing']                                              = 'Удаление теста';
$lang['poll_removing_confirm1']                                     = 'Вы уверены, что хотите удалить, а не архивировать тест "';
$lang['poll_removing_confirm2']                                     = '"? Вся информация об тесте и его результаты будут удалены!';
$lang['poll_removing_fail']                                         = 'Удалить тест не удалось';
$lang['poll_removing_success']                                      = 'Опрос успешно удалён!';
$lang['poll_not_found']                                             = 'Запрашиваемый тест не найден';
$lang['poll_access_check_fail']                                     = 'У Вас не хватает прав доступа к запрашиваемому тесту!';
$lang['poll_quest_order']                                           = 'Порядок вопросов';
$lang['poll_quest_title']                                           = 'Текст вопроса';
$lang['poll_quest_description']                                     = 'Описание (расшифровка) вопроса';
$lang['poll_quest_type']                                            = 'Тип вопроса';
$lang['poll_quest_type_selectone']                                  = 'Выбор одного';
$lang['poll_quest_type_selectmany']                                 = 'Выбор нескольких';
$lang['poll_quest_type_selectscale']                                = 'Шкала';
$lang['poll_quest_required_status']                                 = 'Обязательность ответа на вопрос';
$lang['poll_quest_required_false']                                  = 'Ответ необязателен';
$lang['poll_quest_required_true']                                   = 'Ответ обязателен';
$lang['poll_quests_not_found']                                      = 'Вопросы теста не найдены';
$lang['poll_quest_active_status']                                   = 'Активность';
$lang['poll_quest_adding']                                          = 'Добавление вопроса';
$lang['poll_quest_updating']                                        = 'Изменение параметров вопроса';
$lang['poll_quest_answer_var']                                      = 'Вариант ответа';
$lang['poll_quest_scale_begin']                                     = 'Текст в начале шкалы';
$lang['poll_quest_scale_end']                                       = 'Текст в конце шкалы';
$lang['poll_quest_scale_count']                                     = 'Количество делений шкалы';
$lang['poll_quest_adding_success']                                  = 'Вопрос успешно добавлен в тест';
$lang['poll_quest_active_true']                                     = 'Вопрос активен и используется в тесте';
$lang['poll_quest_active_false']                                    = 'Вопрос деактивирован';
$lang['poll_quest_removing']                                        = 'Удаление вопроса';
$lang['poll_quest_removing_confirm1']                               = 'Вы уверены, что хотите удалить вопрос "';
$lang['poll_quest_removing_confirm2']                               = '"?';
$lang['poll_quest_adding_fail']                                     = 'Добавить информацию о вопросе не удалось. Убедитесь, что все введённые данные были корректны';
$lang['poll_quest_removing_fail']                                   = 'Удалить информацию о вопросе не удалось.';
$lang['poll_quest_removing_success']                                = 'Вопрос успешно удалён из теста';
$lang['poll_quest_updating_fail']                                   = 'Изменить параметры вопроса не удалось.';
$lang['poll_quest_updating_success']                                = 'Параметры вопроса успешно изменены!';
$lang['poll_finished_success']                                      = 'Благодарим Вас за принятие участия в тесте! Теперь Вам доступны результаты';
$lang['poll_result']                                                = 'Результаты теста';
$lang['poll_quest_respondents_count']                               = 'Количество результатов';
$lang['poll_quest_respondents']                                     = 'Респонденты';
$lang['poll_quest_vars']                                            = 'Варианты ответов';
$lang['poll_repeating']                                             = 'Повторное прохождение теста';
$lang['poll_repeat_confirm1']                                       = 'Опрос "';
$lang['poll_repeat_confirm2']                                       = '" Вы уже проходили. Вы уверены, что хотите повторить? Предыдущие результаты будут удалены';
$lang['poll_active_list']                                           = 'Активные тесты';
$lang['poll_archive_list']                                          = 'Архив';
$lang['polls_public_moderating']                                    = 'Модерация публичных тестов';
$lang['poll_author']                                                = 'Автор теста';
$lang['poll_moderator_name']                                        = 'Имя модератора';
$lang['poll_moderate_ok']                                           = 'Одобрить проведение публичного теста';
$lang['poll_moderate_block']                                        = 'Заблокировать проведение теста';
$lang['poll_moderate_success']                                      = 'Статус публичного теста успешно изменён';
$lang['poll_user_has_notfinished_polls']                            = 'Для Вас имеются незавершённые тесты';
$lang['polls_not_found']                                            = 'Опросы не найдены';
$lang['polls_playing']                                              = 'Прохождение теста';
$lang['poll_starting_success']                                      = 'Опрос успешно запущен и в ближайшее время будет предложен для прохождения участникам комнаты';
$lang['poll_stopping_success']                                      = 'Доступ к тесту участникам комнаты приостановлен';
$lang['poll_theme_name']											= 'Название темы';
$lang['poll_quest_answer']											= 'Ответ';

$lang['poll_error_not_found']                                       = 'Опрос с указанным идентификатором не найден';
$lang['poll_error_get_poll']                                        = 'Не удалось получить информацию об тесте';

$lang['poll_event_started']                                         = 'В комнате был запущен тест';


/*
 * Кнопки
 */
$lang['btn_save']                                                   = 'Сохранить';
$lang['btn_confirm']                                                = 'Подтвердить';
$lang['btn_cancel']                                                 = 'Отмена';
$lang['btn_change_password']                                        = 'Изменить пароль';
$lang['btn_add_user']                                               = 'Добавить пользователя';
$lang['btn_remove']                                                 = 'Удалить';
$lang['btn_add']                                                    = 'Добавить';
$lang['btn_add_tag']                                                = 'Добавить тег';
$lang['btn_add_room']                                               = 'Создать комнату';
$lang['btn_add_temporary_room']                                     = 'Создать комнату';
$lang['btn_add_standing_room']                                      = 'Создать комнату';
$lang['btn_add_project']                                            = 'Добавить проект';
$lang['btn_add_users_in_room']                                      = 'Пригласить участников';
$lang['btn_invite']                                                 = 'Пригласить участников';
$lang['btn_end_inviting']                                           = 'Завершить приглашение пользователей';
$lang['btn_close']                                                  = 'Закрыть';
$lang['btn_clear_log']                                              = 'Очистить весь журнал';
$lang['btn_add_event']                                              = 'Добавить мероприятие';
$lang['btn_add_field']                                              = 'Добавить поле';
$lang['btn_actions']                                                = 'Действия';
$lang['btn_event_remove_this']                                      = 'Отменить мероприятие в этот день или удалить всю цепочку мероприятий';
$lang['btn_event_remove_all']                                       = 'Удалить всю цепочку мероприятий';
$lang['btn_event_edit_this']                                        = 'Изменить параметры мероприятия';
$lang['btn_event_reschedule_this']                                  = 'Перенести мероприятие';
$lang['btn_add_post']                                               = 'Добавить должность';
$lang['btn_add_structure_root']                                     = 'Добавить корневое подразделение';
$lang['btn_add_structure_subitem']                                  = 'Добавить подразделение';
$lang['btn_add_structure_post']                                     = 'Добавить должность';
$lang['btn_invite_in_temporary_room']                               = 'Пригласить отмеченных в комнату';
$lang['btn_clear_metrika']                                          = 'Очистить метрики';
$lang['btn_add_user_to_post']                                       = 'Назначить пользователя на должность';
$lang['btn_remove_user_from_post']                                  = 'Освободить пользователя от должности';
$lang['btn_add_equipment']                                          = 'Добавить оборудование';
$lang['btn_add_building']                                           = 'Добавить здание';
$lang['btn_add_floor']                                              = 'Добавить этаж';
$lang['btn_add_flat']                                               = 'Добавить помещение';
$lang['btn_add_many_flats']                                         = 'Добавить несколько помещений';
$lang['btn_remove_floor']                                           = 'Удалить этаж';
$lang['btn_add_floor_before']                                       = 'Добавить этаж ниже текущего';
$lang['btn_add_floor_after']                                        = 'Добавить этаж выше текущего';
$lang['btn_edit_floor']                                             = 'Редактировать параметры этажа';
$lang['btn_edit_equipment']                                         = 'Справочник оборудования';
$lang['btn_save_canvas']                                            = 'Сохранить подложку';
$lang['btn_remove_flat']                                            = 'Удалить помещение';
$lang['btn_edit_flat']                                              = 'Изменить параметры помещения';
$lang['btn_upload']                                                 = 'Загрузить';
$lang['btn_room_invited_set_title']                                 = 'Организация комнаты из выбранных участников';
$lang['btn_add_temporary_room']                                     = 'Создать комнату';
$lang['btn_clear_notifications']                                    = 'Удалить информацию об уведомлениях';
$lang['btn_back']                                                   = 'Назад';
$lang['btn_on_main']                                                = 'На главную';
$lang['btn_restore_pass']                                           = 'Обновить пароль';
$lang['btn_search']                                                 = 'Искать!';
$lang['btn_remove_background_image']                                = 'Удалить фоновое изображение';
$lang['btn_edit_avatar']                                            = 'Редактировать изображение пользователя';
$lang['btn_remove_avatar']                                          = 'Удалить изображение';
$lang['btn_add_poll']                                               = 'Добавить тест';
$lang['btn_archive']                                                = 'Архивировать';
$lang['btn_add_poll_quest']                                         = 'Добавить вопрос';
$lang['btn_add_poll_quest_var']                                     = 'Добавить вариант ответа';
$lang['btn_poll_finish']                                            = 'Завершить тест';
$lang['btn_add_all_user']                                           = 'Добавить всех пользователей';
$lang['btn_next']                                                   = 'Продолжить';
$lang['btn_filter_apply']                                           = 'Применить фильтр';
$lang['btn_filter_clear']                                           = 'Сбросить фильтр';
$lang['btn_filter_clear_selected']                                  = 'Удалить выбранные элементы';
$lang['btn_add_room_remote_link']                                   = 'Добавить ссылки на экраны пользователей';
$lang['btn_add_note']                                               = 'Добавить объявление';
$lang['btn_create']                                                 = 'Создать';
$lang['btn_to_main']                                                = 'На главную';
$lang['btn_send']                                                   = 'Отправить';


//События
$lang['event_user']                                                 = 'Администрирование. Пользователи';
$lang['event_date']                                                 = 'Дата и время';
$lang['event_type']                                                 = 'Тип';
$lang['event_description']                                          = 'Описание';
$lang['event_main']                                                 = 'Авторизация';
$lang['event_tags']                                                 = 'Администрирование. Теги';
$lang['event_title']                                                = 'Событие';

$lang['event_rooms']                                                = 'Управление комнатой';
$lang['event_projects']                                             = 'Управление проектом';
$lang['event_schedule']                                             = 'Управление мероприятиями';
$lang['event_polls']                                                = 'Управление тестами';
$lang['event_posts']                                                = 'Управление должностями';
$lang['event_equipment']                                            = 'Управление оборудованием';
$lang['event_structure']                                            = 'Управление организационной диаграммой';
$lang['event_notes']                                                = 'Управление объявлениями';
$lang['event_files']												= 'Управление файлами';
$lang['event_sked']													= 'Управление справочником тем';


$lang['event_profile']                                              = 'Профиль пользователя';
$lang['event_deauth']                                               = 'Намеренный выход из системы';
$lang['event_signin']                                               = 'Вход в систему';
$lang['event_signup']                                               = 'Регистрация в системе';
$lang['event_profile_edit']                                         = 'Пользователь обновил основную информацию профиля';
$lang['event_admin_profile_edit']                                   = 'Администратор обновил профиль пользователя:';
$lang['event_admin_type_edit']                                      = 'Администратор изменил тип пользователя:';
$lang['event_profile_contacts_edit']                                = 'Пользователь обновил контактную информацию профиля';
$lang['event_profile_password_update']                              = 'Пользователь изменил пароль';
$lang['event_profile_password_restore']                             = 'Пользователь сбросил пароль и восстановил доступ';
$lang['event_users_block']                                          = 'Администратором был заблокирован пользователь:';
$lang['event_users_unblock']                                        = 'Администратором был разблокирован пользователь:';
$lang['event_admin_user_remove']                                    = 'Администратором был удалён пользователь:';
$lang['event_admin_ldap_user_add']                                  = 'Администратором был добавлен доменный пользователь';
$lang['event_admin_simple_user_add']                                = 'Администратором был добавлен пользователь';
$lang['event_admin_configuration_changed']                          = 'Администратор изменил базовые настройки системы';
$lang['event_admin_tag_add']                                        = 'Администратор добавил новый тег';
$lang['event_admin_tag_remove']                                     = 'Администратор удалил тег';
$lang['event_admin_tag_update']                                     = 'Администратор изменил тег';
$lang['event_user_tag_add']                                         = 'Администратор при редактировании пользователя добавил тег';
$lang['event_user_tag_to_user_add']                                 = 'Администратор назначил пользователю тег';
$lang['event_room_create']                                          = 'Пользователь добавил комнату';
$lang['event_room_create_temporary']                                = 'Пользователь добавил комнату';
$lang['event_room_edit']                                            = 'Пользователь изменил параметры комнаты';
$lang['event_room_remove']                                          = 'Пользователь удалил комнату';
$lang['event_room_autoremove']                                      = 'Комната автоматически удалена';
$lang['event_project_create']                                       = 'Пользователь добавил проект';
$lang['event_project_update']                                       = 'Пользователь обновил данные проекта';
$lang['event_schedule_event_create']                                = 'Пользователь добавил мероприятие';
$lang['event_schedule_event_update']                                = 'Пользователь изменил информацию о мероприятии';
$lang['event_schedule_exclusion_adding']                            = 'Пользователь отменил мероприятие';
$lang['event_admin_post_add']                                       = 'Администратор добавил должность';
$lang['event_admin_post_update']                                    = 'Администратор обновил информацию о должности';
$lang['event_admin_post_remove']                                    = 'Администратор удалил должность';
$lang['event_admin_structure_item_add']                             = 'Администратор добавил подразделение';
$lang['event_admin_post_in_structure_add']                          = 'Администратор добавил должность для подразделения';
$lang['event_admin_post_from_structure_remove']                     = 'Администратор исключил должность из подразделения';
$lang['event_admin_post_from_structure_update']                     = 'Администратор обновил параметры должности в подразделении';
$lang['event_admin_structure_item_updated']                         = 'Администратор изменил данные подразделения';
$lang['event_admin_structure_item_remove']                          = 'Администратор удалил подразделение';
$lang['event_admin_user_to_post_add']                               = 'Администратор назначил новую должность для пользователя';
$lang['event_admin_user_to_post_remove']                            = 'освобождён администратором от должности';
$lang['event_admin_user_to_post_update']                            = 'Администратор обновил свойства пользователя на должности';
$lang['event_admin_equipment_add']                                  = 'Администратор добавил оборудование';
$lang['event_admin_equipment_update']                               = 'Администратор обновил информацию об оборудовании';
$lang['event_admin_userpass_reseting']                              = 'Администратор сбросил пароль пользователю';
$lang['event_admin_equipment_remove']                               = 'Администратор удалил устройство';
$lang['event_admin_building_add']                                   = 'Администратор добавил информацию о здании';
$lang['event_admin_building_update']                                = 'Администратор обновил информацию о здании';
$lang['event_admin_building_remove']                                = 'Администратор удалил информацию о здании';
$lang['event_admin_floor_add']                                      = 'Администратор добавил этаж в здание';
$lang['event_admin_building_floor_update']                          = 'Администратор обновил информацию об этаже';
$lang['event_admin_floor_remove']                                   = 'Администратор удалил информацию об этаже';
$lang['event_admin_flat_add']                                       = 'Администратор добавил помещение в здание';
$lang['event_admin_flat_update']                                    = 'Администратор изменил информацию о помещении';
$lang['event_admin_flat_remove']                                    = 'Администратор удалил информацию о помещении';
$lang['event_admin_profile_field_add']                              = 'Администратор добавил поле учётной записи';
$lang['event_admin_profile_field_update']                           = 'Администратор изменил параметры поля';
$lang['event_admin_profile_field_remove']                           = 'Администратор удалил информацию о поле учётной записи';
$lang['event_poll_create']                                          = 'Пользователь добавил тест';
$lang['event_poll_update']                                          = 'Пользователь обновил параметры теста';
$lang['event_poll_archive']                                         = 'Пользователь архивировал тест';
$lang['event_poll_quest_added']                                     = 'Пользователь добавил вопрос в тест';
$lang['event_poll_quest_removed']                                   = 'Пользователь удалил вопрос из теста';
$lang['event_poll_quest_updated']                                   = 'Пользователь изменил параметры вопроса';
$lang['event_poll_started']                                         = 'Пользователь начал прохождение теста';
$lang['event_poll_finished']                                        = 'Пользователь завершил прохождение теста';
$lang['event_poll_moderate']                                        = 'Администратор подтвердил проведение публичного теста';
$lang['event_poll_unmoderate']                                      = 'Администратор заблокировал проведение публичного теста';
$lang['event_room_remote_links_edit']                               = 'Пользователь обновил данные о ссылках на экраны пользователей';


//Ошибки
$lang['error_system']                                               = 'Ошибка';
$lang['error_system_authorization_required']                        = 'Ошибка авторизации. Попробуйте выполнить команду "kinit" и ввести пароль';
$lang['error_form_any']                                             = 'Форма была заполнена некорректно';
$lang['error_ldap_use']                                             = 'Ошибка взаимодействия по протоколу LDAP';
$lang['error_ldap_module_not_loaded']                               = 'Не найден модуль LDAP';
$lang['error_ldap_server_errors']                                   = 'Ошибка взаимодействия с сервером LDAP';
$lang['error_admin_user_ldap_add_fail']                             = 'Не удалось добавить доменного пользователя. Проверьте корректность введённых данных';
$lang['error_login_ldap_user_blocked']                              = 'Ваша учётная запись заблокирована. Обратитесь к администратору';
$lang['error_login_ldap_user_not_registred']                        = 'Ваша учётная запись не зарегистрирована в системе. Обратитесь к администратору';
$lang['error_restore_pass_mail_send']                               = 'На указанный Вами электронный адрес отправлена ссылка для сброса пароля';
$lang['error_restore_pass_mail_not_found']                          = 'Указанному Вами электронному адресу не соответствует ни одна учётная запись';
$lang['error_restore_hash_incorrect']                               = 'Полученная ссылка для обновления пароля некорректна. Попробуйте запросить восстановление пароля ещё раз';
$lang['error_restore_pass_failed']                                  = 'Не удалось сбросить пароль:';
$lang['error_building_item_not_found']                              = 'Информация об элементе схемы размещения не найдена';
$lang['error_admin_user_simple_add_fail']                           = 'Не удалось добавить пользователя. Проверьте корректность введённых данных';
$lang['error_admin_tag_create_fail']                                = 'Не удалось добавить тег. Проверьте корректность введённых данных';
$lang['error_admin_tag_update_fail']                                = 'Не удалось изменить информацию о теге. Проверьте корректность введённых данных';
$lang['error_admin_tag_removing_fail']                              = 'Не удалось удалить тег';
$lang['error_admin_post_create_fail']                               = 'Не удалось добавить информацию о должности. Проверьте корректность введённых данных';
$lang['error_admin_post_update_fail']                               = 'Не удалось обновить информацию о должности. Проверьте корректность введённых данных';
$lang['error_admin_post_removing_fail']                             = 'Не удалось удалить должность';
$lang['error_admin_structure_create_fail']                          = 'Не удалось создать подразделение. Проверьте корректность введённых данных';
$lang['error_admin_struct_item_removing_fail']                      = 'Не удалось удалить подразделение. Проверьтет корректность введённых данных';
$lang['error_admin_post_structure_adding_fail']                     = 'Назначить пользователя на должность не удалось. Проверьте корректность введённых данных';
$lang['error_admin_post_structure_editing_fail']                    = 'Изменить описание пользователя на должности не удалось. Проверьте корректность введённых данных';
$lang['error_admin_post_structure_removing_fail']                   = 'Освободить пользователя от занимаемой должности не удалось. Проверьте корректность введённых данных';
$lang['error_admin_equipment_create_fail']                          = 'Не удалось добавить оборудование';
$lang['error_admin_equipment_editing_fail']                         = 'Изменить информацию об оборудовании не удалось';
$lang['error_admin_userpass_reseting_fail']                         = 'Сбросить пароль пользователя не удалось';
$lang['error_admin_equipment_removing_fail']                        = 'Не удалось удалить информацию об устройстве';
$lang['error_admin_building_adding_fail']                           = 'Не удалось добавить информацию о здании';
$lang['error_admin_building_adding_file_fail']                      = 'Не удалось добавить информацию о здании. Ошибка загрузки файла-изображения';
$lang['error_admin_building_adding_back_fail']                      = 'Не удалось добавить информацию о подложке. Ошибка загрузки файла-изображения';
$lang['error_admin_building_updating_fail']                         = 'Не удалось обновить информацию о здании. Проверьте корректность введённых данных';
$lang['error_admin_building_updating_file_fail']                    = 'Не удалось обновить информацию о здании. Ошибка загрузки файла-изображения';
$lang['error_admin_building_removing_fail']                         = 'Не удалось удалить информацию о здании';
$lang['error_admin_building_not_found']                             = 'Информация о здании не найдена';
$lang['error_admin_building_floor_not_found']                       = 'Информация об этаже не найдена';
$lang['error_admin_building_floor_update_fail']                     = 'Не удалось обновить информацию об этаже. Проверьте корректность введённых данных';
$lang['error_admin_building_floor_add_fail']                        = 'Не удалось добавить информацию об этаже. Проверьте корректность введённых данных';
$lang['error_admin_building_flat_add_fail']                         = 'Не удалось добавить помещение. Проверьте корректность введённых данных';
$lang['error_admin_building_flat_not_found']                        = 'Информация о помещении не найдена';
$lang['error_admin_building_flat_updating_fail']                    = 'Не удалось обновить информацию о помещении. Проверьте корректность введённых данных';
$lang['error_admin_building_item_not_found']                        = 'Информация об элементе схемы размещения не найдена';
$lang['error_admin_back_not_json']                                  = 'Информация о подложке не была обновлена. Вам необходимо выбрать файл с расширением JSON';
$lang['error_admin_profile_field_adding_fail']                      = 'Информацию о поле профиля добавить не удалось: ';
$lang['error_admin_profile_field_updating_fail']                    = 'Не удалось обновить информацию о поле учетной записи: ';
$lang['error_admin_profile_field_removing_fail']                    = 'Не удалось удалить информацию о поле: ';
$lang['error_admin_structure_create_double_name_fail']              = 'Не удалось добавить или обновить информацию о подразделении, так как подразделение с таким именем уже существует в этой организации';


//Ошибки AJAX
$lang['error_ajax_no_roomid']                                       = 'Не указан идентификатор комнаты. Обратитесь к администратору';
$lang['error_ajax_no_projectid']                                    = 'Не указан идентификатор проекта. Обратитесь к администратору';
$lang['error_ajax_content_search']                                  = 'Ошибка контекстного поиска. Обратитесь к администратору';
$lang['error_ajax_userid_not_exist']                                = 'Не указан идентификатор пользователя. Обратитесь к администратору';
$lang['error_ajax_no_eventid']                                      = 'Не указан идентификатор мероприятия. Обратитесь к администратору';
$lang['error_ajax_no_login']                                        = 'Не указан логин пользователя. Обратитесь к администратору';
$lang['error_ajax_no_itemid']                                       = 'Не указан идентификатор структурного подразделения. Обратитесь к администратору';
$lang['error_ajax_no_projectid']                                    = 'Не указан идентификатор проекта';
$lang['error_ajax_add_structure_post']                              = 'Не удалось сохранить должность в подразделении. Проверьте корректность введённых данных';
$lang['error_ajax_remove_structure_post']                           = 'Не удалось исключить должность из подразделения. Обратитесь к администратору';
$lang['error_ajax_update_structure_item']                           = 'Не удалось обновить данные о структурном подразделении. Проверьте корректность введённых данных';
$lang['error_ajax_save_coords']                                     = 'Не удалось сохранить координаты объекта';
$lang['error_ajax_get_equipments']                                  = 'Не удалось получить информацию об оборудовании';
$lang['error_ajax_update_building_equipments']                      = 'Не удалось совершить операцию с оборудованием';
$lang['error_ajax_update_equipments_rotation']                      = 'Не удалось обновить информацию об угле поворота оборудования';
$lang['error_ajax_download_canvas']                                 = 'Не удалось найти параметры подложки';
$lang['error_ajax_user_invite_queue_failed']                        = 'Операция по изменения статуса пользователя в очереди на создание комнаты не удалась';
$lang['error_ajax_user_not_found']                                  = 'Информация о пользователе не найдена';
$lang['error_ajax_room_not_found']                                  = 'Информация о комнате не найдена';
$lang['error_ajax_cant_remove_standing_room']                       = 'Удаление постоянной комнаты невозможно';
$lang['error_ajax_search_users']                                    = 'Не удалось получить список пользователей';
$lang['error_ajax_update_notifications']                            = 'Не удалось обновить информацию уведомления';
$lang['error_ajax_update_background']                               = 'Не удалось обновить информацию о фоновом изображении';
$lang['error_ajax_save_poll_answer']                                = 'Не удалось сохранить ответ';
$lang['error_ajax_get_project_tags']                                = 'Не удалось получить информацию о тегах';
$lang['error_ajax_set_project_tags']                                = 'Не удалось добавить информацию о тегах';
$lang['error_ajax_project_not_found']                               = 'Проект не найден';
$lang['error_ajax_user_not_authorized']                             = 'Вы не прошли аутентификацию, необходим вход в систему';


//Внимание
$lang['alert_cant_create_schedule_event']                           = 'Вы не можете добавить мероприятие, так как у Вас нет личных комнат';
$lang['alert_not_accepted_users']                                   = 'В системе имеются свободные от должностей пользователи';


//Конфигурация
$lang['config_title']                                               = 'Название системы';
$lang['config_section_main']                                        = 'Основный настройки';
$lang['config_section_mail']                                        = 'Настройки почты';
$lang['config_section_xmpp']                                        = 'Настройки XMPP';
$lang['config_section_components']                                  = 'Компоненты системы';
$lang['config_title_short']                                         = 'Краткое название системы';
$lang['config_audio_server_url']                                    = 'Адрес сервера аудиоконференций';
$lang['config_datetime_format']                                     = 'Формат представления даты и времени';
$lang['config_allow_registration']                                  = 'Самостоятельная регистрация (при отсутствии доменной аутентификации)';
$lang['config_allow_tag_adding']                                    = 'Возможность добавления новых тегов при работе с категориями';
$lang['config_components_posts']                                    = 'Компонент для организации штатного состава';
$lang['config_components_messages']                                 = 'Компонент для организации обмена сообщениями';
$lang['config_components_seating']                                  = 'Компонент для организации схемы размещения';
$lang['config_rec_server']                                          = 'Адрес хранилища записанных во время конференции медиа-данных (например, http://kurento/catalogue/api/irida/getfiles)';
$lang['config_additional_fields_in_structure']                      = 'Ввод описания выполняемых функций сотрудника';
$lang['config_main_mail']                                           = 'Адрес электронной почты администратора системы';
$lang['config_main_mail_name']                                      = 'Имя, от которого будет отправляться электронная почта';
$lang['config_mail_smtp_host']                                      = 'Имя сервера исходящей почты';
$lang['config_mail_smtp_port']                                      = 'SMTP-порт на сервере исходящей почты';
$lang['config_mail_smtp_user']                                      = 'Имя пользователя для аутентификации на сервере исходящей почты';
$lang['config_mail_smtp_pass']                                      = 'Пароль пользователя для аутентификации на сервере исходящей почты';
$lang['config_allow_pass_restore']                                  = 'Разрешить самостоятельное восстановление пароля учётной записи (при отсутствии доменной аутентификации)';
$lang['config_xmpp_host']                                           = 'XMPP-хост';
$lang['config_xmpp_port']                                           = 'XMPP-порт';
$lang['config_xmpp_domain']                                         = 'XMPP-домен';
$lang['config_xmpp_resource']                                       = 'Имя пользователя (ресурса) XMPP';
$lang['config_xmpp_status']                                         = 'Статус пользователя (ресурса)';
$lang['config_xmpp_user']                                           = 'XMPP-пользователь';
$lang['config_xmpp_pass']                                           = 'Пароль XMPP-пользователя';
$lang['config_superusers_logins']                                   = 'Логины пользователей, которые могут являться суперадминистраторами (по умолчанию только admin; если несколько, то через запятую)';
$lang['config_components_polls']                                    = 'Компонент для организации тестов';
$lang['config_components_notes']                                    = 'Компонент для просмотра объявлений';
$lang['config_svg_server']                                          = 'Сервер для получения векторных изображений доски (например, http://alfa:8092/)';
$lang['config_janus_videoroom_server']                              = 'Сервер для организации видеокомнат (например, http://predator:8088/)';
$lang['config_board_videoroom_server']                              = 'Сервер для организации доски в конференции (например, irida6.unit.spb:4432, без протокола)';
$lang['config_board_gallery_server']                                = 'Сервер для организации галереи в конференции (например, irida6.unit.spb:4433, без протокола)';
$lang['config_board_share_server']                                  = 'Сервер для организации доступа к рабочим столам в конференции (например, irida6.unit.spb:4434, без протокола)';
$lang['config_svg_series_delimiter']                                = 'Время в секундах, являющееся признаком смены серии полученных изображений с доски (по умолчанию 3 часа)';
$lang['config_media_server_chanel_name']                            = 'Название UPnP-канала для получения видеозаписей';
$lang['config_media_server']                                        = 'Сервер обработки медиа-данных и ведения трансляций (например, http://korplan2:8880/)';
$lang['config_board_path']                                          = 'Путь к менеджеру холстов (например, https://predator/canvas/irida)';
$lang['config_sip_websocket_url']                                   = 'Адрес сервера организации SIP-звонков (например, wss://irida-msk.unit.spb:4443)';
$lang['config_only_office_server_api']                              = 'Ссылка для доступа к API сервиса организации совместной работы с документами (например, https://engels/web-apps/apps/api/documents/api.js)';


//Оповещения
$lang['notification_room_invite_subject']                           = 'Приглашение в комнату';
$lang['notification_room_invite_body']                              = 'Вы были приглашены в комнату ';
$lang['notification_room_invite_body2']                             = 'пригласил(a) Вас в комнату';
$lang['notification_department']                                    = 'Центр уведомлений';
$lang['notification_type_rooms']                                    = 'Комната';
$lang['notification_type_polls']                                    = 'Опрос';
$lang['notification_type_event']                                    = 'Мероприятие';
$lang['notification_type_project']                                  = 'Проект';
$lang['notification_notifications_removing']                        = 'Удаление уведомлений';
$lang['notification_notifications_removing_confirm']                = 'Вы уверены, что хотите удалить информацию обо всех уведомлениях, в том числе и ожидающих отправки?';
$lang['notification_date']                                          = 'Дата создания';
$lang['notification_author']                                        = 'Автор';
$lang['notification_recipient']                                     = 'Получатель';
$lang['notification_entity_type']                                   = 'Объект';
$lang['notification_entity_type_desc']                              = 'Описание объекта';
$lang['notification_data']                                          = 'Уведомление';
$lang['notification_status']                                        = 'Статус';
$lang['notification_status_send']                                   = 'Отправлено';
$lang['notification_status_not_send']                               = 'В очереди';
$lang['notification_data_text']                                     = 'Содержимое уведомления';
$lang['notification_removing_success']                              = 'Информация об уведомлениях успешно удалена';
$lang['notification_type']                                          = 'Тип уведомления';
$lang['notification_poll_invite_subject']                           = 'Приглашение к участию в тесте';
$lang['notification_poll_invite_body']                              = 'Вы были приглашены к участию в тесте. Для его прохождения перейдите по следующей ссылке: ';
$lang['notification_type_note']                                     = 'Объявление';
$lang['notification_type_feedback']                                 = 'Обратная связь';


//Проверка форм
$lang['form_validation_password']                                   = 'Пароль';
$lang['form_validation_login']                                      = 'Логин';
$lang['form_validation_room_name']                                  = 'Название комнаты';
$lang['form_validation_user_pass']                                  = 'Пароль пользователя';



//Сортируемые таблицы
$lang['sorted_table_loading_data']                                  = 'Загрузка данных';
$lang['sorted_table_operationaffectmany']                           = 'Эта операция необратимо влияет на большое количество данных. Продолжить?';
$lang['sorted_table_filter']                                        = 'Фильтр';
$lang['sorted_table_rowsperpage']                                   = 'Строк на странице';
$lang['sorted_table_displayingresults']                             = 'Переход';
$lang['sorted_table_outof']                                         = 'из';
$lang['sorted_table_showing_active']                                = 'Показаны только активные записи';
$lang['sorted_table_showing_inactive']                              = 'Показаны только неактивные записи';
$lang['sorted_table_showing_all']                                   = 'Показаны все записи';
$lang['sorted_table_first_page']                                    = 'Первая страница';
$lang['sorted_table_previous_page']                                 = 'Предыдущая страница';
$lang['sorted_table_next_page']                                     = 'Следующая страница';
$lang['sorted_table_last_page']                                     = 'Последняя страница';
$lang['sorted_table_data_not_found']                                = 'Данные не найдены';


$lang['filemanager']                                                = 'Файловый менеджер';


/*
 * Объявления
 */
$lang['notes']                                                      = 'Объявления';
$lang['notes_main_desk']                                            = 'Доска общих объявлений';
$lang['notes_for_me_desk']                                          = 'Доска объявлений';
$lang['notes_top_title']                                            = 'Главное';
$lang['notes_all_title']                                            = 'Лента';
$lang['notes_my_title']                                             = 'Мои объявления';
$lang['note_adding']                                                = 'Добавление объявления';
$lang['note_subject']                                               = 'Тема объявления';
$lang['note_body']                                                  = 'Текст объявления';
$lang['note_top_status']                                            = 'Важность объявления';
$lang['note_top_status_true']                                       = 'Высокая';
$lang['note_top_status_false']                                      = 'Низкая';
$lang['note_target_structure']                                      = 'Целевое подразделение';
$lang['note_target_structure_all']                                  = 'Общие для всех подразделений объявления';
$lang['note_target_structure_help']                                 = 'Объявление увидят все пользователи, работающие в целевом и его дочерних подразделениях';
$lang['note_start_time']                                            = 'Дата публикации объявления';
$lang['note_stop_time']                                             = 'Дата окончания публикации объявления';
$lang['note_type']                                                  = 'Тип объявления';
$lang['note_type_success']                                          = 'Поздравление';
$lang['note_type_primary']                                          = 'Информирование';
$lang['note_type_warning']                                          = 'Предупреждение';
$lang['note_type_danger']                                           = 'Последнее предупреждение';

$lang['note_error_note_create_fail']                                = 'Не удалось добавить объявление';
$lang['note_error_note_not_found']                                  = 'Информация об объявлении не найдена';
$lang['note_error_note_update_fail']                                = 'Не удалось изменить параметры объявления';
$lang['note_error_note_remove_fail']                                = 'Не удалось удалить объявление';
$lang['note_error_noteid_not_isset']                                = 'Не удалось найти информацию об объявлении, так как не указан его идентификатор';

$lang['note_ajax_error_no_noteid']                                  = 'Не удалось выполнить операцию, так как не указан идентификатор объявления';
$lang['note_ajax_error_note_not_found']                             = 'Информация об объявлении не найдена';


$lang['note_event_note_added']                                      = 'Пользователь добавил объявление с темой';
$lang['note_event_note_set_as_read']                                = 'Пользователь отметил как прочитанное объявление с темой';
$lang['note_event_note_updated']                                    = 'Пользователь обновил параметры объявления с темой';
$lang['note_event_note_removed']                                    = 'Пользователь удалил объявление с темой';

$lang['note_btn_view_structure_tree']                               = 'Структура организации';
$lang['note_btn_view_desk_for_me']                                  = 'Все объявления';

$lang['note_created_success']                                       = 'Объявление успешно добавлено';
$lang['notes_views_progress']                                       = 'Процент просмотров';
$lang['note_view_body']                                             = 'Подробнее';
$lang['note_viewing']                                               = 'Просмотр объявления';
$lang['notes_btn_check_as_readed']                                  = 'Отметить как прочитанное';
$lang['notes_users_attitude']                                       = 'Отношение к объявлению';
$lang['note_attitude_dislikes']                                     = 'Процент неодобряющих';
$lang['note_attitude_likes']                                        = 'Процент одобряющих';
$lang['note_editing']                                               = 'Изменение параметров объявления';
$lang['note_updated_success']                                       = 'Параметры объявления успешно изменены';
$lang['notes_structure_desk']                                       = 'Доска объявлений подразделения';
$lang['note_removing']                                              = 'Удаление информации об объявлении';
$lang['note_removing_confirm']                                      = 'Вы действительно хотите удалить информацию об объявлении';
$lang['note_removed_success']                                       = 'Объявление успешно удалено';
$lang['note_structure_tree_viewing']                                = 'Просмотр структуры организации';
$lang['note_structure_tree_not_found']                              = 'Информация о подразделениях организации не найдена';
$lang['note_user_has_notviewed_notes']                              = 'У Вас имеются непросмотренные объявления';
$lang['note_public_time']                                           = 'Даты публикации';
$lang['notes_menage_title']                                         = 'Управление объявлениями';
$lang['note_author']                                                = 'Автор объявления';
$lang['notes_wood_title']                                           = 'Доска';
$lang['notes_top_status_notifications']                             = 'У Вас имеются непросмотренные важные объявления';
$lang['notes_top_status_notifications_title']                       = 'Важных объявлений';
$lang['note_btn_view_viewers']                                      = 'Список просмотревших объявление пользователей';
$lang['note_viewed_users_viewing']                                  = 'Пользователи, прочитавшие объявление';
$lang['note_viewing_date']                                          = 'Дата отметки';
$lang['note_attitude_short']                                        = 'Отношение';
$lang['note_notification_new_note_subject']                         = 'Новое объявление';
$lang['note_notification_new_note_body']                            = 'В разделе "Объявления" добавлено новое объявление с темой';
$lang['note_site']                                                  = 'Страница объявления';
$lang['notes_structure_breadcrumb_title']                           = 'Объявления подразделения';
$lang['note_one_title']                                             = 'Объявление';
$lang['note_viewed_users']                                          = 'Пользователи и их отношение к объявлению';
$lang['note_visibility']                                            = 'Видимость для пользователей';


$lang['help']                                                       = 'Руководство оператора';



/*********************************************************************************************************************************************************************
 ******************************************************************** Справочник *************************************************************************************
**********************************************************************************************************************************************************************/
$lang['sked']                                                       = 'Справочник учебных тем';
$lang['sked_menage']                                                = 'Статусы и типы';
$lang['sked_type']                                                  = 'Тип элемента справочника';
$lang['sked_type_types']                                            = 'Типы комментируемых событий';
$lang['sked_type_statuses']                                         = 'Статусы комментируемых событий';
$lang['sked_name']                                                  = 'Название темы';
$lang['sked_color']                                                 = 'Условный цвет';
$lang['sked_image']                                                 = 'Изображение';
$lang['sked_description']                                           = 'Описание темы';
$lang['sked_adding']                                                = 'Добавление учебной темы';
$lang['sked_item_added_successfull']                                = 'Учебная тема успешно добавлена в справочник';
$lang['sked_editing']                                               = 'Изменение параметров элемента справочника';
$lang['sked_edit_image']                                            = 'Действие с изображением';
$lang['sked_item_updated_success']                                  = 'Информация об учебной теме успешно обновлена';
$lang['sked_removing']                                              = 'Удаление информации об учебной теме';
$lang['sked_removing_confirm']                                      = 'Вы действительно хотите удалить учебную тему?';
$lang['sked_item_removed_success']                                  = 'Информация об учебной теме успешно удалена';
$lang['sked_projects_edit']                                         = 'Проекты элемента';
$lang['sked_projects_editing']                                      = 'Изменение проектов элемента';
$lang['sked_item_projects_count']                                   = 'Количество проектов';
$lang['sked_req_status']                                            = 'Используется по умолчанию для новых проектов';
$lang['sked_status_ok']                                             = 'Закрыт';
$lang['sked_status_opened']                                         = 'Открыт';
$lang['sked_type_quest']                                            = 'Вопрос';
$lang['sked_type_warning']                                          = 'Замечание';

//Кнопки
$lang['sked_btn_add_item']                                          = 'Добавить учебную тему';

//Ошибки
$lang['sked_error_item_create_fail']                                = 'Не удалось добавить учебную тему в справочник';
$lang['sked_error_adding_file_fail']                                = 'Не удалось загрузить файл';
$lang['sked_error_item_update_fail']                                = 'Не удалось обновить информацию об учебной теме';
$lang['sked_error_item_remove_fail']                                = 'Не удалось удалить учебную тему из справочника';
$lang['sked_error_ajax_get_projects']                               = 'Не удалось получить информацию о проекте элемента';
$lang['sked_error_item_not_found']                                  = 'Тема не найдена';

//События
$lang['sked_event_admin_item_add']                                  = 'Администратор добавил в справочник информацию об учебной теме';
$lang['sked_event_admin_item_update']                               = 'Администратор изменил информацию об учебной теме';
$lang['sked_event_item_removed']                                    = 'Администратор удалил из справочника информацию об учебной теме';



/*
 * Обратная связь
 */
$lang['feedback_title']                                             = 'Обратная связь';
$lang['feedback_link']                                              = 'через форму';
$lang['feedback_form_info']                                         = 'Оставьте сообщение о возникшей проблеме администратору, а также свои контакты. С Вами обязательно свяжутся в ближайшее время!';
$lang['feedback_problem']                                           = 'Описание проблемы';
$lang['feedback_success_sending']                                   = 'Информация о проблеме успешно отправлена! В ближайшее время администратор рассмотрит заявку и свяжется с Вами. Спасибо за использование нашей системы!';
$lang['feedback_notification_new_subject']                          = 'Новое сообщение о проблеме в разделе обратной связи';
$lang['feedback_notification_new_body']                             = 'В раздел обратной связи поступило новое сообщение о проблеме. Перейдите в систему, чтобы ознакомиться с сообщением.';
$lang['feedback_title_short']                                       = 'Связь';

$lang['feedback_success_removing']                                  = 'Информация о сообщения успешно удалена';

$lang['feedback_event_notification_new']                            = 'Новое сообщение о проблеме в разделе обратной связи';



/*
 * Файлы
 */
$lang['files_error_upload_info']                                    = 'Загрузить информацию о файле не удалось';
$lang['files_event_upload_info']                                    = 'Пользователь загрузил новый файл';



/*
 * Стартовая
 */
$lang['landing_populated_users']                                    = 'Частые контакты';
$lang['landing_connect_over_id']                                    = 'Подключиться по ID';
$lang['landing_fast_videoroom']                                     = 'Видеозвонок';
$lang['landing_favorites_users']                                    = 'Избранные контакты';
$lang['landing_today_schedule']                                     = 'Мероприятия на сегодня';
$lang['landing_write_message']                                      = 'Написать сообщение';
$lang['landing_document_coloborating']                              = 'Обсудить документ';
$lang['landing_chronometria']                                       = 'Хронометраж';
$lang['landing_chronometria_title_description']                     = 'Диаграмма показывает, в какое время суток Вы чаще всего пользуетесь системой';
$lang['landing_chronometria_description']                           = 'Чаще всего Вы использовали систему в период с ';
$lang['landing_chronometria_description_2']                         = ' до ';
$lang['landing_rooms_stat']                                         = 'Комнаты';
$lang['landing_stat_perzentil']                                     = 'ПЕРЦЕНТИЛЬ';
$lang['landing_rooms_stat_count']                                   = 'Количество созданных Вами комнат';
$lang['landing_rooms_stat_perzentil']                               = 'Перцентиль показывает в процентах количество пользователей, которые создали меньше комнат, чем Вы';
$lang['landing_rooms_stat_title_description']                       = 'Диаграмма показывает, насколько Вы активнее других участников системы с точки зрения создания комнат';

/*
 * SIP
 */
$lang['sip_title']                                                  = 'SIP-телефония';


/*
 * IM
 */
$lang['im_title']                                                   = 'Сообщения';
$lang['im_add_thread']                                              = 'Создать';
$lang['im_splash_text']                                             = 'Пожалуйста, выберите диалог или создайте новый';
$lang['im_placeholder']                                             = 'Напишите сообщение...';
$lang['im_thread_adding']                                           = 'Создание диалога';
$lang['im_create_thread']                                           = 'Создать диалог';
$lang['im_thread_adding_comment']                                   = 'Укажите участников диалога';
$lang['im_thread_removing']                                         = 'Удаление диалога';
$lang['im_thread_removing_confirm']                                 = 'Вы действительно хотите удалить диалог';

$lang['im_btn_send_message']                                        = 'Отправить сообщение';
$lang['im_btn_create_thread']                                       = 'Создать диалог';
$lang['im_btn_load_users']                                          = 'Загрузить ещё';

$lang['im_error_users_list_empty']                                  = 'Для создания диалога не выбрано ни одного участника';
$lang['im_error_thread_not_found']                                  = 'Диалог не найден';
$lang['im_error_thread_removing_failed']                            = 'Удалить диалог не удалось';

$lang['im_event_description']                                       = 'Диалоги';
$lang['im_event_thread_creating']                                   = 'Пользователь создал диалог';
$lang['im_event_thread_from_user_removing']                         = 'Пользователь удалил диалог';