<?php

class Tools_model extends CI_Model {
    
    /**
     * Полезные преобразования
     * 
     */
    public function getIndexedArray($array = array()) {
        
        $result = array();
        
        foreach($array as $arrkey=>$record) {
            if (mb_strlen($record['name'])) {
                $result[mb_strtoupper(mb_substr($record['name'],0,1))][] = $record;
            }
        }
        
        ksort($result);
        
        return $result;
    }
    
    
    
    public function copyDirectory($sourcePath,$destinationPath) {
        
        $files = array();
        
        $this->getFilesOverDir($sourcePath,$files);
                
        foreach($files as $file) {
            
            $fdirName = $destinationPath.str_replace($sourcePath, '', $file['name']);
            
			if ($file['type'] == 'dir') {
                if (!is_dir($fdirName)) mkdir($fdirName,0777,TRUE);
            } else {
                copy(realpath($file['name']),$fdirName);
            }
            
        }
		
    }
    
    
    private function getFilesOverDir($dir,&$files) {
        if($objs = glob($dir."/*")) {
            
            foreach($objs as $obj) {
                if (is_dir($obj)) {
                    $files[] = array('name' => $obj,'type' => 'dir');
                    $this->getFilesOverDir($obj,$files);
                } else {
                    $files[] = array('name' => $obj,'type' => 'file');
                }
                
            }
        }
    }
    
    
    
    public function removeDirectory($dir) {
        
        if (is_dir($dir)) {
            if($objs = glob($dir."/*")) {
                foreach($objs as $obj) {
                    is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
                    is_dir($obj) ? @rmdir($obj) : null;
                }
            }
            
            @rmdir($dir);
        }
    }
    
    
    
    /**
     * Замена стандартной функции basename, которая работает неверно для кириллицы.
     * Аргументы, как у basename
     *
     */
    public static function pcgbasename($param, $suffix=null) {
        if ( $suffix ) {
            $tmpstr = ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR);
            if ( (strpos($param, $suffix)+strlen($suffix) )  ==  strlen($param) ) {
                    return str_ireplace( $suffix, '', $tmpstr);
            } else {
                    return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR);
            }
        } else {
            return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR);
        }
    }
    
    
    /**
     * Архивация
     */
    public function compressDir($dir, $destinationDir , $name) {
        
        $this->load->library('pzip',$destinationDir.$name);
        $this->pzip->zipname = $destinationDir.$name;
        $result = $this->pzip->create($dir,77003,$destinationDir,78003,'myPreAddCallBack');
        
        if (!$result) throw new Exception($this->pzip->errorInfo(true));

        return $destinationDir.$name;
        
        /*
        $this->load->library('zip');
        
        $this->zip->read_dir($dir,FALSE);
        $this->zip->archive($destinationDir.$name);

        // Download the file to your desktop. Name it "my_backup.zip"
        //$this->zip->download($name);
         * 
         */
        
    }
    
    
    /**
     * Деархивация каталога
     */
    public function unCompressFile($filePath, $destinationDir) {
        
        $this->load->library('pzip',$filePath);
        $this->pzip->zipname = $filePath;
        
        $result = $this->pzip->extract(PCLZIP_OPT_PATH,$destinationDir,PCLZIP_CB_PRE_EXTRACT,'myPreExtractCallBack');

        if (!$result) throw new Exception($this->pzip->errorInfo(true));
        
        return $result;
    }
    
    
    
    /**
     * Проверка наличия каталогов для распаковки или запаковки данных
     * 
     * @param type $ts
     * @return type
     */
    public function makeTempPath($ts = 0) {
        
        $globTempPath = './upload/temp/';
        if (!is_dir($globTempPath)) mkdir($globTempPath,0777,TRUE);
        
        //Каталог для сбора данных об УМР
        $tempPath = $globTempPath.$ts.'/';
        if (!is_dir($tempPath)) mkdir($tempPath,0777,TRUE);

        //Каталог для сбора файлов УМР
        $tempFilesPath = $tempPath.'files/';
        if (!is_dir($tempFilesPath)) mkdir($tempFilesPath,0777,TRUE);
        
        return array($globTempPath,$tempPath,$tempFilesPath);
        
    }

    
    /**
     * Перевод миллиметров в типографические твипы
     */
    public function m2t($millimeters){
        return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
    }
    
    
    /**
     * Чистка имени файла перед скачиванием
     */
    public function cleanDownloadedFileName($fileName) {
        $fileName = preg_replace('/\"/U','_',$fileName);
        $fileName = preg_replace('/\s/U','_',$fileName);
        
        return $fileName;
    }
    
    
    public function convertArrayToString($array) {
        
        $string = '';
        foreach($array as $rkey => $value) {
			if (!is_array($value))
	            $string .= '&nbsp;&nbsp;&nbsp;&nbsp;'.$rkey.': '.$value.'<br>';
        }
        
        return $string;
        
    }
    
	
    public function makeAbbr($source = '')  {

        $abbr = '';

        $sArray = explode(' ', $source);
        foreach($sArray as $word) {
                $abbr .= mb_strtoupper(mb_substr($word,0,1));
        }

        return $abbr;
        
    }
	
	
    /**
     * Поиск файлов в каталоге
     * 
     * @param type $path - путь
     * @param type $typesArray - массив с типами
     */
    public function searchFilesInDirectory($path,$typesArray,$prefix = '') {

            $this->load->helper('directory');

            $map = directory_map($path);

            $files = array();

            foreach($map as $key => $file) {

                    if (!is_array($file)) {
                            if (in_array(mb_strtolower(mb_substr(strrchr($file,'.'),1)),$typesArray)) $files[] = $prefix.$file;
                    } else {
                            $subfiles = $this->searchFilesInDirectory($path.'/'.$key, $typesArray,$key);
                            $files = array_merge($files,$subfiles);
                    }

            }


            return $files;

    }

        
        
    public function getDirectoryMap($path,$prefix = '') {

        $this->load->helper('directory');

        $map = directory_map($path);

        $files = array();

        foreach($map as $key => $file) {

            if (!is_array($file)) {
                $files[] = $prefix.$file;
            } else {
                $subfiles = $this->getDirectoryMap($path.'/'.$key, $key);
                $files = array_merge($files,$subfiles);
            }

        }

        return $files;

    }
	
    
	
    /**
     * Преобразование пути к файлу в URL
     */
    public function getURLFromFilePath($pathToFile) {

            if (mb_strpos($pathToFile,'./upload') == 0) {

                    return base_url().mb_substr($pathToFile,2);

            }

            return '';
    }



    public function getArrayAsString($array = array()) {

        $dataString = '';

        if (!empty($array)) {
            foreach($array as $field => $value) {
                if (is_array($value)) {
                    $dataString .= $field.': {'.$this->getArrayAsString($value).' } | ';
                } else {
                    $dataString .= $field.': '.$value.'  |  ';
                }
            }
        }

        return $dataString;

    }
    
    
    public function getCleanArrayForJson($arr = array(),$keys = array()) {
        
        foreach($arr as $rkey => $value) {
            
            if (in_array($rkey, $keys)) $arr[$rkey] = prejson ($value);
            
        }
        
        return $arr;
        
    }
    

}
