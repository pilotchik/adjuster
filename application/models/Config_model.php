<?php

class Config_model extends CI_Model {
    
    /**
     * Получение списка всех настроек
     * 
     * @return type
     */
    public function getConfig() {
        
        $this->initConfiguration();

        $result_array = array();
        
        $cacheDriver = $this->config->item('config_cache_driver');
        if (($this->cache->$cacheDriver->is_supported() && !$result_array = $this->cache->$cacheDriver->get('db_config')) || !is_writable('./application/cache')) {
        
            $query = $this->db->get('configuration');
            $db_answer = $query->result_array();

            foreach($db_answer as $value) {
                $result_array[$value['name']] = $value['value'];
            }

            $this->cache->$cacheDriver->save('db_config', $result_array, $this->config->item('config_cache_view_time') * 60 * 5);
            
        }
        
        return $result_array;
    }
    
    
    public function getConfigForAdmin() {
        
        $configs = array();
        
        //Сделаем так, чтобы основные настройки были на первом месте
        $sections = array('main');
        
        $this->db->distinct();
        $this->db->select('section');
        $this->db->from('configuration');
        $this->db->where('section !=', 'main');
        $ra = $this->db->get()->result_array();
        
        foreach($ra as $record) {
            $sections[] = $record['section'];
        }
        
        foreach($sections as $section) {
        
            $this->db->reset_query();
            $this->db->select('*');
            $this->db->from('configuration');
            $this->db->where('section', $section);
            $this->db->order_by('name', 'ASC');
            $query = $this->db->get();
            $configs[$section] = $query->result_array();
            
        }
        
        return $configs;
    }

    
    /**
     * Обновление конфигурации
     * 
     * @param type $key - ключ
     * @param type $value - значение
     */
    public function updateConfig($key = '',$value = '') {
        
        $this->db->where('name', $key);
        $this->db->update('configuration', array('value' => $value));
        
        $cacheDriver = $this->config->item('config_cache_driver');
        $this->cache->$cacheDriver->delete('db_config');
        $this->cache->$cacheDriver->delete('db_config_item_'.$key);
    }
    
    
    /**
     * Проверка существования поля с конкретным именем
     */
    public function checkConfigItem($name = '') {
        
        $cacheDriver = $this->config->item('config_cache_driver');
        
        if (!$this->cache->$cacheDriver->is_supported() || $this->cache->$cacheDriver->is_supported() && !$ra = $this->cache->$cacheDriver->get('db_config_item_'.$name)) {
            
            $this->db->reset_query();
            
            $this->db->select('name');
            $this->db->from('configuration');
            $this->db->where('name', $name);
            
            $query = $this->db->get();
            $ra = $query->result_array();
            
            $ra = empty($ra) ? false : $ra[0]['name'];
            
            if ($ra) $this->cache->$cacheDriver->save('db_config_item_'.$name, $ra, $this->config->item('config_cache_view_time') * 60);
        }
        
        return $ra;
        
    }
    
    
    /**
     * Добавление записи
     * 
     */
    public function createConfig($fields = array()) {

        $this->db->insert('configuration', $fields);

        $cacheDriver = $this->config->item('config_cache_driver');
        $this->cache->$cacheDriver->delete('db_config');
        $this->cache->$cacheDriver->delete('db_config_item_'.$fields['name']);
        
        
    }
    
    
    /**
     * Проверка существования полей в базе данных
     */
    public function initConfiguration() {
        
        //Массив с параметрами конфигурации
        $config_array = array(
            array('name' => 'config_superusers_logins','value' => 'admin','type' => 'text','required' => 0,'section' => 'main'),
            array('name' => 'config_title','value' => 'Система проверки знаний','type' => 'text','required' => 1,'section' => 'main'),
            array('name' => 'config_datetime_format','value' => 'H:i d.m.Y','type' => 'text','required' => 1,'section' => 'main'),
            array('name' => 'config_allow_registration','value' => '1','type' => 'boolean','required' => 1,'section' => 'main'),
            array('name' => 'config_title_short','value' => 'Ирида','type' => 'text','required' => 1,'section' => 'main'),
            array('name' => 'config_allow_pass_restore','value' => '1','type' => 'boolean','required' => 1,'section' => 'main'),
            array('name' => 'config_main_mail','value' => 'irida@'.$_SERVER['HTTP_HOST'],'type' => 'text','required' => 1,'section' => 'mail'),
            array('name' => 'config_main_mail_name','value' => 'Ирида','type' => 'text','required' => 1,'section' => 'mail'),
            array('name' => 'config_mail_smtp_host','value' => 'bark','type' => 'text','required' => 1,'section' => 'mail'),
            array('name' => 'config_mail_smtp_user','value' => '','type' => 'text','required' => 0,'section' => 'mail'),
            array('name' => 'config_mail_smtp_pass','value' => '','type' => 'text','required' => 0,'section' => 'mail'),
            array('name' => 'config_mail_smtp_port','value' => '25','type' => 'text','required' => 1,'section' => 'mail')
        );
        
        //Пройти массив настроек
        foreach($config_array as $config) {
            if (!$this->checkConfigItem($config['name'])) {
                
                //Если настройки нет, то создать запись в базе
                $this->createConfig($config);
                
            } else {
                //echo $this->checkConfigItem($config['name']);
            }
        }
        
    }
    
}