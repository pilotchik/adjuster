<?php

class Log_model extends CI_Model {
    
    /**
     * Добавление данных в журнал
     */
    public function addLog($fields = array()) {
        $sql = "INSERT INTO log (user_id,ts,status,controller) VALUES (".$fields['user_id'].",".time().",'".pg_escape_string($fields['status'])."','".$fields['controller']."')";
        $this->db->query($sql);
    }
    
    
    public function addIndexCounter($controller = '',$entity_id = 0) {
        
        $fields = array(
            'user_id' => isset($this->session->userdata['userID']) ? $this->session->userdata['userID'] : 0,
            'ts' => time(),
            'controller' => $controller,
            'entity_id' => $entity_id
        );
        
        $this->db->insert('index_counter', $fields);
        return $this->db->insert_id();
        
    }
    
    
    public function addMessageLog($message,$controller,$userID = 0) {
        $fields = array(
            'ts' => time(),
            'user_id' => !$userID && isset($this->session->userdata['userID']) ? $this->session->userdata['userID'] : $userID,
            'status' => $message,
            'controller' => $controller
        );
        $this->db->insert('log', $fields);
    }
    
    
    
    public function getMetriksCount($controller = '') {
        
        $this->db->from('index_counter');
        
        if ($controller == '') {
            $this->db->where('entity_id', 0);
        } else {
            $this->db->where('controller', $controller);
            $this->db->where('entity_id', 0);
        }
        
        return $this->db->count_all_results();
    }
    
    
    /**
     * Очистка метрик
     */
    public function removeAllMetrics() {
        $sql = "DELETE FROM index_counter";
        $this->db->query($sql);
    }
    
    
    /**
     * Получение лога
     */
    public function getEvents($startTS = 0, $endTS = 0) {
        
        $this->db->select('*');
        $this->db->from('log');
        
        if ($startTS || $endTS) {
            $this->db->where('ts > ', $startTS);
            $this->db->where('ts < ', $endTS);
        } else {
            $this->db->limit(500);
        }
        
        
        $this->db->order_by('ts', 'DESC');
        return $this->db->get()->result_array();
        
        
    }
    
    
    /**
     * Удаление всех записей
     */
    public function removeAllLogs() {
        $sql = "DELETE FROM log";
        $this->db->query($sql);
    }
 
    
    /**
     * Удаление одной записи
     */
    public function removeOneLogEvent($logID) {
        $sql = "DELETE FROM log WHERE id = ".$logID;
        $this->db->query($sql);
    }
    
    
    /**
     * Получить дату начала статистики
     */
    public function getStartMetriks($controller = '',$entityID = 0) {
        
        $this->db->select_min('ts');
        $this->db->from('index_counter');
        if ($controller) {
            $this->db->where('controller',$controller);
            $this->db->where('entity_id',$entityID);
        }
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return $ra[0]['ts'] == '' ? time() : $ra[0]['ts'];
    }
    
    
    /**
     * Получить записи о просмотре в конкретном диапазоне
     */
    public function getMetriksRecordsInRange($start = 0, $stop = 0) {
        
        $this->db->select('id,ts');
        $this->db->from('index_counter');
        
        $this->db->where('ts >=', $start);
        $this->db->where('ts <', $stop);
        $this->db->where('entity_id', 0);
        
        return $this->db->get()->result_array();
        
    }
    
    
    
    
    /**
     * Получить количество просмотров в заданном интервале времени
     */
    public function getMetriksDateRangeCount($start = 0,$stop = 0,$controller = '',$entityID = 0) {

        $this->db->from('index_counter');
        
        $this->db->where('ts >=', $start);
        $this->db->where('ts <', $stop);
        
        if ($controller == '') {
           $this->db->where('entity_id', 0);
        } else {
            $this->db->where('controller', $controller);
            $this->db->where('entity_id', $entityID); 
        }
        
        return $this->db->count_all_results();
        
    }
    
    
    public function getMetriksUsersDateRangeCount($start = 0,$stop = 0,$controller = '',$entityID = 0,$returnCount = true) {
        
        $this->db->distinct();
        $this->db->select('user_id,entity_id');
        $this->db->from('index_counter');
        
        $this->db->where('ts >=', $start);
        $this->db->where('ts <', $stop);
        
        if ($controller == '') {
           $this->db->where('entity_id', 0);
        } else {
            $this->db->where('controller', $controller);
            if ($entityID) 
                $this->db->where('entity_id', $entityID); 
            else
                $this->db->where('entity_id > ', 0); 
            
        }
        
        $ra = $this->db->get()->result_array();
        
        if ($returnCount)
            return count($ra);
        else
            return $ra;
        
    }
    
    
    public function getMetriksEntityIDInDateRange($start = 0, $stop = 0, $controller = '', $userID = 0, $distinct = true) {
        
        
        if ($distinct) {
            $this->db->distinct();
        } else {
            $this->db->select('ts');
        }
        $this->db->select('entity_id,user_id');
        $this->db->from('index_counter');
        
        $this->db->where('ts >=', $start);
        $this->db->where('ts <', $stop);
        
        $this->db->where('controller', $controller);
        $this->db->where('entity_id !=', 0);
        
        if ($userID)
            $this->db->where('user_id', $userID);
        else
            $this->db->where('user_id >', 0);
        
        //echo $this->db->get_compiled_select();
        
        $query = $this->db->get();
        return $query->result_array();
        
    }
    
    
    
    /**
     * Добавить информацию о проблеме
     * 
     * @param type $fields
     */
    public function addFeedbackMessage($fields) {
        
        $this->db->insert('feedback', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Получение краткой информации о проблеме
     */
    public function getOneFeedbackProblem($feedbackID) {
        
        $this->db->select('problem');
        $this->db->from('feedback');
        $this->db->where('id',$feedbackID);
        $query = $this->db->get();
        $ra = $query->result_array();
        
        if (empty($ra)) {
            return '';
        } else {
            $problem = mb_strlen($ra[0]['problem']) > 50 ? mb_substr($ra[0]['problem'],0,50).'...' : $ra[0]['problem'];
        }
        
    }
    
    
    public function getLastFeedbackMessages() {
        
        $this->db->select('*');
        $this->db->from('feedback');
        $this->db->limit(500);
        $this->db->order_by('ts', 'DESC');
        $query = $this->db->get();
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            
            $ra[$rkey]['date'] = date($this->load->get_var('config_datetime_format'),$record['ts']);
            
        }
        
        return $ra;
        
    }
    
    
    public function removeAllFeedback() {
        
        $sql = "DELETE FROM feedback";
        $this->db->query($sql);
        
    }
    
    
    /**
     * 
     * @param type $error_message
     * @param type $error_type
     */
    public function addErrorInfo($error_message,$error_type) {
        
        $error_info = array(
            'error' => array(
                'system_error' => $error_message,
                'system_error_type' => $error_type
            )
        );

        $this->session->set_userdata($error_info);
    }
    
    
    /**
     * Получить записи о просмотре в конкретном диапазоне
     */
    public function getMetriksRecordForLastInputInRoom($userID) {
        
        $this->db->select('ts');
        $this->db->from('index_counter');
        
        $this->db->where('controller', 'rooms');
        $this->db->where('entity_id !=', 0);
        $this->db->where('user_id', $userID);
        
        $this->db->where('entity_id !=', 0);
        $this->db->order_by('ts', 'DESC');
        $this->db->limit(1);
        
        $ra = $this->db->get()->result_array();
        
        return empty($ra) ? 0 : $ra[0]['ts'];
        
    }
    
    
}