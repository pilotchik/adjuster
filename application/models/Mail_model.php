<?php

class Mail_model extends CI_Model {
    
    /**
     * Отправка почты
     * 
     * fields:
     *      from_name - имя отправителя
     *      from_mail - почта отправителя
     *      to - почта получателя - обязательно
     *      subject - тема - обязательно
     *      message - сообщение - обязательно
     * 
     */
    public function sendMail($fields) {
        
        /*
        
        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $this->load->get_var('config_mail_smtp_host');
        $config['smtp_port'] = $this->load->get_var('config_mail_smtp_port');
        $config['smtp_user'] = $this->load->get_var('config_mail_smtp_user');
        $config['smtp_pass'] = $this->load->get_var('config_mail_smtp_pass');
        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        $from_mail = $this->load->get_var('config_main_mail');
        $from_name = isset($fields['from_name']) ? $fields['from_name'] : $this->load->get_var('config_main_mail_name');
        
        $this->email->from($from_mail, $from_name);
        $this->email->to($fields['to']);
        if ($this->config->item('config_mail_bcc_address') != '') $this->email->bcc($this->config->item('config_mail_bcc_address'));

        $this->email->subject($fields['subject']);

        $message = $fields['message'].'<br><br><a href="'.base_url().'"><i>'.$this->load->get_var('config_title_short').'</i></a>';
        $this->email->message($message);

        try {
            $this->email->send();
            return 1;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
         * 
         */
        
        $headers = 'MIME-Version:1.0'."\r\n";
        $headers .= 'Content-type:text/html;charset=utf-8'."\r\n";
        $headers .= 'From:'.$this->load->get_var('config_main_mail')."\r\n";
        if ($this->config->item('config_mail_bcc_address') != '') $headers .= 'Bcc:'.$this->config->item('config_mail_bcc_address')."\r\n";
        
        $message = "<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />".$fields['message'].'<br><br><a href="'.base_url().'"><i>'.$this->load->get_var('config_title_short').'</i></a>';
        
        return mail($fields['to'],$fields['subject'],$message,$headers);
        
    }
    
    
    
}