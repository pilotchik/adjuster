<?php

class Notification_model extends CI_Model {
    
    
    /*
     * Сформировать очередь на отправку уведомлений указанных в конфигурации типов
     * Учесть, что у некоторых пользователей может стоять блокировка на уведомление по некоторым типам
     */
    public function makeNotification($fields = array(), $send = true) {
     
        $usersSettings = unserialize($this->users_model->getUsersSettings($fields['recipient_id']));

        $notificationTypes = explode(',',$this->config->item('config_notification_types'));
        foreach($notificationTypes as $notifyType) {

            if (!isset($usersSettings[$notifyType]) || isset($usersSettings[$notifyType]) && $usersSettings[$notifyType]) {

                $notify_array = array(
                    'recipient_id' => $fields['recipient_id'],
                    'author_id' => isset($this->session->userdata['userID']) ? $this->session->userdata['userID'] : 0,
                    'entity_type' => $fields['entity_type'],
                    'entity_id' => $fields['entity_id'],
                    'subject' => $fields['subject'],
                    'body' => $fields['body'],
                    'status' => 0,
                    'type_of_notification' => $notifyType,
                    'ts' => time()
                );

                $this->addNotification($notify_array);
            }
        }
        
        if ($send) $this->notification_model->sendAllNotifications();
        
    }
    
    
    
    
    /**
     * Добавление данных в журнал
     */
    public function addNotification($fields = array()) {
        
        $this->db->insert('notifications', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Получение уведомлений
     */
    public function getNotifications($limit = 20, $startPosition = 0,$startTS = 0, $endTS = 0) {
        
        //$startPosition = $startPage*$limit + 1;
        
        $limitStr = $limit ? ' LIMIT '.$limit : '';
        
        if (!$startTS && !$endTS) 
            $sql = "SELECT id FROM notifications ORDER BY ts DESC OFFSET ".$startPosition.$limitStr;
        elseif ($startTS)
            $sql = "SELECT id FROM notifications WHERE ts > ".$startTS." ORDER BY ts DESC OFFSET ".$startPosition.$limitStr;
        elseif ($endTS)
            $sql = "SELECT id FROM notifications WHERE ts < ".$endTS." ORDER BY ts DESC OFFSET ".$startPosition.$limitStr;
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            $ra[$rkey] = $this->getOneNotification($record['id']);
        }
        
        
        return $ra;
    }
    
    
    /**
     * Получить уведомления для конкретного пользователя
     * 
     * @param type $userID
     * @return type
     */
    public function getUserNotifications($userID) {
        
        $this->db->select('id');
        $this->db->from('notifications');
        $this->db->where('recipient_id',$userID);
        $this->db->like('type_of_notification','popup');
        $this->db->order_by('ts','DESC');
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            $ra[$rkey] = $this->getOneNotification($record['id']);
        }
        
        return $ra;
        
    }
    
    
    public function getOneNotification($notifyID) {
        
        $this->load->model('polls_model');
        $this->load->model('notes_model');
                    
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where('id',$notifyID);
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        if (!empty($ra)) {
            
            $notify = $ra[0];
            
            $notify['author'] = $this->users_model->getOneUserInfoOverID($notify['author_id']);
            
            $notify['authorUserName'] = $this->users_model->getFullNameOverID($notify['author_id'],true);
            $notify['authorUserFullName'] = $this->users_model->getFullNameOverID($notify['author_id']);
            $notify['recipientUserName'] = $this->users_model->getFullNameOverID($notify['recipient_id'],true);
            $notify['recipientUserFullName'] = $this->users_model->getFullNameOverID($notify['recipient_id']);
            $notify['date'] = date($this->load->get_var('config_datetime_format'),$notify['ts']);
            switch($notify['entity_type']) {
                case 'room': 
                    $notify['type'] = $this->lang->line('notification_type_rooms'); 
                    $notify['typeObject'] = $this->rooms_model->getRoomName($notify['entity_id']);
                    break;
                case 'poll': 
                    $notify['type'] = $this->lang->line('notification_type_polls');
                    $notifyObject = $this->polls_model->getOnePollOverID($notify['entity_id']);
                    $notify['typeObject'] = !empty($notifyObject) ? $notifyObject['title'] : '';
                    break;
                case 'note':
                    $notify['type'] = $this->lang->line('notification_type_note'); 
                    $notify['typeObject'] = $this->notes_model->getOneNoteSubject($notify['entity_id']);
                    break;
                case 'event':
                    $notify['type'] = $this->lang->line('notification_type_event'); 
                    $notify['typeObject'] = $this->schedule_model->getOneEventTitle($notify['entity_id']);
                    break;
                case 'project':
                    $notify['type'] = $this->lang->line('notification_type_project'); 
                    $notify['typeObject'] = $this->projects_model->getOneProjectTitle($notify['entity_id']);
                    break;
                case 'feedback':
                    $notify['type'] = $this->lang->line('notification_type_feedback'); 
                    $notify['typeObject'] = $this->log_model->getOneFeedbackProblem($notify['entity_id']);
                    break;
            }
            
        }
        
        return $notify;
        
    }
    
    
    
    /**
     * Получение новых уведомлений для конкретного пользователя
     */
    public function getUserNewPopupNotifications($userID) {
        
        //Выбрать все уведомления с нулевым статусом и отправить их способом, который есть в type_of_notification после слова "notifications_"
        //$sql = "SELECT * FROM notifications WHERE status = 0 AND type_of_notification LIKE '%popup%' AND recipient_id = $userID ORDER BY ts DESC";
        
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where('status', 0);
        $this->db->where('recipient_id', $userID);
        $this->db->like('type_of_notification', 'popup');
        $this->db->order_by('ts', 'DESC');
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        
        foreach($ra as $rkey => $record) {
            $ra[$rkey]['authorFullName'] = $this->users_model->getFullNameOverID($record['author_id']);
            $ra[$rkey]['authorShortName'] = $this->users_model->getFullNameOverID($record['author_id'],true);
            $ra[$rkey]['date'] = date('d.m.Y') == date('d.m.Y',$record['ts']) ? date('H:i',$record['ts']) : date($this->load->get_var('config_datetime_format'),$record['ts']);
        }
        return $ra;
    }
    
    
    /**
     * Удаление всех уведомлений
     */
    public function removeAllNotifications($userID = 0) {
        
        if (!$userID) {
            $this->db->where('recipient_id >', 0);
        } else {
            $this->db->where('recipient_id', $userID);
        }
        
        $this->db->delete('notifications');
        
    }
    
    
    /**
     * Отправка всех уведомлений, которые не были отправлены
     */
    public function sendAllNotifications() {
        
        /*
         * Почта 
         */
        
        //Выбрать все уведомления с нулевым статусом и отправить их способом, который есть в type_of_notification после слова "notifications_"
        //$sql = "SELECT * FROM notifications WHERE status = 0 AND type_of_notification LIKE '%mail%'";
        
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where('status',0);
        $this->db->like('type_of_notification','mail');
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        foreach($ra as $notify) {
            
            //Обновить статус уведомления
            $this->updateNotification($notify['id'],array('status' => 1),$notify['recipient_id']);
            
            $recipient = $this->users_model->getOneUserInfoOverID($notify['recipient_id']);
            
            if ($recipient['email'] != '') {
                
                $mail_fields = array(
                    'to' => $recipient['email'],
                    'subject' => $notify['subject'],
                    'message' => $notify['body']
                );
                
                if (!$this->mail_model->sendMail($mail_fields)) {
                    
                    $this->load->model('tools_model');
                    $dataString = $this->tools_model->getArrayAsString($mail_fields);
                    
                    log_message('error', '');
                    log_message('error', 'MAIL NOT SEND: ');
                    log_message('error', $dataString);
                    log_message('error', '');
                }
                
            }
        }
        
        
        /*
         * XMPP
         */
        //Выбрать все уведомления с нулевым статусом и отправить их способом, который есть в type_of_notification после слова "notifications_"
        
        if (mb_strpos($this->config->item('config_notification_types'),'xmpp') !== FALSE) {
        
            $this->db->reset_query();
            
            $this->db->select('*');
            $this->db->from('notifications');
            $this->db->where('status',0);
            $this->db->like('type_of_notification','xmpp');

            $query = $this->db->get();
            $ra = $query->result_array();
            
            
            if (!empty($ra)) {
            
                $xmpp_params = array(
                    'user' => $this->load->get_var('config_xmpp_user'),
                    'pass' => $this->load->get_var('config_xmpp_pass'),
                    'host' => $this->load->get_var('config_xmpp_host'),
                    'port' => $this->load->get_var('config_xmpp_port'),
                    'domain' =>  $this->load->get_var('config_xmpp_domain'),
                    'resource' => '',
                    'status' => $this->load->get_var('config_xmpp_status'),
                    'logtxt' => $this->config->item('config_xmpp_logtxt'),
                    'log_file_name' => $this->config->item('config_xmpp_logfilename'),//'log_file_name' => '/home/kudralex/projects/irida/application/libraries/loggerxmpp.log',//
                    'tls_off' => $this->config->item('config_xmpp_tlsoff')
                );

                $this->load->library('xmpp', $xmpp_params);

                $this->xmpp->connect();
                
            
                foreach($ra as $notify) {
                    //Обновить статус уведомления

                    $recipient = $this->users_model->getOneUserInfoOverID($notify['recipient_id']);
                    $author = $this->users_model->getOneUserInfoOverID($notify['author_id']);
                    
                    $this->updateNotification($notify['id'],array('status' => 1),$notify['recipient_id']);

                    if ($recipient['email'] != '' && mb_strpos($recipient['email'],$this->load->get_var('config_xmpp_domain')) !== FALSE) {

                        if ($notify['entity_type'] == 'room' && !empty($author)) {

                            $roomName = $this->rooms_model->getRoomName($notify['entity_id']);

                            $notify['body'] = $author['fullName'].' '.$this->lang->line('notification_room_invite_body2').' "'.$roomName.'"';

                        }

                        @$this->xmpp->sendMessage($recipient['email'],strip_tags($notify['body']), '<strong>'.$notify['subject'].'</strong>'.' '.strip_tags($notify['body']).'   '.base_url());

                    }

                }
                
            }
            
        }
        
    }
    
    
    /**
     * Обновление информации уведомления
     */
    public function updateNotification($id,$fields,$userID = 0) {
        
        $this->db->where('id', $id);
        $this->db->update('notifications', $fields);
        
        if ($userID) $this->clearUsersNotificationsCache($userID);
        
    }
    
    
    /**
     * Получение уведомлений, по типу, идентификатору и реципиенту
     */
    public function getUserEntityNotifications($entity_type,$entity_id,$recipient_id) {
        
        $this->db->select('*');
        $this->db->from('notifications');
        
        $this->db->where('entity_type', $entity_type);
        $this->db->where('entity_id', $entity_id);
        $this->db->where('recipient_id', $recipient_id);
        
        return $this->db->get()->result_array();
        
    }
    
    
    public function clearUsersNotificationsCache($userID) {
        
        $cacheDriver = $this->config->item('config_cache_driver');
        
        $this->cache->$cacheDriver->delete('db_notifications_user_'.$userID);
        
    }
    
    
    
}