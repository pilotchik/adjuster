<?php

class Tags_model extends CI_Model {
    
    /**
     * Получение списка всех тегов
     * 
     * @return type
     */
    public function getAllTags() {
        
        $this->db->select('id');
        $this->db->from('tags');
        $this->db->order_by('name','ASC');
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            $ra[$rkey] = $this->getOneTag($record['id']);
        }
        
        return $ra;
        
    }
    
    
    /**
     * Получение общего количества тегов
     */
    public function getTagsCount() {
        
        $this->db->select('id');
        $this->db->from('tags');
        return $this->db->count_all_results();
        
    }
    
    
    /**
     * Получение количества использований тега
     */
    public function getTagUseCount($tagID) {
        
        $this->db->select('id');
        $this->db->from('tags_to_users');
        $this->db->where('tag_id',$tagID);
        return $this->db->count_all_results();
        
    }
    
    
    /**
     * Добавление тега
     * 
     * @param mixed $tag_fields - параметры тега
     */
    public function createTag($fields = array()) {
        
        $this->db->insert('tags', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Изменение информации о теге
     */
    public function updateTag($id,$fields = array()) {
        
        $this->db->where('id', $id);
        $this->db->update('tags', $fields);
        
    }
    
    
    /**
     * Получение информации об одном теге
     */
    public function getOneTag($tagID) {
        
        $this->db->select('*');
        $this->db->from('tags');
        $this->db->where('id',$tagID);
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        if (!empty($ra)) {
            $ra = $ra[0];
            $ra['createDate'] = date($this->load->get_var('config_datetime_format'),$ra['ts']);
            switch($ra['entity_type']) {
                case 'user': $ra['entityTypeTranslate'] = $this->lang->line('tag_entity_type_user'); break;
                default : $ra['entityTypeTranslate'] = '';
            }
            $ra['useCount'] = $this->getTagUseCount($ra['id']);
        }
        return $ra;
    }
    
    
    /**
     * Удаление тега
     */
    public function removeTag($tagID) {
        
        $this->db->where('id',$tagID);
        $this->db->delete('tags');
        
        $this->db->where('tag_id',$tagID);
        $this->db->delete('tags_to_users');
        
    }
    
    
    /**
     * Удаление всех тегов пользователя
     */
    public function removeAllUsersTags($userID) {
        
        $this->db->where('user_id',$userID);
        $this->db->delete('tags_to_users');
        
    }
    
    
    /**
     * Получение идентификатора тега по его имени
     */
    public function getTagIDOverName($tagName) {
        
        $this->db->select('id');
        $this->db->from('tags');
        $this->db->where('name',$tagName);
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return empty($ra) ? 0 : $ra[0]['id'];
        
    }
    
    
    /**
     * Прикрепить тег к пользователю
     */
    public function addTagsToUser($fields) {
        
        $this->db->insert('tags_to_users', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Проверить, не прикреплён ли уже тег к пользователю
     */
    public function checkTagForUserStatus($tagID,$userID) {
        
        $this->db->select('id');
        $this->db->from('tags_to_users');
        $this->db->where('tag_id',$tagID);
        $this->db->where('user_id',$userID);
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return empty($ra) ? false : true;
        
    }
    
    
    
    /**
     * Получение информации из указанной таблицы
     */
    public function getContext($fields) {
        if ($fields['addColumn'] && $fields['addColumnValue'])
            $sql = "SELECT * FROM ".$fields['table']." WHERE ".$fields['column']." LIKE '%".$fields['item']."%' AND ".$fields['addColumn']." = '".$fields['addColumnValue']."' ORDER BY ".$fields['column']." ASC LIMIT ".$fields['limit'];
        else
            $sql = "SELECT * FROM ".$fields['table']." WHERE ".$fields['column']." LIKE '%".$fields['item']."%' ORDER BY ".$fields['column']." ASC LIMIT ".$fields['limit'];
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
}