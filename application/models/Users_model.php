<?php

class Users_model extends CI_Model {
    
    /**
     * Получение списка всех пользователей
     * 
     * @return type
     */
    public function getAllUsers($fields = '*',$shortMode = false) {
        
        //$sql = "SELECT * FROM users WHERE archive = 0 AND login != 'admin' ORDER BY surname ASC LIMIT ".$limit." OFFSET ".$offset;
        $sql = "SELECT $fields FROM users WHERE archive = 0 AND active = 1 AND login != 'admin' ORDER BY surname ASC";
        
        $this->db->select($fields);
        $this->db->from('users');
        $this->db->where('archive', 0);
        $this->db->where('active', 1);
        $this->db->where('login !=', 'admin');
        $this->db->order_by('surname ASC');
        $ra = $this->db->get()->result_array();
        
        if ($fields == '*') {
            foreach($ra as $rkey => $user) {
                $ra[$rkey] = $this->getOneUserInfoOverID($user['id'],$shortMode);
            }
        }
        
        return $ra;
    }
    
    
    
    public function getAllUsersForDialogs($excludedIDs = array(),$limit = 10) {
        
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('archive', 0);
        $this->db->where('active', 1);
        $this->db->where('login !=', 'admin');
        if (!empty($excludedIDs)) $this->db->where_not_in('id', $excludedIDs);
        $this->db->order_by('last_heartbeat DESC');
        $this->db->limit($limit);
        
        $ra = $this->db->get()->result_array();
        
        foreach($ra as $rkey => $user) {
            $ra[$rkey] = $this->getOneUserInfoOverID($user['id'],true);
        }
        
        return $ra;
    }
    
    
    
    /**
     * Результаты поиска
     */
    public function getUsersOverSearch($text = '',$includeSelf = false,$limit = 0,$excludedIDs = array(),$filteredUsersIDs = array()) {
        
        $addParam = $includeSelf ? '' : ' AND id != '.$this->session->userdata('userID'); 
        $text = mb_strtolower($text);
        
        $textA = explode(' ', $text);
        
        if (!empty($filteredUsersIDs)) {
            
            $fiteredIDs = '';
            
            foreach($filteredUsersIDs as $filterID) {
                $fiteredIDs .= $filterID.',';
            }
            
            if ($fiteredIDs) {
                $addParam .= ' AND ID in ('.mb_substr($fiteredIDs,0,-1).')';
            }
            
            
            
        }
        
        $addParam .= ' AND archive = 0 ';
        
        $rall = array();
        
        if (count($textA) == 1) {
            
            //Получить идентификаторы пользователей, которые подходят по тегу
            $this->db->select('tags_to_users.user_id');
            $this->db->from('tags_to_users');
            $this->db->join('tags', 'tags.id = tags_to_users.tag_id','left');
            $this->db->where('tags.entity_type', 'user');
            $this->db->like('lower(tags.name)', $text);
            
            $tagsUsers = '';
            
            $query = $this->db->get();
            $ra = $query->result_array();
            
            foreach($ra as $record) {
                $tagsUsers .= $record['user_id'].',';
            }
            
            $usersParam = '';
            
            if ($tagsUsers != '') {
                $tagsUsers = mb_substr($tagsUsers,0,-1);
                
                $usersParam = ' OR id in ('.$tagsUsers.')';
            }
            
            
            $sql = "SELECT id FROM users WHERE 
                    (LOWER(surname) LIKE '%".$text."%' OR
                    LOWER(name) LIKE '%".$text."%' OR
                    LOWER(middlename) LIKE '%".$text."%' OR 
                    LOWER(login) LIKE '%".$text."%' $usersParam)
                    $addParam ORDER BY surname ASC".($limit ? ' LIMIT '.$limit : '');
        } else {
            $sql = "SELECT id FROM users WHERE 
                    (
                    CONCAT(LOWER(surname),' ',LOWER(name),' ',LOWER(middlename)) LIKE '%".$text."%' OR
                    CONCAT(LOWER(surname),' ',LOWER(middlename),' ',LOWER(name)) LIKE '%".$text."%' OR
                    CONCAT(LOWER(middlename),' ',LOWER(surname),' ',LOWER(name)) LIKE '%".$text."%' OR
                    CONCAT(LOWER(middlename),' ',LOWER(name),' ',LOWER(surname)) LIKE '%".$text."%' OR
                    CONCAT(LOWER(name),' ',LOWER(surname),' ',LOWER(middlename)) LIKE '%".$text."%' OR
                    CONCAT(LOWER(name),' ',LOWER(middlename),' ',LOWER(surname)) LIKE '%".$text."%'
                    )
                    $addParam ORDER BY surname ASC".($limit ? ' LIMIT '.$limit : '');
        }
            
        $query = $this->db->query($sql);
        $rall = $query->result_array();

        $users = array();
        $usersIDs = array();
        
        if (!empty($rall)) {
            foreach($rall as $rkey => $user) {
                if ((empty($excludedIDs) || !in_array($user['id'], $excludedIDs)) && !in_array($user['id'],$usersIDs)) {
                
                    $ra[$rkey] = $this->getOneUserInfoOverID($user['id']);

                    $ra[$rkey]['inCart'] = 0;
                    foreach($this->cart->contents() as $item) {
                        if ($item['id'] == $user['id']) {
                            $ra[$rkey]['inCart'] = 1;
                            break;
                        }
                    }
                    
                    $users[] = $ra[$rkey];
                    $usersIDs[] = $user['id'];
                } 
                
            }
        }
        return $users;
    }
    
    
    
    /**
     * Получение общего количества пользователей
     */
    public function getUsersCount() {
        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('archive',0);
        $this->db->where('login != ','admin');
        return $this->db->count_all_results();
        
    }
    
    
    /**
     * Проверка логина
     * 
     */
    public function checkLogin($login,$userID = 0) {
        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('archive',0);
        if ($userID) $this->db->where('id != ',$userID);
        $this->db->group_start();
        $this->db->where('login',$login);
        $this->db->or_where('email',$login);
        $this->db->group_end();
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return empty($ra) ? false : true;
    }
    
    
    public function checkPass($login,$pass) {
        $sql = "SELECT id FROM users WHERE (login = '".$login."' OR email = '".$login."') AND password = '".$pass."' AND archive = 0";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        return empty($ra) ? false : true;
    }
    
    
    /**
     * Получение информации по одной конкретной учётной записи
     * 
     * @param type $login
     * @return type
     */
    public function getOneUserInfo($login) {
        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('login',$login);
        $this->db->or_where('email',$login);
        $this->db->limit(1);
        
        $ra = $this->db->get()->result_array();
        
        if (!empty($ra) && isset($ra[0]['id'])) {
            $ra = $this->getOneUserInfoOverID($ra[0]['id']);
        }
        
        return $ra;
        
    }
    
    
    /**
     * Получение информации о пользователе по ID
     */
    public function getOneUserInfoOverID($user_id = 1,$shortMode = false) {
        
        $cacheDriver = $this->config->item('config_cache_driver');
        
        if (!$this->cache->$cacheDriver->is_supported() || $this->cache->$cacheDriver->is_supported() && !$ra = $this->cache->$cacheDriver->get('db_user_'.$user_id)) {
            
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('id',$user_id);
            $this->db->limit(1);

            $query = $this->db->get();
            $ra = $query->result_array();
            
            if (!empty($ra)) {
                
                $ra = $ra[0];
                $ra['fullName'] = $this->getFullNameOverID($ra['id']);
                $ra['shortName'] = $this->getFullNameOverID($ra['id'],true);
                $ra['avatarURL'] = file_exists('./upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_big.png') && $ra['avatar'] != '' ? base_url().'upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_big.png?r='.time() : base_url().'assets/images/icon_user200_1.png';
                $ra['avatarURL_small'] = file_exists('./upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_small.png') && $ra['avatar'] != '' ? base_url().'upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_small.png?r='.time() : base_url().'assets/images/user_60x60_1.png';
                $ra['avatarURL_path'] = file_exists('./upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_small.png') && $ra['avatar'] != '' ? './upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_small.png' : './assets/images/user_60x60_1.png';
                $ra['avatarURL_conf'] = file_exists('./upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_conf.png') && $ra['avatar'] != '' ? base_url().'upload/users/'.$ra['id'].'/'.'avatar_'.$ra['id'].'_conf.png?r='.time() : '';
                $ra['reg_date'] = date($this->load->get_var('config_datetime_format'),$ra['timestamp']);
                $ra['last_date'] = $ra['last_heartbeat'] ? date($this->load->get_var('config_datetime_format'),$ra['last_heartbeat']) : '-';
                $ra['user_state'] = time() - $ra['last_heartbeat'] < $this->config->item('config_online_execution_time') ? 1 : 0;
                //Получить теги
                $ra['tags'] = $this->getUserTags($ra['id'],true);

                $ra['birthday_status'] = $ra['birthday'] ? date("d.m",$ra['birthday']) == date("d.m") ? 1 : 0 : 0;
                $ra['seating'] = array();
                if (!$shortMode && $this->load->get_var('config_components_seating')) $ra['seating'] = $this->getUserSeating($ra['id']);

            } else {
                $ra = array();
            }
            
            //Сохраняем кэш
            $this->cache->$cacheDriver->save('db_user_'.$user_id, $ra, $this->config->item('config_cache_view_time') * 60);
            
        }
        
        if (!empty($ra) && isset($ra['id'])) {
            $ra['inCart'] = 0;
            foreach($this->cart->contents() as $item) {
                if ($item['id'] == $ra['id']) {
                    $ra['inCart'] = 1;
                    break;
                }
            }
        }
        
        return $ra;
    }
    
    
    /**
     * 
     * Получение информации об учётной записи по ldap-логину
     * 
     * @param type $ldap_login
     * @return type
     */
    public function getOneUserInfoOverLDAPLogin($ldap_login) {
        $sql = "SELECT id FROM users WHERE archive = 0 AND ldap_login = '".$ldap_login."' LIMIT 1";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        if (!empty($ra)) {
            $ra = $this->getOneUserInfoOverID($ra[0]['id']);
        }
        
        return $ra;
    }
    
    
    
    /**
     * Создание учётной записи
     */
    public function createUser($fields) {
        
        $sql = "SELECT id FROM users WHERE (login = '".$fields['login']."' OR email = '".$fields['login']."')";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        if (!empty($ra)) {
            
            //Такой логин уже был... вот беда, обнуляем к чертям
            $this->db->where('id', $ra[0]['id']);
            $this->db->update('users', array('login' => $fields['login'].'_'.$ra[0]['id'],'email' => $fields['login'].'_'.$ra[0]['id']));
            
        }
        
        
        
        $notification_types = explode(',',$this->config->item('config_notification_types'));
        
        $settings = array();
        foreach($notification_types as $notifyType) {
            $settings[$notifyType] = 1;
        }
        
        $insertFields = array(
            'login' => $fields['login'],
            'password' => @crypt($fields['password']),
            'email' => $fields['email'],
            'name' => $fields['name'],
            'middlename' => $fields['middlename'],
            'surname' => $fields['surname'],
			'group' => $fields['group'],
            'active' => 1,
            'user_type' => isset($fields['user_type']) ? $fields['user_type'] : 'user',
            'timestamp' => time(),
            'ldap_login' => isset($fields['ldap_login']) ? $fields['ldap_login'] : '',
            'comment' => isset($fields['comment']) ? pg_escape_string($fields['comment']) : '',
            'settings' => serialize($settings)
        );
        
        $this->db->insert('users', $insertFields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Обновление информации о пользователе
     */
    public function updateUser($userID,$fields = array()) {
        
        if (isset($fields['login'])) {
        
            $this->db->select('id');
            $this->db->from('users');
            $this->db->group_start();
            $this->db->where('login',$fields['login']);
            $this->db->or_where('email',$fields['login']);
            $this->db->group_end();
            $this->db->where('id !=',$userID);
            $query = $this->db->get();
            $ra = $query->result_array();

            if (!empty($ra)) {

                //Такой логин уже был... вот беда, обнуляем к чертям
                $this->db->where('id', $ra[0]['id']);
                $this->db->update('users', array('login' => $fields['login'].'_'.$ra[0]['id'],'email' => $fields['login'].'_'.$ra[0]['id']));

            }

            $this->db->reset_query();
        }
        
        $this->db->where('id', $userID);
        $this->db->update('users', $fields);
        
        $this->clearUsersCache($userID);
        
        return $this->getOneUserInfoOverID($userID);
        
    }
    
    
    /**
     * Проверить активность пользователя
     */
    public function checkActivity($id = 0){
        
        $user = $this->getOneUserInfoOverID($id);
        
        if (!empty($user) && !$user['archive'] && $user['active'])
            return true;
        else
            return false;
    }
    
    
    /**
     * Получить полное имя
     */
    public function getFullNameOverID($user_id,$use_initials = false) {
        
        if ($user_id) {
            
            $cacheDriver = $this->config->item('config_cache_driver');
        
            if (!$this->cache->$cacheDriver->is_supported() || $this->cache->$cacheDriver->is_supported() && !$name = $this->cache->$cacheDriver->get('db_user_name_'.($use_initials ? 'full' : 'short').'_'.$user_id)) {
                
                $sql = "SELECT surname,name,middlename FROM users WHERE id = ".$user_id." LIMIT 1";
                $query = $this->db->query($sql);
                $ra = $query->result_array();
             
                if (!$use_initials)
                    $name = empty($ra) ? '' : $ra[0]['surname'].' '.$ra[0]['name'].' '.$ra[0]['middlename'];
                else
                    $name = empty($ra) ? '' : $ra[0]['surname'].' '.mb_substr($ra[0]['name'],0,1).'. '.($ra[0]['middlename'] ? mb_substr($ra[0]['middlename'],0,1).'.' : '');

                $this->cache->$cacheDriver->save('db_user_name_'.($use_initials ? 'full' : 'short').'_'.$user_id, $name, $this->config->item('config_cache_view_time') * 60);
                
                
            }
            
            return $name;
            
            
        } else {
            return '';
        }
        
    }
    
    
    /**
     * Получить всех пользователей LDAP
     */
    public function getAllLDAPUsers() {
        $sql = "SELECT * FROM users WHERE archive = 0 AND ldap_login != ''";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    /**
     * Получение новых LDAP-пользователей (которых нет в нашей базе)
     */
    public function getNewLDAPUsers() {
        
        $ldap_users = array();
        $ldap_logins = array();
    
        if (!extension_loaded('ldap')) {
            throw new Exception($this->lang->line('error_ldap_module_not_loaded'),400);
        } else {
            $ds = ldap_connect($this->config->item('config_ldap_server'));
            
            if ($ds) {

                try {
                    $sr = ldap_search($ds, $this->config->item('config_ldap_dc'),"(uid=*)");
                    
                    if ($sr && ldap_count_entries($ds, $sr) != 0) {

                        $info = ldap_get_entries($ds, $sr);

                        foreach ($info as $users) {

                            if ($users['uid'][0] != '') {
                                $user = self::getOneUserInfoOverLDAPLogin($users['uid'][0]);
                                if (empty($user)) {

                                    /*
                                     * Проверить по вторичной группе
                                     */
                                    
                                    $userGroup = '';

                                    $secGroup = $this->config->item('config_ldap_secondary_groups');
                                    $secGroupArray = explode(',', $secGroup);
                                    foreach($secGroupArray as $group) {
                                        if ($group != '') {
                                            $sr2 = ldap_search($ds, "cn = ".$group.", ".$this->config->item('config_ldap_dc_groups'),"(objectClass=*)");
                                            $info2 = ldap_get_entries($ds, $sr2);
                                            foreach($info2[0]['memberuid'] as $grMember) {
                                                if ($grMember == $users['uid'][0]) {
                                                    $userGroup = $group;
                                                    break;
                                                }
                                            }
                                            
                                            if ($userGroup != '') break;
                                        }
                                    }
                                    
                                    
                                    if ($userGroup != '') $ldap_users[] = array('login' => $users['uid'][0],'fullname' => $users['cn'][0],'group' => $userGroup);

                                }

                                $ldap_logins[] = $users['uid'][0];
                            }
                        }

                    }

                    ldap_close($ds);

                    /*
                     * Выявить "Лишние" учётные записи и удалить их
                     */
                    $real_logins_arr = self::getAllLDAPUsers();
                    foreach($real_logins_arr as $query) {
                        $real_logins[] = $query['ldap_login'];
                    }

                    $diff_logins = array_diff($real_logins,$ldap_logins);
                    foreach($diff_logins as $diff_login) {
                        $user = self::getOneUserInfoOverLDAPLogin($diff_login);
                        if (!empty($user)) {
                            //Проверка того, что логин не админа и не суперадминистратора
                            if (!in_array($user['login'],array('admin','superuser'))) {
                                @unlink('./upload/users/'.$user['id']);
                                self::updateUser($user['id'],array('archive' => 1));
                            }
                        }
                    }
                } catch (Exception $ex) {
                    throw new Exception($this->lang->line('error_ldap_server_errors').': '.$ex->getMessage().' ('.$ex->getCode().')',500);
                }

            }

        }
        
        //$ldap_users[] = array('login' => 'ruk_ob','fullname' => 'Иванов Фарид Иванович','description' => 'Руководитель обучения','group' => 'student');
        //$ldap_users[] = array('login' => 'op1','fullname' => 'Смирнов Ленар Андреевич','description' => 'Обучаемый','group' => 'teacher');
        //$ldap_users[] = array('login' => 'op2','fullname' => 'Сорокин Николай Сергеевич','description' => 'Обучаемый','group' => 'student');

        return $ldap_users;
        
    }
    
    
    /**
     * Создание роли (при использовании LDAP
     */
    public function createRole($ldap_login) {
        
        $checking = $this->checkDBRole($ldap_login);
        if (!$checking) {
            $sql = 'CREATE ROLE "'.$ldap_login.'" LOGIN CREATEROLE';
            $this->db->query($sql);
        }
    }
 
    
    /**
     * GRANT роли
     */
    public function grantRole($ldap_login,$role) {
        
        $sql = 'GRANT '.$role.' to '.$ldap_login;
        $this->db->query($sql);
        
    }
    
    
    /**
     * Удаление данных, связанных с пользователем
     */
    public function removeUserInfo($userID) {
        
        $sql = "DELETE FROM users_to_rooms WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM tags_to_users WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM users_to_posts WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        /*
         * Удалить данные опросов
         */
        $sql = "DELETE FROM polls_results WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_answers WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_users_invites WHERE user_id = ".$userID;
        $this->db->query($sql);

	$sql = "DELETE FROM users_to_posts WHERE user_id = ".$userID;
        $this->db->query($sql);
        
        /*
         * Удалить статусы объявлений пользователя (для чистой статистики)
         */
        $this->db->where('creator_id',$userID);
        $this->db->delete('note_users_status');
        
        $this->db->where('user_id',$userID);
        $this->db->delete('note_users_positions');
        
        $this->db->where('user_id', $userID);
        $this->db->delete('users_to_projects');

        $this->clearUsersCache($userID);
        
    }
    
    
    public function removeUserDirectory($dir) {

        if (is_dir($dir)) {
            if($objs = glob($dir."/*")) {
                foreach($objs as $obj) {
                    is_dir($obj) ? $this->removeUserDirectory($obj) : unlink($obj);
                    is_dir($obj) ? @rmdir($obj) : null;
                }
            }
            
            @rmdir($dir);
        }
        
    }
    
    
    
    /**
     * Проверка наличия роли
     */
    public function checkDBRole($role) {
        
        $sql = "SELECT rolname FROM pg_roles";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        foreach($ra as $value) {
            if ($value['rolname'] == $role) return true;
        }
        return false;
        
    }
    
    
    /**
     * Получение тегов пользователя
     */
    public function getUserTags($user_id,$instring = false) {
        
        $sql = "SELECT t.* FROM tags t LEFT JOIN tags_to_users ttu ON ttu.tag_id = t.id WHERE ttu.user_id = ".$user_id." ORDER BY name ASC";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        if (!$instring) {
            
            return $ra;
            
        } else {
            
            $tags_str = '';
            foreach($ra as $value) {
                $tags_str .= '<span class="oneIridaTag one-irida-tag">'.htmlspecialchars($value['name']).'</span>';
            }
            
            return $tags_str;
            
        }
        
    }
    
    
    /**
     * Получение последнего идентификатора
     */
    public function getLastID() {
        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->order_by('id','DESC');
        $this->db->limit(1); 
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return empty($ra) ? 0 : $ra[0]['id'];
        
    }
    
    
    /**
     * Просмотр всех дополнительных полей для учётной записи
     */
    public function getAllAccountFields($onlyadminedited = false,$groupByType = false) {
        
        if ($groupByType) $fields = array('profile' => array(),'contacts' => array());
        
        if (!$onlyadminedited)
            $sql = "SELECT * FROM account_fields ORDER BY name ASC";
        else
            $sql = "SELECT * FROM account_fields WHERE admin_changing = 1 ORDER BY name ASC";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        foreach($ra as &$record) {
            $record['count'] = $this->getFieldUsingCount($record['id']);
            if ($groupByType) {
                $fields[$record['info_type']][] = $record;
            }
        }
        return $groupByType ? $fields : $ra;
        
    }
    
    
    /**
     * Получение количества использования поля
     */
    public function getFieldUsingCount($fieldId) {
        
        $this->db->where('field_id', $fieldId);
        $this->db->where('value !=', '');
        $this->db->from('fields_to_users');
        return $this->db->count_all_results();
        
    }
    
    /**
     * Создание поля учётной записи
     */
    public function createField($fields){
        
        $this->db->insert('account_fields', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Изменение поля учётной записи
     */
    public function updateField($id,$fields) {
        
        $this->db->where('id', $id);
        $this->db->update('account_fields', $fields);
        
    }
    
    
    /**
     * Получение информации по одному полю учётной записи
     */
    public function getOneField($id) {
        
        $this->db->select('*');
        $this->db->from('account_fields');
        $this->db->where('id', $id);
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
        if (!empty($ra)) {
            $ra = $ra[0];
            $ra['count'] = $this->getFieldUsingCount($ra['id']);
        }
        
        return $ra;
        
    }
    
    
    /**
     * Удаление информации о поле
     */
    public function removeField($id) {
        $sql = "DELETE FROM account_fields WHERE id = ".$id;
        $this->db->query($sql);
        
        $sql = "DELETE FROM fields_to_users WHERE field_id = ".$id;
        $this->db->query($sql);
    }
    
    
    /**
     * Получение списка дополнительных полей
     */
    public function getProfileAdditionalFields($userID,$onlyadminedited = false) {
        
        $fields = array('profile' => array(),'contacts' => array());
        
        // Узнать идентификатор... если нет такого поля, то создать его
        if (!$onlyadminedited)
            $sql = "SELECT af.* FROM account_fields af WHERE af.status = 1 ORDER BY af.name ASC";
        else
            $sql = "SELECT af.* FROM account_fields af WHERE af.admin_changing = 1 ORDER BY af.name ASC";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        foreach($ra as $record):
            $record['value'] = $this->getAdditionalFieldValue($record['id'], $userID);
            $fields[$record['info_type']][] = $record;
        endforeach;
            
        return $fields;
    }
    
    
    public function getAdditionalFieldValue($fieldID,$userID,$onlyValue = true) {
        $sql = "SELECT value FROM fields_to_users WHERE field_id = $fieldID AND user_id = $userID";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        if ($onlyValue)
            return empty($ra) ? '' : $ra[0]['value'];
        else
            return empty ($ra) ? $ra : $ra[0];
        
    }
    
    
    /**
     * Обновление дополнительного поля
     */
    public function updateAdditionalField($fieldID,$userID,$value) {
        
        // Узнать идентификатор... если нет такого поля, то создать его
        $fieldValueRecord = $this->getAdditionalFieldValue($fieldID, $userID,false);
        
        if (empty($fieldValueRecord)) {
            
            //$sql = "INSERT INTO fields_to_users (field_id,user_id,value,creator_id,ts) VALUES ('".$fieldID."','".$userID."','".pg_escape_string($value)."','".$this->session->userdata['userID']."','".time()."')";
            
            $fields = array(
                'field_id' => $fieldID,
                'user_id' => $userID,
                'value' => $value,
                'creator_id' => $this->session->userdata['userID'],
                'ts' => time()
            );
            
            $this->db->insert('fields_to_users', $fields);
            
        } else {
            
            //$sql = "UPDATE fields_to_users SET value = '".pg_escape_string($value)."' WHERE field_id = ".$fieldID." AND user_id = ".$userID;
            
            $this->db->where('field_id', $fieldID);
            $this->db->where('user_id', $userID);
            $this->db->update('fields_to_users', array('value' => $value));
            
        }
        
        $cacheDriver = $this->config->item('config_cache_driver');
        $this->cache->$cacheDriver->delete('db_user_'.$userID);
    }
    
    
    /**
     * Проверить существование учётной записи с указанной почтой
     */
    public function checkMail($email = '') {
        
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email',$email);
        $ra = $this->db->get()->result_array();
        
        return empty($ra) ? false : true;
        
    }
    
    
    
    /**
     * Добавить хэш
     */
    public function addRestoreHash($fields) {
        
        $this->db->insert('restore_hashs', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Проверка существования хэша
     */
    public function checkHash($hash = '') {
        
        $this->db->select('id');
        $this->db->from('restore_hashs');
        $this->db->where('hash',$hash);
        $ra = $this->db->get()->result_array();
        
        return empty($ra) ? false : true;
        
    }
    
    
    /**
     * Получение учётной записи по хэшу
     */
    public function getUserOverHash($hash = '',$type = 'mail') {
        
        $sql = "SELECT * FROM users WHERE email in (SELECT input FROM restore_hashs WHERE hash = '".$hash."')";
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        return empty($ra) ? array() : $ra[0];
        
    }
    
    
    /**
     * Удаление хэша
     */
    public function removeHash($input = '',$type = 'mail') {
        
        $sql = "DELETE FROM restore_hashs WHERE input = '".$input."'";
        $this->db->query($sql);
        
    }
    
    
    /**
     * Получение настроек пользователя
     */
    public function getUsersSettings($userID) {
        
        $cacheDriver = $this->config->item('config_cache_driver');
        
        $ra = array();

        if (!$this->cache->$cacheDriver->is_supported() || $this->cache->$cacheDriver->is_supported() && !$ra = $this->cache->$cacheDriver->get('db_user_settings_'.$userID)) {
            
            $this->db->select('settings');
            $this->db->from('users');
            $this->db->where('id',$userID);

            $ra = $this->db->get()->result_array();
            
            $ra = !empty($ra) ? $ra[0]['settings'] : array();
            
            //Сохраняем кэш
            $this->cache->$cacheDriver->save('db_user_settings_'.$userID, $ra, $this->config->item('config_cache_view_time') * 60);
            
        }
        
        return $ra;
        
    }
    
    
    /**
     * Получение комнаты пользователя
     */
    public function getUserSeating($userID) {
        $sql = "SELECT eq.audience_id,eq.floor_id FROM
                equipments eq,users u
                WHERE (u.remote_host = eq.hostname OR u.remote_ip = eq.ip) AND u.id = ".$userID." AND (eq.audience_id != 0 OR eq.floor_id != 0)";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        if (empty($ra)) 
            return $ra;
        else {
            //как жаль, что этажи и комнаты в разных таблицах...
            $info = array();
            if ($ra[0]['audience_id']) {
                $info = $this->seating_model->getOneFlat($ra[0]['audience_id'],true);
                $info['type'] = 'flat';
            } else {
                $info = $this->getOneFloor($ra[0]['floor_id'],true);
                $info['type'] = 'floor';
            }
            
            return $info;
        }
    }
    
    
    public function getUserFilesOverDir($dir,&$files) {
        if($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? !mb_strpos($obj,'old') ? $this->getUserFilesOverDir($obj,$files) : '' : $files[] = $obj;
            }
        }
    }
    
    
    public function getFavoriteUsersIDs($creatorID) {
        
        $this->db->select('user_id');
        $this->db->where('creator_id',$creatorID);
        $this->db->from('users_favorite_to_users');
        
        $ra = $this->db->get()->result_array();

        $ids = array();
        
        foreach($ra as $record) {
            $ids[] = $record['user_id'];
        }
        
        return $ids;
        
    }
    
    
    /**
     * Удалить из избранного
     */
    public function usersFavoriteStatusRemove($userID,$creatorID) {
        
        $this->db->where('creator_id',$creatorID);
        $this->db->where('user_id',$userID);
        $this->db->delete('users_favorite_to_users');
        
    }
    
    
    public function usersFavoriteStatusAdd($fields) {
        
        $this->db->insert('users_favorite_to_users', $fields);
        return $this->db->insert_id();
        
    }
    
    
    
    public function clearUsersCache($userID) {
        
        $cacheDriver = $this->config->item('config_cache_driver');
        
        $this->cache->$cacheDriver->delete('db_user_'.$userID);
        $this->cache->$cacheDriver->delete('db_user_name_full_'.$userID);
        $this->cache->$cacheDriver->delete('db_user_name_short_'.$userID);
        
        $this->cache->$cacheDriver->delete('db_user_settings_'.$userID);
        
    }
    
    
}