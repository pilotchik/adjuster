<?php

class Polls_model extends CI_Model {
    
    /**
     * Получение списка активных опросов для конкретного пользователя
     * 
     */
    public function getPollsForMe($userID) {
        
        $sql = "SELECT p.* FROM polls p WHERE 
            (p.access = 'public' AND p.moderator_id != 0 OR p.access = 'persons' AND p.id IN (SELECT poll_id FROM polls_users_invites WHERE user_id = ".$userID."))
            AND p.public_status = 1 AND p.active = 1 AND p.creator_id != ".$userID." ORDER BY p.title";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        foreach($ra as $key=>$record) {
            
            $pollResult = $this->getMyPollResult($userID,$record['id']);
            $ra[$key]['poll_finish'] = $record['poll_finish'] = empty($pollResult) ? 0 : $pollResult['info']['ts_end'];
            
            $ra[$key]['creatorName'] = $this->users_model->getFullNameOverID($record['creator_id']);
            $ra[$key]['createDate'] = date($this->load->get_var('config_datetime_format'),$record['ts']);
            $ra[$key]['finishDate'] = date($this->load->get_var('config_datetime_format'),$record['poll_finish']);
            $ra[$key]['respsCount'] = $this->getPollRespondentsCount($record['id']);
            $ra[$key]['questsCount'] = count($this->getPollQuests($record['id']));
        }
        return $ra;
    }
    
    
    /**
     * Получение списка созданных пользователем вопросов
     */
    public function getMyPolls($userID,$active = true,$roomID = 0) {
        
        //$sql = "SELECT p.* FROM polls p
        //   WHERE p.creator_id = ".$userID." ".($active ? ' AND p.active = 1' : ' AND p.active=0')." ORDER BY p.title";
        
        $this->db->select('*');
        $this->db->from('polls');
        $this->db->where('room_id', $roomID);
        $this->db->where('creator_id', $userID);
        if ($active) 
            $this->db->where('active', 1);
        else
            $this->db->where('active', 0);
        $this->db->order_by('title', 'ASC');
        
        $query = $this->db->get();
        $ra = $query->result_array();
        
		$this->load->model('sked_model');
        
        foreach($ra as $key=>$record) {
            
            $pollResult = $this->getMyPollResult($userID,$record['id']);
            
            $ra[$key]['poll_finish'] = $record['poll_finish'] = empty($pollResult) ? 0 : $pollResult['info']['ts_end'];
            
            $ra[$key]['invited_users'] = array();
            if ($record['access'] == 'persons') {
                $sql = "SELECT pui.user_id FROM polls_users_invites pui,users u WHERE pui.poll_id = ".$record['id']." AND pui.user_id = u.id ORDER BY u.surname";
                $subquery = $this->db->query($sql);
                $subra = $subquery->result_array();
                foreach($subra as $key2=>$record2) {
                    $subra[$key2]['finish'] = $this->getUserFinishStatus($record['id'],$record2['user_id']);
                    $subra[$key2]['finishDate'] = $subra[$key2]['finish'] ? date($this->load->get_var('config_datetime_format'),$subra[$key2]['finish']) : ''; 
                }
                $ra[$key]['invited_users'] = $subra;
            }
            
            $ra[$key]['createDate'] = date('d.m.Y',$record['ts']);
            $ra[$key]['finishDate'] = date($this->load->get_var('config_datetime_format'),$record['poll_finish']);
            $ra[$key]['respsCount'] = $this->getPollRespondentsCount($record['id']);
            $ra[$key]['questsCount'] = count($this->getPollQuests($record['id']));
			$ra[$key]['themeName'] = $this->sked_model->getOneSkedItemName($record['theme_id']);
			
        }
        return $ra;
    }
    
    
    /**
     * Получение статуса прохождения опроса
     */
    public function getUserFinishStatus($pollID,$userID) {
        
        $this->db->select('ts_end');
        $this->db->from('polls_results');
        $this->db->where('poll_id', $pollID);
        $this->db->where('user_id', $userID);
        $ra = $this->db->get()->result_array();
        
        return empty($ra) ? 0 : $ra[0]['ts_end'];
        
    }
    
    
    /**
     * Получение количество пользователей, завершивших опрос
     */
    public function getPollRespondentsCount($pollID, $withInProcess = false) {
        
        $this->db->from('polls_results');
        
        $this->db->where('poll_id', $pollID);
        if (!$withInProcess)
            $this->db->where('ts_end !=', 0);
        
        return $this->db->count_all_results();
        
    }
    
    
    /**
     * Получение вопросов опроса
     */
    public function getPollQuests($pollID,$onlyActive = true) {
        
        $this->db->select('*');
        $this->db->from('polls_quests');
        $this->db->where('poll_id', $pollID);
        if ($onlyActive) $this->db->where('active', 1);
        $this->db->order_by('numb ASC, title ASC');
        $ra = $this->db->get()->result_array();
        
        foreach($ra as $key=>$record) {
            $ra[$key]['vars'] = $record['data'] != '' ? unserialize($record['data']) : array();
			
			$aa = explode('|', $record['answer']);
			$ra[$key]['html'] = '';
			foreach($aa as $a) {
				if ($a != '')
					$ra[$key]['html'] .= '<div class="answer-span" style="background-image:url(\''.base_url().'assets/images/answers/'.$a.'.gif\')"></div>';	
			}
			
        }
        
        return $ra;
        
    }
	
	
	
    /**
     * Добавление опроса
     * 
     * @param mixed $fields - параметры
     */
    public function createPoll($fields = array()) {
        
        $this->db->insert('polls', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Изменение параметров опроса
     */
    public function updatePoll($id,$fields) {
        
        $this->db->where('id', $id);
        $this->db->update('polls', $fields);
        
    }
    
    
    /**
     * Удаление опроса
     */
    public function removePoll($pollID) {
        $sql = "DELETE FROM polls_users_invites WHERE poll_id = ".$pollID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls WHERE id = ".$pollID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_quests WHERE poll_id = ".$pollID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_answers WHERE poll_id = ".$pollID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_results WHERE poll_id = ".$pollID;
        $this->db->query($sql);
        
    }
    
    
    /**
     * Добавление пользователей к опросу
     */
    public function inviteUserInPoll($fields = array()) {
        
        $this->db->insert('polls_users_invites', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Удаление всех инвайтов
     */
    public function removeInvitesUserInPoll($pollID) {
        $sql = "DELETE FROM polls_users_invites WHERE poll_id = ".$pollID;
        $this->db->query($sql);
    }
    
    
    /**
     * Получение целевых респондентов
     */
    public function getInvitedUsersInPoll($pollID,$onlyIDs = false) {
        
        $this->db->select('user_id');
        $this->db->from('polls_users_invites');
        $this->db->where('poll_id', $pollID);
        $ra = $this->db->get()->result_array();
        
        if (!$onlyIDs) {
            
            return $ra;
            
        } else {
            $users = array();
            foreach($ra as $record) {
                $users[] = $record['user_id'];
            }
            return $users;
        }
        
    }
    
    
    /**
     * Получение информации по конкретному опросу
     */
    public function getOnePollOverID($pollID,$withResults = false) {
        
        $this->db->select('*');
        $this->db->from('polls');
        $this->db->where('id', $pollID);
        $query = $this->db->get();
        $ra = $query->result_array();
        
        if (empty($ra)) {
            return $ra;
        } else {
            $poll = $ra[0];
            
            $poll['date_format'] = date($this->load->get_var('config_datetime_format'),$poll['ts']);
            $poll['quests'] = $this->getPollQuests($poll['id'],false);
            $poll['respondents'] = $this->getPollRespondentsCount($poll['id'],true);
            
            if ($withResults) {
                
                foreach($poll['quests'] as $key => $quest) {
                    
                    $poll['quests'][$key]['answers'] = $this->getQuestResults($poll['id'],$quest['id'],true);
                    
                    $vars_results = array();
                    
                    if ($quest['type'] == 'selectone' || $quest['type'] == 'selectmany') {
                        foreach($quest['vars'] as $varkey => $var) {

                            $temp['text'] = $var;
                            $temp['count'] = 0;
                            $temp['users_string'] = '';

                            foreach($poll['quests'][$key]['answers'] as $answer) {
                                if (in_array($varkey, $answer['vars_keys'])) {
                                    if (!$answer['anonStatus']) $temp['users_string'] .= $answer['fullName'].', ';
                                    $temp['count']++;
                                }
                            }

                            if ($temp['users_string'] != '') $temp['users_string'] = mb_substr($temp['users_string'],0,-2);

                            $temp['proz'] = $temp['count'] ? round($temp['count']/$poll['respondents']*100,1) : 0;
                            
                            $vars_results[] = $temp;
                        }
                        
                    } elseif ($quest['type'] == 'selectscale') {
                        
                        for($i = 1; $i <= $quest['vars'][2];$i++) {
                            if ($i == 1) {
                                $temp['text'] = $quest['vars'][0];
                            } elseif ($i == $quest['vars'][2]) {
                                $temp['text'] = $quest['vars'][1];
                            } else {
                                $temp['text'] = $i;
                            }
                            $temp['count'] = 0;
                            $temp['users_string'] = '';
                            
                            foreach($poll['quests'][$key]['answers'] as $answer) {
                                if (in_array($i, $answer['vars_keys'])) {
                                    if (!$answer['anonStatus']) $temp['users_string'] .= $answer['fullName'].', ';
                                    $temp['count']++;
                                }
                            }

                            if ($temp['users_string'] != '') $temp['users_string'] = mb_substr($temp['users_string'],0,-2);

                            $temp['proz'] = $temp['count'] ? round($temp['count']/$poll['respondents']*100,1) : 0;
                            
                            $vars_results[] = $temp;
                        }
                    }
                    
                    $poll['quests'][$key]['vars_results'] = $vars_results;
                }
                
                
            }
            
            return $poll;
        }
        
    }
    
    
    /**
     * Получить опросы для комнаты
     * 
     * @param type $roomID
     * @return type
     */
    public function getPollsForRoom($roomID) {
        
        $this->db->select('id');
        $this->db->from('polls');
        $this->db->where('room_id', $roomID);
        $this->db->order_by('ts', 'DESC');
        $query = $this->db->get();
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            $ra[$rkey] = $this->getOnePollOverID($record['id'],true);
        }
        
        return $ra;
        
    }
    
    
    
    /**
     * Добавление вопроса в опрос
     */
    public function createPollQuest($fields) {
        
        $this->db->insert('polls_quests', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Удаление вопроса опроса
     */
    public function removePollQuest($questID) {
        
        $this->db->where('id',$questID);
        $this->db->delete('polls_quests');
        
        $this->db->where('poll_quest_id',$questID);
        $this->db->delete('polls_answers');
        
    }
    
    
    /**
     * Обновление параметров вопроса
     */
    public function updatePollQuest($id,$fields) {
        
        $this->db->where('id', $id);
        $this->db->update('polls_quests', $fields);
        
    }
    
    
    /**
     * Получение ответов на вопрос
     */
    public function getQuest($questID) {
        
        $this->db->select('*');
        $this->db->from('polls_quests');
        $this->db->where('id', $questID);
        $query = $this->db->get();
        $ra = $query->result_array();
        
        return empty($ra) ? $ra : $ra[0];
        
    }
    
    
    /**
     * Получение статус прохождения опроса
     */
    public function getMyPollResult($userID,$pollID) {
        
        $this->db->select('*');
        $this->db->from('polls_results');
        $this->db->where('poll_id', $pollID);
        $this->db->where('user_id', $userID);
		$this->db->order_by('ts_begin','DESC');
        $ra = $this->db->get()->result_array();
        
        if (empty($ra)) {
            
            return array();
            
        } else {
            
            $answer['info'] = $ra[0];
            
            //Получить пройденные вопросы
            $this->db->select('*');
            $this->db->from('polls_answers');
            $this->db->where('result_id', $ra[0]['id']);
            $answer['answers'] = $this->db->get()->result_array();
            
            return $answer;
        }
        
    }
    
    
    /**
     * Удалить все результаты пользователя по опросу
     */
    public function removeMyPollResult($userID,$pollID) {
        $sql = "DELETE FROM polls_results WHERE poll_id = ".$pollID." AND user_id = ".$userID;
        $this->db->query($sql);
        
        $sql = "DELETE FROM polls_answers WHERE poll_id = ".$pollID." AND user_id = ".$userID;
        $this->db->query($sql);
    }
    
    
    /**
     * Обновить запись о результате
     */
    public function updateMyPollResult($id,$fields) {
        
        $this->db->where('id', $id);
        $this->db->update('polls_results', $fields);
        
    }
    
    
    /**
     * Старт опроса
     */
    public function startPollPlay($userID,$pollID,$anonStatus = 1) {
        
        $fields = array(
            'poll_id' => $pollID,
            'ts_begin' => time(),
            'user_id' => $userID,
            'anon_status' => $anonStatus
        );
        
        $this->db->insert('polls_results', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Удаление ответа пользователя
     */
    public function removeUserAnswer($userID,$pollID,$questID) {
        $sql = "DELETE FROM polls_answers WHERE poll_quest_id = ".$questID." AND user_id = ".$userID." AND poll_id = ".$pollID;
        $this->db->query($sql);
    }
    
    
    /**
     * Обновление ответа
     */
    public function updateUserAnswer($userID,$pollID,$questID,$fields) {
        
        $this->db->where('poll_quest_id', $questID);
        $this->db->where('user_id', $userID);
        $this->db->where('poll_id', $pollID);
        $this->db->update('polls_answers', $fields);
        
    }
    
    
    /**
     * Регистрация ответа
     */
    public function createUserAnswer($fields) {
        
        $this->db->insert('polls_answers', $fields);
        return $this->db->insert_id();
        
    }
    
    
    /**
     * Получение ответов
     */
    public function getQuestResults($pollID,$questID,$withInProcess = false) {
        
        $sql = "SELECT * FROM polls_answers WHERE poll_id = ".$pollID." AND poll_quest_id = ".$questID." AND result_id IN (SELECT id FROM polls_results WHERE ".(!$withInProcess ? " ts_end != 0 AND " : "")." poll_id = ".$pollID.")";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        foreach($ra as $key=>$record) {
            $ra[$key]['vars_keys'] = $record['data'] != '' ? unserialize($record['data']) : array();
            $ra[$key]['fullName'] = $this->users_model->getFullNameOverID($record['user_id'],true);
            $ra[$key]['anonStatus'] = $this->getResultAnonStatus($record['user_id'],$pollID);
        }
        return $ra;
    }
    
    
    /**
     * Получить статус анонимности на момент прохождения опроса
     */
    public function getResultAnonStatus($userID,$pollID) {
        $sql = "SELECT anon_status FROM polls_results WHERE poll_id = ".$pollID." AND user_id = ".$userID;
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        return empty($ra) ? 1 : $ra[0]['anon_status'];
    }
    
 
    /**
     * Получить все опросы, нуждающиеся в модерации
     */
    public function getPublicPolls() {
        
        $sql = "SELECT p.* FROM polls p WHERE 
            p.access = 'public' AND p.active = 1 AND p.public_status = 1 ORDER BY p.title";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        foreach($ra as $key=>$record) {
            
            $ra[$key]['creatorName'] = $this->users_model->getFullNameOverID($record['creator_id']);
            $ra[$key]['moderatorName'] = $record['moderator_id'] ? $this->users_model->getFullNameOverID($record['moderator_id']) : '';
            $ra[$key]['createDate'] = date($this->load->get_var('config_datetime_format'),$record['ts']);
            $ra[$key]['respsCount'] = $this->getPollRespondentsCount($record['id']);
            $ra[$key]['quests'] = $this->getPollQuests($record['id']);
            $ra[$key]['questsCount'] = count($ra[$key]['quests']);   
        }   
        return $ra;
    }
    
    
    /**
     * Получить количество активных непройденных опросов
     */
    public function getActivePollsCount($userID) {
        
        $sql = "SELECT COUNT(p.*) as p_count FROM polls p WHERE 
            (p.access = 'public' AND p.moderator_id != 0 OR p.access = 'persons' AND p.id IN (SELECT poll_id FROM polls_users_invites WHERE user_id = ".$userID."))
            AND p.public_status = 1  AND p.room_id = 0  AND p.active = 1 AND p.creator_id != ".$userID." AND p.id NOT IN (SELECT poll_id FROM polls_results WHERE user_id = ".$userID." AND ts_end != 0)";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        return (int)$ra[0]['p_count'];
    }
    
    
    
    public function getOneRoomActivePolls($roomID,$userID) {
        
        //$sql = "SELECT p.* FROM polls p WHERE p.public_status = 1 AND p.room_id = ".$roomID."  AND p.active = 1 AND p.creator_id != ".$userID." AND p.id NOT IN (SELECT poll_id FROM polls_results WHERE user_id = ".$userID." AND ts_end != 0)";
        $sql = "SELECT p.id FROM polls p WHERE p.room_id = ".$roomID."  AND p.active = 1 AND p.id NOT IN (SELECT poll_id FROM polls_results WHERE user_id = ".$userID." AND ts_begin != 0)";
        
        $query = $this->db->query($sql);
        $ra = $query->result_array();
        
        foreach($ra as $rkey => $record) {
            
            $ra[$rkey] = $this->getOnePollOverID($record['id']);
            
        }
        
        return $ra;
        
    }
	
	
	/**
     * Получение статус прохождения опроса
     */
    public function getAllPollResults($pollID) {
        
        $this->db->select('*');
        $this->db->from('polls_results');
        $this->db->where('poll_id', $pollID);
        $this->db->order_by('ts_begin','DESC');
        $ra = $this->db->get()->result_array();
        
        if (empty($ra)) {
            
            return array();
            
        } else {
            
			$results = [];
			
			foreach($ra as $rkey => $record) {
			
				$answer['info'] = $record;

				$answer['info']['user'] = $this->users_model->getOneUserInfoOverID($record['user_id']);
				//Получить пройденные вопросы
				$this->db->select('*');
				$this->db->from('polls_answers');
				$this->db->where('result_id', $ra[0]['id']);
				$answer['answers'] = $this->db->get()->result_array();

				$results[] = $answer;
			}
			
			return $results;
			
        }
        
    }
	
    
    
}