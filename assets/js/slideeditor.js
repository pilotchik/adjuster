

var control_panel = $('#canvasControlPanel');

//$iframeUrl = G_SERVERNAME.'editor/browse.php?for_type=image&mode=lesson';

//Вставить изображение
/*
control_panel.append('<div class="btn-group imgbtn-group" title="Вставить изображение">\n\
    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">\n\
        <span class="glyphicon glyphicon-picture"></span>&nbsp;<span class="caret"></span>\n\
    </button>\n\
    <ul class="dropdown-menu" role="menu">\n\
        <iframe src="'+G_SERVERNAME+'editor/browse.php?for_type=image&mode=lesson&target=slide" frameBorder="0"></iframe>\n\
    </ul>\n\
</div>');
*/


control_panel.append('<div class="btn-toolbar" role="toolbar">');

control_panel.append('<div class="btn-group btn-group-sm">\n\
<span class="btn btn-sm btn-success saveButton" onclick="saveCanvas()" id="saveBackgroundBTNId" title="Сохранить подложку">\n\
        <img src="'+BASE_URL+'images/loading_spinner.gif" class="loadingSpinner" style="display:none;">\n\
        <span class="glyphicon glyphicon-floppy-disk"></span>\n\
    </span></div>');

control_panel.append('<div class="btn-group btn-group-sm">\n\
<a href="'+BASE_URL+'ajax/downloadCanvas?type='+itemType+'&itemID='+itemID+'" class="btn btn-sm btn-primary saveButton" target="blank" id="downloadBackgroundBTNId" title="Скачать параматры подложки">\n\
        <img src="'+BASE_URL+'images/loading_spinner.gif" class="loadingSpinner" style="display:none;">\n\
        <span class="glyphicon glyphicon-download"></span>\n\
    </a></div>');

control_panel.append('<div class="btn-group btn-group-sm">\n\
<span class="btn btn-sm btn-info saveButton" onclick="uploadCanvas()" id="uploadBackgroundBTNId" title="Загрузить параметры подложки">\n\
        <img src="'+BASE_URL+'images/loading_spinner.gif" class="loadingSpinner" style="display:none;">\n\
        <span class="glyphicon glyphicon-upload"></span>\n\
    </span></div>');


control_panel.append('<div class="btn-group btn-group-sm figurebtn-group" title="Вставить фигуру">\n\
    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">\n\
        Фигуры&nbsp;<span class="caret"></span>\n\
    </button>\n\
    <ul class="dropdown-menu" role="menu">\n\
        <li><a href="#" class="figureRect">Прямоугольник</a></li>\n\
        <li><a href="#" class="figureCircle">Круг</a></li>\n\
        <li><a href="#" class="figureLine">Линия</a></li>\n\
        <li><a href="#" class="figureTriangle">Треугольник</a></li>\n\
        <li><a href="#" class="figureRightTriangle">Прямоугольный треугольник</a></li>\n\
        <li><a href="#" class="figureArrow">Стрелка</a></li>\n\
        <li><a href="#" class="figureSector">Сектор</a></li>\n\
		<li class="divider"></li>\n\
		<li><a href="#" class="figureBuildingDoor">Дверь</a></li>\n\
		<li><a href="#" class="figureBuildingWindow">Окно</a></li>\n\
		<li><a href="#" class="figureBuildingAperture">Проём</a></li>\n\
		<li><a href="#" class="figureBuildingWall">Стена</a></li>\n\
		<li><a href="#" class="figureBuildingFlower">Растение</a></li>\n\
		<li><a href="#" class="figureBuildingPiano">Рояль</a></li>\n\
    </ul>\n\
</div>');

control_panel.append('<div class="btn-group btn-group-sm">\n\
	<span class="btn btn-default btn-sm selectAll" title="Выделить все"><span class="glyphicon glyphicon-check"></span></span>\n\
	<span class="btn btn-default btn-sm restoreVertical" title="Вернуть горизонталь или вертикаль"><span class="glyphicon glyphicon-screenshot"></span></span>\n\
	<span class="btn btn-default btn-sm fontBtn" title="Добавить текст"><span class="glyphicon glyphicon-font"></span></span>\n\
	<span class="btn btn-default btn-sm pencilBtn" title="Режим свободного рисования"><span class="glyphicon glyphicon-pencil"></span></span>\n\
	</div>');


//Кнопка добавления/удаления курсива
//Кнопка добавления/удаления жирности
control_panel.append('<div class="btn-group btn-group-sm">\n\
	<span class="btn btn-default btn-sm fontitalicBtn" title="Курсив/нормальный"><span class="glyphicon glyphicon-italic"></span></span>\n\
	<span class="btn btn-default btn-sm fontboldBtn" title="Жирный/нормальный"><span class="glyphicon glyphicon-bold"></span></span>\n\
	</div>');

//Панель выбора размера текста
control_panel.append('<span class="svgeditor prelabel">Размер&nbsp;текста:</span><span class="btn fontsizeWidth" title="Размер текста"><input class="inputText form-control" type="number" value="30" min="1" max="99" title="Размер текста"></span>');


//Выбор цвета заливки
control_panel.append('<span class="svgeditor prelabel">Заливка:</span><span class="btn colorFillPicker" title="Цвет заливки"><input class="inputText colorChoiceInput" type="color" value="#000000" title="Цвет заливки"></span>');
//Прозрачность
control_panel.append('<span class="svgeditor prelabel">Непрозрачность:</span><span class="btn opacityValue" title="Непрозрачность"><input class="inputText form-control" type="number" value="100" min="1" max="100" title="Непрозрачность"></span>');
//Выбор фигуры

//Панель выбора ширины линии
control_panel.append('<span class="svgeditor prelabel">Ширина&nbsp;границы:</span><span class="btn lineWidth" title="Ширина границы"><input class="inputText form-control" type="number" value="1" min="0" max="10" title="Ширина границы"></span>');
//Панель выбора цвета
control_panel.append('<span class="svgeditor prelabel">Цвет&nbsp;границы:</span><span class="btn colorPicker" title="Цвет границы"><input class="inputText colorChoiceInput" type="color" value="#000000" title="Цвет границы"></span>');

//Кнопки управления порядком отображения
control_panel.append('<div class="btn-group btn-group-sm">\n\
    <span class="btn btn-default btn-sm stackdownBtn stackOrderBtns" title="Опустить ниже" disabled><span class="glyphicon glyphicon-arrow-down"></span></span>\n\
    <span class="btn btn-default btn-sm stackupBtn stackOrderBtns" title="Поднять выше" disabled><span class="glyphicon glyphicon-arrow-up"></span></span>\n\
</div>');


//Кнопка удаления объектов
control_panel.append('<div class="btn-group btn-group-sm">\n\
    <span class="btn btn-default btn-sm removeBtn" title="Удалить объект(ы)"><span class="glyphicon glyphicon-trash"></span></span>\n\
    <span class="btn btn-danger btn-sm clearBtn" title="Очистить холст"><span class="glyphicon glyphicon-fire"></span></span>\n\
</div>');

//Отображение сетки
control_panel.append('<span class="svgeditor prelabel">Сетка:</span><span class="viewGrid"><input class="viewGridCheckbox" type="checkbox"></span>');

control_panel.append('</div>');

//Холст
var canvas = new fabric.Canvas('canv');

jQuery(document).ready(function($){
	//Выбрать бОльшее из ширины - либо контрольной панели, либо изображения подложки
	$('#background_id img').load(function(){
		
		console.log($(this).width())
		maxWidth = $('#canvasControlPanel').width() > $(this).width() ? $('#canvasControlPanel').width() : $(this).width();
		canvas.setWidth(maxWidth);
		
		maxHeight = canvas.height > $(this).height() ? canvas.height : $(this).height();
		canvas.setHeight(maxHeight);
	});
	
	var src = $('#background_id img').attr('src');
	$('#background_id img').attr('src','');
	$('#background_id img').attr('src',src);
	
	canvas.setWidth($('#canvasControlPanel').width());
	
});

//Прямоугольники
var rects = new Object;
var circles = new Object;
var triangles = new Object;
var lines = new Object;
var texts = new Object;
var svg_objects = new Object;
var fillColor = "#000000";
var fontSize = 30;
var opacityVal = 1;
var gridObjects = new Object;


//Установление свойств режима свободного рисования
canvas.freeDrawingBrush = new fabric['PencilBrush'](canvas);
canvas.freeDrawingBrush.color = $('.colorPicker input').val();
canvas.freeDrawingBrush.width = parseInt($('.colorPicker input').val(),10) || 1;

fillColor = $('.colorFillPicker input').val();


//Нажатие кнопки "Режим свободного рисования"
$('.pencilBtn').on('mousedown',function(e){
    canvas.isDrawingMode = !canvas.isDrawingMode;
    if (canvas.isDrawingMode) {
        jQuery(this).addClass('btn-primary');
    } else {
        jQuery(this).removeClass('btn-primary');
    }    
})

//Выравнивать к ближайшему углу
$('.restoreVertical').on('mousedown',function(e){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            ang = el.get('angle');
			if (ang % 90 > 45) {
				newAngle = ang + 90 - (ang % 90)
			} else {
				newAngle = ang - ang % 90
			}
			el.set({angle:newAngle});
        });
        canvas.discardActiveGroup().renderAll();
    } else {
        ang = canvas.getActiveObject().get('angle');
		if (ang % 90 > 45) {
			newAngle = ang + 90 - (ang % 90)
		} else {
			newAngle = ang - ang % 90
		}
		canvas.getActiveObject().set({angle:newAngle});
    }
	canvas.renderAll();
})


//Выбрать все
$('.selectAll').on('mousedown',function(e){
    var objs = canvas.getObjects().map(function(o){
		return o.set('active',true);
	});
	canvas.renderAll();
})


//Нажатие на кнопку "Удалить объект(ы)"
$('.removeBtn').on('mousedown',function(e){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            canvas.remove(el)
        });
        canvas.discardActiveGroup().renderAll();
    } else {
        canvas.remove(canvas.getActiveObject());
    }
})

//Очистить поле
$('.clearBtn').on('mousedown',function(e){
    if (confirm('Вы уверены, что хотите очистить холст?')) canvas.clear()
})

//Изменение цвета линии
$('.colorPicker input').on('change',function(){
    canvas.freeDrawingBrush.color = jQuery(this).val();
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            el.set({stroke:canvas.freeDrawingBrush.color})
        });
    } else {
        canvas.getActiveObject().set({stroke:canvas.freeDrawingBrush.color});
    }
    canvas.renderAll();
})

//Изменение цвета заливки
$('.colorFillPicker input').on('change',function(){
    fillColor = jQuery(this).val();
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            el.set({fill:fillColor})
        });
    } else {
        if (canvas.getActiveObject()) canvas.getActiveObject().set({fill:fillColor});
    }
    canvas.renderAll();
})

//Изменение ширины
$('.lineWidth input').on('change',function(){
    canvas.freeDrawingBrush.width = parseInt(jQuery(this).val(),10) || 1;
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            el.set({strokeWidth:canvas.freeDrawingBrush.width})
        });
    } else {
        if(canvas.getActiveObject()) canvas.getActiveObject().set({strokeWidth:canvas.freeDrawingBrush.width});
    }
    canvas.renderAll();
})


//Изменение прозрачности
$('.opacityValue input').on('change',function(){
    opacityVal = parseInt(jQuery(this).val(),10)/100 || 1;
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            el.set({opacity:opacityVal})
        });
    } else {
        canvas.getActiveObject().set({opacity:opacityVal});
    }
    canvas.renderAll();
})

//Изменение размера текста
$('.fontsizeWidth input').on('change',function(){
    fontSize = parseInt(jQuery(this).val(),10) || 1;
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            el.set({fontSize:fontSize})
        });
    } else {
        canvas.getActiveObject().set({fontSize:fontSize});
    }
    canvas.renderAll();
})

//Изменение жирности
$('.fontboldBtn').on('click',function(){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            //el.set({fontSize:fontSize})
            if (el.get('fontWeight') == 'normal')
                el.set({fontWeight:'bold'})
            else
                el.set({fontWeight:'normal'})
        });
        canvas.renderAll();
    } else {
        //canvas.getActiveObject().set({fontSize:fontSize});
        if (canvas.getActiveObject().get('fontWeight') == 'normal')
            canvas.getActiveObject().set({fontWeight:'bold'})
        else
            canvas.getActiveObject().set({fontWeight:'normal'})
        canvas.renderAll();
    }
});


//Изменение курсива
$('.fontitalicBtn').on('click',function(){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            //el.set({fontSize:fontSize})
            if (el.get('fontStyle') == 'normal')
                el.set({fontStyle:'bold'})
            else
                el.set({fontStyle:'normal'})
        });
        canvas.renderAll();
    } else {
        //canvas.getActiveObject().set({fontSize:fontSize});
        if (canvas.getActiveObject().get('fontStyle') == 'normal')
            canvas.getActiveObject().set({fontStyle:'italic'})
        else
            canvas.getActiveObject().set({fontStyle:'normal'})
        canvas.renderAll();
    }
});


//Отправить назад
$('.stackdownBtn').on('click',function(){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            canvas.sendBackwards(el);
        });
    } else {
        canvas.sendBackwards(canvas.getActiveObject());
    }
    canvas.renderAll();
})

//Поднять вверх
$('.stackupBtn').on('click',function(){
    if (canvas.getActiveGroup()) {
        canvas.getActiveGroup().forEachObject(function(el){
            canvas.bringForward(el);
        });
    } else {
        canvas.bringForward(canvas.getActiveObject());
    }
    canvas.renderAll();
})

//Реакция на нажатике кнопки "Delete"
$(document).bind('keydown','del',function(e){
    if (e.key == "Del") {
        if (canvas.getActiveGroup()) {
            canvas.getActiveGroup().forEachObject(function(el){
                canvas.remove(el)
            });
            canvas.discardActiveGroup().renderAll();
        } else {
            canvas.remove(canvas.getActiveObject());
        }
    }
})


//Реакция на нажатике кнопки "Влево"
$(document).bind('keydown','left',function(e){
	if (e.key == "Left") {
        if (canvas.getActiveGroup()) {
            canvas.getActiveGroup().forEachObject(function(el){
                currL = el.get('left');
				el.set({left:currL-2});
            });
        } else {
            currL = canvas.getActiveObject().get('left');
			canvas.getActiveObject().set({left:currL-2});
        }
    }
	
	canvas.renderAll();
})


//Реакция на нажатике кнопки Вправо
$(document).bind('keydown','right',function(e){
	if (e.key == "Right") {
        if (canvas.getActiveGroup()) {
            canvas.getActiveGroup().forEachObject(function(el){
                currL = el.get('left');
				el.set({left:currL+2});
            });
        } else {
            currL = canvas.getActiveObject().get('left');
			canvas.getActiveObject().set({left:currL+2});
        }
    }
	
	canvas.renderAll();
})


//Реакция на нажатике кнопки Вверх
$(document).bind('keydown','up',function(e){
	if (e.key == "Up") {
        if (canvas.getActiveGroup()) {
            canvas.getActiveGroup().forEachObject(function(el){
                currT = el.get('top');
				el.set({top:currT-2});
            });
        } else {
            currT = canvas.getActiveObject().get('top');
			canvas.getActiveObject().set({top:currT-2});
        }
    }
	
	canvas.renderAll();
})


//Реакция на нажатике кнопки Вниз
$(document).bind('keydown','down',function(e){
	if (e.key == "Down") {
        if (canvas.getActiveGroup()) {
            canvas.getActiveGroup().forEachObject(function(el){
                currT = el.get('top');
				el.set({top:currT+2});
            });
        } else {
            currT = canvas.getActiveObject().get('top');
			canvas.getActiveObject().set({top:currT+2});
        }
    }
	
	canvas.renderAll();
})





/*
 * *********************************************************************************************************************************
 * *********************************************************************************************************************************
 * Создание фигур
 */

//Создание квадрата
$('.figureRect').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    rects[index] = new fabric.Rect({
        left: 0,
        top: 0,
        fill: "#FFFFFF",
        strokeWidth: 2,
        stroke: canvas.freeDrawingBrush.color, 
        width: 100,
        height: 100,
        opacity: opacityVal
    });
    canvas.add(rects[index]);
    resetDrawingMode()
});

//Создание круга
$('.figureCircle').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    circles[index] = new fabric.Circle({
        left: 0,
        top: 0,
        fill: fillColor,
        opacity: opacityVal,
        radius: 50
    });
    canvas.add(circles[index]);
    resetDrawingMode()
});

//Создание линии
$('.figureLine').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    lines[index] = new fabric.Line([0,50,100,50],{
        fill: canvas.freeDrawingBrush.color,
        stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
    });
    canvas.add(lines[index]);
    resetDrawingMode()
});

//Создание треугольника
$('.figureTriangle').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    triangles[index] = new fabric.Triangle({
        fill: fillColor,
        left: 0,
        top: 0,
        width: 100,
        opacity: opacityVal,
        height: 100
    });
    canvas.add(triangles[index]);
    resetDrawingMode()
});

//Прямоугольный треугольник
$('.figureRightTriangle').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    var startPoints = [
        {x: 0,y: 0},
        {x: 100,y: 0},
        {x: 0,y: 100}
    ]
    triangles[index] = new fabric.Polygon(startPoints.map(),{
        left: 10,
        top: 10,
        opacity: opacityVal,
    });
    canvas.add(triangles[index]);
    resetDrawingMode()
});


//Стрелка
$('.figureArrow').on('mousedown',function(e){
    line = new fabric.Line([0,50,100,50],{
        fill: canvas.freeDrawingBrush.color,
        stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
    });
    
    triangle = new fabric.Triangle({
        fill: canvas.freeDrawingBrush.color,
        left: 135 - (10-canvas.freeDrawingBrush.width)*1.5,
        top: 40,
        width: 20+canvas.freeDrawingBrush.width,
        opacity: opacityVal,
        height: 20+canvas.freeDrawingBrush.width,
        angle:90
    });
    
    group = new fabric.Group([line,triangle],{
        left:10,
        top:50
    });
    
    canvas.add(group);
    resetDrawingMode()
});


//Сектор
$('.figureSector').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');
    
    alpha_inp = prompt('Укажите угол');
    if (alpha_inp != null) {
        alpha = (alpha_inp) * (Math.PI/180);
        var p = new Object;
        p.x_end = Math.cos(alpha) * 100;
        p.y_end = Math.abs(Math.sin(alpha) * 100);
        p.x_middle = Math.cos(alpha/2) * 100;
        p.y_middle = Math.abs(Math.sin(alpha/2) * 100);
        p.x_start = 100
        p.y_start = 0
        t = 0.5;
        p.x_control = (p.x_middle - t*t*p.x_end - (1-t)*(1-t)*p.x_start)/(2*(1-t)*t);
        p.y_control = (p.y_middle - t*t*p.y_end - (1-t) * (1-t) * p.y_start)/(2*(1-t)*t);
        var path = new fabric.Path('M0.0,0.0L100.0,0.0Q'+p.x_control+','+p.y_control+','+p.x_end+','+p.y_end+'z');
        canvas.add(path.set({left:100,top:100,fill: fillColor,opacity: opacityVal}));
        
        val = (alpha_inp/360)*100;
        text_value = val.toFixed(1)+ '%'
        var text = new fabric.IText(text_value,{
            fill: fillColor,
            left: 50,
            fontsize: fontSize,
            opacity: opacityVal,
            top: 50
        });
        canvas.add(text)
    }
    resetDrawingMode()
});


//Дверь
$('.figureBuildingDoor').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	var rect = new fabric.Rect({
		fill: 'white',
		width: 88,
		height: 15,
		stroke: 'white',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
		//lockScalingY: true
	});

	var path1 = new fabric.Path('M 0 0 L 5 0 L 5 6 L 8 6 L 8 9 L 5 9 L 5 15 L 0 15 Z');
	path1.set({
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var path2 = new fabric.Path('M 5.0,15.0 Q 6.0,28.0,12.0,40.0 L 81.0,13.0 L 82.0,15.0 L 13.0,42.0 L 12.0,40.0');
	path2.set({
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var path3 = new fabric.Path('M 83 15 L 88 15 L 88 0 L 83 0 L 83 6 L 80 6 L 80 9 L 83 9 Z');
	path3.set({
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var group = new fabric.Group([rect,path1,path2,path3 ], {
		left: 100,
		top: 100,
		lockScalingY: true,
		lockScalingX: true,
		fill: 'white'
	});
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'door';
	
	group.setControlVisible('tl',false);
	group.setControlVisible('ml',false);
	group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	group.setControlVisible('br',false);
	group.setControlVisible('mr',false);
	group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);
	
	canvas.add(group);
    resetDrawingMode()
});

//Окно
$('.figureBuildingWindow').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	var rect = new fabric.Rect({
		fill: 'white',
		width: 88,
		height: 20,
		stroke: 'white',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
		lockScalingX: true,
		lockScalingY: true
	});

	var path1 = new fabric.Path('M 0 0 L 5 0 L 5 6 L 8 6 L 8 9 L 5 9 L 5 15 L 0 15 Z');
	path1.set({
		top:3,
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
		lockScalingX: true
	});
	
	var rect2 = new fabric.Rect({
		fill: 'white',
		top: 10,
		left:9,
		width: 72,
		height: 1,
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var path3 = new fabric.Path('M 83 15 L 88 15 L 88 0 L 83 0 L 83 6 L 80 6 L 80 9 L 83 9 Z');
	path3.set({
		top:3,
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var group = new fabric.Group([rect,path1,rect2,path3 ], {
		left: 100,
		top: 100,
		lockScalingY: true,
		lockScalingX: true,
		fill: 'white'
	});
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'window';
	
	group.setControlVisible('tl',false);
	group.setControlVisible('ml',false);
	group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	group.setControlVisible('br',false);
	group.setControlVisible('mr',false);
	group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);

	canvas.add(group);
    resetDrawingMode()
});


//Проём
$('.figureBuildingAperture').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	
	var rect = new fabric.Rect({
		fill: 'white',
		width: 88,
		height: 20,
		stroke: 'white',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
	});
	
	

	var path1 = new fabric.Path('M 0 0 L 5 0 L 5 15 L 0 15 Z');
	path1.set({
		top:3,
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
	});
	
	var path3 = new fabric.Path('M 83 15 L 88 15 L 88 0 L 83 0 Z');
	path3.set({
		top:3,
		fill: 'white',
		stroke: canvas.freeDrawingBrush.color,
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width
	});
	
	var group = new fabric.Group([rect,path1,path3 ], {
		left: 100,
		top: 100,
		lockScalingY: true,
		fill: 'white'
	});
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'aperture';
	
	group.setControlVisible('tl',false);
	//group.setControlVisible('ml',false);
	group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	group.setControlVisible('br',false);
	//group.setControlVisible('mr',false);
	group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);

	canvas.add(group);
    resetDrawingMode()
});


//Стена
$('.figureBuildingWall').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	var rect = new fabric.Rect({
		fill: '#E1E1E1',
		width: 88,
		height: 15,
		stroke: '#F5F5F5',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
	});

	
	var rect2 = new fabric.Rect({
		fill: 'black',
		top: 14,
		left:0,
		width: 88,
		height: 1,
		stroke: 'black',
        opacity: opacityVal,
        strokeWidth: 1
	});
	
	var group = new fabric.Group([rect,rect2 ], {
		left: 100,
		top: 100,
		lockScalingY: true,
		fill: '#E1E1E1'
	});
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'wall';
	
	
	group.setControlVisible('tl',false);
	//group.setControlVisible('ml',false);
	group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	group.setControlVisible('br',false);
	//group.setControlVisible('mr',false);
	group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);

	canvas.add(group);
    resetDrawingMode()
});


//Цветок
$('.figureBuildingFlower').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	var circle = new fabric.Circle({
		left:10,
		top:10,
		radius: 20,
		fill: '#895C00',
		stroke: '#895C00'
	});

	var circle2 = new fabric.Circle({
		left:15,
		top:15,
		radius: 15,
		fill: '#4A4A4A',
		strike: '#4A4A4A'
	});

	var path2 = new fabric.Path('M 0.0,30.0 Q 10.0,15.0,30.0,30.0 Q 10.0,50.0,0.0,30.0');
	path2.set({
		fill: '#449D44',
		stroke:  '#449D44',
        opacity: opacityVal,
        strokeWidth: 1
	});
	
	
	
	var path3 = new fabric.Path('M 30.0,30.0 Q 30.0,10.0,45.0,5.0 Q 55.0,30.0,30.0,30.0');
	path3.set({
		fill: '#449D44',
		stroke:  '#449D44',
        opacity: opacityVal,
        strokeWidth: 1
	});

	var path4 = new fabric.Path('M 30.0,30.0 Q 20.0,45.0,45.0,55.0 Q 50.0,30.0,30.0,30.0');
	path4.set({
		fill: '#449D44',
		stroke:  '#449D44',
        opacity: opacityVal,
        strokeWidth: 1
	});
	
	
	var group = new fabric.Group([circle,circle2,path2,path3,path4], {
		left: 100,
		top: 100,
	});
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'flower';
	
	///group.setControlVisible('tl',false);
	group.setControlVisible('ml',false);
	///group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	///group.setControlVisible('br',false);
	group.setControlVisible('mr',false);
	///group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);
	
	
	canvas.add(group);
    resetDrawingMode()
});



$('.figureBuildingPiano').on('mousedown',function(e){
    jQuery('.figurebtn-group').removeClass('open');

	var path1 = new fabric.Path('\
		M 140.0,0.0 \n\
		L 140.0,150.0 L 30.0,150.0 \n\
		Q 0.0,150.0,0.0,120.0 \n\
		L 0.0,80.0 \n\
		Q 0.0,50.0,30.0,50.0\n\
		L 70.0, 50.0 \n\
		Q 80.0, 50.0, 80.0, 40.0\n\
		L 80.0, 10.0\n\
		Q 80.0, 0.0, 90.0, 0.0 Z\n\
	');
	path1.set({
		fill: 'white',
		stroke:  'black',
        opacity: opacityVal,
        strokeWidth: 1
	});
	
	
	var path2 = new fabric.Path('\
		M 140.0,0.0\n\
		L 160.0,0.0\n\
		Q 170.0,0.0,170.0,10.0\n\
		L 170.0,140.0\n\
		Q 170.0,150.0,160.0,150.0\n\
		L 140.0,150.0 Z\n\
	');
	path2.set({
		fill: 'white',
		stroke:  'black',
        opacity: opacityVal,
        strokeWidth: 1
	});
	
	
	var rect = new fabric.Rect({
		fill: 'white',
		left:150,
		top:10,
		width: 20,
		height: 130,
		stroke: 'black',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
	});
	
	var rect2 = new fabric.Rect({
		fill: 'white',
		left:180,
		top:30,
		width: 25,
		height: 90,
		stroke: 'black',
        opacity: opacityVal,
        strokeWidth: canvas.freeDrawingBrush.width,
	});
	

	
	var group = new fabric.Group([path1,path2,rect,rect2], {
		left: 100,
		top: 100,
		fill: 'white',
	});
	
	///group.setControlVisible('tl',false);
	group.setControlVisible('ml',false);
	///group.setControlVisible('bl',false);
	group.setControlVisible('mb',false);
	///group.setControlVisible('br',false);
	group.setControlVisible('mr',false);
	///group.setControlVisible('tr',false);
	group.setControlVisible('mt',false);
	
	group.toObject = (function(toObject) {
		return function() {
			return fabric.util.object.extend(toObject.call(this), {
				chartType: this.chartType
			});
		};
	})(group.toObject);
	group.chartType = 'piano';
	
	
	canvas.add(group);
    resetDrawingMode()
});



/*
 * 
 * *********************************************************************************************************************
 */




//Добавление текста
$('.fontBtn').on('mousedown',function(e){
    index = Math.random()*1000;
    index = index.toString();
    
    texts[index] = new fabric.IText('Текст',{
        fill: fillColor,
        left: 50,
        fontsize: fontSize,
        opacity: opacityVal,
        top: 50
    });
    canvas.add(texts[index]);
    resetDrawingMode();
});


$('.viewGridCheckbox').on('click',function(e){
    if (jQuery('.viewGridCheckbox').prop('checked')) {
        
        gridColumnRowWidth = 40;
        gridFillColor = '#DDDDDD'
        width_count = parseInt(canvas.width/gridColumnRowWidth);
        
		for(i = 0; i <= width_count; i++) {
            index = Math.random()*1000;
            index = index.toString();

            gridX = i*gridColumnRowWidth;
            gridObjects[index] = new fabric.Line([gridX,0,gridX,canvas.height],{
                fill: gridFillColor,
                stroke: gridFillColor,
                strokeWidth: 1,
                selectable: false
            });
            canvas.add(gridObjects[index]);
            canvas.sendToBack(gridObjects[index]);
        }
        
        height_count = parseInt(canvas.width/gridColumnRowWidth);
        
		for(i = 0; i <= height_count; i++) {
            index = Math.random()*1000;
            index = index.toString();

            gridY = i*gridColumnRowWidth;
            gridObjects[index] = new fabric.Line([0,gridY,canvas.width,gridY],{
                fill: gridFillColor,
                stroke: gridFillColor,
                strokeWidth: 1,
                selectable: false
            });
            canvas.add(gridObjects[index]);
            canvas.sendToBack(gridObjects[index]);
        }
        
    } else {
        for(key in gridObjects) {
            canvas.remove(gridObjects[key]);
        }
    }
    resetDrawingMode();
    
});


//Вставка изображения
/*
insertImage = function(src,width,height) {
    console.log(src,width,height);
    fabric.Image.fromURL(G_SERVERNAME+src, function(oImg) {
        console.log(oImg);
        if (oImg.height > 300) {
            oImg.width = (oImg.width*300)/oImg.height;
            oImg.height = 300;
        }
        
        if (oImg.width > 500) {
            oImg.height = (oImg.height*500)/oImg.width;
            oImg.width = 500;
        }
        
        canvas.add(oImg);
        jQuery('.imgbtn-group').removeClass('open');
    });
    resetDrawingMode();
}
*/

//Обнулить режим свободного рисования
resetDrawingMode = function(){
    canvas.isDrawingMode = false;
    jQuery('.pencilBtn').removeClass('btn-primary');
}

getJSON = function() {
    return canvas.toJSON();
}


//Отображение кнопок управления порядком отображения
canvas.on('object:selected',function(){jQuery('.stackOrderBtns').attr({disabled:false});});
canvas.on('selection:cleared',function(){jQuery('.stackOrderBtns').attr('disabled',true);});



setMaxHeight = function() {
	//Установить максимальную высоту холста
	maxTop = 500;
	maxHeight = 0;
	$('.draggableEl').each(function(numb,el){
		if (parseInt($(el).css('top')) > maxTop) maxTop = parseInt($(el).css('top'));
		if (parseInt($(el).css('height')) > maxHeight) maxHeight = parseInt($(el).css('height'));
	});

	canvas.getObjects().map(function(o){
		if (o.get('top') > maxTop) maxTop = o.get('top');
		if (o.get('height') > maxHeight) maxHeight = o.get('height');
	});
	
	maxHeight = maxTop + maxHeight + 30;
	
	if (maxHeight > 1500) maxHeight = 1500; 

	canvas.setHeight(maxHeight);
}


paintCams = function() {
	/*
	 * Получить параметры камер
	 */
	$('.camFlatContainer').each(function(numb,el){

		//console.log(el)

		startX = parseInt($(el).css('left')) + parseInt($(el).css('width'))/2;
		startY = parseInt($(el).css('top')) + parseInt($(el).css('height'))/2;


		angle = $(el).attr('el-rotation');
		camid = $(el).attr('el-id');

		canvas.getObjects().map(function(el){
			//console.log(el.get('id'));
			//if (el.get('id') == camId) el.set({left:elleft,top:eltop});
		});

		var p = new Object;
		alpha = camViewAngle;
		alpha = (alpha) * (Math.PI/180);
		p.x_end = Math.cos(alpha) * camViewRadius;
		p.y_end = Math.abs(Math.sin(alpha) * camViewRadius);
		p.x_middle = Math.cos(alpha/2) * camViewRadius;
		p.y_middle = Math.abs(Math.sin(alpha/2) * camViewRadius);
		p.x_start = camViewRadius
		p.y_start = 0
		t = 0.5;
		p.x_control = (p.x_middle - t*t*p.x_end - (1-t)*(1-t)*p.x_start)/(2*(1-t)*t);
		p.y_control = (p.y_middle - t*t*p.y_end - (1-t) * (1-t) * p.y_start)/(2*(1-t)*t);
		var path = new fabric.Path('M0.0,0.0L'+camViewRadius+'.0,0.0Q'+p.x_control+','+p.y_control+','+p.x_end+','+p.y_end+'z');

		path.set({
			id:camid,
			left:startX,
			top:startY,
			fill: fillColor,
			opacity: opacityVal,
			centeredRotation: false,
			originX: "left",
			originY: "top",
			selectable: false,
			angle:angle
		});
		path.setGradient('fill', {
			x1: 0,
			y1: 0,
			x2: camViewRadius,
			y2: p.y_middle,
			colorStops: {
			  0: camColor,
			  1: '#fff'
			}
		});

		path.toObject = (function(toObject) {
			return function() {
				return fabric.util.object.extend(toObject.call(this), {
					chartType: this.chartType
				});
			};
		})(path.toObject);
		path.chartType = 'cam';


		canvas.add(path);

	});

}


makeDraggable = function() {
	$("div.draggableEl").draggable({
		zIndex:10,
		snapTolerance: 20,
		snapMode:"inner",
		drag: function(event,ui) {
			if($(this).attr('el-type') == 'cam') {
				camId = $(this).attr('el-id');
				elleft = parseInt($(this).css('left')) + parseInt($(this).css('width'))/2;
				eltop = parseInt($(this).css('top')) + parseInt($(this).css('height'))/2;
				canvas.getObjects().map(function(el){
					//console.log(el.get('id'));
					if (el.get('id') == camId) el.set({left:elleft,top:eltop});
				});

				canvas.renderAll();
			}
		},
		stop: function(event,ui){
			saveCoords($(this).context.id,$(this).css('top'),$(this).css('left'));
		}
	});
}


/*

var current;
var list = [];
var state = [];
var index = 0;
var index2 = 0;
var action = false;
var refresh = true;

canvas.on("object:added", function (e) {
    var object = e.target;
    console.log('object:modified');

    if (action === true) {
        state = [state[index2]];
        list = [list[index2]];

        action = false;
        console.log(state);
        index = 1;
    }
    object.saveState();

    state[index] = JSON.stringify(canvas);
    list[index] = object;
    index++;
    index2 = index - 1;



    refresh = true;
});

canvas.on("object:modified", function (e) {
    var object = e.target;
    console.log('object:modified');

    if (action === true) {
        state = [state[index2]];
        list = [list[index2]];

        action = false;
        console.log(state);
        index = 1;
    }

    object.saveState();

    state[index] = JSON.stringify(canvas);
    list[index] = object;
    index++;
    index2 = index - 1;
	refresh = true;
});

function undo() {

    if (index <= 0) {
        index = 0;
        return;
    }

    if (refresh === true) {
        index--;
        refresh = false;
    }

    console.log('undo');
	index2 = index - 1;
    
	state[index] = JSON.stringify(canvas);
	console.log(index)
	canvas.loadFromJSON(state[index2],canvas.renderAll.bind(canvas));
    
    index--;
    
	canvas.renderAll();
	action = true;
}

function redo() {

    action = true;
    if (index >= state.length) {
        return;
    }

    console.log('redo');

    index2 = index + 1;
	console.log(index2);
	console.log(state[index2]);
	state[index] = JSON.stringify(canvas);
    canvas.loadFromJSON(state[index2],canvas.renderAll.bind(canvas));
    
	
    index++;
    canvas.renderAll();
}

*/