window.saveFileODT = function(data) {

  if(!data) return;

  var url = window.iConfig ? (iConfig.mProtocol + '://' + iConfig.mHost + '/ajax/saveDoc') : 'http://irida/ajax/saveDoc';
  
  $.ajax({
    url: url,
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    crossDomain: true,
    xhrFields: {
      withCredentials: true
    },
    success: function(response) {
      console.log('saveFileODT - ok');
    },
    error: function(a, b, c) {
      console.log(a);
    }
  })
}