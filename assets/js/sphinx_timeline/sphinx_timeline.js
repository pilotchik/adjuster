

var timeline_id = '#timelineDIV';

var g_canv_width = 0;
var g_pixels_per_seconds, 
	g_main_pixels_per_seconds = 0,
	g_zoom_start_time = 0,
	g_zoom_end_time = 0,
	g_zoom_time = 0;

//отступ от начала холста
var g_left_offset = 10,
	g_zoom_start_pix = 10,
	g_top_offset = 10;
	
var g_row_height = 40;


//цвет для timegrid
var g_base_color = '#BBBBBB';
var g_current_abs_time_color = '#999999';

//цвет для линии между блоками
var g_blocks_color = '#222222';

//цвет для линии времени
var g_timeline_color = '#FF8150';

//Цыет линии для видео-полосы
var g_videos_color = '#777777';

//Цвет линии зума
var g_zoom_line_color = '#0000EE';

//Ширина правой колонки
var g_left_column_width = 200;

var current_time = 0;
var g_time_canv_offset = new Object;

var g_cameras_count = 0;

var audios = new Object;
var g_audios_count = 0;

var log_activity = new Object;
var g_log_count = 0;


var g_zoom_end_pix = 0;

//Коэффициент сжатия времени
var g_k_time = 1;



var timeline_head_code = '<table class="sTimelineResultTables sTimelineTable" id="time_canvas_table_id">\n\
		<tr>\n\
			<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
				'+sphinx_timeline_translations['scenario_time'] +'<div class="stimeline_current_time" id="current_time_span_id"></div>\n\
			</td>\n\
			<td style="vertical-align: top;" id="time_canvas_td_id">\n\
				<canvas class="canvas_class time_canvas" id="canv_time_main_base_id" style="z-index:5;position:absolute;"></canvas>\n\
				<canvas class="canvas_class time_canvas" id="canv_time_zoom_line_id" style="z-index:8;position:absolute;"></canvas>\n\
				<canvas class="canvas_class time_canvas" id="canv_time_base_id" style="z-index:10;position:absolute;"></canvas>\n\
				<canvas class="canvas_class time_canvas" id="canv_time_marker_id" style="z-index:15;position:absolute;"></canvas>\n\
				<div style="width:100%;height:20px;position:absolute;margin-top: 20px;" id="time_zoom_begin_div_container">\n\
					<div id="draggable_zoom_start" style="width:20px;height:20px;left:0px;z-index:30;position:absolute;cursor:col-resize;"></div>\n\
					<div id="draggable_zoom_move" style="width:25px;height:20px;z-index:30;position:absolute;cursor:move;"></div>\n\
					<div id="draggable_zoom_end" style="width:20px;height:20px;z-index:30;position:absolute;cursor:col-resize;"></div>\n\
				</div>\n\
				<div style="width:100%;height:20px;position:absolute;margin-top:40px;" id="time_div_container">\n\
					<div id="draggable_pointer" style="width:20px;height:20px;z-index:30;position:absolute;cursor:pointer;"></div>\n\
				</div>\n\
			</td>\n\
		</tr>\n\
	</table>';

var timeline_cameras_code_pre = '<table class="stimeline_cameras_table stimeline_table" id="videos_canvas_table_id" cellpadding="0">';
var timeline_cameras_code_after = '</table>';





function createHTML() {
	$(timeline_id).html('');
	$(timeline_id).append(timeline_head_code);
	
	
	if (g_cameras_count > 0) {
		timeline_cameras_code = timeline_cameras_code_pre;
		
		for(var i = 0;i < g_cameras_count; i++) {
			timeline_cameras_code += '<tr>\n\
				<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
					<span title="'+ (cameras[i]['description'] != '' ? cameras[i]['description'] : '')+'">'+cameras[i]['name']+'</span>\n\
				';
			
			$.each(cameras[i].records,function(numb,el){
				timeline_cameras_code += '\n\
						<div class="stimeline_video_block btn btn-default btn-xs" id="div_cam_video_play_'+i+'_'+numb+'" onclick="open_cam_video('+i+','+numb+')" title = "'+sphinx_timeline_translations['view_video']+'">\n\
									<span class="glyphicon glyphicon-play"></span>\n\
								</div>\n\
						<div id="div_cam_video_container_'+i+'_'+numb+'" style="position:absolute;z-index:295;background-color: yellow">\n\
							<div class="stimeline_video_marker stimeline_video_marker_bs" id="div_cam_video_'+i+'_'+numb+'"></div>\n\
						</div>';
				if (parseInt(el.download)) {
					timeline_cameras_code += '\n\
						<div class="stimeline_video_block btn btn-primary btn-xs" id="div_cam_video_download_'+i+'_'+numb+'" onclick="download_cam_video('+i+','+numb+')" title = "'+sphinx_timeline_translations['download_video']+'">\n\
							<span class="glyphicon glyphicon-floppy-save"></span>\n\
						</div>';
				}
			});
			
			timeline_cameras_code += '</td>';
			
			if (i == '0') {
				timeline_cameras_code += '\n\
					<td rowspan="'+g_cameras_count+'" id="videos_canvas_td_id" style="vertical-align:top;height:'+(g_row_height * g_cameras_count)+'px;">\n\
						<canvas class="canvas_class videos_canvas" id="canv_video_base_id" style="z-index:5;position:absolute;"></canvas>\n\
						<canvas class="canvas_class videos_canvas" id="canv_video_records_id" style="z-index:10;position:absolute;"></canvas>\n\
						<canvas class="canvas_class videos_canvas" id="canv_video_marker_id" style="z-index:20;position:absolute;"></canvas>\n\
					</td>';
			}
			

            timeline_cameras_code += '</tr>';
		}
		
		timeline_cameras_code += timeline_cameras_code_after;
		
		$(timeline_id).append(timeline_cameras_code);
	}
	
	
	//Трансляции
	if (Object.keys(COMMON_TRANSLATIONS).length) {
		
		timeline_translations_code = '';
		
		timeline_translations_code += '<div class="stimeline_user_container">\n\
			<table class="stimeline_table">';


		timeline_translations_code += '<tr>\n\
			<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
				'+sphinx_timeline_translations['translations'];

		$.each(COMMON_TRANSLATIONS,function(ntr,translation) {

			timeline_translations_code += '\n\
				<div class="stimeline_video_block btn btn-default btn-xs" id="div_translation_video_play_'+translation.id+'" onclick="open_translation_record(\''+translation.id+'\')" title = "'+sphinx_timeline_translations['view_video']+'">\n\
					<span class="glyphicon glyphicon-play"></span>\n\
				</div>';
				
				if (translation.end > 0) { 
					timeline_translations_code += '<div id="div_translation_video_container_'+translation.id+'" style="position:absolute;z-index:295;background-color: yellow">\n\
						<div class="stimeline_video_marker stimeline_video_marker_bs" id="div_translation_video_'+translation.id+'"></div>\n\
					</div>';

					if (translation.download) {
						timeline_translations_code += '\n\
							<div class="stimeline_video_block btn btn-primary btn-xs" id="div_translation_video_download_'+translation.id+'" onclick="download_translation_video(\''+translation.id+'\')" title = "'+sphinx_timeline_translations['download_video']+'">\n\
								<span class="glyphicon glyphicon-floppy-save"></span>\n\
							</div>';
					}
				}	
		});
		
		timeline_translations_code += '</td>';

		timeline_translations_code += '\n\
			<td id="canvas_td_id_translation" style="vertical-align:top;height:'+g_row_height+'px;">\n\
				<canvas class="canvas_class users_video_canvas users_canvas_translation" id="canv_base_id_translation" style="z-index:5;position:absolute;"></canvas>\n\
				<canvas class="canvas_class users_video_canvas users_canvas_translation" id="canv_videos_id_translation" style="z-index:10;position:absolute;"></canvas>\n\
				<canvas class="canvas_class users_video_canvas users_canvas_translation" id="canv_timegrid_id_translation" style="z-index:20;position:absolute;"></canvas>\n\
				<canvas class="canvas_class users_video_canvas users_canvas_translation" id="canv_timeline_id_translation" style="z-index:30;position:absolute;"></canvas>\n\
			</td>';


		timeline_translations_code += '</tr>';

		$(timeline_id).append(timeline_translations_code);
	}
	
	
	//Пользователи
	if (g_users_count) {
		
		if (Object.keys(users).length) {
			
			timeline_users_code = '';
		
			$.each(users,function(nuser,u) {

				if (Object.keys(u.videos).length || Object.keys(u.desktops).length) {

					timeline_users_code += '<div class="stimeline_user_container">\n\
						<div class="stimeline_user_title_container" title="'+u.userFullName+'">\n\
							<img class="smallUserAvatarInTables" src="' + (u.avatarURL_conf != '' ? u.avatarURL_conf : u.avatarURL_small) +'">\n\
							<span class="stimeline_user_name">' + u.surname + ' ' + u.name + '</span>\n\
						</div>\n\
						<table class="stimeline_table">';


						if (Object.keys(u.videos).length) {
							timeline_users_code += '<tr>\n\
								<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
									'+sphinx_timeline_translations['video'];

							$.each(u.videos,function(numb,el){
								timeline_users_code += '\n\
									<div class="stimeline_video_block btn btn-default btn-xs" id="div_video_play_'+u.user_id+'_'+numb+'" onclick="open_user_record('+u.user_id+','+numb+')" title = "'+sphinx_timeline_translations['view_video']+'">\n\
										<span class="glyphicon glyphicon-play"></span>\n\
									</div>\n\
									<div id="div_video_container_'+u.user_id+'_'+numb+'" style="position:absolute;z-index:295;background-color: yellow">\n\
										<div class="stimeline_video_marker stimeline_video_marker_bs" id="div_video_'+u.user_id+'_'+numb+'"></div>\n\
									</div>';

									if (el.download) {
										timeline_users_code += '\n\
											<div class="stimeline_video_block btn btn-primary btn-xs" id="div_video_download_'+u.user_id+'_'+numb+'" onclick="download_user_video('+u.user_id+','+numb+')" title = "'+sphinx_timeline_translations['download_video']+'">\n\
												<span class="glyphicon glyphicon-floppy-save"></span>\n\
											</div>';
									}

							});

							timeline_users_code += '</td>';

							timeline_users_code += '\n\
								<td id="canvas_td_id_'+u.user_id+'" style="vertical-align:top;height:'+g_row_height+'px;">\n\
									<canvas class="canvas_class users_video_canvas users_canvas_'+u.user_id+'" id="canv_base_id_'+u.user_id+'" style="z-index:5;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_video_canvas users_canvas_'+u.user_id+'" id="canv_videos_id_'+u.user_id+'" style="z-index:10;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_video_canvas users_canvas_'+u.user_id+'" id="canv_timegrid_id_'+u.user_id+'" style="z-index:20;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_video_canvas users_canvas_'+u.user_id+'" id="canv_timeline_id_'+u.user_id+'" style="z-index:30;position:absolute;"></canvas>\n\
								</td>';


							timeline_users_code += '</tr>';
						} 

						if (Object.keys(u.desktops).length) {
							timeline_users_code += '<tr>\n\
								<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
									'+sphinx_timeline_translations['desktop'];

							$.each(u.desktops,function(numb,el){
								timeline_users_code += '\n\
									<div class="stimeline_video_block btn btn-default btn-xs" id="div_desktop_play_'+u.user_id+'_'+numb+'" onclick="open_user_record('+u.user_id+','+numb+')" title = "'+sphinx_timeline_translations['view_video']+'">\n\
										<span class="glyphicon glyphicon-play"></span>\n\
									</div>\n\
									<div id="div_desktop_container_'+u.user_id+'_'+numb+'" style="position:absolute;z-index:295;background-color: yellow">\n\
										<div class="stimeline_video_marker stimeline_video_marker_bs" id="div_desktop_'+u.user_id+'_'+numb+'"></div>\n\
									</div>';

									if (el.download) {
										timeline_users_code += '\n\
											<div class="stimeline_video_block btn btn-primary btn-xs" id="div_desktop_download_'+u.user_id+'_'+numb+'" onclick="download_user_video('+u.user_id+','+numb+')" title = "'+sphinx_timeline_translations['download_video']+'">\n\
												<span class="glyphicon glyphicon-floppy-save"></span>\n\
											</div>';
									}

							});

							timeline_users_code += '</td>';

							timeline_users_code += '\n\
								<td id="canvas_td_desktops_id_'+u.user_id+'" style="vertical-align:top;height:'+g_row_height+'px;">\n\
									<canvas class="canvas_class users_desktop_canvas users_canvas_'+u.user_id+'" id="canv_desktop_base_id_'+u.user_id+'" style="z-index:5;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_desktop_canvas users_canvas_'+u.user_id+'" id="canv_desktop_id_'+u.user_id+'" style="z-index:10;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_desktop_canvas users_canvas_'+u.user_id+'" id="canv_desktop_timegrid_id_'+u.user_id+'" style="z-index:20;position:absolute;"></canvas>\n\
									<canvas class="canvas_class users_desktop_canvas users_canvas_'+u.user_id+'" id="canv_desktop_timeline_id_'+u.user_id+'" style="z-index:30;position:absolute;"></canvas>\n\
								</td>';


							timeline_users_code += '</tr>';
						}


						timeline_users_code += '</table></div>';
				}
			})
			
		} else {
			
			timeline_users_code = '<div class="">Записей не найдено</div>';
			
		}
		
		
		$(timeline_id).append(timeline_users_code);
	}
	
	
	/*
	 * Аудиозаписи
	 */
	if (g_audios_count) {
		
		timeline_audios_code = '<h3><span class="glyphicon glyphicon-volume-up"></span>&nbsp;'+sphinx_timeline_translations['audio_records'] +'</h3>\n\
				<table class="sTimelineAudioTables sTimelineTable stimeline_table" id="audios_canvas_table_id">\n\
			';
		
		
		
		
		$.each(audios,function(i,audio) {
			
			timeline_audios_code += '<tr>\n\
				<td class="stimeline_title_column_td" style="width:'+g_left_column_width+'">\n\
					'+audio.title+'\n\
				';
			
			$.each(audio.records,function(numb,el){
				timeline_audios_code += '\n\
						<div class="stimeline_audio_block btn btn-default btn-xs" id="div_link_audio_play_'+i+'_'+numb+'" onclick="open_link_audio('+i+','+numb+')" title = "'+sphinx_timeline_translations['view_audio']+'">\n\
									<span class="glyphicon glyphicon-play"></span>\n\
								</div>\n\
						<div id="div_link_audio_container_'+i+'_'+numb+'" style="position:absolute;z-index:295;background-color: yellow">\n\
							<div class="stimeline_audio_marker stimeline_audio_marker_bs" id="div_link_audio_'+i+'_'+numb+'"></div>\n\
						</div>';
				if (parseInt(el.download)) {
					timeline_audios_code += '\n\
						<div class="stimeline_audio_block btn btn-primary btn-xs" id="div_link_audio_download_'+i+'_'+numb+'" onclick="download_link_audio('+i+','+numb+')" title = "'+sphinx_timeline_translations['download_audio']+'">\n\
							<span class="glyphicon glyphicon-floppy-save"></span>\n\
						</div>';
				}
			});
			
			timeline_audios_code += '</td>';
			
			if (i == '0') {
				timeline_audios_code += '\n\
					<td rowspan="'+g_audios_count+'" id="audio_td_id" style="vertical-align:top;height:'+(g_row_height * g_audios_count)+'px;">\n\
						<canvas class="canvas_class audio_canvas" id="canv_audio_base_id" style="z-index:5;position:absolute;"></canvas>\n\
						<canvas class="canvas_class audio_canvas" id="canv_audio_records_id" style="z-index:10;position:absolute;"></canvas>\n\
						<canvas class="canvas_class audio_canvas" id="canv_audio_marker_id" style="z-index:20;position:absolute;"></canvas>\n\
					</td>';
			}
			

            timeline_audios_code += '</tr>';
			
			
		})
		
		timeline_audios_code += '</table>';
		
		$(timeline_id).append(timeline_audios_code);
	}
	
	
	
	
	
}



function draw() {
	
	createHTML();
	
	g_window_width = parseInt($(timeline_id).width());
	g_canv_width = parseInt($(timeline_id).width()) - g_left_column_width;
	g_pixels_per_seconds = g_canv_width/((max_time/60) + 5);

	/**
	 * Получение масштабирующего коэффициента
	 * 
	 */
	function getTimeZoomScale() {
		minutes_count = Math.floor(g_canv_width/g_pixels_per_seconds);
		scale_value = 1;
		if (minutes_count > 30 && minutes_count < 120) {
			scale_value = 2;
		} else if (minutes_count >= 120 && minutes_count < 1440) {
			//от 2 часов до 24 часов
			scale_value = 3;
		} else if (minutes_count >= 1440 && minutes_count < 10060) {
			//От суток до 7
			scale_value = 4;
		} else if (minutes_count >= 10060 && minutes_count < 131040) {
			//От недели до 12 недель (триместр)
			scale_value = 5;
		} else if (minutes_count >= 131040 && minutes_count < 524160) {
			//От 12 недель до года
			scale_value = 6;
		} else if (minutes_count >= 524160 && minutes_count < 2620800) {
			//От 12 недель до года
			scale_value = 7;
		}

		return scale_value;
	}




	g_k_time = getTimeZoomScale();

	g_main_pixels_per_seconds = g_pixels_per_seconds;

	/*
	 * Выравнивать ширину таблицы
	 */
	$('#time_canvas_table_id').css({'width':g_window_width});
	$('#time_canvas_td_id').css({'width':g_canv_width});
	$('#time_div_container').css({'width':g_canv_width});
	$('#time_zoom_begin_div_container').css({'width':g_canv_width});
	$('.time_canvas').attr({'width':g_canv_width});
	$('.time_canvas').attr({'height':'60'});
	$('#time_canvas_td_id').css({'height':60});
	//g_time_canv_offset['left'] = $('.time_canvas').offset().left;
	g_time_canv_offset['left'] = 0;


	$('#videos_canvas_td_id').css({'width':g_canv_width});
	$('.canvas_class').attr({'width':g_canv_width});
	$('.videos_canvas').attr({'height':g_cameras_count*g_row_height});
	$('.stimeline_table').css({'width':g_window_width});
	cameras['canvas_width'] = g_canv_width;
	cameras['canvas_height'] = g_cameras_count*g_row_height;
	
	$('.audio_canvas').attr({'height':g_audios_count*g_row_height});
	
	$('.users_video_canvas').attr({'width':g_canv_width});
	$('.users_video_canvas').attr({'height':g_row_height});
	
	$('.users_desktop_canvas').attr({'width':g_canv_width});
	$('.users_desktop_canvas').attr({'height':g_row_height});
	
	$('#audios_canvas_td_id').css({'width':g_canv_width});
	$('.audios_canvas').attr({'width':g_canv_width});
	$('.audios_canvas').attr({'height':g_audios_count*g_row_height});
	
	
	$('#log_canvas_td_id').css({'width':g_canv_width});
	$('.logs_canvas').attr({'width':g_canv_width});
	$('.logs_canvas').attr({'height':20});
	$('#log_canvas_table_id').css({'width':parseInt(g_canv_width)+150});
	log_activity['canvas_width'] = g_canv_width;
	log_activity['canvas_height'] = 20;

	//Отрисовать временную шкалу и маркер движения времени

	paint_top_timeline();
	paint_zoom_line();




	function paintTimeLineMarkers(context,withOffset,lineHeight,lineStart) {

		local_tol_offset = typeof lineStart == 'undefined' ? g_top_offset : lineStart;

		if (withOffset) {
			seconds = Math.floor((g_zoom_start_time - Math.floor(g_zoom_start_time))*60);
			offset = g_zoom_start_time == 0 ? 0 : Math.floor(((60-seconds)/60)*g_pixels_per_seconds);
			local_k_time = getTimeZoomScale();
		} else {
			offset = 0;
			local_k_time = g_k_time;
		}

		context.strokeStyle = '#CCCCCC';
		context.font = "bold 8pt Courier";
		now_y = local_tol_offset;


		for(i = 0; i <= Math.floor(g_canv_width/g_pixels_per_seconds);i++) {

			//Минуты
			current_time = Math.ceil(g_zoom_start_time) + i;


			if (current_time % 10 == 0 && (current_time % 60 != 0 && local_k_time == 3 || local_k_time < 3)) {

				//Каждые 10 минут
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				if (local_k_time < 3) {
					text_x = now_x;
					text_y = local_tol_offset - 2;
					context.fillText(Math.floor(current_time),text_x,text_y);
				}


			/*
			 * СУТКИ
			 */

			} else if (current_time % 360 == 0 && current_time % 1440 != 0 && local_k_time == 4) {

				//Каждые 6 часов - рисочки
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

			} else if (current_time % 1440 == 0 && local_k_time == 4) {
				//Каждые 24 часа - надпись
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;
				context.fillText(Math.floor(current_time/1440),text_x,text_y);


			/*
			 * НЕДЕЛИ
			 */

			} else if (current_time % 1440 == 0 && current_time % 10080 != 0 && local_k_time == 5) {

				//Каждый день - рисочки
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

			} else if (current_time % 10080 == 0 && local_k_time == 5) {
				//Каждая неделя - надпись
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;
				context.fillText(Math.floor(current_time/10080),text_x,text_y);


			/*
			 * ТРИМЕСТРЫ (кварталы)
			 */

			} else if (current_time % 10080 == 0 && current_time % 131040 != 0 && local_k_time == 6) {

				//Каждую неделю - рисочки
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

			} else if (current_time % 131040 == 0 && local_k_time == 6) {
				//Каждый семестр - надпись
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;
				context.fillText(Math.floor(current_time/131040),text_x,text_y);

			/*
			 * ГОДА
			 */

			} else if (current_time % 131040 == 0 && current_time % 524160 != 0 && local_k_time == 7) {

				//Каждый семестр - рисочки
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

			} else if (current_time % 524160 == 0 && local_k_time == 7) {
				//Каждый год - надпись
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;
				context.fillText(Math.floor(current_time/524160),text_x,text_y);

			} else if (current_time % 60 == 0 && local_k_time == 3) {

				//Каждый час
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;

				context.fillText(Math.floor(current_time/60),text_x,text_y);

			} else if (current_time % 1 == 0 && local_k_time == 1) {
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				text_x = now_x;
				text_y = local_tol_offset - 2;
				context.fillText(Math.floor(current_time),text_x,text_y);

			} else if (current_time % 5 == 0 && local_k_time == 2) {
				context.beginPath();
				now_x = g_left_offset + i * g_pixels_per_seconds + offset;
				now_y = local_tol_offset;
				context.moveTo(now_x,now_y);
				now_y = lineHeight;
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

			} 

		}

		return local_k_time;

	}



	/*
	 * Отрисовка активности
	 */
	function paint_actions(user_key) {

		canvas = document.getElementById('canv_activity_id_'+user_key);
		context = canvas.getContext('2d');

		canv_top = $('#canv_activity_id_'+user_key).offset().top;
		canv_left = $('#canv_activity_id_'+user_key).offset().left;

		context.clearRect(0,0,users[user_key].width,users[user_key].height);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_blocks_color;
		context.fillStyle = g_blocks_color;
		context.font = "normal 8pt Arial";
		context.textBaseLine = 'middle';
		context.textAlign = 'center';

		for(act in users[user_key]['activity']) {

			$('#div_empty_'+user_key+'_'+act).css({'display':'none'});
			$('#div_expected_'+user_key+'_'+act).css({'display':'none'});
			$('#div_real_'+user_key+'_'+act).css({'display':'none'});



			exp_top = canv_top + g_top_offset + 15 + act * 20;
			exp_left = canv_left + g_left_offset + (parseInt(users[user_key]['activity'][act]['time_expected'])/60 - g_zoom_start_time) * g_pixels_per_seconds - 10;

			if (users[user_key]['activity'][act]['status'] == 1 || users[user_key]['activity'][act]['status'] == 2) {

				notexpreal = 0;

				if ('{/literal}{$T_CALENDAR_EVENT.users_login}{literal}' != 'admin') {
					if (parseInt(users[user_key]['activity'][act]['time_expected'])/60 >= g_zoom_start_time && parseInt(users[user_key]['activity'][act]['time_expected'])/60 < g_zoom_end_time) {
						$('#div_expected_'+user_key+'_'+act).css({'display':'block'});
						$('#div_expected_'+user_key+'_'+act).css({'top':exp_top});
						$('#div_expected_'+user_key+'_'+act).css({'left':exp_left});
					} else {
						notexpreal++;
					}
				}


				real_top = canv_top + g_top_offset + 15 + act * 20;
				real_left = canv_left + g_left_offset + (parseInt(users[user_key]['activity'][act]['time_real'])/60 - g_zoom_start_time) * g_pixels_per_seconds - 10;

				if (parseInt(users[user_key]['activity'][act]['time_real'])/60 >= g_zoom_start_time && parseInt(users[user_key]['activity'][act]['time_real'])/60 < g_zoom_end_time) {
					$('#div_real_'+user_key+'_'+act).css({'display':'block'});
					$('#div_real_'+user_key+'_'+act).css({'top':real_top});
					$('#div_real_'+user_key+'_'+act).css({'left':real_left});
				} else {
					notexpreal++;
				}

				if (notexpreal < 2 && '{/literal}{$T_CALENDAR_EVENT.users_login}{literal}' != 'admin') {

					if (users[user_key]['activity'][act]['status'] == 2) {
						context.strokeStyle = '#666666';
					}

					context.beginPath();
					now_x = exp_left - canv_left + 10;
					now_y = exp_top - canv_top + 6;

					context.moveTo(now_x,now_y);
					now_x = real_left - canv_left + 10;
					context.lineTo(now_x,now_y);
					context.closePath();
					context.stroke();
				}

			} else if (users[user_key]['activity'][act]['status'] == 0) {
				if (parseInt(users[user_key]['activity'][act]['time_expected'])/60 >= g_zoom_start_time && parseInt(users[user_key]['activity'][act]['time_expected'])/60 < g_zoom_end_time) {
					$('#div_empty_'+user_key+'_'+act).css({'display':'block'});
					$('#div_empty_'+user_key+'_'+act).css({'top':exp_top});
					$('#div_empty_'+user_key+'_'+act).css({'left':exp_left});
				}
			} else if (users[user_key]['activity'][act]['status'] == 3) {

				real_left = canv_left + g_left_offset + (parseInt(users[user_key]['activity'][act]['time_real'])/60 - g_zoom_start_time) * g_pixels_per_seconds - 10;

				if (parseInt(users[user_key]['activity'][act]['time_real'])/60 >= g_zoom_start_time && parseInt(users[user_key]['activity'][act]['time_real'])/60 < g_zoom_end_time) {
					$('#div_empty_'+user_key+'_'+act).css({'display':'block'});
					$('#div_empty_'+user_key+'_'+act).css({'top':exp_top});
					$('#div_empty_'+user_key+'_'+act).css({'left':real_left});
				}
			}

		}

	}


	/**
	 * Отрисовка видео-событий 
	 */
	function paint_videos(user_key) {

		

		canvas = document.getElementById('canv_videos_id_'+user_key);
		context = canvas.getContext('2d');

		canv_top = $('#canv_videos_id_'+user_key).offset().top - parseInt($('.container-fluid').height());
		canv_left = g_left_column_width + 24;

		context.clearRect(0,0,g_canv_width,g_row_height);
		context.lineJoin = 'miter';
		context.lineWidth = 7;
		context.strokeStyle = g_videos_color;

		$.each(users[user_key].videos,function(vid,el) {
			
			//console.log(vid);

			$('#div_video_'+user_key+'_'+vid).css({'display':'none'});
			$('#div_video_play_'+user_key+'_'+vid).css({'display':'none'});
			$('#div_video_download_'+user_key+'_'+vid).css({'display':'none'});

			if (!(parseInt(users[user_key]['videos'][vid]['end'])/60 < g_zoom_start_time || parseInt(users[user_key]['videos'][vid]['start'])/60 > g_zoom_end_time)) {

				start_top = canv_top;
				
				if (parseInt(users[user_key]['videos'][vid]['start'])/60 <= g_zoom_start_time)
					start_left = canv_left + g_left_offset;
				else
					start_left = canv_left + g_left_offset + (parseInt(users[user_key]['videos'][vid]['start'])/60 - g_zoom_start_time) * g_pixels_per_seconds;


				users[user_key]['videos'][vid]['start_pix'] = start_left - canv_left;

				el_width = ((parseInt(users[user_key]['videos'][vid]['end']) - parseInt(users[user_key]['videos'][vid]['start']) - 5)/60) * g_pixels_per_seconds;

				//Если контейнер выходит за границы полотна
				if (el_width + start_left > parseInt(g_canv_width) + canv_left) {
					el_width = parseInt(g_canv_width) + canv_left - start_left - 14;
				}

				//Если время старта раньше начала зумированного времени, то и ширину контейнера уменьшить на эту разницу
				if (parseInt(users[user_key]['videos'][vid]['start'])/60 < g_zoom_start_time) {
					el_width -= (g_zoom_start_time - parseInt(users[user_key]['videos'][vid]['start'])/60) * g_pixels_per_seconds;
				}

				/*
				 * Изменить свойства видео-контейнера, в котором будет перемещаться ползунок
				 */
				$('#div_video_container_'+user_key+'_'+vid).css({'top':21});
				$('#div_video_container_'+user_key+'_'+vid).css({'left':start_left - 18});
				$('#div_video_container_'+user_key+'_'+vid).css({'width':el_width+14});

				$('#div_video_play_'+user_key+'_'+vid).css({'display':'block'});
				$('#div_video_play_'+user_key+'_'+vid).css({'top':21});
				$('#div_video_play_'+user_key+'_'+vid).css({'left':start_left-36});
				$('#div_video_play_'+user_key+'_'+vid).attr('onclick','open_user_record(\''+users[user_key]['videos'][vid]['id']+'\','+el.length+')');

				$('#div_video_'+user_key+'_'+vid).css({'display':'block'});

				end_left = canv_left + g_left_offset + (parseInt(users[user_key]['videos'][vid]['end'])/60 - g_zoom_start_time) * g_pixels_per_seconds;

				users[user_key]['videos'][vid]['end_pix'] = end_left - canv_left;
				users[user_key]['videos'][vid]['canv_left'] = canv_left;
				users[user_key]['videos'][vid]['el_width'] = el_width;

				$('#div_video_download_'+user_key+'_'+vid).css({'display':'block'});
				$('#div_video_download_'+user_key+'_'+vid).css({'top':21});
				$('#div_video_download_'+user_key+'_'+vid).css({'left':end_left-5});
				$('#div_video_download_'+user_key+'_'+vid).attr('onclick','download_user_record(\''+users[user_key]['videos'][vid]['id']+'\','+el.length+')');

				context.beginPath();
				now_x = start_left - canv_left;
				now_y = 27;
				
				context.moveTo(now_x,now_y);
				//now_x = end_left - canv_left;
				now_x = now_x + el_width + 4;
				
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				$('#div_video_'+user_key+'_'+vid).draggable({
					containment:"parent",
					stop: function(event,ui){
						//user_key = event.target.id.split('_')[2];
						user_key = '';
						for(var i = 2; i < event.target.id.split('_').length-1; i++) {
							user_key += event.target.id.split('_')[i] + '_';
						}
						user_key = user_key.substring(0,user_key.length - 1);
						vid = parseInt(event.target.id.split('_')[event.target.id.split('_').length - 1]);
						pos = $(this).css('left').split('px')[0];

						users[user_key]['videos'][vid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

					}

				});
			}

		});

	}



	/**
	 * Отрисовка видео с десктопов
	 */
	function paint_desktops(user_key) {

		canvas = document.getElementById('canv_desktop_id_'+user_key);
		context = canvas.getContext('2d');

		canv_top = $('#canv_desktop_id_'+user_key).offset().top - parseInt($('.container-fluid').height());
		canv_left = g_left_column_width + 24;

		context.clearRect(0,0,g_canv_width,g_row_height);
		context.lineJoin = 'miter';
		context.lineWidth = 7;
		context.strokeStyle = g_videos_color;

		$.each(users[user_key].desktops,function(vid,el) {
			
			//console.log(vid);

			$('#div_desktop_'+user_key+'_'+vid).css({'display':'none'});
			$('#div_desktop_play_'+user_key+'_'+vid).css({'display':'none'});
			$('#div_desktop_download_'+user_key+'_'+vid).css({'display':'none'});

			if (!(parseInt(el.end)/60 < g_zoom_start_time || parseInt(el.start)/60 > g_zoom_end_time)) {

				start_top = canv_top;
				
				if (parseInt(el.start)/60 <= g_zoom_start_time)
					start_left = canv_left + g_left_offset;
				else
					start_left = canv_left + g_left_offset + (parseInt(el.start)/60 - g_zoom_start_time) * g_pixels_per_seconds;


				el.start_pix = start_left - canv_left;

				el_width = ((parseInt(el.end) - parseInt(el.start) - 5)/60) * g_pixels_per_seconds;

				//Если контейнер выходит за границы полотна
				if (el_width + start_left > parseInt(g_canv_width) + canv_left) el_width = parseInt(g_canv_width) + canv_left - start_left - 14;

				//Если время старта раньше начала зумированного времени, то и ширину контейнера уменьшить на эту разницу
				if (parseInt(el.start)/60 < g_zoom_start_time) {
					el_width -= (g_zoom_start_time - parseInt(el.start)/60) * g_pixels_per_seconds;
				}

				/*
				 * Изменить свойства видео-контейнера, в котором будет перемещаться ползунок
				 */
				$('#div_desktop_container_'+user_key+'_'+vid).css({'top':54});
				$('#div_desktop_container_'+user_key+'_'+vid).css({'left':start_left - 18});
				$('#div_desktop_container_'+user_key+'_'+vid).css({'width':el_width+14});

				$('#div_desktop_play_'+user_key+'_'+vid).css({'display':'block'});
				$('#div_desktop_play_'+user_key+'_'+vid).css({'top':54});
				$('#div_desktop_play_'+user_key+'_'+vid).css({'left':start_left-36});
				$('#div_desktop_play_'+user_key+'_'+vid).attr('onclick','open_user_record(\''+el.id+'\','+el.length+')');

				$('#div_desktop_'+user_key+'_'+vid).css({'display':'block'});

				end_left = canv_left + g_left_offset + (parseInt(el.end)/60 - g_zoom_start_time) * g_pixels_per_seconds;

				el.end_pix = end_left - canv_left;
				el.canv_left = canv_left;
				el.el_width = el_width;

				$('#div_desktop_download_'+user_key+'_'+vid).css({'display':'block'});
				$('#div_desktop_download_'+user_key+'_'+vid).css({'top':54});
				$('#div_desktop_download_'+user_key+'_'+vid).css({'left':end_left-5});

				context.beginPath();
				now_x = start_left - canv_left;
				now_y = 27;
				
				context.moveTo(now_x,now_y);
				//now_x = end_left - canv_left;
				now_x = now_x + el_width + 4;
				
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();

				$('#div_desktop_'+user_key+'_'+vid).draggable({
					containment:"parent",
					stop: function(event,ui){
						//user_key = event.target.id.split('_')[2];
						user_key = '';
						for(var i = 2; i < event.target.id.split('_').length-1; i++) {
							user_key += event.target.id.split('_')[i] + '_';
						}
						user_key = user_key.substring(0,user_key.length - 1);
						vid = parseInt(event.target.id.split('_')[event.target.id.split('_').length - 1]);
						pos = $(this).css('left').split('px')[0];

						users[user_key]['desktops'][vid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

					}

				});
			}

		});

	}



	/*
	 * Отрисовка событий (перемещение и отображение div`ов)
	 */
	function paint_logs(user_key) {

		canv_top = $('#canv_log_id_'+user_key).offset().top;
		canv_left = $('#canv_log_id_'+user_key).offset().left;

		for(log in users[user_key]['log']) {

			$('#div_log_'+user_key+'_'+log).css({'display':'none'});

			if (parseInt(users[user_key]['log'][log]['start'])/60 >= g_zoom_start_time && parseInt(users[user_key]['log'][log]['start'])/60 < g_zoom_end_time) {

				start_top = canv_top + g_top_offset - 1;
				start_left = canv_left + g_left_offset + (parseInt(users[user_key]['log'][log]['start'])/60 - g_zoom_start_time) * g_pixels_per_seconds - 10;

				$('#div_log_'+user_key+'_'+log).css({'display':'block'});
				$('#div_log_'+user_key+'_'+log).css({'top':start_top});
				$('#div_log_'+user_key+'_'+log).css({'left':start_left});

			}

		}

	}

	/*
	 * Отрисовка верхней временной шкалы
	 */
	function paint_top_timeline() {
		
		
		max_date = max_time > 0 ? getTimeString(new Date(min_date.getTime() + max_time*1000)) : '';

		var zoom_end_left = parseInt(g_canv_width) - parseInt(20);
		g_zoom_end_pix = parseInt(g_canv_width) - 10;
		$('#draggable_zoom_end').css({'left':zoom_end_left+'px'});

		start_marker_left = $('#draggable_zoom_start').css('left');
		
		canonical_left = (parseInt(g_zoom_end_pix)-parseInt(start_marker_left))/2 - 13;
		
		$('#draggable_zoom_move').css({'left':canonical_left+'px'});


		canvas = document.getElementById('canv_time_main_base_id');
		context = canvas.getContext('2d');

		context.clearRect(0,0,g_canv_width,60);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_base_color;
		context.fillStyle = g_base_color;
		context.textBaseLine = 'middle';
		context.textAlign = 'center';

		paintTimeLineMarkers(context,0,40,30);

		context.strokeStyle = '#DDDDDD';
		context.font = "normal 7pt Courier";
		text_x = g_canv_width - 35;
		text_y = 25;
		if (g_k_time < 3) {
			context.fillText('минуты',text_x,text_y);
		} else if (g_k_time == 3) {
			context.fillText('часы',text_x,text_y);
		} else if (g_k_time == 4) {
			context.fillText('сутки',text_x,text_y);
		} else if (g_k_time == 5) {
			context.fillText('недели',text_x,text_y);
		} else if (g_k_time == 6) {
			context.fillText('триместры',text_x,text_y);
		} else if (g_k_time == 7) {
			context.fillText('года',text_x,text_y);
		}
		
		text_x = g_canv_width - 55;
		text_y = 10;
		context.fillText(max_date,text_x,text_y);
		
		

	}


	/*
	 * Функция отрисовывает линию зума
	 */
	function paint_zoom_line() {

		canvas = document.getElementById('canv_time_zoom_line_id');
		context = canvas.getContext('2d');

		context.clearRect(0,0,g_canv_width,60);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_zoom_line_color;
		context.fillStyle = '#3071A9';
		
		offset = 20;

		zoom_middle = Math.ceil((g_zoom_end_pix - g_zoom_start_pix)/2 + g_zoom_start_pix);
		zoom_middle_start = zoom_middle - 10;
		zoom_middle_end = zoom_middle + 10;

		context.beginPath();

		now_x = g_zoom_start_pix;
		now_y = 10 + offset;
		context.moveTo(now_x,now_y);
		now_x = g_zoom_start_pix + 7;
		now_y = 17 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_start;
		now_y = 17 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_start + 3;
		now_y = 15 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_end - 3;
		now_y = 15 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_end;
		now_y = 17 + offset;
		context.lineTo(now_x,now_y);
		now_x = g_zoom_end_pix - 7;
		now_y = 17 + offset;
		context.lineTo(now_x,now_y);
		now_x = g_zoom_end_pix;
		now_y = 10 + offset;
		context.lineTo(now_x,now_y);
		now_x = g_zoom_end_pix;
		now_y = 20 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_end;
		now_y = 20 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_end - 3;
		now_y = 22 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_start + 3;
		now_y = 22 + offset;
		context.lineTo(now_x,now_y);
		now_x = zoom_middle_start;
		now_y = 20 + offset;
		context.lineTo(now_x,now_y);
		now_x = g_zoom_start_pix;
		now_y = 20 + offset;
		context.lineTo(now_x,now_y);

		context.closePath();
		context.stroke();
		context.fill();


		/*
		 * Рассчитать параметры, необходимые для всей отрисовки полей
		 */
		//Время начала и окончания зумлайн
		g_zoom_start_time = (g_zoom_start_pix-g_left_offset)/g_main_pixels_per_seconds;
		g_zoom_end_time = (g_zoom_end_pix+18-g_left_offset)/g_main_pixels_per_seconds;

		g_zoom_time = g_zoom_end_time - g_zoom_start_time;

		g_pixels_per_seconds = g_canv_width/g_zoom_time;

		//Переместить временную линию и маркер
		$('#draggable_pointer').css({'left':g_left_offset + g_time_canv_offset.left - 10});
		paint_top_marker(g_left_offset);


		if (Object.keys(COMMON_TRANSLATIONS).length) {
			
			paint_not_user_timegrid('canv_timegrid_id_translation',1,g_row_height);
			paint_translations_videos();
			
		}

		/*
		 * Подготовка для просмотра информации об обучаемых
		 */
		$.each(users,function(key) {
			
			student = users[key]
			//Временная сетка
			paint_timegrid(key);

			//Действия
			if (student.activity.length) paint_actions(key);

			//Видео
			if (Object.keys(student.videos).length) paint_videos(key);
			
			if (Object.keys(student.desktops).length) paint_desktops(key);

			//События
			if (student.log.length) paint_logs(key);

		});
		
		

		if (g_log_count > 0) {
			paint_not_user_timegrid('canv_log_base_id',1,20);
			paint_all_logs();
		}

		/*
		 * Подготовка к просмотру информации с камер
		 * 
		 * paint_cameras_timegrid() - Отрисовка временной сетки и видео
		 * paint_cameras_videos() - Отрисовка линий видео
		 * 
		 */
		if (g_cameras_count > 0) {
			paint_not_user_timegrid('canv_video_base_id',g_cameras_count,g_row_height);
			paint_cameras_videos();
		}

		/*
		 * Подготовка к прослушиванию информации сетей
		 * 
		 * paint_audios_timegrid() - Отрисовка временной сетки и видео
		 * paint_audios_links() - Отрисовка линий видео
		 * 
		 */
		if (g_audios_count > 0) {
			paint_not_user_timegrid('canv_audio_base_id',g_audios_count,g_row_height);
			paint_audios_links();
		}
		
		

		paint_zoom_time();
		//Отрисовать timegrid для событий тренажа

	}




	/**
	 * Отрисовка времени зума
	 * 
	 */
	function paint_zoom_time() {

		canvas = document.getElementById('canv_time_base_id');
		context = canvas.getContext('2d');

		context.clearRect(0,0,g_canv_width,60);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_base_color;
		context.fillStyle = g_base_color;

		context.textBaseLine = 'middle';
		context.textAlign = 'center';

		l_time = paintTimeLineMarkers(context,1,60,55);

		context.strokeStyle = '#DDDDDD';
		context.font = "normal 7pt Courier";
		text_x = g_canv_width - 35;
		text_y = 55;
		if (l_time < 3) {
			context.fillText('минуты',text_x,text_y);
		} else if (l_time == 3) {
			context.fillText('часы',text_x,text_y);
		} else if (l_time == 4) {
			context.fillText('сутки',text_x,text_y);
		} else if (l_time == 5) {
			context.fillText('недели',text_x,text_y);
		} else if (l_time == 6) {
			context.fillText('триместры',text_x,text_y);
		} else if (l_time == 7) {
			context.fillText('года',text_x,text_y);
		}

	}


	/*
	 * Отрисовка верхнего маркера
	 */
	function paint_top_marker(m_offset) {

		var canvas = document.getElementById('canv_time_marker_id');
		var context = canvas.getContext('2d');

		paint_marker = function(offset) {

			offset = offset > g_left_offset ? offset : g_left_offset;
			y_offset = 20;
			context.clearRect(0,0,g_canv_width,60);
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = '#398439';
			context.fillStyle = '#EC971F';

			context.beginPath();
			now_x = offset-10;
			now_y = 23 + y_offset;
			context.moveTo(now_x,now_y);
			now_x = offset+10;
			context.lineTo(now_x,now_y);
			now_x = offset;
			now_y = 37 + y_offset;
			context.lineTo(now_x,now_y);
			context.closePath();
			context.stroke();
			context.fill();

			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_timeline_color;

			context.beginPath();
			now_x = offset;
			now_y = 35 + y_offset;
			context.moveTo(now_x,now_y);
			now_y = 40 + y_offset;
			context.lineTo(now_x,now_y);
			context.closePath();
			context.stroke();
			
			
			
			current_x = offset - g_left_offset
			mins = Math.floor(current_x/g_pixels_per_seconds + g_zoom_start_time);
			
			start_date = getTimeString(new Date(min_date.getTime() + mins * 60 * 1000));
			
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_timeline_color;
			
			context.beginPath();
			now_x = offset;
			now_y = 0;
			context.moveTo(now_x,now_y);
			now_y = 15;
			context.lineTo(now_x,now_y);
			context.closePath();
			context.stroke();
			
			context.strokeStyle = g_base_color;
			context.fillStyle = g_current_abs_time_color;
			context.textBaseLine = 'middle';
			context.textAlign = 'center';
			
			text_x = (55 + offset) < g_canv_width - 160 ? 55 + offset : g_canv_width - 160;
			text_y = 10;
			context.fillText(start_date,text_x,text_y);

			

			set_current_time(offset);

		}

		if (typeof(m_offset) == 'undefined') {m_offset = 0;}

		paint_marker(m_offset);


	}

	function set_current_time(current_x) {
		current_x = current_x - g_left_offset;
		if (current_x >= 0) {

			local_k_time = getTimeZoomScale();
			display_time = '';

			mins = Math.floor(current_x/g_pixels_per_seconds + g_zoom_start_time);
			seconds = Math.floor(((current_x + g_zoom_start_time * g_pixels_per_seconds - mins*g_pixels_per_seconds)/g_pixels_per_seconds)*60);
			if (seconds < 10) seconds = '0'+seconds;

			hours = Math.floor(mins/60);
			day = Math.floor(hours/24);
			week = Math.floor(day/7);
			trimester = Math.floor(week/13);
			year = Math.floor(trimester/4);

			if (mins > 0) {
				if (local_k_time < 3) {
					//context.fillText('минуты',text_x,text_y);
					display_time = ' ('+mins+'мин:'+seconds+'сек)';

				} else if (local_k_time == 3) {
					//context.fillText('часы',text_x,text_y);
					display_time = ' ('+hours+'час:'+(mins - hours*60)+'мин)';
				} else if (local_k_time == 4) {
					//context.fillText('сутки',text_x,text_y);
					display_time = ' ('+day+'дней:'+(hours - day*24)+'час)';
				} else if (local_k_time == 5) {
					//context.fillText('недели',text_x,text_y);
					display_time = ' ('+week+'недель:'+(day - week*7)+'дней)';
				} else if (local_k_time == 6) {
					//context.fillText('триместры',text_x,text_y);
					display_time = ' ('+trimester+'триместр:'+(week - trimester*12)+'недель)';
				} else if (local_k_time == 7) {
					//context.fillText('года',text_x,text_y);
					display_time = ' ('+year+'год:'+(trimester - year*4)+'триместр)';
				}
			}

			$('#current_time_span_id').html(display_time);

			//Перерисовывать слои с линией маркера
			$.each(users,function(key) {
				repaint_time_markerline(key,current_x);
			});

			//Перерисовать слой с линей маркера
			if (g_cameras_count)
				repaint_videos_time_markerline(current_x);
			if (g_audios_count > 0)
				repaint_audios_time_markerline(current_x);
			if (g_log_count > 0)
				repaint_not_user_time_markerline('canv_log_marker_id',current_x);
			
			if (Object.keys(COMMON_TRANSLATIONS).length)
				repaint_not_user_time_markerline('canv_timeline_id_translation',current_x);

		}
	}
	

	/*
	 * Перерисовка линии времени на панелях обучаемых + сдвиг маркеров времени
	 */
	function repaint_time_markerline(user_key,current_x) {
		
		if ($('#canv_timeline_id_'+user_key).length) {
		
			canvas = document.getElementById('canv_timeline_id_'+user_key);
			context = canvas.getContext('2d');

			context.clearRect(0,0,g_canv_width,g_row_height);
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_timeline_color;

			context.beginPath();
			now_x = g_left_offset + current_x;
			now_y = 0;
			context.moveTo(now_x,now_y);
			now_y = g_row_height;
			context.lineTo(now_x,now_y);
			context.closePath();
			context.stroke();

			$.each(users[user_key].videos,function(vid,el) {

				if (current_x <= users[user_key]['videos'][vid]['start_pix']) {

					$('#div_video_'+user_key+'_'+vid).css({'left':0});
					users[user_key]['videos'][vid]['current_time'] = 0;

				} else if (current_x > users[user_key]['videos'][vid]['start_pix'] && current_x < users[user_key]['videos'][vid]['end_pix']) {

					//Текущая позиция в пикселях
					pos = current_x - users[user_key]['videos'][vid]['start_pix'];

					//Текущее время в секундах
					users[user_key]['videos'][vid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) -2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

					//Сдвиг маркера
					$('#div_video_'+user_key+'_'+vid).css({'left':pos + 4});

				} else {

					//Сдвиг маркера в конец контейнера
					$('#div_video_'+user_key+'_'+vid).css({'left':0});
					users[user_key]['videos'][vid]['current_time'] = 0;

				}


			});
		}
		
		
		
		if ($('#canv_desktop_timeline_id_'+user_key).length) {
		
			canvas = document.getElementById('canv_desktop_timeline_id_'+user_key);
			context = canvas.getContext('2d');

			context.clearRect(0,0,g_canv_width,g_row_height);
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_timeline_color;

			context.beginPath();
			now_x = g_left_offset + current_x;
			now_y = 0;
			context.moveTo(now_x,now_y);
			now_y = g_row_height;
			context.lineTo(now_x,now_y);
			context.closePath();
			context.stroke();

			$.each(users[user_key].desktops,function(vid,el) {

				if (current_x <= el.start_pix) {

					$('#div_desktop_'+user_key+'_'+vid).css({'left':0});
					el.current_time = 0;

				} else if (current_x > el.start_pix && current_x < el.end_pix) {

					//Текущая позиция в пикселях
					pos = current_x - el.start_pix;

					//Текущее время в секундах
					el.current_time = Math.ceil(pos/(g_pixels_per_seconds/60)) -2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

					//Сдвиг маркера
					$('#div_desktop_'+user_key+'_'+vid).css({'left':pos + 4});

				} else {

					//Сдвиг маркера в конец контейнера
					$('#div_desktop_'+user_key+'_'+vid).css({'left':0});
					el.current_time = 0;

				}


			});
		}

	}


	/*
	 * Прозрачный div над указателем заставляет его перемещаться
	 */
	$('#draggable_pointer').draggable({
		containment:"parent",
		drag: function(event,ui){
			paint_top_marker(ui.position.left - g_time_canv_offset.left + g_left_offset);
		}
	});

	$('#draggable_zoom_start').draggable({
		containment:"parent",
		drag: function(event,ui){

			end_marker_left = $('#draggable_zoom_end').css('left');

			if (ui.position.left >= parseInt(end_marker_left.split('px')[0]) - 25) return false;
			g_zoom_start_pix = ui.position.left - g_time_canv_offset.left + g_left_offset;

			end_marker_left = $('#draggable_zoom_end').css('left');                                        
			canonical_left = (parseInt(end_marker_left.split('px')[0]) - ui.position.left)/2 + ui.position.left;

			$('#draggable_zoom_move').css({'left':canonical_left +'px'});

			paint_zoom_line();
		},
		stop: function(event,ui) {

		}

	});

	$('#draggable_zoom_end').draggable({
		containment:"parent",
		drag: function(event,ui){
			start_marker_left = $('#draggable_zoom_start').css('left');
			if (ui.position.left <= parseInt(start_marker_left.split('px')[0]) + 25) return false;
			g_zoom_end_pix = ui.position.left - g_time_canv_offset.left + g_left_offset;

			start_marker_left = $('#draggable_zoom_start').css('left');
			end_marker_left = $('#draggable_zoom_end').css('left');                                        
			canonical_left = (parseInt(end_marker_left.split('px')[0])-parseInt(start_marker_left.split('px')[0]))/2 + parseInt(start_marker_left.split('px')[0]);
			$('#draggable_zoom_move').css({'left':canonical_left +'px'});

			paint_zoom_line();
		},
		stop: function(event,ui) {

		}

	});


	$('#draggable_zoom_move').draggable({
		containment:"parent",
		drag: function(event,ui){

			start_marker_left = $('#draggable_zoom_start').css('left');
			start_marker_left = parseInt(start_marker_left.split('px')[0]);

			end_marker_left = $('#draggable_zoom_end').css('left');
			end_marker_left = parseInt(end_marker_left.split('px')[0]);

			canonical_left = (end_marker_left - start_marker_left)/2 + start_marker_left;

			current_left = ui.position.left;

			offset = canonical_left - current_left;


			/*
			 * Если после сдвига zoomline края будут выходить за пределы холста, то остановить drag
			 */
			if (start_marker_left - offset < g_time_canv_offset.left) return false;
			if (end_marker_left - offset > g_time_canv_offset.left + parseInt(g_canv_width) - 20) return false;

			new_start = start_marker_left - offset;
			$('#draggable_zoom_start').css({'left':new_start +'px'});
			g_zoom_start_pix = new_start - g_time_canv_offset.left + g_left_offset;

			new_end = end_marker_left - offset;
			$('#draggable_zoom_end').css({'left':new_end +'px'});
			g_zoom_end_pix = new_end - g_time_canv_offset.left + g_left_offset;

			paint_zoom_line();
			//$('#draggable_zoom_move').css({'left':canonical_left +'px'});

			/*
			if (ui.position.left <= parseInt(start_marker_left.split('px')[0]) + 25) return false;
			g_zoom_end_pix = ui.position.left - g_time_canv_offset.left + g_left_offset;
			paint_zoom_line();
			*/

		},
		stop: function(event,ui) {

		}

	});


	/*
	 * Отрисовка видео-событий с камеры наблюдения
	 */
	function paint_cameras_videos() {

		canvas = document.getElementById('canv_video_records_id');
		context = canvas.getContext('2d');

		canv_top = $('#canv_video_records_id').offset().top - parseInt($('.container-fluid').height()) + 5;
		canv_left = g_left_column_width + 24;

		context.clearRect(0,0,cameras['canvas_width'],cameras['canvas_height']);
		context.lineJoin = 'miter';
		context.lineWidth = 7;
		context.strokeStyle = g_videos_color;

		for(var cam = 0; cam < g_cameras_count;cam++) {

			$.each(cameras[cam].records,function(vid,camel){

				$('#div_cam_video_'+cam+'_'+vid).css({'display':'none'});
				$('#div_cam_video_play_'+cam+'_'+vid).css({'display':'none'});
				$('#div_cam_video_download_'+cam+'_'+vid).css({'display':'none'});

				if (!(cameras[cam]['records'][vid]['end']/60 < g_zoom_start_time || cameras[cam]['records'][vid]['start']/60 > g_zoom_end_time)) {

					start_top = canv_top + parseInt(cam) * g_row_height + 5;
					

					if (parseInt(cameras[cam]['records'][vid]['start'])/60 <= g_zoom_start_time)
						start_left = canv_left + g_left_offset;
					else
						start_left = canv_left + g_left_offset + (parseInt(cameras[cam]['records'][vid]['start'])/60 - g_zoom_start_time) * g_pixels_per_seconds;



					cameras[cam]['records'][vid]['start_pix'] = start_left - canv_left;

					el_width = ((parseInt(cameras[cam]['records'][vid]['end']) - parseInt(cameras[cam]['records'][vid]['start']) - 5)/60) * g_pixels_per_seconds;

					//Если контейнер выходит за границы полотна
					if (el_width + start_left > parseInt(g_canv_width) + canv_left) {
						el_width = parseInt(g_canv_width) + canv_left - start_left - 14;
					}

					//Если время старта раньше начала зумированного времени, то и ширину контейнера уменьшить на эту разницу
					if (parseInt(cameras[cam]['records'][vid]['start']/60) < g_zoom_start_time) {
						el_width -= (g_zoom_start_time - parseInt(cameras[cam]['records'][vid]['start'])/60) * g_pixels_per_seconds;
					}

					$('#div_cam_video_container_'+cam+'_'+vid).css({'top':start_top});
					$('#div_cam_video_container_'+cam+'_'+vid).css({'left':start_left});
					$('#div_cam_video_container_'+cam+'_'+vid).css({'width':el_width+14});

					$('#div_cam_video_play_'+cam+'_'+vid).css({'display':'block'});
					$('#div_cam_video_play_'+cam+'_'+vid).css({'top':start_top});
					$('#div_cam_video_play_'+cam+'_'+vid).css({'left':start_left-5});

					$('#div_cam_video_'+cam+'_'+vid).css({'display':'block'});

					end_left = canv_left + g_left_offset + (parseInt(cameras[cam]['records'][vid]['end'])/60 - g_zoom_start_time) * g_pixels_per_seconds;

						if (end_left - canv_left > g_canv_width) end_left = g_canv_width + canv_left;

						cameras[cam]['records'][vid]['end_pix'] = end_left - canv_left;
						cameras[cam]['records'][vid]['canv_left'] = canv_left;
						cameras[cam]['records'][vid]['el_width'] = el_width;

						$('#div_cam_video_download_'+cam+'_'+vid).css({'display':'block'});
						$('#div_cam_video_download_'+cam+'_'+vid).css({'top':start_top});
						$('#div_cam_video_download_'+cam+'_'+vid).css({'left':end_left});


						context.beginPath();
						now_x = start_left - canv_left;
						now_y = start_top - canv_top + 15;

						context.moveTo(now_x,now_y);
						now_x = end_left - canv_left;

						context.lineTo(now_x,now_y);
						context.closePath();
						context.stroke();

						$('#div_cam_video_'+cam+'_'+vid).draggable({
							containment:"parent",
							stop: function(event,ui){
								cam = parseInt(event.target.id.split('_')[3]);
								vid = parseInt(event.target.id.split('_')[4]);
								pos = $(this).css('left').split('px')[0];

								cameras[cam]['records'][vid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

							}
						});
					}

				});
		}

	}
	
	
	
	function paint_translations_videos() {
		
		canvas = document.getElementById('canv_videos_id_translation');
		context = canvas.getContext('2d');

		
		//$.each(COMMON_TRANSLATIONS,function(ntr,translation) {
		
		canv_top = $('#canv_videos_id_translation').offset().top - parseInt($('.container-fluid').height());
		canv_left = g_left_column_width + 24;

		context.clearRect(0,0,g_canv_width,g_row_height);
		context.lineJoin = 'miter';
		context.lineWidth = 7;
		context.strokeStyle = g_videos_color;

		nel = 0;
		$.each(COMMON_TRANSLATIONS,function(ntr,translation) {
			
			//console.log(vid);
			nel = nel+3;

			$('#div_translation_video_play_'+translation.id).css({'display':'none'});
			//$('#div_video_download_'+translation.id).css({'display':'none'});

			if (!(parseInt(translation.end)/60 < g_zoom_start_time || parseInt(translation.start)/60 > g_zoom_end_time) || translation.end === 0) {

				start_top = canv_top;
				
				if (parseInt(translation.start)/60 <= g_zoom_start_time)
					start_left = canv_left + g_left_offset;
				else
					start_left = parseInt(canv_left + g_left_offset + (parseInt(translation.start)/60 - g_zoom_start_time) * g_pixels_per_seconds) + nel;


				translation.start_pix = start_left - canv_left;

				el_width = ((parseInt(translation.end) - parseInt(translation.start) - 5)/60) * g_pixels_per_seconds;

				//Если контейнер выходит за границы полотна
				if (el_width + start_left > parseInt(g_canv_width) + canv_left) {
					el_width = parseInt(g_canv_width) + canv_left - start_left - 14;
				}

				//Если время старта раньше начала зумированного времени, то и ширину контейнера уменьшить на эту разницу
				if (parseInt(translation.start)/60 < g_zoom_start_time) {
					el_width -= (g_zoom_start_time - parseInt(translation.start)/60) * g_pixels_per_seconds;
				}

				/*
				 * Изменить свойства видео-контейнера, в котором будет перемещаться ползунок
				 */
				$('#div_translation_video_container_'+translation.id).css({'top':21});
				$('#div_translation_video_container_'+translation.id).css({'left':start_left - 18});
				$('#div_translation_video_container_'+translation.id).css({'width':el_width+14});

				$('#div_translation_video_play_'+translation.id).css({'display':'block'});
				$('#div_translation_video_play_'+translation.id).css({'top':21});
				$('#div_translation_video_play_'+translation.id).css({'left':start_left-36});
				$('#div_translation_video_play_'+translation.id).attr('onclick','open_translation_record(\''+translation.id+'\','+translation.length+')');

				if (translation.end === 0) $('#div_translation_video_play_'+translation.id).removeClass('btn-default').addClass('btn-success');

				$('#div_translation_video_'+translation.id).css({'display':'block'});

				end_left = parseInt(canv_left + g_left_offset + (parseInt(translation.end)/60 - g_zoom_start_time) * g_pixels_per_seconds) + nel;

				translation.end_pix = end_left - canv_left;
				translation.canv_left = canv_left;
				translation.el_width = el_width;
				translation.end_left = end_left;

				$('#div_translation_video_download_'+translation.id).css({'display':'block'});
				$('#div_translation_video_download_'+translation.id).css({'top':21});
				$('#div_translation_video_download_'+translation.id).css({'left':end_left-5});
				$('#div_translation_video_download_'+translation.id).attr('onclick','download_translation_record(\''+translation.id+'\','+translation.length+')');
				
				if (translation.end > 0) {

					context.beginPath();
					now_x = start_left - canv_left;
					now_y = 27;

					context.moveTo(now_x,now_y);
					//now_x = end_left - canv_left;
					now_x = now_x + el_width + 4;

					context.lineTo(now_x,now_y);
					context.closePath();
					context.stroke();
				}

				$('#div_translation_video_'+translation.id).draggable({
					containment:"parent",
					stop: function(event,ui){
						
						pos = $(this).css('left').split('px')[0];

						translation.current_time = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

					}

				});
			}

		});
		
	}
	
	


	/*
	 * Перерисовка линии времени на панели с камерами + сдвиг маркеров времени
	 */
	function repaint_videos_time_markerline(current_x) {

		repaint_not_user_time_markerline('canv_video_marker_id',current_x);

		for(cam in cameras) {

			if (typeof(cameras[cam]) == 'object') {

				for (vid in cameras[cam]['records']) {

					if (current_x <= cameras[cam]['records'][vid]['start_pix']) {
						$('#div_cam_video_'+cam+'_'+vid).css({'left':0});
						cameras[cam]['records'][vid]['current_time'] = 0;
					} else if (current_x > cameras[cam]['records'][vid]['start_pix'] && current_x < cameras[cam]['records'][vid]['end_pix']) {

						pos = current_x - cameras[cam]['records'][vid]['start_pix'];

						cameras[cam]['records'][vid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;
						$('#div_cam_video_'+cam+'_'+vid).css({'left':pos+10});

					} else {
						$('#div_cam_video_'+cam+'_'+vid).css({'left':0});
						cameras[cam]['records'][vid]['current_time'] = 0;
					}
				}
			}
		}
	}


	/*
	 * Отрисовка аудио-событий
	 */
	function paint_audios_links() {

		canvas = document.getElementById('canv_audio_records_id');
		context = canvas.getContext('2d');

		canv_top =  $('#canv_audio_records_id').offset().top - parseInt($('.container-fluid').height());
		canv_left = g_left_column_width + 24;

		context.clearRect(0,0,g_canv_width,g_audios_count*g_row_height);
		context.lineJoin = 'miter';
		context.lineWidth = 7;
		context.strokeStyle = g_videos_color;

		$.each(audios,function(link,audio) {

			$.each(audio['records'],function(lid){

				$('#div_link_audio_play_'+link+'_'+lid).css({'display':'none'});
				$('#div_link_audio_'+link+'_'+lid).css({'display':'none '});

				if (!(audios[link]['records'][lid]['end']/60 < g_zoom_start_time || audios[link]['records'][lid]['start']/60 > g_zoom_end_time)) {

					start_top = canv_top + parseInt(link) * g_row_height + 10;

					if (parseInt(audios[link]['records'][lid]['start'])/60 <= g_zoom_start_time)
						start_left = canv_left + g_left_offset;
					else
						start_left = canv_left + g_left_offset + (parseInt(audios[link]['records'][lid]['start'])/60 - g_zoom_start_time) * g_pixels_per_seconds;



					audios[link]['records'][lid]['start_pix'] = start_left - canv_left;

					el_width = ((parseInt(audios[link]['records'][lid]['end']) - parseInt(audios[link]['records'][lid]['start']) - 5)/60) * g_pixels_per_seconds;

					//Если контейнер выходит за границы полотна
					if (el_width + start_left > parseInt(g_canv_width) + canv_left) {
						el_width = parseInt(g_canv_width) + canv_left - start_left - 14;
					}

					//Если время старта раньше начала зумированного времени, то и ширину контейнера уменьшить на эту разницу
					if (parseInt(audios[link]['records'][lid]['start']/60) < g_zoom_start_time) {
						el_width -= (g_zoom_start_time - parseInt(audios[link]['records'][lid]['start'])/60) * g_pixels_per_seconds;
					}

					$('#div_link_audio_container_'+link+'_'+lid).css({'top':start_top});
					$('#div_link_audio_container_'+link+'_'+lid).css({'left':start_left+3});
					$('#div_link_audio_container_'+link+'_'+lid).css({'width':el_width+14});

					$('#div_link_audio_play_'+link+'_'+lid).css({'display':''});
					$('#div_link_audio_play_'+link+'_'+lid).css({'top':start_top});
					$('#div_link_audio_play_'+link+'_'+lid).css({'left':start_left-22});

					$('#div_link_audio_'+link+'_'+lid).css({'display':'block'});

					end_left = canv_left + g_left_offset + (parseInt(audios[link]['records'][lid]['end'])/60 - g_zoom_start_time) * g_pixels_per_seconds;

					if (end_left - canv_left > g_canv_width) end_left = g_canv_width + canv_left;

					audios[link]['records'][lid]['end_pix'] = end_left - canv_left;
					audios[link]['records'][lid]['canv_left'] = canv_left;
					audios[link]['records'][lid]['el_width'] = el_width;

					context.beginPath();
					now_x = start_left - canv_left;
					now_y = start_top - canv_top + 11;

					context.moveTo(now_x,now_y);
					now_x = end_left - canv_left;

					context.lineTo(now_x,now_y);
					context.closePath();
					context.stroke();

					$('#div_link_audio_'+link+'_'+lid).draggable({
						containment:"parent",
						stop: function(event,ui){
							link = parseInt(event.target.id.split('_')[3]);
							lid = parseInt(event.target.id.split('_')[4]);
							pos = $(this).css('left').split('px')[0];

							audios[link]['records'][lid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;

						}
					});
				}

			});
		});

	}


	/*
	 * Перерисовка линии времени на панели с аудиозаписями + сдвиг маркеров времени
	 */
	function repaint_audios_time_markerline(current_x) {

		repaint_not_user_time_markerline('canv_audio_marker_id',current_x);

		$.each(audios,function(link,audio) {

			$.each(audio['records'],function(lid) {
			
				if (current_x <= audios[link]['records'][lid]['start_pix']) {
					$('#div_link_audio_'+link+'_'+lid).css({'left':0});
					audios[link]['records'][lid]['current_time'] = 0;
				} else if (current_x > audios[link]['records'][lid]['start_pix'] && current_x < audios[link]['records'][lid]['end_pix']) {

					pos = current_x - audios[link]['records'][lid]['start_pix'];

					audios[link]['records'][lid]['current_time'] = Math.ceil(pos/(g_pixels_per_seconds/60)) - 2 < 0 ? 0 : Math.ceil(pos/(g_pixels_per_seconds/60)) - 2;
					$('#div_link_audio_'+link+'_'+lid).css({'left':pos+4});

				} else {
					$('#div_link_audio_'+link+'_'+lid).css({'left':0});
					audios[link]['records'][lid]['current_time'] = 0;
				}

			});
		});
	}




	/**
	 * Временная сетка для пользователя
	 */
	function paint_timegrid(user_key) {

		if ($('#canv_base_id_'+user_key).length) {

			canvas = document.getElementById('canv_base_id_'+user_key);
			context = canvas.getContext('2d');

			context.clearRect(0,0,g_canv_width,g_row_height);
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_base_color;
			context.fillStyle = g_base_color;

			context.textBaseLine = 'middle';
			context.textAlign = 'center';

			paintTimeLineMarkers(context,1,g_row_height);
			
		}
		
		if ($('#canv_desktop_base_id_'+user_key).length) {

			canvas = document.getElementById('canv_desktop_base_id_'+user_key);
			context = canvas.getContext('2d');

			context.clearRect(0,0,g_canv_width,g_row_height);
			context.lineJoin = 'round';
			context.lineWidth = 1;
			context.strokeStyle = g_base_color;
			context.fillStyle = g_base_color;

			context.textBaseLine = 'middle';
			context.textAlign = 'center';

			paintTimeLineMarkers(context,1,g_row_height);
			
		}


	}


	/**
	 * Отрисовка временной сетки
	 * 
	 * base_id - идентификатор холста для отрисовки линий
	 * count - количество промежуточных линий
	 * offset - отступ между линиями
	 */
	function paint_not_user_timegrid(base_id,count,offset) {
		canvas2 = document.getElementById(base_id);
		context = canvas2.getContext('2d');

		obj_height = $('#'+base_id).attr('height');

		lineHeight = Math.floor(obj_height/count);

		obj_width = $('#'+base_id).attr('width');
		context.clearRect(0,0,obj_width,obj_height);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_base_color;
		context.fillStyle = g_base_color;
		context.font = "normal 8pt Courier";
		context.textBaseLine = 'middle';
		context.textAlign = 'center';

		seconds = Math.floor((g_zoom_start_time - Math.floor(g_zoom_start_time))*60);
		offset = g_zoom_start_time == 0 ? 0 : Math.floor(((60-seconds)/60)*g_pixels_per_seconds);

		paintTimeLineMarkers(context,1,parseInt(obj_height));

		if (lineHeight < parseInt(obj_height)) {

			for (i = 1; i < count; i++) {
				context.strokeStyle = g_base_color;
				context.beginPath();
				now_x = 0;
				now_y = lineHeight * i;
				context.moveTo(now_x,now_y);
				now_x = parseInt(obj_width);
				context.lineTo(now_x,now_y);
				context.closePath();
				context.stroke();
			}
		}

	}


	/*
	 * Отрисовка событий (перемещение и отображение div`ов)
	 */
	function paint_all_logs() {

		canv_top = $('#canv_log_id').offset().top;
		canv_left = $('#canv_log_id').offset().left;

		for(log in log_activity) {

			if (typeof(log_activity[log]) == 'object') {

				//$('#div_tr_log_'+log).css({'display':'none'});
				$('#div_tr_log_'+log).css({'display':'none'});
				/*
				 * Отображать только те, которые вмещаются между g_zoom_start_time и g_zoom_end_time
				 */
				if (log_activity[log]['start']/60 >= g_zoom_start_time && log_activity[log]['start']/60 < g_zoom_end_time) {
					start_top = canv_top + g_top_offset - 2;
					start_left = canv_left + g_left_offset + (parseInt(log_activity[log]['start'])/60 - g_zoom_start_time) * g_pixels_per_seconds - 10;

					$('#div_tr_log_'+log).css({'display':'block'});
					$('#div_tr_log_'+log).css({'top':start_top});
					$('#div_tr_log_'+log).css({'left':start_left});
				}
			}
		}
	}


	/**
	 * 
	 * Перерисовка линии времени на панели с событиями
	 * 
	 */
	function repaint_not_user_time_markerline(base_id,current_x) {

		obj_height = $('#'+base_id).attr('height');
		obj_width = $('#'+base_id).attr('width');

		canvas = document.getElementById(base_id);
		context = canvas.getContext('2d');

		context.clearRect(0,0,obj_width,obj_height);
		context.lineJoin = 'round';
		context.lineWidth = 1;
		context.strokeStyle = g_timeline_color;

		context.beginPath();
		now_x = g_left_offset + current_x;
		now_y = 0;
		context.moveTo(now_x,now_y);
		now_y = obj_height;
		context.lineTo(now_x,now_y);
		context.closePath();
		context.stroke();

	}
	
	
	function getTimeString(date) {
		return date.getFullYear()+'.'+(date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : (date.getMonth()+1))+'.'+(date.getDate() < 10 ? '0'+date.getDate() : date.getDate())+' '+(date.getUTCHours() < 10 ? '0'+date.getUTCHours() : date.getUTCHours())+':'+(date.getUTCMinutes() < 10 ? '0'+date.getUTCMinutes() : date.getUTCMinutes());
	}
	
}

							
							
$(window).resize(function(){
	draw();
	setTimeout(function(){draw();},300);
});

$('.blockHandle').click(function(){
	setTimeout(function(){draw();},300);
});


open_div_tooltip = function (user_key,act_key,e,elem_type) {

	m_top = e.pageY + 10 + 100 > $(document).height() ? $(document).height() - 110 : e.pageY + 10;
	m_left = e.pageX + 10 + 210 > $(document).width() ? $(document).width() - 230 : e.pageX + 10;
	$('#div_tooltip_'+user_key+'_'+act_key).css({'top':m_top});
	$('#div_tooltip_'+user_key+'_'+act_key).css({'left':m_left});
	$('#div_tooltip_'+user_key+'_'+act_key).css({'display':'block'});

	$('#div_'+elem_type+'_'+user_key+'_'+act_key).mouseleave(function(){

		$('#div_tooltip_'+user_key+'_'+act_key).fadeOut();
	})

}

open_div_log_tooltip = function (user_key,log_key,e) {

	m_top = e.pageY + 10 + 100 > $(document).height() ? $(document).height() - 110 : e.pageY + 10;
	m_left = e.pageX + 10 + 210 > $(document).width() ? $(document).width() - 230 : e.pageX + 10;

	$('#div_log_tooltip_'+user_key+'_'+log_key).css({'top':m_top});
	$('#div_log_tooltip_'+user_key+'_'+log_key).css({'left':m_left});
	$('#div_log_tooltip_'+user_key+'_'+log_key).css({'display':'block'});

	$('#div_log_'+user_key+'_'+log_key).mouseleave(function(){
		$('#div_log_tooltip_'+user_key+'_'+log_key).fadeOut();
	});

}


open_div_tr_log_tooltip = function (log_key,e) {
	m_top = e.pageY + 10 + 100 > $(document).height() ? $(document).height() - 110 : e.pageY + 10;
	m_left = e.pageX + 10 + 210 > $(document).width() ? $(document).width() - 230 : e.pageX + 10;

	$('#div_tr_log_tooltip_'+log_key).css({'top':m_top});
	$('#div_tr_log_tooltip_'+log_key).css({'left':m_left});
	$('#div_tr_log_tooltip_'+log_key).css({'display':'block'});

	$('#div_tr_log_'+log_key).mouseleave(function(){
		$('#div_tr_log_tooltip_'+log_key).fadeOut();
	});

}


/*
 * Открытие записей с камер наблюдения
 */
open_cam_video = function(cam_key,rec_key) {
	rec_id = cameras[cam_key]['records'][rec_key]['id'];
	type = cameras[cam_key]['records'][rec_key]['type'];
	curr_time = cameras[cam_key]['records'][rec_key]['current_time'];
	session_id = cameras[cam_key]['records'][rec_key]['session_id'];
	window.open(base_url+'rooms/play?session_id='+session_id+'&record_id='+rec_id+'&from_time='+curr_time+'&type='+type,rec_id,'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=600,left=0,top=0');
}

/*
 * Download
 */
download_cam_video = function(cam_key,rec_key) {
	rec_id = cameras[cam_key]['records'][rec_key]['id'];
	session_id = cameras[cam_key]['records'][rec_key]['session_id'];
	window.open('{/literal}{$smarty.server.PHP_SELF}{literal}?ctg=content&popup=1&download_media_file=1&session_id='+session_id+'&record_id='+rec_id+'&calendar_id={/literal}{$smarty.get.calendar_id}{literal}',rec_id,'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=100,height=100,left=0,top=0');
}


/*
 * Открытие записей с рабочих столов
 */
open_user_video = function(user_key,vid) {
	rec_id = users[user_key]['videos'][vid]['id'];
	curr_time = users[user_key]['videos'][vid]['current_time'];
	session_id = users[user_key]['videos'][vid]['session_id'];
	if (users[user_key]['videos'][vid]['url'] != '')
		window.open(base_url+'rooms/play?type=hls&media='+users[user_key]['videos'][vid]['url']+'&from_time='+curr_time+'&start='+curr_time,rec_id,'scrollbars=no,status=no,location=no,width=400,height=280,toolbar=no,menubar=no,left=0,top=0');
	else
		window.open(base_url+'rooms/play?session_id='+session_id+'&record_id='+rec_id+'&from_time='+curr_time,rec_id,'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=640,height=480,left=0,top=0');
}		

/*
 * Download записей с рабочих столов
 */
download_user_video = function(user_key,vid) {
	rec_id = users[user_key]['videos'][vid]['id'];
	session_id = users[user_key]['videos'][vid]['session_id'];
	window.open('{/literal}{$smarty.server.PHP_SELF}{literal}?ctg=content&popup=1&download_media_file=1&session_id='+session_id+'&record_id='+rec_id+'&calendar_id={/literal}{$smarty.get.calendar_id}{literal}',rec_id,'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=100,height=100,left=0,top=0');
}


/*
 * Открытие аудиозаписей
 */
open_link_audio = function(link,rec_key) {
	rec_id = audios[link]['records'][rec_key]['id'];
	curr_time = audios[link]['records'][rec_key]['current_time'];
	session_id = audios[link]['records'][rec_key]['session_id'];
	window.open(base_url+'rooms/record/play?session_id='+session_id+'&record_id='+rec_id+'&from_time='+curr_time,rec_id,'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=500,height=30,left=0,top=0');
}



