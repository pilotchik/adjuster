var sphinx_timeline_translations = new Array();


sphinx_timeline_translations['scenario_time']		= 'Время';
sphinx_timeline_translations['view_video']			= 'Просмотреть видео';
sphinx_timeline_translations['download_video']		= 'Скачать видео';
sphinx_timeline_translations['video']				= 'Видео публичного канала';
sphinx_timeline_translations['desktop']				= 'Рабочий стол';
sphinx_timeline_translations['audio_records']		= 'Аудиозаписи';
sphinx_timeline_translations['download_audio']		= 'Скачать аудиозапись';
sphinx_timeline_translations['translations']		= 'Трансляции';