function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function getCookie( name ) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) && ( name != document.cookie.substring( 0, name.length ) ) ) {
		return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ';', len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}

function setCookie( name, value, expires, path, domain, secure ) {
	var today = new Date();
	today.setTime( today.getTime() );
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );
	document.cookie = name+'='+escape( value ) +
		( ( expires ) ? ';expires='+expires_date.toGMTString() : '' ) + //expires.toGMTString()
		( ( path ) ? ';path=' + path : '' ) +
		( ( domain ) ? ';domain=' + domain : '' ) +
		( ( secure ) ? ';secure' : '' );
}

function deleteCookie( name, path, domain ) {
	if ( getCookie( name ) ) document.cookie = name + '=' +
			( ( path ) ? ';path=' + path : '') +
			( ( domain ) ? ';domain=' + domain : '' ) +
			';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}


function stripTags(str) {
	return str.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi, '');
}

function strip(str) {
    return str.replace(/^\s+/, '').replace(/\s+$/, '');
}

function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
  }
  
  
function escapeHTML(text) {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
	};

	return text.replace(/[&<>"']/g,function(m){return map[m];});
}


function cleanTextForRegExp(text) {
	
	t = text.replace('$','\\$');
	t = t.replace('"','\\"');
	t = t.replace("'","\\'");
	t = t.replace("(","\\(");
	t = t.replace(")","\\)");
	t = t.replace(".","\\.");
	t = t.replace("^","\\^");
	t = t.replace("[","\\[");
	t = t.replace("]","\\]");
	t = t.replace("|","\\|");
    
	return t;
	
}



function showAlert(text,type){
	
	$(".top-alert .alert-text").html(text);
	$(".top-alert").removeClass("danger").removeClass('success');
	$(".top-alert").addClass(type).addClass("show");
	
	if (type === 'success') {
		setTimeout(function(){
			$(".top-alert").removeClass("show")
		},20000);
	}
	
}

$(document).on("click",".top-alert",function(){
	$(this).removeClass("show")
})



function getAJAXProjectInfo(projectID,tt) {
        
	tt = typeof(tt) !== 'undefined' ? tt : 0;
		
	project = new Object;

	jQuery.ajax({
		type: 'get',
		async: false,
		data: {projectID:projectID,tt:tt},
		url: BASE_URL+'ajax/getProjectInfo',
		success: function(msg) {
			project = JSON.parse(msg);
		},
		error: function(jq) {
			if (jq) showAlert(JSON.parse(jq.responseText).error,'danger');
		}
	});

	return project;

}




function roomGetUserIDOverClientID (clientID) {
	if (typeof(clientID) != 'undefined') 
		return parseInt(clientID.substr(clientID.indexOf('|||')+3));
	else
		return 0;
}


function roomGetClientIDOverUserID (userID) {
	
	return ROOM_NAME + '|||' + userID;
	
}



function roomGetUserIDOverDisplay(display) {
	if (typeof(display) != 'undefined') 
		return parseInt(display.substr(display.indexOf('-|||-')+5));
	else
		return 0;
}


function roomHeartBeat() {
	
	ROOM_NAME = typeof(ROOM_NAME) !== 'undefined' ? ROOM_NAME : 0
	
	jQuery.ajax({
		type: 'get',
		data: {userID:LOCAL_USER,roomID:ROOM_NAME},
		url: BASE_URL+'ajax/setHeartBeat',
		success: function(msg) {
			//Дописать обработку сессионных пользователей
			
			try {
			
				sanswer = JSON.parse(msg);

				if (typeof(sanswer['status']) !== 'undefined' && sanswer['status'] === 403) {

					console.log(sanswer)
					
					window.location.reload();

				} else {

					if (typeof(sanswer['roomUsers']) !== 'undefined' && typeof(ROOM_USERS) !== 'undefined') {

						startSumm = 0;
						$.each(ROOM_USERS,function(nel,el){
							startSumm += parseInt(nel)
						})

						$.each(sanswer['roomUsers'],function(nel,el){
							if (typeof(ROOM_USERS[nel]) === 'undefined') {
								ROOM_USERS[nel] = el;
							}
						})

						$.each(ROOM_USERS,function(nel,el){
							if (typeof(sanswer['roomUsers'][nel]) === 'undefined') {

								roomNewUserRole(nel,'listener')

								delete ROOM_USERS[nel];
							}
						})

						endSumm = 0;
						$.each(ROOM_USERS,function(nel,el){
							endSumm += parseInt(nel)
						})

						if (startSumm != endSumm) {
							channel.publish('irida.users',{
								users: ROOM_USERS
							});
						}

					}
					
					
					if (typeof(sanswer['roomInfo']) !== 'undefined' && typeof(ROOM.info) !== 'undefined') {
					
						var changingString = '';
						
						if (ROOM.info.cooperation_type === sanswer.roomInfo.cooperation_type) {
						
							if (ROOM.info.c_self_betweenfaces_switching !== sanswer.roomInfo.c_self_betweenfaces_switching) {
								ROOM.info.c_self_betweenfaces_switching = sanswer.roomInfo.c_self_betweenfaces_switching;
								changingString += ROOM_TRANSLATOR.room_config_c_self_betweenfaces_switching + ' - ' + (ROOM.info.c_self_betweenfaces_switching === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
								roomPanelInitializion();
							}

							if (ROOM.info.c_self_docsonboard_publication !== sanswer.roomInfo.c_self_docsonboard_publication) {
								ROOM.info.c_self_docsonboard_publication = sanswer.roomInfo.c_self_docsonboard_publication;
								changingString += ROOM_TRANSLATOR.room_config_c_self_docsonboard_publication + ' - ' + (ROOM.info.c_self_docsonboard_publication === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false)  + ' <br>';
							}

							if (ROOM.info.c_self_betweendocs_switching !== sanswer.roomInfo.c_self_betweendocs_switching) {
								ROOM.info.c_self_betweendocs_switching = sanswer.roomInfo.c_self_betweendocs_switching;
								changingString += ROOM_TRANSLATOR.room_config_c_self_betweendocs_switching + ' - ' + (ROOM.info.c_self_betweendocs_switching === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_self_scale_changing !== sanswer.roomInfo.c_self_scale_changing) {
								ROOM.info.c_self_scale_changing = sanswer.roomInfo.c_self_scale_changing;
								changingString += ROOM_TRANSLATOR.room_config_c_self_scale_changing + ' - ' + (ROOM.info.c_self_scale_changing === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_board_listeners_drawing !== sanswer.roomInfo.c_board_listeners_drawing) {
								ROOM.info.c_board_listeners_drawing = sanswer.roomInfo.c_board_listeners_drawing;
								changingString += ROOM_TRANSLATOR.room_config_c_board_listeners_drawing + ' - ' + (ROOM.info.c_board_listeners_drawing === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_work_foreign_boards_elements !== sanswer.roomInfo.c_work_foreign_boards_elements) {
								ROOM.info.c_work_foreign_boards_elements = sanswer.roomInfo.c_work_foreign_boards_elements;
								changingString += ROOM_TRANSLATOR.room_config_c_work_foreign_boards_elements + ' - ' + (ROOM.info.c_work_foreign_boards_elements === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_share_self_screen !== sanswer.roomInfo.c_share_self_screen) {
								ROOM.info.c_share_self_screen = sanswer.roomInfo.c_share_self_screen;
								changingString += ROOM_TRANSLATOR.room_config_c_share_self_screen + ' - ' + (ROOM.info.c_share_self_screen === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_self_desktop_connection !== sanswer.roomInfo.c_self_desktop_connection) {
								ROOM.info.c_self_desktop_connection = sanswer.roomInfo.c_self_desktop_connection;
								changingString += ROOM_TRANSLATOR.room_config_c_self_desktop_connection + ' - ' + (ROOM.info.c_self_desktop_connection === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (ROOM.info.c_self_documents_view !== sanswer.roomInfo.c_self_documents_view) {
								ROOM.info.c_self_documents_view = sanswer.roomInfo.c_self_documents_view;
								changingString += ROOM_TRANSLATOR.room_config_c_self_documents_view + ' - ' + (ROOM.info.c_self_documents_view === '1' ? ROOM_TRANSLATOR.room_config_true : ROOM_TRANSLATOR.room_config_false) + ' <br>';
							}

							if (changingString !== '') {
								
								roomVideoInitRoles && roomVideoInitRoles();
								roomIridaBoardInit && roomIridaBoardInit();
								changingString = ROOM_TRANSLATOR.room_config_changing + '<br>' + changingString;
								showAlert(changingString,'success');
								
							}

						} else {
							
							showAlert(ROOM_TRANSLATOR.room_config_cooperation_type_was_changed,'success');
							setTimeout(roomReloadWindow,5000);
							
						}
						
					}
					
					
					if (!ROOM_COMMON_CONFIG.currentPollAnswerWaiting && Object.keys(sanswer.roomPolls).length && typeof(ROOM.info) !== 'undefined') {
						
						roomPollsPlaying(sanswer.roomPolls);
						
					} else if (!Object.keys(sanswer.roomPolls).length) {
						
						roomPollsPlayingStop();
						
					}
					
					
					
					setTimeout(roomHeartBeat,20000);

				}
				
			} catch (e) {
				console.error(e);
			}

		},
		error: function(jq) {
			if (jq && jq.status > 0 && jq.responseText != '' && jq.statusText != '') showAlert(JSON.parse(jq.responseText).error,'danger');
		}
		
	});
		
}


/**
 * F5 и перезагрузка
 * 
 * @returns {undefined}
 */
function roomReloadWindow() {
	window.location.reload();
}


/**
 * Генерация токена для трансляции
 * 
 * @param {type} status
 * @param {type} callback
 * @returns {undefined}
 */
function roomMakeToken(status,callback) {
	
	jQuery.ajax({
		type: 'get',
		data: {roomID:ROOM_NAME,status:(status ? 1 : 0)},
		url: BASE_URL+'rooms/tokenManager',
		success: function(msg) {
			//Дописать обработку сессионных пользователей
			sanswer = JSON.parse(msg);
			
			
			typeof(callback) !== 'undefined' && callback(sanswer.token,sanswer.url);

		},
		error: function(jq) {
			if (jq && jq.status > 0 && jq.responseText != '' && jq.statusText != '') showAlert(JSON.parse(jq.responseText).error,'danger');
		}
		
	});
	
	
}


/**
 * Проверка занятости комнат
 * 
 * @returns {undefined}
 */
function checkRoomsOnlines() {
        
	jQuery.ajax({
		type: 'post',
		async: true,
		data: {roomsArray:rooms},
		url: BASE_URL+'ajax/getRoomsOnlines',
		success: function(msg) {
			info = JSON.parse(msg);

			$.each(info.answer,function(numb,el){
				if (el.count > 0) {
					$('#roomOnlineStatus_'+el.room_id).css('display','');
					$('#roomOnlineStatus_'+el.room_id).html(el.count);
					$('#roomOnlineStatus_'+el.room_id).attr('title',ROOM_TRANSLATOR.room_users_online_now_list+el.users);
				} else {
					$('#roomOnlineStatus_'+el.room_id).css('display','none');
					$('#roomOnlineStatus_'+el.room_id).attr('title',ROOM_TRANSLATOR.room_users_online_now);
					
				}
			});

			progressTimeoutID = setTimeout(checkRoomsOnlines,15000);
		}
	});


}


/**
 * JSColorPicker
 * 
 * @param {type} picker
 * @param {type} id
 * @returns {undefined}
 */
function setTextColor(picker,id) {
	$('#'+id).attr('value',picker.toString());
}


function landingConnectOverID() {
	
	$('#roomConnectOverIDModal').modal('show');
	
	
}



function landingConnectOverIDChecking(el) {
	
	$(el).prepend('<i class="fa fa-circle-o-notch fa-spin" style="margin-right:5px;"></i>');
	
	roomID = $('#connectOverRoomIDValue').val();
	
	jQuery.ajax({
		type: 'post',
		async: true,
		data: {roomID:roomID},
		url: BASE_URL+'ajax/checkRoomID',
		success: function(msg) {
			info = JSON.parse(msg);

			$(el).find('i').remove();
			
			//$('#roomLinkHREFID').remove();
			//$('body').append('<a href="'+BASE_URL+'rooms/one_room/'+roomID+'" id="roomLinkHREFID" target="blank">123</a>')
			//$('#roomLinkHREFID').click();
			window.open(BASE_URL+'rooms/one_room/'+roomID);
			$('#connectOverRoomIDValue').val('')
			$('#roomConnectOverIDModal').modal('hide');
			
		},
		error: function(jq) {
			$(el).find('i').remove();
			showAlert(JSON.parse(jq.responseText).error,'danger');
		}
	});
	
	
}



function showRoomInfo(roomID,el) {
        
	if (typeof(el) !== 'undefined') $(el).find('i').removeClass('fa-info-circle').addClass('fa-circle-o-notch').addClass('fa-spin');
		
	room = getAJAXRoomInfo(roomID);
    
	if (typeof(el) !== 'undefined') $(el).find('i').addClass('fa-info-circle').removeClass('fa-circle-o-notch').removeClass('fa-spin');
        
	$('#roomInfoName').html(room.info.name);
	$('#roomInfoID').html(room.info.id);
	$('#roomInfoCreator').html(room.info.creatorShortName);
	$('#roomInfoCreator').attr('title',room.info.creatorFullName);
	$('#roomInfoCreateDate').html(room.info.createDate);
	$('#roomInfoCooperation').html(room.info.cooperationTranslate);

	if (room.info.cooperation_type === 'lesson' || room.info.cooperation_type === 'meeting') {
		//$('#viewBehaviorTab').fadeIn();
		$('#viewDesktopsTab').fadeIn();
	} else {
		//$('#viewBehaviorTab').fadeOut();
		$('#viewDesktopsTab').fadeOut();
	}



	if (room.info.projectID != room.info.default_project_id) {

		$('#roomInfoProject').html(room.info.projectName);

	} else $('#roomInfoProject').html('-');

	if (room.info.connection_notinvited_users === "1") {
		$('#roomInfoNotinvited').html(ROOM_TRANSLATOR.room_connection_notinvited_users_yes);
	} else {
		$('#roomInfoNotinvited').html(ROOM_TRANSLATOR.room_connection_notinvited_users_no);
	}

	if (room.info.connection_files_public_access == "1") 
		$('#roomInfoFilesPublic').html(ROOM_TRANSLATOR.room_connection_files_public_access_yes);
	else
		$('#roomInfoFilesPublic').html(ROOM_TRANSLATOR.room_connection_files_public_access_no);


	$('.inputEditRoomSpeechChannels').html(room.info.public_speech_channels_count);

	activeDeactive('.inputEditRoomNotinvitedConnection',room.info.connection_notinvited_users);
	activeDeactive('.inputEditRoomFilesPublic',room.info.connection_files_public_access);
	activeDeactive('.inputEditRoomUserInviting',room.info.p_user_inviting);
	activeDeactive('.inputEditRoomEditing',room.info.p_room_editing);
	activeDeactive('.inputEditRoomWebcamTranslating',room.info.c_webcams_translating);
	activeDeactive('.inputEditRoomInteractiveBoard',room.info.c_interactive_board);
	activeDeactive('.inputEditRoomMessaging',room.info.c_messaging);
	activeDeactive('.inputEditRoomListViewing',room.info.c_listenerslist_viewing);
	activeDeactive('.inputEditRoomPointerUsing',room.info.c_pointer_using);
	activeDeactive('.inputEditRoomRandomConnection',room.info.c_random_connection);
	activeDeactive('.inputEditRoomRecordChannels',room.info.record_status);
	activeDeactive('.inputEditRoomMasterlessMode',room.info.masterless_mode);

	//Behaviors
	activeDeactive('.inputEditRoomc_self_secvideoch_maximizing',room.info.c_self_secvideoch_maximizing);
	activeDeactive('.inputEditRoomc_self_betweenfaces_switching',room.info.c_self_betweenfaces_switching);
	activeDeactive('.inputEditRoomc_self_betweendocs_switching',room.info.c_self_betweendocs_switching);
	activeDeactive('.inputEditRoomc_board_listeners_drawing',room.info.c_board_listeners_drawing);
	activeDeactive('.inputEditRoomc_share_self_screen',room.info.c_share_self_screen);
	activeDeactive('.inputEditRoomc_self_betweendocssites_switching',room.info.c_self_betweendocssites_switching);
	activeDeactive('.inputEditRoomc_self_scale_changing',room.info.c_self_scale_changing);
	activeDeactive('.inputEditRoomc_self_desktop_connection',room.info.c_self_desktop_connection);
	activeDeactive('.inputEditRoomc_self_docsonboard_publication',room.info.c_self_docsonboard_publication);
	activeDeactive('.inputEditRoomc_self_video_starting',room.info.c_self_video_starting);
	activeDeactive('.inputEditRoomc_work_foreign_boards_elements',room.info.c_work_foreign_boards_elements);
	activeDeactive('.inputEditRoomc_self_documents_view',room.info.c_self_documents_view);

	$('.inputEditRoomс_upload_maxfilesize').html(room.info.с_upload_maxfilesize)

	//Remote links
	$('.remoteLinksContainerID tbody').html('');

	if (room.info.remoteLinks.length) {

		room.info.remoteLinks.forEach(function(el,numb) {
			code = '<tr>\n\
				<td>'+el.link+'<input type="hidden" name="links[]" value="'+el.link+'"></td>\n\
				<td>'+el.description+'<input type="hidden" name="links_desc[]" value="'+el.description+'"></td>\n\
			</tr>';
			$('.remoteLinksContainerID .table-responsive').css('display','');
			$('.remoteLinksContainerID tbody').append(code)
		});

	}

	//Listeners
	$('#roomInfoUsers tbody').html('');
	$('.usersClass').css('display','none');

	for (el in room.users){
		$('.usersClass').css('display','');
		$('#roomInfoUsers tbody').append('<tr><td><span class="userInfo" title="'+room.users[el].userFullName+'">'+room.users[el].userShortName+'</span></td>\n\
			<td class="center-align">'+room.users[el].userRoleTranslate+'</td>\n\
			<td><span class="userInfo" title="'+room.users[el].inviterFullName+'">'+room.users[el].inviterShortName+'</span></td>\n\
			<td class="center-align date-inactive-status">'+room.users[el].inviteDate+'</td>\n\
		</tr>');
	}

	$('#roomInfoModal').modal('show');
}
	
	
	
function getAJAXRoomInfo(roomID,tt) {

	tt = typeof(tt) !== 'undefined' ? tt : 0;

	room = new Object;

	jQuery.ajax({
		type: 'get',
		async: false,
		data: {roomID:roomID,tt:tt},
		url: BASE_URL + 'ajax/getRoomInfo',
		success: function(msg) {
			room = JSON.parse(msg);
		},
		error: function(jq) {
			try {
				showAlert(JSON.parse(jq.responseText).error,'danger');
			} catch (e) {
				console.error(jq);
			}

		}
	});

	return room;
}


function activeDeactive(el,type) {
	if (type === "1") {
		$(el).addClass('glyphicon-ok');
		$(el).removeClass('glyphicon-minus');
	} else {
		$(el).addClass('glyphicon-minus');
		$(el).removeClass('glyphicon-ok');
	}
}



function openRoomStat(roomID,el) {

	if (typeof(el) !== 'undefined') $(el).find('i').removeClass('fa-bar-chart').addClass('fa-circle-o-notch').addClass('fa-spin');

	//Асинхронно получить SVG доски и подгрузить в аватарки комнат
	jQuery.ajax({
		type: 'post',
		async: true,
		data: {roomID:roomID},
		url: BASE_URL + 'rooms/getRoomsStatistic',
		success: function(msg) {

			if (typeof(el) !== 'undefined') $(el).find('i').addClass('fa-bar-chart').removeClass('fa-circle-o-notch').removeClass('fa-spin');

			stat = JSON.parse(msg);
			//console.log(stat);

			var dataPointsFromJSON = [];

			$.each(stat.metriks,function(nel,mdate){
				dataPointsFromJSON.push({label: mdate.date,y: mdate.count});
			});

			$('#roomStatModal').modal('show');

			$('#roomStatModal').on('shown.bs.modal', function (e) {
				var chart2 = new CanvasJS.Chart("chartRoomStatistic", {

					title:{
						text: ROOM_TRANSLATOR.room_statistic_balls
					},
					data: [//array of dataSeries              
						{ //dataSeries object
							/*** Change type "column" to "bar", "area", "line" or "pie"***/
							type: "line",
							dataPoints: dataPointsFromJSON
						}
					]
				});

				chart2.options.title = {fontSize:12};

				chart2.render();

			})

			//$('#chartRoomStatistic').css('width',parseInt($('#roomStatModal .modal-body').width()));

		},
		error: function(jq) {

			if (typeof(el) !== 'undefined') $(el).find('i').addClass('fa-bar-chart').removeClass('fa-circle-o-notch').removeClass('fa-spin');

			if (jq) showAlert(JSON.parse(jq.responseText).error,'danger');

		}
	});


}

	
	
function inviteUsers(roomID,el,openModal) {
    
	if (openModal) {

		TRANSACTION_TIME = moment().format('x')

		//$(el).find('.glyphicon').css('display','none');
		//$(el).find('.spinner-sm').css('display','');
		$(el).find('.fa').removeClass('fa-users').addClass('fa-circle-o-notch').addClass('fa-spin');
	}

	room = getAJAXRoomInfo(roomID,TRANSACTION_TIME);

	if (openModal) {
		//$(el).find('.spinner-sm').css('display','none');
		//$(el).find('.glyphicon').css('display','');
		$(el).find('.fa').addClass('fa-users').removeClass('fa-circle-o-notch').removeClass('fa-spin');
	}

	$('#inputEditRoomID').val(room.info.id);


	$('.roomAccessLevelsUsersContainer').html('');

	//console.log(room)

	var roomCreatorID = parseInt(room.info.creator_id);

	//Поместить в заголовок контейнера владельца, который есть всегда и менять его нельзя
	var i = 3;
	$.each(room.users,function(nel,user){

		i += 1;
		code = '';

		if (parseInt(user.user_id) == roomCreatorID)
			roleCode = ROOM_TRANSLATOR.room_creator_short;
		else {

			roleCode = '<div class="btn-group dropup">\n\
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" title="'+(user.role == 'secondary_master' ? ROOM_TRANSLATOR.room_user_role_secondary_master : ROOM_TRANSLATOR.room_user_role_default)+'">\n\
					<i class="fa '+(user.role == 'secondary_master' ? 'fa-microphone' : ' fa-microphone-slash') + '" id="roomAccessLevelsButtonImage_'+user.user_id+'"></i>&nbsp;<span class="caret"></span>\n\
				</button>\n\
				<ul class="dropdown-menu dropdown-menu-right" role="menu">\n\
				  <li><a href="#" onclick="inviteUsersChangeRole(this,event,\'secondary_master\')" user-id="'+user.user_id+'">'+ROOM_TRANSLATOR.room_user_role_secondary_master+'</a></li>\n\
				  <li><a href="#" onclick="inviteUsersChangeRole(this,event,\'default\')" user-id="'+user.user_id+'">'+ROOM_TRANSLATOR.room_user_role_default+'</a></li>\n\
				</ul>\n\
			</div>';

		}

		code += '<div class="roomAccessLevelsUserContainer row" id="roomAccessLevelsUserContainer_' + user.user_id + '">\n\
			<div class="roomAccessLevelsAvatar col-sm-1 center-align">\n\
				<img src="' + (user.avatarURL_conf != '' ? user.avatarURL_conf : user.avatarURL_small) +'" class="smallUserAvatarInTables">\n\
			</div>\n\
			<div class="roomAccessLevelsInfo col-sm-8" title="'+user.userFullName+'">\n\
				<div>' + user.userFullName + (user.user_id == LOCAL_USER_ID ? ' ('+ROOM_TRANSLATOR.room_users_access_levels_you+')' : '') + '<br><span>' + user.login + '</span></div>\n\
			</div>\n\
			<div class="roomAccessLevelsRole col-sm-2" >\n\
				'+roleCode+'\n\
			</div>\n\
			<div class="roomAccessLevelsDel col-sm-1">'+(user.user_id != LOCAL_USER_ID && parseInt(user.user_id) != roomCreatorID ? '<div onclick="inviteUsersRemoveParticipant(this,event)" user-id="'+user.user_id+'"><span class="glyphicon glyphicon-remove" title="'+ROOM_TRANSLATOR.room_users_access_levels_remove+'"></span><img src="'+BASE_URL+'assets/images/loading_spinner.gif" class="spinner-sm" style="display: none;width:18px;height:18px;margin-top: 10px;"></div>' : '')+'</div>\n\
		</div>';

		if (parseInt(user.user_id) == roomCreatorID) {
			$('.roomAccessLevelsCreatorContainer').append(code);
		} else if (user.role == 'secondary_master') {
			$('.roomAccessLevelsSecondaryMastersContainer').append(code);
		} else {
			$('.roomAccessLevelsParticipantsContainer').append(code);
		}

	});

	if (openModal) $('#inviteUsersInRoomModal').modal('show');
}



function updateRoomInfo(roomID,el,projectID) {
        
	projectID = typeof(projectID) === 'undefined' ? 0 : projectID;
		
		
	$(el).find('.fa').removeClass('fa-edit').addClass('fa-circle-o-notch').addClass('fa-spin');
        
	room = getAJAXRoomInfo(roomID);
        
	//$(el).find('.glyphicon').css('display','');
	//$(el).find('.spinner-sm').css('display','none');
	$(el).find('.fa').addClass('fa-edit').removeClass('fa-circle-o-notch').removeClass('fa-spin');
        
        
	$('#inputEditRoomID').val(room.info.id);

	$('#inputEditProjectID').val(projectID);

	$('#inputEditRoomName').val(room.info.name);

	$('#inputEditRoomCooperation').val(room.info.cooperation_type);
	//$('#inputEditRoomCooperation').html(room.info.cooperationTranslate);

	if (room.info.cooperation_type === 'lesson' || room.info.cooperation_type === 'meeting') {
		$('#editBehaviorTab').fadeIn();
	} else {
		$('#editBehaviorTab').fadeOut();
	}

	room.info.connection_notinvited_users === "1" ? $('.inputEditRoomNotinvitedConnection').prop('checked',true) : $('.inputEditRoomNotinvitedConnection').prop('checked',false);
	room.info.connection_files_public_access === "1" ? $('.inputEditRoomFilesPublic').prop('checked',true) : $('.inputEditRoomFilesPublic').prop('checked',false);


	$('.inputEditRoomSpeechChannels').val(room.info.public_speech_channels_count);

	room.info.p_user_inviting === "1" ? $('.inputEditRoomUserInviting').prop('checked',true) : $('.inputEditRoomUserInviting').prop('checked',false);
	room.info.p_room_editing === "1" ? $('.inputEditRoomEditing').prop('checked',true) : $('.inputEditRoomEditing').prop('checked',false);


	room.info.c_webcams_translating === "1" ? $('.inputEditRoomWebcamTranslating').prop('checked',true) : $('.inputEditRoomWebcamTranslating').prop('checked',false);
	room.info.c_interactive_board === "1" ? $('.inputEditRoomInteractiveBoard').prop('checked',true) : $('.inputEditRoomInteractiveBoard').prop('checked',false);
	room.info.c_messaging === "1" ? $('.inputEditRoomMessaging').prop('checked',true) : $('.inputEditRoomMessaging').prop('checked',false);
	room.info.c_listenerslist_viewing === "1" ? $('.inputEditRoomListViewing').prop('checked',true) : $('.inputEditRoomListViewing').prop('checked',false);
	room.info.c_pointer_using === "1" ? $('.inputEditRoomPointerUsing').prop('checked',true) : $('.inputEditRoomPointerUsing').prop('checked',false);
	room.info.c_random_connection === "1" ? $('.inputEditRoomRandomConnection').prop('checked',true) : $('.inputEditRoomRandomConnection').prop('checked',false);
	room.info.record_status === "1" ? $('.inputEditRoomRecordChannels').prop('checked',true) : $('.inputEditRoomRecordChannels').prop('checked',false);
	room.info.masterless_mode === "1" ? $('.inputEditRoomMasterlessMode').prop('checked',true) : $('.inputEditRoomMasterlessMode').prop('checked',false);

	$('.inputEditRoomUploadMaxfilesize').val(room.info.с_upload_maxfilesize)

	//Behaviors
	room.info.c_self_secvideoch_maximizing === "1" ? $('.inputEditRoomc_self_secvideoch_maximizing').prop('checked',true) : $('.inputEditRoomc_self_secvideoch_maximizing').prop('checked',false);
	room.info.c_self_betweenfaces_switching === "1" ? $('.inputEditRoomc_self_betweenfaces_switching').prop('checked',true) : $('.inputEditRoomc_self_betweenfaces_switching').prop('checked',false);
	room.info.c_self_betweendocs_switching === "1" ? $('.inputEditRoomc_self_betweendocs_switching').prop('checked',true) : $('.inputEditRoomc_self_betweendocs_switching').prop('checked',false);
	room.info.c_board_listeners_drawing === "1" ? $('.inputEditRoomc_board_listeners_drawing').prop('checked',true) : $('.inputEditRoomc_board_listeners_drawing').prop('checked',false);
	room.info.c_share_self_screen === "1" ? $('.inputEditRoomc_share_self_screen').prop('checked',true) : $('.inputEditRoomc_share_self_screen').prop('checked',false);
	room.info.c_self_betweendocssites_switching === "1" ? $('.inputEditRoomc_self_betweendocssites_switching').prop('checked',true) : $('.inputEditRoomc_self_betweendocssites_switching').prop('checked',false);
	room.info.c_self_scale_changing === "1" ? $('.inputEditRoomc_self_scale_changing').prop('checked',true) : $('.inputEditRoomc_self_scale_changing').prop('checked',false);
	room.info.c_self_desktop_connection === "1" ? $('.inputEditRoomc_self_desktop_connection').prop('checked',true) : $('.inputEditRoomc_self_desktop_connection').prop('checked',false);
	room.info.c_self_documents_view === "1" ? $('.inputEditRoomc_self_documents_view').prop('checked',true) : $('.inputEditRoomc_self_documents_view').prop('checked',false);
	room.info.c_self_docsonboard_publication === "1" ? $('.inputEditRoomc_self_docsonboard_publication').prop('checked',true) : $('.inputEditRoomc_self_docsonboard_publication').prop('checked',false);
	room.info.c_self_video_starting === "1" ? $('.inputEditRoomc_self_video_starting').prop('checked',true) : $('.inputEditRoomc_self_video_starting').prop('checked',false);
	room.info.c_work_foreign_boards_elements === "1" ? $('.inputEditRoomc_work_foreign_boards_elements').prop('checked',true) : $('.inputEditRoomc_work_foreign_boards_elements').prop('checked',false);


	//console.log(room);

	$('#updateRoomInfoModal').modal('show');
}

    
	
/**
 * Создание новой комнаты с динамическим набором пользователей
 * 
 * @returns {undefined}
 */
function landingCreateNewRoom() {
	
	ROOM_REAL_CANDIDATES = {};
	ROOM_CANDIDATES = {};
	$('#roomCreatingOverUsersIDModal').modal('show');
	
}



function loadUsersForCreating(elem,projectID) {
	
	projectID = typeof(projectID) === 'undefined' ? 0 : projectID;
	
	input_text = $(elem).val();
	re = /\s*,\s*/;
	taglist = input_text.split(re);
	text = taglist[taglist.length - 1];
	text = text.trim();
	$('#userAjaxLoadPanelID').css({'display':'none'});

	roomID = $('#inputEditRoomID').val();

	if (text !== "" && text.length > 0) {

		$('#userAjaxLoadPanelID').css({'display':'block'});

		$('#userAjaxLoadPanelID').html('<div style="width:100%;text-align:center;"><img src="'+BASE_URL+'assets/images/loading_spinner.gif" class="spinner-sm"></div>');

		jQuery.ajax({
			type: 'post',
			async: true,
			data: {text:text,limit:10,projectID:projectID,projectCreatorID:LOCAL_USER_ID},
			url: BASE_URL+ 'ajax/getUserSearchForProject',
			success: function(msg) {
				result = JSON.parse(msg);
				console.log(result);

				$('#roomSearchRoleButtonSpanID').html(ROOM_TRANSLATOR.room_user_role);

				if (result.users.length) {

					$('#userAjaxLoadPanelID').html('');

					result.users.forEach(function(el){

						if (typeof(el.id) !== 'undefined' && el.id !== LOCAL_USER_ID && typeof(ROOM_REAL_CANDIDATES[el.id]) === 'undefined') {

							if (typeof(ROOM_CANDIDATES[el.id]) === 'undefined') ROOM_CANDIDATES[el.id] = el;

							code = '<div class="row loadedSearchedUser" onclick="inviteUsersAddParticipant('+el.id+')">\n\
								<div class="col-sm-1 center-align"><img class="smallUserAvatarInTables" src="' + el.avatarURL_small + '"></div>\n\
								<div class="col-sm-5" title="'+el.fullName+'">' + el.fullName + '<br><span class="loadedSearchedUserLogin">'+el.login+'</span></div>\n\
								<div class="col-sm-6">'+el.tags+'</div>\n\
							</div>';
							$('#userAjaxLoadPanelID').append(code);
						}
					});

				} else {
					$('#userAjaxLoadPanelID').html('<span class="notFoundedData">'+ROOM_TRANSLATOR.data_not_found+'</span>');
				}

			},
			error: function(jq) {

				$('#roomSearchRoleButtonSpanID').html(ROOM_TRANSLATOR.room_user_role);

				if (jq) {
					showAlert(JSON.parse(jq.responseText).error,'danger');
				}
			}
		});

	}
	
}



function inviteUsersAddParticipant(id) {
        
	$('#userAjaxLoadPanelID').css({'display':'none'});
	$('#inputEditUserTag').val('');

	if (typeof(ROOM_CANDIDATES[id]) !== 'undefined') {

		ROOM_REAL_CANDIDATES[id] = user = ROOM_CANDIDATES[id];

		roleCode = '<div class="btn-group dropup">\n\
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">\n\
				<i class="fa fa-lg ' + (user.role === 'secondary_master' ? 'fa-microphone' : 'fa-microphone-slash') + '"  id="roomAccessLevelsButtonImage_'+user.id+'"></i>&nbsp;<span class="caret"></span>\n\
			</button>\n\
			<ul class="dropdown-menu dropdown-menu-right" role="menu">\n\
			  <li><a href="#" onclick="inviteUsersChangeRoleInvite(' + user.id + ',\'secondary_master\')" user-id="'+id+'">'+ROOM_TRANSLATOR.room_user_role_secondary_master+'</a></li>\n\
			  <li><a href="#" onclick="inviteUsersChangeRoleInvite(' + user.id + ',\'default\')" user-id="'+id+'">'+ROOM_TRANSLATOR.room_user_role_default+'</a></li>\n\
			</ul>\n\
		</div>';

		code = '<div class="roomAccessLevelsUserContainer row" id="roomAccessLevelsUserContainer_' + user.id + '">\n\
			<div class="roomAccessLevelsAvatar col-sm-1 center-align">\n\
				<img src="' + (user.avatarURL_conf !== '' ? user.avatarURL_conf : user.avatarURL_small ) +'" class="smallUserAvatarInTables">\n\
			</div>\n\
			<div class="roomAccessLevelsInfo col-sm-8" title="'+user.fullName+'">\n\
				<div>' + user.fullName + '<br><span>' + user.login + '</span></div>\n\
				<input type="hidden" name="users_participants[]" id="roomAccessLevel_'+user.id+'" value="' + user.id + '">\n\
			</div>\n\
			<div class="roomAccessLevelsRole col-sm-2">\n\
				'+roleCode+'\n\
			</div>\n\
			<div class="room-access-levels-del col-sm-1">\n\
				<div onclick="inviteUsersRemoveParticipantFromInvite(' + user.id + ')" user-id="' + user.id + '">\n\
					<i class="fa fa-trash fa-lg" title="'+ROOM_TRANSLATOR.room_users_access_levels_remove+'"></i>\n\
				</div>\n\
			</div>\n\
		</div>';

		$('.roomAccessLevelsParticipantsContainer').append(code);

	}
	
}
    
	
function inviteUsersRemoveParticipantFromInvite(userID) {
	
	$('#roomAccessLevelsUserContainer_'+userID).remove();
	delete ROOM_REAL_CANDIDATES[userID];
	
}


function inviteUsersChangeRoleInvite(userID,role) {
        
	if (role === 'secondary_master') {
		$('#roomAccessLevelsUserContainer_'+userID).appendTo('.roomAccessLevelsSecondaryMastersContainer');
		$('#roomAccessLevel_'+userID).attr('name','users_secondary_masters[]');
		$('#roomAccessLevelsButtonImage_'+userID).removeClass('fa-microphone-slash').addClass('fa-microphone');
	} else {
		$('#roomAccessLevelsUserContainer_'+userID).appendTo('.roomAccessLevelsParticipantsContainer');
		$('#roomAccessLevel_'+userID).attr('name','users_participants[]');
		$('#roomAccessLevelsButtonImage_'+userID).addClass('fa-microphone-slash').removeClass('fa-microphone');
	}

}






function getClassElMaxHeight(cn) {
	
	potH = 0;
	$.each($('.'+cn),function(nel,el){if($(el).height() > potH) potH = $(el).height();});
	return potH;
	
}



function loadAnswersForm() {
    
    $.each($('.test-answers-form'),function(nel,el){
       
        targetObject = $('#'+$(el).attr('target'));
        targetLine = $('#'+$(el).attr('target')+'Line');
        
        code = '<div class="answers-container">'
        
        for (var i = 1; i < 18; i++) {
            
            code += '<div class="answer-span" onclick="manageAnswer(\''+$(el).attr('target')+'\',\'' + $(el).attr('target')+'Line' + '\',\''+BASE_URL+'assets/images/answers/'+ i + '.gif\',' + i + ')" style="background-image:url('+BASE_URL+'assets/images/answers/' + i + '.gif)"></div>';
            
        }
        
        code += '</div>';
        
        $(el).append(code);
        
        $(targetObject).on('keypress',function(e){
        
            if (e.keyCode === 8 && $(this).html().substr(-1) === '>') {

                $(this).find('div:last').remove();
                
                $(targetLine).val($(targetLine).val().substr(0,$(targetLine).val().lastIndexOf('|')));
                
            } else {
                
                e.preventDefault();
                
            }
            console.log(e,$(this))

        })

        
        
    });
    
    
}


function manageAnswer(to,tl,url,numb) {
    
    $('#'+to).append('<div class="answer-span" style="background-image:url('+url+')"></div>');
    
    $('#'+tl).val($('#'+tl).val()+'|'+numb);
    
}



