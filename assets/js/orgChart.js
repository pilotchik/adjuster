//Вертикальное расстояние между элементами
var chartVerticalOffset = 40;
//Горизонтальное расстояние между элементами
var chartHorizontalOffset = 20;
//Отступ сверху
var chartTopOffset = 10;
//Высота полотна
var canvasHeight = 0;
//Ширина полотна
var canvasWidth = 0;
//Высота блока
var blockHeight = 0;
//Ширина блока
var blockWidth = 0;
//Отступ холста сверху
var canvasTopOffset = 0;
//Отступ холста слева
var canvasLeftOffset = 0;
//Корневые подразделения
var canvas,context;

var coords = new Object();


function paintChart() {

	blockHeight = $('.structureChartsItem').height();
	blockWidth = $('.structureChartsItem').width();

	canvasTopOffset = $('#orgchartCanvasID').offset().top - 100;
	canvasLeftOffset = parseInt($('.main').css('padding-left'));

	canvasHeight = chartTopOffset + blockHeight*levelsCount + (levelsCount-1)*chartVerticalOffset;
	canvasWidth = $('.main').width()
	

	$('#orgchartCanvasID').attr({'width':canvasWidth,'height':canvasHeight});

	for(levelNumb = 1; levelNumb <= levelsCount;levelNumb++) {

		var additionalOffset = 0;
		var addNumb = -1;
		var elParentID = 0;
		coords[levelNumb] = new Array();
		
		$('.level'+levelNumb).each(function(numb,el){

			//Получить значения из атрибутов элементов
			elChildsCount = parseInt($(el).attr('childs-count'));

			//Если идентификатор родителя не совпадает с предыдущим значением, то обнулить нумерацию и назначить новый идентификатор родителя
			if (parseInt($(el).attr('parent-id')) != elParentID) {
				elParentID = parseInt($(el).attr('parent-id'));
				addNumb = -1;
			}

			//Вычислить отступ сверху
			levelTopOffset = canvasTopOffset + chartTopOffset + levelNumb*blockHeight + (levelNumb-1)*chartVerticalOffset;

			//Вычислить стартовый отступ
			if (elParentID > 0) {
				addNumb++;
				levelStartXOffset = parseInt($('#chartItem'+elParentID).css('left'));
			} else {
				addNumb = numb;
				levelStartXOffset = canvasLeftOffset + chartHorizontalOffset;
			}

			levelItemLeftOffset = levelStartXOffset + (chartHorizontalOffset + blockWidth) * addNumb + additionalOffset;
			$(el).css({top:levelTopOffset,left:levelItemLeftOffset});
			coords[levelNumb].push([levelTopOffset,levelItemLeftOffset,parseInt($(el).attr('item-id'))]);
			additionalOffset += elChildsCount > 0 ? (elChildsCount - 1) * (chartHorizontalOffset + blockWidth) : 0;
		});
	}

	/*
	 * Пройтись обратно и сравнить позиции первого ребёнка и родителя
	 * Если позиции не совпадают, то выровнять родителя по первому ребёнку
	 */
	elParentID = 0;

	for(levelNumb = levelsCount; levelNumb > 0;levelNumb--) {
		$('.level'+levelNumb).each(function(numb,el){

			//Если нам ещё не встречался элемент с таким elParentID, то выравнивать элемент
			if (parseInt($(el).attr('parent-id')) != elParentID && parseInt($(el).attr('parent-id')) != 0) {
				elParentID = parseInt($(el).attr('parent-id'));
				/*
				 * Вот здесь происходит выравнивание
				 * Выравнивать посередине. Для этого: определить количество элементов с таким родителем, добавить отступ и разделить пополам
				 */
				zoneOffset = parseInt($(el).css('left'));
				maxZoneWidth = 0;
				$('.elementsWithParent'+elParentID).each(function(numbEl,itemEl){
					itemLeftOffset = parseInt($(itemEl).css('left'));
					if (itemLeftOffset - zoneOffset > maxZoneWidth) maxZoneWidth = itemLeftOffset - zoneOffset;
				});

				newParentLeftPosition = $('.elementsWithParent'+elParentID).length > 1 ? parseInt((maxZoneWidth + blockWidth)/2 - blockWidth/2) : 0;
				
				chn = 0;
				for (citem in coords[levelNumb-1]){
					//Найти все элементы без детей после выравниваемого и сдвинуть их
					if (coords[levelNumb-1][citem][2] == elParentID) chn = 1;
					if (chn > 0) chn += 1
					if ($('#chartItem'+elParentID).attr('parent-id') == $('#chartItem'+coords[levelNumb-1][citem][2]).attr('parent-id') && chn > 2 && coords[levelNumb-1][citem][2] != elParentID) {
						//Если элемент не имеет детей, то сдвигать его
						if ($('#chartItem'+coords[levelNumb-1][citem][2]).attr('childs-count') == '0')
							$('#chartItem'+coords[levelNumb-1][citem][2]).css('left',parseInt($(el).css('left')) + newParentLeftPosition + (chn-2)*(blockWidth + chartHorizontalOffset));
					}
						
				}
				
				$('#chartItem'+elParentID).css('left',newParentLeftPosition + parseInt($(el).css('left')));
				
			}
		});
	}

	var rootXOffset = 0;
	//Вычислить максимальные отступ для всей ветки корневого подразделения и сдвинуть элементы следующего
	rootIDs.forEach(function(el,numb){

		if (numb < rootIDs.length - 1) {

			$('.elementsWithRoot'+el).each(function(numbEl,itemEl){
				itemLeftOffset = parseInt($(itemEl).css('left'));
				if (itemLeftOffset > rootXOffset) rootXOffset = itemLeftOffset
			});

			//Вычислить минимальный отступ по X у следующей структурной ветки
			var minNextXOffset = 100000;
			$('.elementsWithRoot'+rootIDs[numb+1]).each(function(numbEl,itemEl){
				itemLeftOffset = parseInt($(itemEl).css('left'));
				if (itemLeftOffset < minNextXOffset) minNextXOffset = itemLeftOffset
			});

			$('.elementsWithRoot'+rootIDs[numb+1]).each(function(numbEl,itemEl){
				itemLeftOffset = parseInt($(itemEl).css('left')) - minNextXOffset + rootXOffset + blockWidth + chartHorizontalOffset;
				$(itemEl).css('left',itemLeftOffset);
			});

			//console.log(el,numb,rootIDs.length,'next',rootIDs[numb+1],rootXOffset,minNextXOffset)


		}

	});
	
	maxXPosition = 0;
	//Увеличить ширину холста
	$('.structureChartsItem').each(function(numbEl,itemEl){
		itemLeftOffset = parseInt($(itemEl).css('left'));
		if (itemLeftOffset > maxXPosition) maxXPosition = itemLeftOffset
	});
	$('#orgchartCanvasID').attr({'width':maxXPosition+blockWidth});


	canvas = document.getElementById('orgchartCanvasID');
	context = canvas.getContext('2d');

	context.clearRect(0,0,canvasWidth,canvasHeight);
	context.lineJoin = 'round';
	context.lineWidth = 2;
	context.strokeStyle = '#777777';
	context.fillStyle = '#3071A9';

	//Вычислить координаты точек и отрисовать линии
	for(levelNumb = levelsCount; levelNumb > 0;levelNumb--) {
		$('.level'+levelNumb).each(function(numb,el){

			elParentID = parseInt($(el).attr('parent-id'));

			if (elParentID > 0) {
				startX = parseInt(parseInt($(el).css('left')) + blockWidth/2) - canvasLeftOffset;
				startY = parseInt($(el).css('top')) - canvasTopOffset - chartVerticalOffset - 7;
				endX = parseInt(parseInt($('#chartItem'+elParentID).css('left')) + blockWidth/2 - canvasLeftOffset);
				endY = parseInt($('#chartItem'+elParentID).css('top')) + blockHeight - canvasTopOffset - chartVerticalOffset - 7;

				paintLine(startX,startY,endX,endY);

			}

		});
	}

}

function paintLine(startX,startY,endX,endY) {
	
	middleLineY = endY + parseInt(chartVerticalOffset/2);

	context.beginPath();

	now_x = startX;
	now_y = startY;
	context.moveTo(now_x,now_y);
	now_x = startX;
	now_y = middleLineY;
	context.lineTo(now_x,now_y);
	now_x = endX;
	now_y = middleLineY;
	context.lineTo(now_x,now_y);
	now_x = endX;
	now_y = endY;
	context.lineTo(now_x,now_y);

	//context.closePath();
	context.stroke();
} 
