/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function editProfileField(id) {

	$('.profileFieldIDs').val(id);
	
	$('#inputUpdateProfileFieldNameID').val($('#profile_field_name_'+id).html());
	$('#inputUpdateProfileFieldDescriptionID').val($('#profile_field_desc_'+id).html());
	$('#inputUpdateProfileFieldTypeID').val($('#profile_field_type_'+id).html());
	$('#inputUpdateProfileFieldInfoTypeID').val($('#profile_field_info_type_'+id).html());
	$('#inputUpdateProfileFieldInfoStatusID').val($('#profile_field_info_status_'+id).html());
	$('#inputUpdateProfileFieldAdminChangingID').prop('checked',($('#profile_field_admin_changing_'+id).html() === "1" ? true : false));
	$('#inputUpdateProfileFieldRequiredID').prop('checked',($('#profile_field_required_'+id).html() === "1" ? true : false));
	
	$('#editProfileFieldModal').modal('show');

}


function removeProfileField(id) {
	
	$('.profileFieldIDs').val(id);
	
	$('#inputRemoveProfileFieldNameID').html($('#profile_field_name_'+id).html());
	
	
	$('#removeProfileFieldModal').modal('show');
}