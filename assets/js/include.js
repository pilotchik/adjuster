function includeCSS(url) {
	var link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = url;
	document.head.appendChild(link);
}

function includeJS(url, callback) {
	var script = document.createElement('script');
	script.src = url;
  script.async = true;
  if(callback) {
    script.onload = function() {
      var state = script.readyState;
      if(!callback.done && (!state || /loaded|complete/.test(state))) {
        callback.done = true;
        callback();
      }
    }
  }
	document.head.appendChild(script);
}
