/*
 * Плагин взят из проекта "LMS" и может иметь уходящие туда "корни"
 */


//Минимальное количество строк для включения инструментов пагинации и фильтрации
var minimumRows  = 3;
//Количество строк по умолчанию
var defaultRowsPerPage  = 20;
       
//Получение всех таблиц на странице
var allTables    = document.getElementsByTagName('table');
//Массив с таблицами, отмеченными классом "sortedTable" (где требуется работа плагина)    
var sortedTables = new Array();
var useAjax      = new Array();
var autoAjax	 = new Array();
var rowsPerPage  = new Array();
var tableSize    = new Array();
var ajaxUrl      = new Array();
var noFooter     = new Array();
    
var currentOffset = new Array();
var currentSort   = new Array();
var currentOrder  = new Array();
var currentOther  = new Array();
var currentFilter = new Array();
//Номер текущей страницы
var currentPage = 0;
var checkedEntries = new Array();

//Иконка прелоадера при AJAX-обновлении таблицы...
var progressImg = 'js/ajax_sorted_table/images/progress1.gif';
	

function viewTables(cmd) {
	sortedTables = new Array();
	useAjax      = new Array();
	autoAjax	 = new Array();
	rowsPerPage  = new Array();
	tableSize    = new Array();
	ajaxUrl      = new Array();
	noFooter     = new Array();

	currentOffset = new Array();
	currentSort   = new Array();
	currentOrder  = new Array();
	currentOther  = new Array();
	currentFilter = new Array();
	currentPage = 0;
	checkedEntries = new Array();
	
	for (var k = 0; k < allTables.length; k++) {                            //Walk through all document tables
		if (allTables[k].className.match('sortedTable')) {                  //Get all tables that have 'sortedTable' as part of their class definition. These will be paging/sorting enabled
			table = allTables[k];                                           //For this table, create the sorting table headers            
			init(table, true,false,cmd);
			
		}
	}
}

viewTables();

/**
 * Инициализация таблиц с классом "sortedTable"
 */
function init(table, isFirst, idx, cmd) {

	if (isFirst) {
		sortedTables.push(table);
		tableIndex = sortedTables.length-1;
	} else {
		sortedTables[idx] = table;
		tableIndex = idx;
	}

	other = '';
	if (table.getAttribute('other')) other = table.getAttribute('other');
	
	for (i = 0; i < table.rows[0].cells.length; i++) {              //the first table row, table.row[0] should hold the cell headlines.
		//Если ячейка в заголовке таблицы содержит класс "noSort", то такой столбец не сортируется
		if (!table.rows[0].cells[i].className.match('noSort')) {
			//Создание пустой ссылки, которая будет использоваться для сортировки
			var anchor = document.createElement('a');
			anchor.setAttribute('href', 'javascript:void(0)');
			if (table.rows[0].cells[i].getAttribute('name')) {
				anchor.setAttribute('id', tableIndex + '_' + table.rows[0].cells[i].getAttribute('name'));      //The id corresponds to <current table>_<current link>, so that we know which table is sorted, if there are multiple paged tables, and which link was pressed
			}
			anchor.setAttribute('tableIndex', tableIndex);
			anchor.setAttribute('order', 'asc');
			anchor.style.verticalAlign = 'middle';
			anchor.setAttribute('column_name', table.rows[0].cells[i].getAttribute('name'));
			//Вешаем событие пересортировки на клик по ссылке
			anchor.onclick = function () {eF_js_sortTable(this, other);};
			//Копируем содержимое ячейки внутрь ссылки
			anchor.innerHTML = table.rows[0].cells[i].innerHTML;
			//Удаляем содержимое ячейки вне ссылки
			table.rows[0].cells[i].innerHTML = '';
			//Добавляем ссылку в ячейку заголовка таблицы
			table.rows[0].cells[i].appendChild(anchor);
			//Если у таблицы есть атрибут sortBy и этот атрибут равен номеру столбца, то сортировать
			if (table.getAttribute('sortBy') && table.getAttribute('sortBy') == i) {
				var sortBy = anchor;
				if (table.getAttribute('order') && table.getAttribute('order') == 'desc') anchor.setAttribute('order', 'desc');
			}
		}
	}

	if (!$('loading_'+table.id)) {
		loadingDiv = new Element('div', {id:'loading_'+table.id}).addClassName('loading');
		loadingDiv.setOpacity(0.9);	    
		loadingDiv.insert(new Element('div').insert(new Element('img', {src:'js/ajax_sorted_table/images/progress1.gif'}).setStyle({verticalAlign:'middle'}))
											.insert(new Element('span').setStyle({verticalAlign:'middle'}).update('&nbsp;'+sorted_translations["loadingdata"]))
											.setStyle({top:'50%',left:'45%',position:'absolute'}));
	}


	if (isFirst) {
		
		checkedEntries[tableIndex] = new Array();
		
		//Определить количество строк на страницу
		if (getCookie('cookieTableRows')) {
			rowsPerPage[tableIndex] = getCookie('cookieTableRows');
		} else if (table.getAttribute('rowsPerPage')) {
			rowsPerPage[tableIndex] = parseInt(table.getAttribute('rowsPerPage'));
		} else {
			rowsPerPage[tableIndex] = defaultRowsPerPage;
		}

		//Если имеется атрибут "useAjax", то инициализировать настройки асинхронного взаимодействия
		if (table.getAttribute('useAjax')) {
			document.body.appendChild(loadingDiv);
			loadingDiv.clonePosition(table);
			//alert(loadingDiv.getDimensions().width +'x'+loadingDiv.getDimensions().height);
			if (loadingDiv.getDimensions().height == 0 && loadingDiv.getDimensions().width == 0) {
				loadingDiv.setStyle({display:'none'});
			}

			useAjax[tableIndex]   = true;
			ajaxUrl[tableIndex]   = table.getAttribute('url');
			autoAjax[tableIndex]  = true;
			if (table.getAttribute('no_auto')) {
				loadingDiv.hide();
				loadingDiv.writeAttribute({loaded:true});
				autoAjax[tableIndex] = false;
			}
		}
		
		if (table.getAttribute('noFooter') == 'true') {
			noFooter[tableIndex]   = true;
			rowsPerPage[tableIndex] = 100000;
		}
		
		tableSize[tableIndex] = Math.ceil(table.getAttribute('size') / rowsPerPage[tableIndex]);

		eF_js_pageTable(tableIndex,cmd);                         //Convert this table to paged table.

		if (sortBy && autoAjax[tableIndex]) {
			eF_js_sortTable(sortBy);
		} else if (useAjax[tableIndex] && autoAjax[tableIndex]) {
			eF_js_sortTable(null, other);                                        //Ajax must be initialized some way, and sortBy is a convenient one.
		}
		sortBy = false;


	} else {
		tableSize[tableIndex] = Math.ceil(table.getAttribute('size') / rowsPerPage[tableIndex]);
		eF_js_pageTable(tableIndex);
	}
}


function eF_js_redrawPage(tableIndex, draw, other) {
	if ($(tableIndex)) {
		for (var i = 0; i < sortedTables.size(); i++) {
			if (sortedTables[i].id.match(tableIndex)) {
				tableIndex = i;
			}
		}			    			
	}
	if (other) {
		currentOther[tableIndex] = other;
	}
	if (draw) {
		eF_js_rebuildTable(tableIndex, currentOffset[tableIndex], currentSort[tableIndex], currentOrder[tableIndex], currentOther[tableIndex]); 
	} else {
		eF_js_rebuildTable(tableIndex, currentOffset[tableIndex], currentSort[tableIndex], currentOrder[tableIndex], currentOther[tableIndex], true, true);
	}
}
    
	
/**
 * Перестроение таблицы при использовании AJAX-обновления
 */
function eF_js_rebuildTable(tableIndex, offset, column_name, order, other, noDiv, noRefresh) {

	try {
		if ($(tableIndex)) {
			for (var i = 0; i < sortedTables.size(); i++) {
				if (sortedTables[i].id.match(tableIndex)) {
					tableIndex = i;
				}
			}			    			
		}
		if (window.onBeforeSortedTable) {
			window.onBeforeSortedTable(sortedTables[tableIndex]);
		}

		if (!column_name) column_name = '';
		if (!order) order = '';
		
		currentOffset[tableIndex] = offset;
		currentSort[tableIndex]   = column_name;
		currentOrder[tableIndex]  = order;
		if (Object.isUndefined(other)) {
			other = '';
		} 
		currentOther[tableIndex]  = other;

		var el  = document.getElementById(tableIndex+ '_' + column_name);
		url = ajaxUrl[tableIndex]+'ajax='+sortedTables[tableIndex].id+'&limit='+rowsPerPage[tableIndex]+'&offset='+offset+'&sort='+column_name+'&order='+order+'&other='+other;

		if (currentFilter[tableIndex]) {
			url = url + '&filter='+encodeURI(currentFilter[tableIndex]+'||||||||');
		}

		var loadingDiv = $('loading_'+sortedTables[tableIndex].id);
		loadingDiv.clonePosition(sortedTables[tableIndex]);
		//sortedTables[tableIndex].ancestors().each(function (s) {alert(s.getDimensions().height + ' ' + s.tagName + ' ' + s.id);});
		//alert(sortedTables[tableIndex].up().getDimensions().up().up().up().height);
		if (!noDiv && (loadingDiv.getDimensions().height > 0 || loadingDiv.getDimensions().width > 0)) {
			loadingDiv.show();
		}

		new Ajax.Request(url, {
			method:'get',
			asynchronous:true,
			onFailure: function (transport) {
			var tableId     = sortedTables[tableIndex].id;
			var spanElement = document.createElement('span');
			spanElement.innerHTML += transport.responseText;
			sortedTables[tableIndex].parentNode.replaceChild(spanElement, sortedTables[tableIndex]);
			loadingDiv.hide();
			loadingDiv.writeAttribute({loaded:true});
			//alert(transport.responseText);
		},
		onSuccess: function (transport) {

			if (noRefresh) { return true;}

			var tableId     = sortedTables[tableIndex].id;
			var spanElement = document.createElement('span');
			var re2         = new RegExp("<!--ajax:"+tableId+"-->((.*[\n])*)<!--\/ajax:"+tableId+"-->");	//Does not work with smarty {strip} tags!
			var tableText   = re2.exec(transport.responseText);

			if (!tableText) {
				var re      = new RegExp("<!--ajax:"+tableId+"-->((.*[\r\n\u2028\u2029])*)<!--\/ajax:"+tableId+"-->");	//Does not work with smarty {strip} tags!
				tableText   = re.exec(transport.responseText);
			}

			spanElement.innerHTML += tableText[1];

			//spanElement.innerHTML += transport.responseText;

			sortedTables[tableIndex].parentNode.replaceChild(spanElement, sortedTables[tableIndex]);

			loadingDiv.hide();
			//loadingDiv.setStyle({width:'0px', height:'0px'});
			loadingDiv.writeAttribute({loaded:true});

			init(document.getElementById(tableId), false, tableIndex);

			document.getElementById(tableIndex+'_sortedTable_currentPage').selectedIndex = Math.ceil(currentOffset[tableIndex]/rowsPerPage[tableIndex]);

			sortedTables[tableIndex].style.visibility = 'visible';
			//loadingDiv.clonePosition(sortedTables[tableIndex]);

			if (el) {
				if (currentOrder[tableIndex] == 'desc') {              //Set the icons through the class to reflect the order, ascending or descending			
					$(el.id).className = 'sortDescending';
					$(el.id).setAttribute('order', 'asc');

									if (bootstrapTheme == 0) {
										if ($(el.id).up().select('img').length == 0) {
											$(el.id).up().insert(new Element('img', {src:'themes/default/images/others/transparent.gif'}).addClassName('sprite16').addClassName('sprite16-navigate_down').setStyle({verticalAlign:'middle'}));
												//$(el.id).up().insert(new Element('img', {src:'themes/default/images/others/transparent.gif'}).addClassName('sprite16').addClassName('sprite16-navigate_down').setStyle({verticalAlign:'middle'}));
										} else {
											$(el.id).up().select('img')[0].src = 'themes/default/images/others/transparent.gif';
											$(el.id).up().select('img')[0].addClassName('sprite16').addClassName('sprite16-navigate_down');
										}
									} else {
										if ($(el.id).up().select('span').length == 0) {
											$(el.id).up().insert(new Element('span',{'style':'margin-left:5px;'}).addClassName('glyphicon').addClassName('glyphicon-chevron-down').setStyle({verticalAlign:'middle'}));
										} else {
											$(el.id).up().select('span')[0].class = 'glyphicon glyphicon-chevron-down';
											//$(el.id).up().select('img')[0].addClassName('sprite16').addClassName('sprite16-navigate_down');
										}
									}
				} else {	
									$(el.id).className = 'sortAscending';
					$(el.id).setAttribute('order', 'desc');    					
					if (currentSort[tableIndex] !== 'null') {  // when sortby not set, don't display arrow    						
						if (bootstrapTheme == 0) {
											if ($(el.id).up().select('img').length == 0) {
							$(el.id).up().insert(new Element('img', {src:'themes/default/images/others/transparent.gif'}).addClassName('sprite16').addClassName('sprite16-navigate_up').addClassName('glyphicon').addClassName('glyphicon-chevron-up').setStyle({verticalAlign:'middle'}));
						} else {		
							$(el.id).up().select('img')[0].src = 'themes/default/images/others/transparent.gif';
							$(el.id).up().select('img')[0].addClassName('sprite16').addClassName('sprite16-navigate_up').addClassName('glyphicon').addClassName('glyphicon-chevron-up');
						}
										} else {
											if ($(el.id).up().select('span').length == 0) {
												$(el.id).up().insert(new Element('span',{'style':'margin-left:5px;'}).addClassName('glyphicon').addClassName('glyphicon-chevron-up').setStyle({verticalAlign:'middle'}));
											} else {
												$(el.id).up().select('span')[0].class = 'glyphicon glyphicon-chevron-up';
												//$(el.id).up().select('img')[0].addClassName('sprite16').addClassName('sprite16-navigate_down');
											}
										}
					}
				}
			}

			generateTips();
			repositionFileManager(sortedTables[tableIndex]);

			if (window.onSortedTableComplete) {
				window.onSortedTableComplete();
			}

			if (sortedTables[tableIndex].hasClassName('subSection')) {
				onLoadSubSection(sortedTables[tableIndex]);
			}
		}
		});
	} catch (e) {
		handleException(e);
	}
}

	

function eF_js_sortTable(el, other) {
	
	//Element.extend(el);
	
	var column_name = "null";
	if (el) {
	        tableIndex = el.getAttribute('tableIndex');                                                   //Get the id of the element
	       	column_name = el.getAttribute('column_name');
	       	order = el.getAttribute('order');
	} else {
		column_name = "null";
		order = "desc";
	}
	
	if (useAjax[tableIndex]) {

		eF_js_rebuildTable(tableIndex, 0, column_name, order, other, true);

	} else {

		parentTable = sortedTables[tableIndex];                               //Get the table object, depending on the array offset that el.getAttribute('tableIndex') represents

		var counter = 0;                                                    //counter is used in the search for the clicked column
		for (i = 0; i < parentTable.rows[0].cells.length; i++) {       //Traverse through all table header cells
			//if (parentTable.rows[0].childNodes[i].tagName == 'TD' || parentTable.rows[0].childNodes[i].tagName == 'TH') {   //filter out any non cell elements that are children of the top table row
			if (el && parentTable.rows[0].cells[i] == el.parentNode) {   //If this node is the same as el (the clicked one), hold its index at 'pressed'
				var pressed = counter;
			} else {
				if (parentTable.rows[0].cells[i].getElementsByTagName('A').length > 0){
					parentTable.rows[0].cells[i].getElementsByTagName('A')[0].className = '';     //For every other cell, eliminate its class name, thus making the ascending or descending icon disspear
				}
			}
			counter++;
		}

		var tableRowIndex = new Array();               //tableRowIndex holds the table rows and their index in the table
		var tableRows     = new Array();               //tableRows holds a copy of the table rows

		for (i = 0; i < parentTable.rows.length - 2; i++) {
			if (typeof(parentTable.rows[i+1].cells[pressed]) != 'undefined') {            	
				tableRowIndex[i] = new Array(getText(parentTable.rows[i+1].cells[pressed]).toLowerCase(), i);
			} else {
				tableRowIndex[i] = new Array('', i);
			}
			
			tableRows[i]     = parentTable.rows[i+1].cloneNode(true);

			selects_source = parentTable.rows[i+1].getElementsByTagName('select');                             //Microsoft IE has a bug, and does not copy selected and checked attributes when cloning! See http://channel9.msdn.com/wiki/default.aspx/Channel9.InternetExplorerProgrammingBugs
			selects_target = tableRows[i].getElementsByTagName('select');
			for (var k = 0; k < selects_source.length; k++) {
				selects_target[k].options.selectedIndex = selects_source[k].options.selectedIndex;
			}

			checkboxes_source = parentTable.rows[i+1].getElementsByTagName('input');                             //Microsoft IE has a bug, and does not copy selected and checked attributes when cloning! See http://channel9.msdn.com/wiki/default.aspx/Channel9.InternetExplorerProgrammingBugs
			checkboxes_target = tableRows[i].getElementsByTagName('input');
			for (var k = 0; k < checkboxes_source.length; k++) {
				if (checkboxes_target[k].type == 'checkbox') {
					checkboxes_target[k].checked = checkboxes_source[k].checked;
				}
			}
		}

		if (tableRowIndex[0][0].match(/^(-?\d\d*)/)) {										  //If it's a number, use other sorting function 
			tableRowIndex.sort(sortNumber);
		} else {
			tableRowIndex.sort();
			
		}


		$(el).parent().parent().find('span.sort-glyph').each(function(s,elem) {
			$(elem).remove();
		});

		if (parseInt(document.getElementById(tableIndex+'_sortedTable_sortBy').value) == pressed) {
			//parseInt is needed here, since if inputs[counter].value is empty and pressed is 0, the clause evaluates to true! We need to make sure that if inputs[counter].value is empty is not converted implicitly to 0
			if (el.className == 'sortAscending') {              //Set the icons through the class to reflect the order, ascending or descending
				el.className = 'sortDescending';

				if ($(el).find('span.sort-glyph').length == 0) {
					//el.up().insert(new Element('span',{'style':'margin-left:5px;'}).addClassName('glyphicon').addClassName('glyphicon-chevron-up').setStyle({verticalAlign:'middle'}));
					code = '<span class="glyphicon glyphicon-chevron-up sort-glyph"></span>';
					$(el).append(code);
				} else {
					$(el).find('span.sort-glyph').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
				}
			} else {

				tableRowIndex.reverse();                            //If the column clicked is already sorted, we need to reverse the elements order
				el.className = 'sortAscending';

				if ($(el).find('span.sort-glyph').length == 0) {
					code = '<span class="glyphicon glyphicon-chevron-down sort-glyph"></span>';
					$(el).append(code);
				} else {
					$(el).find('span.sort-glyph').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
				}
			}
		} else {

			document.getElementById(tableIndex+'_sortedTable_sortBy').value = pressed;        //Set the hidden element value to the current sorted field
			el.className = 'sortDescending';                    //Update the sorting icon

			if ($(el).find('span.sort-glyph').length == 0) {
				code = '<span class="glyphicon glyphicon-chevron-up sort-glyph"></span>';
				$(el).append(code);
			} else {
				//$(el).find('span.sort-glyph')[0].class = 'glyphicon glyphicon-chevron-up sort-glyph';
				$(el).find('span.sort-glyph').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
			}

		}		    

		counter = parentTable.rows.length - 2;
		while (tableRowIndex.length > 0) {                                     //Replace the table rows with the copies corresponding to the sorted order
			val = tableRowIndex.pop();
			parentTable.rows[counter].parentNode.replaceChild(tableRows[val[1]], parentTable.rows[counter]);
			counter--;
		}

		eF_js_refreshPage(el.getAttribute('tableIndex'));                                           //Refresh the current page so that it holds the correct data
	}

	$('td.sortedTableFooter').attr('colspan','100%');
}

 
function eF_js_refreshPage(tableIndex) {

	var parentTable = sortedTables[tableIndex];                           //Get the current table

	var page  = document.getElementById(tableIndex+'_sortedTable_currentPage').options[document.getElementById(tableIndex+'_sortedTable_currentPage').selectedIndex].value;      //Get the current page from the select box
	var count = 0;
	for (var j = 1; j < parentTable.rows.length - 1; j++) {             //Update the contents of this page, by hiding each row that should not be part of it
		if (j < page * rowsPerPage[tableIndex] + 1 || j > parseInt(rowsPerPage[tableIndex]) + page * rowsPerPage[tableIndex]) {
			parentTable.rows[j].style.display = 'none';
		} else {
			parentTable.rows[j].style.display = '';
			count++ % 2 ? newClass = 'evenRowColor' : newClass = 'oddRowColor';
			parentTable.rows[j].className = parentTable.rows[j].className.replace(/evenRowColor|oddRowColor/i, newClass);
		}
	}
	$('td.sortedTableFooter').attr('colspan','100%');
}


/**
 * Изменение текущей страницы
 * 
 */
function eF_js_changePage(tableIndex, page, resetCookie) {

	resetCookie = typeof(resetCookie) === 'undefined' ? true : resetCookie;


	//Получение текущей таблицы
	var parentTable = sortedTables[tableIndex];

	if (page == 'next') {
		//page = parseInt($('#'+tableIndex+'_sortedTable_currentPage').val()) + 1;
		page = currentPage + 1;
		if (page > document.getElementById(tableIndex+'_sortedTable_currentPage').options.length - 1) {
			page = document.getElementById(tableIndex+'_sortedTable_currentPage').options.length - 1;
		}
	} else if (page == 'previous') {
		//page = parseInt($('#'+tableIndex+'_sortedTable_currentPage').val()) - 1;
		page = currentPage - 1;
		if (page < 0 ) page = 0;
	}

	if (useAjax[tableIndex] && !isNaN(currentOffset[tableIndex])) {
		eF_js_rebuildTable(tableIndex, page * rowsPerPage[tableIndex], currentSort[tableIndex], currentOrder[tableIndex], currentOther[tableIndex]);
	} else {

		for (var j = 1; j < parentTable.rows.length - 1; j++) {             //Update the contents of the current page, by hiding each row that should not be part of it
			if (j < page*rowsPerPage[tableIndex] + 1 || j > parseInt(rowsPerPage[tableIndex]) + page * rowsPerPage[tableIndex]) {
				parentTable.rows[j].style.display = 'none';
			} else {
				parentTable.rows[j].style.display = '';
			}
		}
		var endResult   = parseInt(rowsPerPage[tableIndex]) + page * rowsPerPage[tableIndex];
		if (endResult > parentTable.rows.length - 2) {
			endResult = parentTable.rows.length - 2;
		}
	}
	
	$('#'+tableIndex+'_sortedTable_currentPage').val(page);
	$('td.sortedTableFooter').attr('colspan','100%');

	currentPage = parseInt(page);
	
	
	//Обновить куки с номером текущей страницы
	if (resetCookie) {
		tableString = getCookie('cookieTableLocationPage');
		tableObject = tableString != '' && tableString !== null ? JSON.parse(tableString) : {};

		if (typeof(tableObject[window.location]) === 'undefined') tableObject[window.location] = {};
		if (typeof(tableObject[window.location][tableIndex]) === 'undefined') tableObject[window.location][tableIndex] = 0;

		tableObject[window.location][tableIndex] = currentPage
		setCookie('cookieTableLocationPage', JSON.stringify(tableObject));
	}
	
}
    

function eF_js_pageTable(tableIndex,cmd) {
	
	//Получить параметры текущей таблицы
	var table = sortedTables[tableIndex];
	
	//Определение общего количества страницы
	if (useAjax[tableIndex] && tableSize[tableIndex]) {
		pages = tableSize[tableIndex];
	} else {
		pages = Math.ceil((table.rows.length - 1) / rowsPerPage[tableIndex]);
	}
	
	//Создание ячейки с панелью управления 
	var td = document.createElement('td');
	td.colSpan = '100%';
	td.className = 'sortedTableFooter';
	td.colSpan = '100%';

	var div   = document.createElement('div');
	
	//Текстовое поле для фильтра
	var input = document.createElement('input');
	input.setAttribute('placeholder', sorted_translations["filter"]+'...');
	input.setAttribute('style','width:250px;height:initial;');
	input.setAttribute('type', 'text');
	input.setAttribute('id', tableIndex+'_sortedTable_filter');
	input.setAttribute('size', '10');
	//input.setAttribute('onkeypress', 'if (event.which == 13) eF_js_filterData('+tableIndex+')');
	//input.setAttribute('onkeypress', 'if (event.which == 13 || event.keyCode == 13) {eF_js_filterData('+tableIndex+'); return false;}');
	input.setAttribute('oninput', 'eF_js_filterData('+tableIndex+'); return false;');

	if (currentFilter[tableIndex]) {
		input.setAttribute("value", currentFilter[tableIndex]);
		div.innerHTML += '<span style = "display:none" id = "'+table.id+'_currentFilter">' + currentFilter[tableIndex]+'</span>';
	}
	input.className = 'inputSearchText form-control';

	//Текст перед полем ввода данных фильтра
	//div.innerHTML += '<span style = "vertical-align:middle">&nbsp;'+sorted_translations["filter"]+':&nbsp;</span>';
	//Если у таблицы есть класс withFilter, то отображать фильтр
	if ($(table).hasClass('withFilter')) div.appendChild(input);
	//input.setAttribute('onclick', 'if (this.value.match("'+sorted_translations["filter"]+'...'+'")) this.value = "";'); 

	div.className = 'sortTablefilter';
	td.appendChild(div);

	if (useAjax[tableIndex]) {
		var endResult = 0;
		if (currentOffset[tableIndex] === 0) {
			endResult = rowsPerPage[tableIndex]; 
		} else if (currentOffset[tableIndex]) {
			endResult = parseInt(currentOffset[tableIndex]) + parseInt(rowsPerPage[tableIndex]); 
		}
		
		if (endResult > parseInt(table.getAttribute('size'))) endResult = table.getAttribute('size');
	} 

	//Добавление элемента выбора количества строк на странице
	var select = document.createElement('select');
	rowsPerPageArray = new Array('5','10', '15', '20', '50', '100', '200', '500');
	for (var i = 0; i < rowsPerPageArray.length; i++) {
		var option = document.createElement('option');
		option.setAttribute('value', rowsPerPageArray[i]);
		option.innerHTML = rowsPerPageArray[i];
		select.appendChild(option);
		if (rowsPerPage[tableIndex] == rowsPerPageArray[i]) {
			option.setAttribute('selected', 'selected');
		}
	}
	select.setAttribute('onchange', 'numRows = parseInt(this.options[this.selectedIndex].value);eF_js_changeRowsPerPage('+tableIndex+', numRows)');        //If we ommit parseInt, then rowsPerPage becomes string. So, if for example rowsPerPage is 10 and we add 5, it becoomes 105 instead of 15
	select.setAttribute('id', tableIndex+'_sortedTable_rowsPerPage');     //Set its id so we can retrieve its data easily
	select.setAttribute('class','form-control');
	select.style.verticalAlign = 'middle';
	select.style.margin = '0 10px 0 0';
	select.style.width = '70px';
	select.style.display = 'inline';
	td.innerHTML += '<span style = "vertical-align:middle;">'+sorted_translations["rowsperpage"]+':&nbsp;</span>';
	td.appendChild(select);                                             //Append it to the footer cell


	var input = document.createElement('input');                        //Create a hidden element, that holds the current page.
	input.setAttribute('type', 'hidden');
	input.setAttribute('id', tableIndex+'_sortedTable_sortBy');
	td.appendChild(input);                                              //Append it to the footer cell

	//console.log(table.rows.length,tableIndex,$(table).find('td.sortedTableFooter').length);
	if (cmd == 'reset' && !$(table).find('td.sortedTableFooter').length) table.toolsCell = false;
	
	if (table.toolsCell) {
		//If we are repaginating table, then we will replace the previous footer cell with this one
		table.toolsCell.parentNode.replaceChild(td, table.toolsCell);
	} else {
		var tr = document.createElement('tr');
		tr.appendChild(td);
		tr.setAttribute('class', 'defaultRowHeight');
		table.getElementsByTagName('tbody')[0].appendChild(tr);	
	}

	//Create a select element, that lists the pages
	var select = document.createElement('select');
	select.setAttribute('class','form-control');
	select.style.verticalAlign = 'middle';
	select.style.width = '100px';
	select.style.display = 'inline';
	select.setAttribute('id', tableIndex+'_sortedTable_currentPage');
	select.setAttribute('onchange', 'eF_js_changePage('+tableIndex+', $(this).val())');      //Set an onchange event, so that changing the value fires a change on the page


	for (var i = 0; i < pages; i++) { 

		if ( (1 + i*rowsPerPage[tableIndex]) <= Math.min((i + 1)*rowsPerPage[tableIndex], table.getAttribute('size') ? table.getAttribute('size') : table.rows.length-2) )
		{
		//Add an option for each page
		var option = document.createElement('option');
		option.setAttribute('value', i);
		option.innerHTML = (1 + i*rowsPerPage[tableIndex])+'-'+Math.min((i + 1)*rowsPerPage[tableIndex], table.getAttribute('size') ? table.getAttribute('size') : table.rows.length-2);
		select.appendChild(option);
		}
	}

	select.style.verticalAlign = 'middle';

	td.innerHTML += '<span style = "vertical-align:middle">&nbsp;'+sorted_translations["displayingresults"]+':&nbsp;</span>';
	//ссылка на первую страницу
	td.innerHTML += '<a href = \"javascript:void(0)\" onclick = \"eF_js_changePage('+tableIndex+',0)\"><span class="glyphicon glyphicon-fast-backward" title="'+sorted_translations["first_page"]+'" style = "vertical-align:middle"></span></a>&nbsp;';
	td.innerHTML += '<a href = \"javascript:void(0)\" onclick = \"eF_js_changePage('+tableIndex+',\'previous\')\"><span class="glyphicon glyphicon-chevron-left" title="'+sorted_translations['previous_page']+'" style = "vertical-align:middle"></span></a>&nbsp;';

	td.appendChild(select);
	td.innerHTML += '<span style = "vertical-align:middle">&nbsp;'+sorted_translations["outof"]+'&nbsp;' + (table.getAttribute('size') ? table.getAttribute('size') : table.rows.length-2) + '</span>';

	td.innerHTML += '&nbsp;<a href = \"javascript:void(0)\" onclick = \"eF_js_changePage('+tableIndex+',\'next\')\"><span class="glyphicon glyphicon-chevron-right" title="'+sorted_translations['next_page']+'" style = "vertical-align:middle"></span></a>';
	td.innerHTML += '&nbsp;<a href = \"javascript:void(0)\" onclick = \"eF_js_changePage('+tableIndex+','+(pages - 1)+')\"><span class="glyphicon glyphicon-fast-forward" title="'+sorted_translations['last_page']+'" style = "vertical-align:middle"></span></a>';

	if (!(typeof noFooter[tableIndex] == "undefined") || ((table.rows.length < minimumRows + 2 || parseInt(table.getAttribute('size')) < minimumRows) && !currentFilter[tableIndex] && !currentOffset[tableIndex])) {
		$(table).find('td.sortedTableFooter').parent().css('display','none');
		if (!(typeof noFooter[tableIndex] == "undefined")) tr.setAttribute('id', 'noFooterRow'+tableIndex);
	}

	table.toolsCell = td;

	if (!useAjax[tableIndex]) {
		eF_js_changePage(tableIndex, 0, false);
		table.style.visibility = 'visible';
	}
}


function eF_js_filterData(tableIndex,code) {

	if (useAjax[tableIndex]) {
		var showing_image = false;
		
		if (!showing_image) {
			Element.extend($(tableIndex+'_sortedTable_filter')).addClassName('loadingImg').setStyle({background:'url("'+progressImg+'") center right no-repeat'});
		}

		var str = document.getElementById(tableIndex+'_sortedTable_filter').value;        //Get the filter value, from the corresponding text box
		currentFilter[tableIndex] = str;
		currentOffset[tableIndex] = 0;

		eF_js_rebuildTable(tableIndex, currentOffset[tableIndex], currentSort[tableIndex], currentOrder[tableIndex], currentOther[tableIndex], true);
	} else {

		var table = sortedTables[tableIndex];                                 //Get the current table
		
		var str = document.getElementById(tableIndex+'_sortedTable_filter').value;        //Get the filter value, from the corresponding text box
		
		
		if (table.filteredRows) {
			for (var i = 0; i < table.filteredRows.length; i++) {
				table.rows[table.rows.length-1].parentNode.insertBefore(table.filteredRows[i], table.rows[table.rows.length-1]);
			}
		}

		//Массив со отфильтрованными строками
		table.filteredRows = new Array();

		var i = 0;
		while (i < table.rows.length - 2) {
			keepRow = false;
			j = 0;
			
			str = cleanTextForRegExp(str);
			
			re = new RegExp(str, "i");

			j = 0;
			//Проверка наличия текста в ячейке
			while (table.rows[i+1].cells[j] != null && !$(table.rows[i+1].cells[j]).hasClass('noFilter') && !$(table.rows[i+1].cells[j]).hasClass('actionsTD') && !(keepRow = (strip(stripTags(table.rows[i+1].cells[j++].innerHTML.toString()))).match(re))) {}

			if (!keepRow) {
				//Если ячейка содержит текст, то удалить из основной таблицы и доставить в массив filteredRows
				table.filteredRows.push(table.rows[i+1].parentNode.removeChild(table.rows[i+1]));
			} else {
				i++;
			}
		}

		
		newPages = (Math.ceil((table.rows.length - 2) / rowsPerPage[tableIndex]));

		if (newPages == 0)  {
			new_r = table.insertRow(1);
			new_r.setAttribute('class', 'notFoundedDataTD');
			new_r.setAttribute('id', 'for_empty');
			new_r.setAttribute('name', 'for_empty');
			sel = new_r.insertCell(0);
			sel.innerHTML += 'Данные не найдены';
			sel.colSpan = '100%';
			sel.setAttribute('class', 'emptyCategory notFoundedDataTD');
			$('.notFoundedDataTD').attr('colspan','100%');
		} else {
			var str_empty = document.getElementsByTagName('tr');

			if (str_empty.length > 0) {

				for (var o = 0; o < str_empty.length; o++) {
					
					if (str_empty[o].getAttribute('name') == 'for_empty') {
						str_empty[o].parentNode.removeChild(str_empty[o]);
					}
				}
			}
		}
		
		//Если есть хотя бы одна найденная строка, то удалять строку с текстом "Данные не найдены"
		if (i) $(table).find('tr#for_empty').remove();


		var select = document.getElementById(tableIndex+'_sortedTable_currentPage');
		for (var i = select.options.length - 1; i >= 0; i--) {
			select.removeChild(select.options[i]);
		}
		for (var i = 0; i < newPages; i++) {
			var option = document.createElement('option');
			option.setAttribute('value', i);
			option.innerHTML = i + 1;
			select.appendChild(option);
		}

		currentFilter[tableIndex] = str;
		eF_js_pageTable(tableIndex);
		$('#'+tableIndex+'_sortedTable_filter').focus().val('').val(str);
		
	}
	$('td.sortedTableFooter').attr('colspan','100%');
}


function eF_js_changeRowsPerPage(tableIndex, numRows) {
    rowsPerPage[tableIndex] = numRows;
    if (useAjax[tableIndex]) {
        eF_js_rebuildTable(tableIndex, 0, currentSort[tableIndex], currentOrder[tableIndex], currentOther[tableIndex]);
    } else {

        eF_js_pageTable(tableIndex);
    }
    setCookie('cookieTableRows', numRows);
	
	$('td.sortedTableFooter').attr('colspan','100%');
}


//Cross-browser function for getting element text
function getText(control) {
    if (document.all) {
        return control.innerText;
    } else {
        return control.textContent;
    }
}


/**
 * Sorting for numbers
 * 
 * @param {type} a
 * @param {type} b
 * @returns {Number}
 */
function sortNumber(a, b) {
	var val1 = parseInt(a[0]);
	var val2 = parseInt(b[0]);
	if (isNaN(val1)) {
	    val1 = 0;
	}
	if (isNaN(val2)) {
	    val2 = 0;
	}
	return val1 - val2;
}


//In order to display the loading... div when in tab. onTabDisplay() is called automatically by the tabber library 
if (typeof(tabberObj) != 'undefined') {
	tabberObj.prototype.onTabDisplay = function(obj) {
		Element.extend(obj.tabber.tabs[obj.index].div);
		obj.tabber.tabs[obj.index].div.select('table').each(
			function (s) {
				if (s.hasClassName('sortedTable') && s.getDimensions().height > 0) {
					$$('div.loading').each(function (s) {s.hide();});								//Hide all
					if ($('loading_'+s.id) && $('loading_'+s.id).readAttribute('loaded') !== 'loaded') {
						$('loading_'+s.id).show().clonePosition(s);									//Show this one
					}
				} 
			});
	};
}


function toggleSubSection(el, id, sectionId, trailUrl) {
	try {
		Element.extend(el);
		var sectionTable   = $(sectionId);
		var tr = el.up().up();				//The table row holding the clicked element
	
		if (el.hasClassName('sprite16-plus') || el.hasClassName('sprite16-plus2')) {
			
			tr.up().select('img.sprite16-minus').each(function (s) {setImageSrc(s, 16, 'plus');});
			tr.up().select('img.sprite16-minus2').each(function (s) {setImageSrc(s, 16, 'plus2');});
			el.hasClassName('sprite16-plus') ? setImageSrc(el, 16, 'minus') : setImageSrc(el, 16, 'minus2'); 
			newTr = new Element('tr', {id:'subsection_row_'+sectionId+id}).insert(new Element('td', {colspan:'100%'}).insert(sectionTable.show().remove()));
			tr.insert({after:newTr});
	
			sectionTable.writeAttribute({no_auto:0});
			for (var i = 0; i < sortedTables.size(); i++) {
				if (sortedTables[i].id.match(sectionId) && ajaxUrl[i]) {
					ajaxUrl[i] = ajaxUrl[i] + sectionId+'_source=' + id + '&' + trailUrl + '&';
					eF_js_rebuildTable(i, 0);
				}
			}		
		} else {		
			onCloseSubSection(sectionTable);
			el.hasClassName('sprite16-minus') ? setImageSrc(el, 16, 'plus') : setImageSrc(el, 16, 'plus2');
			document.body.insert({after:sectionTable.hide().remove()});
			$('subsection_row_'+sectionId+id).remove();
		}	
	} catch (e) {alert(e);}
}


function onCloseSubSection(table) {
	containers = findContainers(table);
	activeRows = findActiveRows(table);

	containers[0].select('tr').each(function (s) {
		enableRow(s);
	});
	activeRows.each(function (s) {
		enableRow(s.previous());
		enableRow(s);		
	});
	
}


function onLoadSubSection(table) {
	try {
		containers = findContainers(table);
		activeRows = findActiveRows(table);
		
		containers.each( function (c) {
			c.select('tr').each(function (s) {
				enableRow(s);
			});
			c.select('tr').each(function (s) {
				if (s.visible()) {
					disableRow(s);
				};
			});
		});
		activeRows.each(function (a) {
			enableRow(a);
		});
		if (activeRows[0]) {
			enableRow(activeRows[0].previous());
		}
		table.select('tr').each(function (s) {
			enableRow(s);
		});
	} catch (e) {
		handleException(e);
	}
}


function findActiveRows(el) {
	var activeRows = new Array();
	el.ancestors().each(function (s) {
		if (s.id.match('subsection_row')) {
			activeRows.push(s);
		}});	
	return activeRows;	
}


function findContainers(el) {
	var containers = new Array();
	el.ancestors().each(function (s) {
		if (s.hasClassName('sortedTable')) {
			containers.push(s);
		}});	
	//containers.reverse();
	return containers;
}


function isDisabledRow(tr) {
	if ($('loading_'+tr.identify())) {
		return true;
	} else {
		return false;
	}
}


function disableRow(tr) {
    var loadingDiv = new Element('div', {id:'loading_'+tr.identify()}).addClassName('loading');//.setStyle({border:'2px solid green'});
    loadingDiv.setOpacity(0.4);	 
    document.body.appendChild(loadingDiv);
    //Unfortunately, IE doesn't like clonePosition(tr), so we have to go with this solution    
    loadingDiv.setStyle({width:tr.getDimensions().width+'px', 
    					 height:tr.getDimensions().height+'px', 
    					 left:tr.down().cumulativeOffset().left+'px',
    					 top:tr.down().cumulativeOffset().top+'px'});    					 
}


function enableRow(tr) {
	if ($('loading_'+tr.identify())) $('loading_'+tr.identify()).remove();
}


function augmentUrl(table_id) {
	augmentedUrl = '';
	tables = sortedTables.size();
	for (var i = 0; i < tables; i++) {
		if (sortedTables[i].id.match(table_id) && ajaxUrl[i]) {
			tableIndex = i;
			augmentedUrl = ajaxUrl[tableIndex]+'ajax='+sortedTables[tableIndex].id+'&limit='+rowsPerPage[tableIndex]+'&offset='+currentOffset[tableIndex]+'&sort='+currentSort[tableIndex]+'&order='+currentOrder[tableIndex]+'&other='+currentOther[tableIndex];
		}
	}		
	return augmentedUrl;
}


function findSortedTableIndex(name) {
    tables = sortedTables.size();
    for (var i = 0; i < tables; i++) {
        if (sortedTables[i].id.match(name)) {
        	return i;
        }
    }			
    
}


function toggleActive(el, tableIndex) {
	Element.extend(el);
	if (getCookie('toggle_active') == 1) {
		el.writeAttribute({src:'js/ajax_sorted_table/images/trafficlight_red.png'});
		setCookie('toggle_active', -1);
	} else if (getCookie('toggle_active') == -1) {
		el.writeAttribute({src:'js/ajax_sorted_table/images/trafficlight_on.png'});
		setCookie('toggle_active', 0);
	} else {
		el.writeAttribute({src:'js/ajax_sorted_table/images/trafficlight_green.png'});
		setCookie('toggle_active', 1);
	}
	eF_js_rebuildTable(tableIndex, 0, 'null', 'desc');
}


function repositionFileManager(table) {
	if (table.id == 'filesTable') {
		var heightValue;
		if (table.getDimensions().height != 0) {
			heightValue = parseInt(table.getDimensions().height+50);
		} else {
			heightValue = 0;
		}
		$('filemanager_cell').setStyle({width:table.getDimensions().width+'px', height:heightValue+'px', verticalAlign:'top'});
	}	
}

if (typeof(readCookieForSortedTablePreset) != 'undefined' && readCookie(readCookieForSortedTablePreset)) {
	var sort_id = readCookie(readCookieForSortedTablePreset).split('--')[0]; 
	var sort_order = readCookie(readCookieForSortedTablePreset).split('--')[1];	//sort order

	if ($(sort_id)) {
		if (sort_order == 'asc') {
			eF_js_sortTable($(sort_id));
			eF_js_sortTable($(sort_id));
		} else {
			eF_js_sortTable($(sort_id));
		}
		if (window.resetFormRows) {
			resetFormRows($(sort_id).up());
		}
	}
}




function eF_js_changePageFromCookie() {
	
	tableString = getCookie('cookieTableLocationPage');
	
	try {
	
		tableObject = tableString != '' && tableString !== null  ? JSON.parse(tableString) : {};

		if (typeof(tableObject[window.location]) !== 'undefined') {

			$.each(tableObject[window.location],function(nel,el){
				eF_js_changePage(nel, el)
			});

		}
	
    } catch (e) {console.log(e);}
	
}
